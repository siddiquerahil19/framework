let accordions = document.querySelectorAll(".accordion-textbox");

const expandAll = () => {
    let btns = event.srcElement.parentElement;
    btns.children[0].classList.add("text-dark");
    btns.children[0].disabled = true;
    btns.children[1].disabled = false;
    btns.children[1].classList.remove("text-dark");

    accordions.forEach((checkbox) => {
        checkbox.checked = true;
    });
};

const expandChemicalInformation = () => {
    let chemicalInformation = document.getElementById("chemInfo-toggle")
    chemicalInformation.checked = true;
};

const collapseAll = () => {
    let btns = event.srcElement.parentElement;
    btns.children[1].classList.add("text-dark");
    btns.children[1].disabled = true;
    btns.children[0].disabled = false;
    btns.children[0].classList.remove("text-dark");

    accordions.forEach((checkbox) => {
        checkbox.checked = false;
    });
};
