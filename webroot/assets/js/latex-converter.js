$(function () {
    var imageFiles = {}
    var images = []
    var requestedCitations = []
    var formattedCitations = undefined

    function convertLatex () {
        var editor = CKEDITOR.instances['content-html']
        var latex = $('#content-latex').val()

        if (latex === '') {
            return
        }

        if (editor.checkDirty()) {
            if (!window.confirm(latexConverter.messages.confirmOverwrite)) {
                return
            }
        }

        $('#converter-progress').addClass('animate')

        $.ajax({
            method: 'POST',
            url: latexConverter.urls.convertLatex,
            contentType: 'application/json',
            dataType: 'json',
            data: JSON.stringify({
                latex: latex,
                citations: formattedCitations
            }),
        }).done(function (results) {
            editor.setData(results.html)
            editor.resetDirty()

            requestedCitations = results.citations
            images = results.images

            updateBibManager()
            updateImageManager()
            appendMessage('light', 'LaTeX processed, HTML updated')

            var undefinedCommands = results.log.undefinedCommands.join(', ')
            if (undefinedCommands) {
                appendMessage('warning', 'Found unknown commands in LaTeX: ' + undefinedCommands)
            }
        }).fail(function () {
            appendMessage('danger', 'Processing LaTeX failed')
        }).always(function () {
            $('#converter-progress').removeClass('animate')
        })
    }

    function convertBibtex () {
        var file = $('#content-references').prop('files')[0]

        if (!file) {
            return
        }

        $('#converter-progress').addClass('animate')
        var data = new FormData()
        data.append('file', file)
        for (var i = 0; i < requestedCitations.length; i++) {
            data.append('citations[]', requestedCitations[i])
        }

        $.ajax({
            method: 'POST',
            url: latexConverter.urls.convertBibtex,
            dataType: 'json',
            data: data,
            processData: false,
            contentType: false
        }).done(function (results) {
            formattedCitations = results
            updateBibManager()
            appendMessage('light', 'BibTeX processed')
        }).fail(function () {
            appendMessage('danger', 'Processing BibTeX failed')
        }).always(function () {
            $('#converter-progress').removeClass('animate')
        })
    }

    function updateBibManager () {
        var table = $('#converter-references-table tbody')
        table.empty()

        var missing = requestedCitations
        var bibliography = []

        if (formattedCitations) {
            missing = formattedCitations.missing
            bibliography = formattedCitations.bibliography
        }

        for (var i = 0; i < missing.length; i++) {
            table.append('<tr><td>' + missing[i] + '</td><td style="color: red;">Missing</span></td></tr>')
        }

        for (var i = 0; i < bibliography.length; i++) {
            var entry = bibliography[i]
            table.append('<tr><td>' + entry[0] + '</td><td>' + entry[1] + '</td></tr>')
        }
    }

    function updateImageManager () {
        var table = $('#converter-images-table tbody')
        table.empty()

        for (var i = 0; i < images.length; i++) {
            var image = images[i]
            var id = 'images-' + (i + 1)

            table.append(
                '<tr><td><label for="' + id + '">' + image.url + '</label></td><td></td><td>' + image.caption + '</td></tr>'
            )

            var file = imageFiles[image.url]
            if (!file) {
                file = $(
                    '<input id="' + id + '" name="images[]" type="file" class="form-control form-control-file" />'
                )
                imageFiles[image.url] = file
            }
            table.find('tr').last().find('td').eq(1).append(file)
        }
    }

    function appendMessage (level, message) {
        $('#converter-logs').append(
            '<div class="alert alert-' + level + '" role="alert">' +
            '[' + (new Date()).toLocaleString() + '] ' + message + '</div>'
        )
    }

    $('#converter-action-open, #converter-action-close').on('click', function () {
        $('#metadata').toggle()
        $('#converter').toggle()
    })

    $('#converter-action-refresh').on('click', convertLatex)
    $('#content-references').on('change', convertBibtex)

    $('#latex').on('change', function () {
        var fr = new FileReader()
        fr.onload = function () {
            $('#content-latex').val(fr.result)
            $('#latex').val('')
            convertLatex()
        }
        fr.readAsText(this.files[0])
    })
})
