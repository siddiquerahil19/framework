import sys
sys.path.append('../atf2conll-convertor')

from atf2conll_convertor.convertor import ATFCONLConvertor

# adapted from convertor.py to support STDIN

def main(file):
    converter = ATFCONLConvertor(file, False)

    # From convert() of ATFCONLConvertor
    for (i, line) in enumerate(sys.stdin):
        converter._ATFCONLConvertor__parse(i, line.strip())

    # From write2file() of ATFCONLConvertor
    IDlist = list(map(lambda x: x[0], converter.tokens))
    if len(IDlist) != len(set(IDlist)):
        click.echo('Error: File {0}, Text {1} : IDs generated are not unique'.format(converter.inputFileName, converter.outputFilename))
    print("#new_text=" + converter.outputFilename)
    print("# ID\tFORM\tSEGM\tXPOSTAG\tHEAD\tDEPREL\tMISC")
    for tok in converter.tokens:
        print(tok[0] + '\t' + tok[1])

if __name__ == "__main__":
    main(sys.argv[1])
