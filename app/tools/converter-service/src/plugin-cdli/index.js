const { plugins } = require('@citation-js/core')
const articles = require('./article')
const artifacts = require('./artifact')
const publications = require('./publication')

plugins.add('@cdli', {
    input: {
        '@cdli/article': {
            parse (article) {
                return articles.convertToTarget(article)
            },
            parseType: {
                dataType: 'SimpleObject',
                propertyConstraint: {
                    props: 'article_type',
                    value (type) {
                        return ['cdlj', 'cdlb', 'cdlp', 'cdln'].includes(type)
                    }
                }
            }
        },
        '@cdli/artifact': {
            parse (artifact) {
                return artifacts.convertToTarget(artifact)
            },
            parseType: {
                dataType: 'SimpleObject',
                propertyConstraint: {
                    props: ['designation', 'has_fragments']
                }
            }
        },
        '@cdli/publication': {
            parse (publication) {
                return publications.convertToTarget(publication)
            },
            parseType: {
                dataType: 'SimpleObject',
                propertyConstraint: {
                    props: ['bibtexkey']
                }
            }
        },
        '@cdli/parsed-bibtex': {
            parse ({ _type: type, 'citation-key': label, ...properties }) {
                return { type, label, properties }
            },
            parseType: {
                dataType: 'SimpleObject',
                propertyConstraint: {
                    props: ['_type', 'citation-key']
                }
            }
        }
    },
    output: {
        bibliographyandcitations (data, bibliographyEntries = [], citationEntries = [], options = {}) {
            const entries = new Set(data.map(({ id }) => id))
            const included = new Set()
            const missing = new Set()
            for (const ids of [bibliographyEntries, ...citationEntries]) {
                for (let i = ids.length - 1; i >= 0; i--) {
                    const id = ids[i].id
                    if (!entries.has(id)) {
                        ids.splice(i, 1)
                        missing.add(id)
                    } else {
                        included.add(id)
                    }
                }
            }

            const filteredData = data.filter(({ id }) => included.has(id))
            const bibliography = plugins.output.format('bibliography', filteredData, {
                ...options,
                entry: Array.from(included)
            })
            const citation = citationEntries.map(entries =>
                plugins.output.format('citation', filteredData, {
                    ...options,
                    entry: entries
                })
            )

            return JSON.stringify({
                bibliography,
                citation,
                missing: Array.from(missing)
            })
        }
    }
})
