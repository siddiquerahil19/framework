const { util } = require('@citation-js/core')
const { parse: parseDate } = require('@citation-js/date')
const { parse: parseName } = require('@citation-js/name')

//  type    container-title                      abbr.   ISSN
const journals = {
    cdlj: ['Cuneiform Digital Library Journal', 'CDLJ', '1540-8779'],
    cdlb: ['Cuneiform Digital Library Bulletin', 'CDLB', '1540-8760'],
    cdlp: ['Cuneiform Digital Library Preprints', 'CDLP', undefined],
    cdln: ['Cuneiform Digital Library Notes', 'CDLN', '1546-6566']
}

module.exports = new util.Translator([
    { source: null, target: 'type', convert: { toTarget () { return 'article-journal' } } },
    { source: 'title', target: 'title' },
    {
        source: 'authors',
        target: 'author',
        convert: {
            toTarget (authors) {
                return authors
                    .sort((a, b) => a._joinData.sequence - b._joinData.sequence)
                    .map(author => {
                        if (author.first) {
                            return {
                                given: author.first,
                                family: author.last
                            }
                        } else {
                            return parseName(author.author, true)
                        }
                    })
            }
        }
    },
    {
        source: 'article_type',
        target: ['container-title', 'container-title-short', 'ISSN'],
        convert: {
            toTarget (type) {
                return journals[type]
            }
        }
    },
    {
        source: null,
        target: ['publisher', 'publisher-place'],
        convert: {
            toTarget () {
                return ['Cuneiform Digital Library Initiative', 'Oxford; Berlin; Los Angeles']
            }
        }
    },
    {
        source: 'created',
        target: 'issued',
        convert: {
            toTarget (date) { return date && parseDate(date.split('T')[0]) }
        }
    },
    {
        source: null,
        target: 'accessed',
        convert: {
            toTarget () { return parseDate(Date.now()) }
        }
    },
    {
        source: 'year',
        target: 'volume'
    },
    {
        source: 'article_no',
        target: 'issue'
    },
    {
        source: ['year', 'article_no', 'article_type'],
        target: 'URL',
        convert: {
            toTarget (year, number, type) {
                const id = type === 'cdlp' ? number : `${year}-${number}`
                return `https://cdli.mpiwg-berlin.mpg.de/articles/${type}/${id}`
            }
        }
    }
])
