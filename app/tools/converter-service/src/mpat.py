import sys
import os
sys.path.append('../morphology-pre-annotation-tool')

from cdli_mpa_tool.checker import CONLChecker

# adapted from converter.py to support STDIN
def main():
    checker = CONLChecker(0, True)
    checker.check()

if __name__ == "__main__":
    main()
