const { spawn } = require('child_process')
const path = require('path')
const express = require('express')
const app = express()

const PATH = path.resolve(__dirname, 'mpat.py')

app.use(express.json({ limit: '500kb' }))
app.post('/check', ({ body, query: { base } }, res) => {
    const mpat = spawn('python3', [PATH])

    let stdout = '';
    let stderr = '';
    mpat.stdout.on('data', data => { stdout += data.toString() });
    mpat.stderr.on('data', data => { stderr += data.toString() });

    mpat.on('exit', (code) => {
        if (code > 0) {
            res.status(500).send('Checking failed')
        } else {
            res.send(stdout)
        }
    })

    mpat.stdin.write(body.annotation)
    mpat.stdin.end()
})

module.exports = {
    app,
    message: `Running Morphology Pre-Annotation Tool...`
}
