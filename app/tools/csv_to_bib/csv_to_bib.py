import argparse
import csv
import os
import sys

DEFAULT_BIB_TYPE = 'article'

# https://en.wikibooks.org/wiki/LaTeX/Bibliography_Management#BibTeX
bib_fields_map = {
    # Standard bib fields
    'key': 'key',
    'authors': 'author',
    'year': 'year',
    'address': 'address',
    'annote': 'annote',
    'book_title': 'booktitle',
    'chapter': 'chapter',
    'crossref': 'crossref',
    'edition': 'edition',
    'editors': 'editor',
    'how_published': 'howpublished',
    'institution': 'institution',
    'journal': 'journal',
    'month': 'month',
    'note': 'note',
    'number': 'number',
    'organization': 'organization',
    'pages': 'pages',
    'publisher': 'publisher',
    'school': 'school',
    'title': 'title',
    'volume': 'volume',
    'series': 'series',

    # Non-standard bib fields
    # 'entry_type': 'entry_type',
    # 'publication_history': 'publication_history',
    # 'designation': 'designation'
}

required_fields = {
    'article': ['author', 'journal', 'title', 'year'],
    'book': ['publisher', 'title', 'year'],
    'booklet': ['title'],
    'inbook': ['publisher', 'title', 'year'],
    'incollection': ['author', 'booktitle', 'publisher', 'title', 'year'],
    'inproceedings': ['author', 'booktitle', 'title', 'year'],
    'conference': ['author', 'booktitle', 'title', 'year'],
    'manual': ['title'],
    'mastersthesis': ['author', 'school', 'title', 'year'],
    'phdthesis': ['author', 'school', 'title', 'year'],
    'misc': [],
    'proceedings': ['title', 'year'],
    'tech report': ['author', 'institution', 'title', 'year'],
    'unpublished': ['author', 'note', 'title']
}


def generate_input(key_sub: str, year: str, row: dict, fields) -> str:
    BIB_TYPE = row['entry_type'] or DEFAULT_BIB_TYPE
    assert(BIB_TYPE in required_fields)

    write_str = '@{btype}{{{bibtexkey},\n'.format(btype=BIB_TYPE, bibtexkey=key_sub)
    note_str = ''
    for field in fields:
        if field == 'year' and bib_fields_map[field] in required_fields[BIB_TYPE]:
            write_str += '\t{fieldname} = {{{fieldvalue}}},\n' \
                .format(fieldname=bib_fields_map[field], fieldvalue=year or 'MISSING FIELD')
        elif field == 'authors' and bib_fields_map[field] in required_fields[BIB_TYPE]:
            write_str += '\t{fieldname} = {{{fieldvalue}}},\n' \
                .format(fieldname=bib_fields_map[field],
                        fieldvalue=row[field].replace('; ', ' and ') or 'MISSING FIELD')
        elif field == 'note':
            continue
        elif field not in bib_fields_map:
            if (field == 'designation' and row[field]):
                note_str += '{fieldname}:{fieldvalue}'.format(fieldname=field, fieldvalue=row[field])
        elif row[field] or bib_fields_map[field] in required_fields[BIB_TYPE]:
            write_str += '\t{fieldname} = {{{fieldvalue}}},\n' \
                .format(fieldname=bib_fields_map[field], fieldvalue=row[field] or 'MISSING FIELD')

    write_str += '\t{fieldname} = {{{fieldvalue}}},\n' \
            .format(fieldname='note', fieldvalue=note_str)
    write_str += '}'
    return write_str


def multiple_bib_output(reader: csv.DictReader, fields, out_dir: str):
    for row in reader:
        assert (row['bibtexkey'])

        extracted_key = row['bibtexkey']
        key_spl = row['bibtexkey'].split(', ')
        for i in range(len(key_spl)):
            if len(key_spl) > 1 and i == len(key_spl) - 1:
                extracted_key = key_spl[i].split("; ")[1]
            key_spl[i] = key_spl[i][:4]
        for i in range(len(key_spl)):
            key_sub = extracted_key if len(key_spl) == 1 else key_spl[i] + extracted_key

            with open(out_dir + '/' + key_sub + '.bib', 'w', encoding='utf-8') as bibFile:
                if len(key_spl) > 1:
                    bibFile.write(generate_input(key_sub, key_spl[i], row, fields))
                else:
                    bibFile.write(generate_input(key_sub, row['year'], row, fields))


def single_bib_output(filename: str, reader: csv.DictReader, fields, out_dir: str):
    with open(out_dir + '/' + filename.replace('.csv', '.bib'), 'w', encoding='utf-8') as bibFile:
        for row in reader:
            assert (row['bibtexkey'])

            extracted_key = row['bibtexkey']
            key_spl = row['bibtexkey'].split(', ')
            for i in range(len(key_spl)):
                if len(key_spl) > 1 and i == len(key_spl) - 1:
                    extracted_key = key_spl[i].split("; ")[1]
                key_spl[i] = key_spl[i][:4]
            for i in range(len(key_spl)):
                key_sub = extracted_key if len(key_spl) == 1 else key_spl[i] + extracted_key

                if len(key_spl) > 1:
                    bibFile.write(generate_input(key_sub, key_spl[i], row, fields) + '\n')
                else:
                    bibFile.write(generate_input(key_sub, row['year'], row, fields) + '\n')


def csv_to_bib(file_path: str, multiple_bib_files: bool, out_dir: str) -> None:
    if not os.path.exists(out_dir):
        os.mkdir(out_dir)
    if not os.path.exists(file_path):
        print("[Failed] csv file not found.")
        return

    with open(file_path, 'r', newline='', encoding='utf-8') as csvfile:
        reader = csv.DictReader(csvfile)
        fields = reader.fieldnames

        if multiple_bib_files:
            multiple_bib_output(reader, fields, out_dir)

        else:
            single_bib_output(os.path.basename(file_path), reader, fields, out_dir)

    print('[Success] bib files are generated under ' + out_dir + ' dir.')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Csv to bib converter.')
    parser.add_argument('-n', '--file_name', type=str, help='the input csv filename')
    parser.add_argument('-o', '--out_dir', type=str,
                        default='./tmp', help='output dir (default: ./tmp)')
    parser.add_argument('-m', '--mul_out', type=int,
                        default=0, help='parse each csv entry into seperate bib file (default: 0)')

    args = parser.parse_args()

    if sys.version_info[0] < 3:
        print('[Failed] Python 3 or a more recent version is required.')
        exit(0)

    csv_to_bib(args.file_name, args.mul_out, args.out_dir)
