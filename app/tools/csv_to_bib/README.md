## CSV to BIB converter

Reads a DB generated csv file and converts it to a valid bibtex file.

Runing on python 3 is required.

### Arguments

```
'-n', '--file_name', help='the input csv filename'  
'-o', '--out_dir', default='./bib', help='the output directory' 
'-m', '--mul_out', default=0, help='parse each csv entry into seperate bib file'
```

### Usage Example
```
python3 ./csv_to_bib.py n test.csv [-o ./out/bib] [-m 1]
```
In the example above:

 - `./test.csv` is the source csv file. This value **must** be provided.
 
 - `./out/bib` is the place to generate all bib files.
 
 - `1` is to let the program generate seperate for each csv row.

### Data robustness
The script handles the following data input problems:
  - Field name errors (e.g. authors => author)
  - 
  - Required field missing (e.g. empty author => "MISSING FIELD")
  - 
  - Duplicate publications (e.g. bibtexkey "1963, 1969; -155326" => \["1963-155326", "1969-155326"\]) (Please let me know if this is incorrect)


## License

This project is licensed under the MIT License - see the [LICENSE](./LICENSE.txt) file for details.

## Acknowledgments

* I appreciate [Émilie Pagé-Perron](https://gitlab.com/epageperron) for making my contribution to this project easy.
