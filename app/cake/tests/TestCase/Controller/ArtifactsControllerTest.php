<?php

namespace App\Test\TestCase\Controller;

use App\Controller\ArtifactsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\ArtifactsController Test Case
 */
class ArtifactsControllerTest extends IntegrationTestCase
{
    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.artifacts',
        'app.credits',
        'app.proveniences',
        'app.periods',
        'app.artifact_types',
        'app.archives',
        'app.artifact_dates',
        'app.artifacts_composites',
        'app.artifacts_date_referenced',
        'app.artifacts_seals',
        'app.artifacts_shadow',
        'app.inscriptions',
        'app.retired_artifacts',
        'app.collections',
        'app.dates',
        'app.external_resources',
        'app.genres',
        'app.languages',
        'app.materials',
        'app.publications',
        'app.artifacts_credits',
        'app.artifacts_collections',
        'app.artifacts_dates',
        'app.artifacts_external_resources',
        'app.artifacts_genres',
        'app.artifacts_languages',
        'app.artifacts_materials',
        'app.artifacts_publications'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
