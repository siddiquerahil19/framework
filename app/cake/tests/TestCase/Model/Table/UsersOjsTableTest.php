<?php

declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UsersOjsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UsersOjsTable Test Case
 */
class UsersOjsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\UsersOjsTable
     */
    protected $UsersOjs;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.UsersOjs',
        'app.Auths',
        'app.EmailLog',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('UsersOjs') ? [] : ['className' => UsersOjsTable::class];
        $this->UsersOjs = $this->getTableLocator()->get('UsersOjs', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->UsersOjs);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\UsersOjsTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\UsersOjsTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     * @uses \App\Model\Table\UsersOjsTable::defaultConnectionName()
     */
    public function testDefaultConnectionName(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
