<?php

declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PublicationsOjsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PublicationsOjsTable Test Case
 */
class PublicationsOjsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\PublicationsOjsTable
     */
    protected $PublicationsOjs;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.PublicationsOjs',
        'app.PrimaryContacts',
        'app.Sections',
        'app.Submissions',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('PublicationsOjs') ? [] : ['className' => PublicationsOjsTable::class];
        $this->PublicationsOjs = $this->getTableLocator()->get('PublicationsOjs', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->PublicationsOjs);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\PublicationsOjsTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\PublicationsOjsTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     * @uses \App\Model\Table\PublicationsOjsTable::defaultConnectionName()
     */
    public function testDefaultConnectionName(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
