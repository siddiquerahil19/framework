<?php

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ArtifactsDatesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ArtifactsDatesTable Test Case
 */
class ArtifactsDatesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ArtifactsDatesTable
     */
    public $ArtifactsDates;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.artifacts_dates',
        'app.artifacts',
        'app.dates'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ArtifactsDates') ? [] : ['className' => ArtifactsDatesTable::class];
        $this->ArtifactsDates = TableRegistry::getTableLocator()->get('ArtifactsDates', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ArtifactsDates);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
