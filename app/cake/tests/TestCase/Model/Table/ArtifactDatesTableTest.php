<?php

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ArtifactDatesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ArtifactDatesTable Test Case
 */
class ArtifactDatesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ArtifactDatesTable
     */
    public $ArtifactDates;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.artifact_dates',
        'app.artifacts',
        'app.dates'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ArtifactDates') ? [] : ['className' => ArtifactDatesTable::class];
        $this->ArtifactDates = TableRegistry::getTableLocator()->get('ArtifactDates', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ArtifactDates);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
