<?php

declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SubmissionCommentsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SubmissionCommentsTable Test Case
 */
class SubmissionCommentsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\SubmissionCommentsTable
     */
    protected $SubmissionComments;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.SubmissionComments',
        'app.Roles',
        'app.Submissions',
        'app.Assocs',
        'app.Authors',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('SubmissionComments') ? [] : ['className' => SubmissionCommentsTable::class];
        $this->SubmissionComments = $this->getTableLocator()->get('SubmissionComments', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->SubmissionComments);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\SubmissionCommentsTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\SubmissionCommentsTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     * @uses \App\Model\Table\SubmissionCommentsTable::defaultConnectionName()
     */
    public function testDefaultConnectionName(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
