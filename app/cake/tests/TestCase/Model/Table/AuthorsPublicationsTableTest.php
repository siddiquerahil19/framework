<?php

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AuthorsPublicationsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AuthorsPublicationsTable Test Case
 */
class AuthorsPublicationsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\AuthorsPublicationsTable
     */
    public $AuthorsPublications;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.authors_publications',
        'app.publications',
        'app.authors'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('AuthorsPublications') ? [] : ['className' => AuthorsPublicationsTable::class];
        $this->AuthorsPublications = TableRegistry::getTableLocator()->get('AuthorsPublications', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->AuthorsPublications);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
