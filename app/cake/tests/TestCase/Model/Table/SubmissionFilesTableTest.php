<?php

declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SubmissionFilesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SubmissionFilesTable Test Case
 */
class SubmissionFilesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\SubmissionFilesTable
     */
    protected $SubmissionFiles;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.SubmissionFiles',
        'app.Files',
        'app.SourceFiles',
        'app.Submissions',
        'app.Genres',
        'app.UploaderUsers',
        'app.Assocs',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('SubmissionFiles') ? [] : ['className' => SubmissionFilesTable::class];
        $this->SubmissionFiles = $this->getTableLocator()->get('SubmissionFiles', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->SubmissionFiles);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\SubmissionFilesTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\SubmissionFilesTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     * @uses \App\Model\Table\SubmissionFilesTable::defaultConnectionName()
     */
    public function testDefaultConnectionName(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
