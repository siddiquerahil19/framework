<?php

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MaterialAspectsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MaterialAspectsTable Test Case
 */
class MaterialAspectsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\MaterialAspectsTable
     */
    public $MaterialAspects;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.material_aspects',
        'app.artifacts_materials'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('MaterialAspects') ? [] : ['className' => MaterialAspectsTable::class];
        $this->MaterialAspects = TableRegistry::getTableLocator()->get('MaterialAspects', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->MaterialAspects);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
