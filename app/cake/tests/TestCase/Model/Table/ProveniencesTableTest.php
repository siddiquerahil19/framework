<?php

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProveniencesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ProveniencesTable Test Case
 */
class ProveniencesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ProveniencesTable
     */
    public $Proveniences;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.proveniences',
        'app.regions',
        'app.archives',
        'app.artifacts',
        'app.dynasties'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Proveniences') ? [] : ['className' => ProveniencesTable::class];
        $this->Proveniences = TableRegistry::getTableLocator()->get('Proveniences', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Proveniences);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
