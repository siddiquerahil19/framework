<?php

declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PublicationSettingsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PublicationSettingsTable Test Case
 */
class PublicationSettingsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\PublicationSettingsTable
     */
    protected $PublicationSettings;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.PublicationSettings',
        'app.Publications',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('PublicationSettings') ? [] : ['className' => PublicationSettingsTable::class];
        $this->PublicationSettings = $this->getTableLocator()->get('PublicationSettings', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->PublicationSettings);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\PublicationSettingsTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\PublicationSettingsTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     * @uses \App\Model\Table\PublicationSettingsTable::defaultConnectionName()
     */
    public function testDefaultConnectionName(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
