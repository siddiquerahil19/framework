<?php

declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AnnotationTypesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AnnotationTypesTable Test Case
 */
class AnnotationTypesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\AnnotationTypesTable
     */
    protected $AnnotationTypes;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.AnnotationTypes',
        'app.ImageAnnotations',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('AnnotationTypes') ? [] : ['className' => AnnotationTypesTable::class];
        $this->AnnotationTypes = $this->getTableLocator()->get('AnnotationTypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->AnnotationTypes);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\AnnotationTypesTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
