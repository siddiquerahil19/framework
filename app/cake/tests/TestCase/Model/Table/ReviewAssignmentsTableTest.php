<?php

declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ReviewAssignmentsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ReviewAssignmentsTable Test Case
 */
class ReviewAssignmentsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ReviewAssignmentsTable
     */
    protected $ReviewAssignments;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.ReviewAssignments',
        'app.Submissions',
        'app.Reviewers',
        'app.ReviewerFiles',
        'app.ReviewRounds',
        'app.Stages',
        'app.ReviewForms',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('ReviewAssignments') ? [] : ['className' => ReviewAssignmentsTable::class];
        $this->ReviewAssignments = $this->getTableLocator()->get('ReviewAssignments', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->ReviewAssignments);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\ReviewAssignmentsTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\ReviewAssignmentsTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     * @uses \App\Model\Table\ReviewAssignmentsTable::defaultConnectionName()
     */
    public function testDefaultConnectionName(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
