<?php

declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ArtifactAssetsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ArtifactAssetsTable Test Case
 */
class ArtifactAssetsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ArtifactAssetsTable
     */
    protected $ArtifactAssets;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.ArtifactAssets',
        'app.Artifacts',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('ArtifactAssets') ? [] : ['className' => ArtifactAssetsTable::class];
        $this->ArtifactAssets = $this->getTableLocator()->get('ArtifactAssets', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->ArtifactAssets);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\ArtifactAssetsTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\ArtifactAssetsTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
