<?php

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ArtifactsDateReferencedFixture
 *
 */
class ArtifactsDateReferencedFixture extends TestFixture
{
    /**
     * Table name
     *
     * @var string
     */
    public $table = 'artifacts_date_referenced';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'artifact_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'ruler_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'month_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'month_no' => ['type' => 'string', 'length' => 50, 'null' => true, 'default' => null, 'collate' => 'utf8mb4_unicode_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'year_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'day_no' => ['type' => 'string', 'length' => 50, 'null' => true, 'default' => null, 'collate' => 'utf8mb4_unicode_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        '_indexes' => [
            'fk_artifacts_idx_adr' => ['type' => 'index', 'columns' => ['artifact_id'], 'length' => []],
            'fk_rulers_idx_adr' => ['type' => 'index', 'columns' => ['ruler_id'], 'length' => []],
            'fk_months_idx_adr' => ['type' => 'index', 'columns' => ['month_id'], 'length' => []],
            'fk_years_idx_adr' => ['type' => 'index', 'columns' => ['year_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'id_UNIQUE' => ['type' => 'unique', 'columns' => ['id'], 'length' => []],
            'fk_artifacts_adr' => ['type' => 'foreign', 'columns' => ['artifact_id'], 'references' => ['artifacts', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'fk_months_adr' => ['type' => 'foreign', 'columns' => ['month_id'], 'references' => ['months', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'fk_rulers_adr' => ['type' => 'foreign', 'columns' => ['ruler_id'], 'references' => ['rulers', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'fk_years_adr' => ['type' => 'foreign', 'columns' => ['year_id'], 'references' => ['years', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8mb4_unicode_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'id' => 1,
                'artifact_id' => 1,
                'ruler_id' => 1,
                'month_id' => 1,
                'month_no' => 'Lorem ipsum dolor sit amet',
                'year_id' => 1,
                'day_no' => 'Lorem ipsum dolor sit amet'
            ],
        ];
        parent::init();
    }
}
