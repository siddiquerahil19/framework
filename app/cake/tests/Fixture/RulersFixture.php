<?php

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * RulersFixture
 *
 */
class RulersFixture extends TestFixture
{
    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'sequence' => ['type' => 'integer', 'length' => 2, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'ruler' => ['type' => 'string', 'length' => 100, 'null' => true, 'default' => null, 'collate' => 'utf8mb4_unicode_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'period_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'dynasty_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => true, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'fk_period_idx' => ['type' => 'index', 'columns' => ['period_id'], 'length' => []],
            'fk_dynasty_idx' => ['type' => 'index', 'columns' => ['dynasty_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'id_UNIQUE' => ['type' => 'unique', 'columns' => ['id'], 'length' => []],
            'ruler_unique_idx' => ['type' => 'unique', 'columns' => ['ruler'], 'length' => []],
            'fk_period' => ['type' => 'foreign', 'columns' => ['period_id'], 'references' => ['periods', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_rulers_dynasties_dynasty_id' => ['type' => 'foreign', 'columns' => ['dynasty_id'], 'references' => ['dynasties', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8mb4_unicode_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'id' => 1,
                'sequence' => 1,
                'ruler' => 'Lorem ipsum dolor sit amet',
                'period_id' => 1,
                'dynasty_id' => 1
            ],
        ];
        parent::init();
    }
}
