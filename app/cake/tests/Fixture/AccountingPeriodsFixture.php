<?php

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * AccountingPeriodsFixture
 *
 */
class AccountingPeriodsFixture extends TestFixture
{
    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'start_ruler_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'start_month_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'start_year_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'start_date' => ['type' => 'string', 'length' => 10, 'null' => true, 'default' => null, 'collate' => 'utf8mb4_unicode_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'end_ruler_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'end_month_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'end_year_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'end_date' => ['type' => 'string', 'length' => 10, 'null' => true, 'default' => null, 'collate' => 'utf8mb4_unicode_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        '_indexes' => [
            'fk_rulers_1_idx' => ['type' => 'index', 'columns' => ['start_ruler_id'], 'length' => []],
            'fk_rulers_2_idx' => ['type' => 'index', 'columns' => ['end_ruler_id'], 'length' => []],
            'fk_month_1_idx' => ['type' => 'index', 'columns' => ['start_month_id'], 'length' => []],
            'fk_month_2_idx' => ['type' => 'index', 'columns' => ['end_month_id'], 'length' => []],
            'fk_year_1_idx' => ['type' => 'index', 'columns' => ['start_year_id'], 'length' => []],
            'fk_year_2_idx' => ['type' => 'index', 'columns' => ['end_year_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'id_UNIQUE' => ['type' => 'unique', 'columns' => ['id'], 'length' => []],
            'fk_month_1' => ['type' => 'foreign', 'columns' => ['start_month_id'], 'references' => ['months', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'fk_month_2' => ['type' => 'foreign', 'columns' => ['end_month_id'], 'references' => ['months', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'fk_rulers_1' => ['type' => 'foreign', 'columns' => ['start_ruler_id'], 'references' => ['rulers', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'fk_rulers_2' => ['type' => 'foreign', 'columns' => ['end_ruler_id'], 'references' => ['rulers', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'fk_year_1' => ['type' => 'foreign', 'columns' => ['start_year_id'], 'references' => ['years', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'fk_year_2' => ['type' => 'foreign', 'columns' => ['end_year_id'], 'references' => ['years', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8mb4_unicode_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'id' => 1,
                'start_ruler_id' => 1,
                'start_month_id' => 1,
                'start_year_id' => 1,
                'start_date' => 'Lorem ip',
                'end_ruler_id' => 1,
                'end_month_id' => 1,
                'end_year_id' => 1,
                'end_date' => 'Lorem ip'
            ],
        ];
        parent::init();
    }
}
