<?php

declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * OjsNote Entity
 *
 * @property int $note_id
 * @property int|null $assoc_type
 * @property int|null $assoc_id
 * @property int $user_id
 * @property \Cake\I18n\FrozenTime $date_created
 * @property \Cake\I18n\FrozenTime|null $date_modified
 * @property string|null $title
 * @property string|null $contents
 *
 * @property \App\Model\Entity\Assoc $assoc
 * @property \App\Model\Entity\OjsUserSetting[] $user_settings
 */
class OjsNote extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * OjsNote that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'assoc_type' => true,
        'assoc_id' => true,
        'user_id' => true,
        'date_created' => true,
        'date_modified' => true,
        'title' => true,
        'contents' => true,
        'assoc' => true,
        'user_settings' => true,
    ];
}
