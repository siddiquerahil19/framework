<?php

namespace App\Model\Entity;

use App\Utility\CdliProcessor\Check;
use App\Utility\CdliProcessor\TokenList;
use Cake\ORM\Entity;

/**
 * Inscription Entity
 *
 * @property int $id
 * @property int|null $artifact_id
 * @property string|null $atf
 * @property string|null $jtf
 * @property string|null $transliteration
 * @property string|null $transliteration_clean
 * @property string|null $transliteration_sign_names
 * @property string|null $transliteration_for_search
 * @property string|null $annotation
 * @property bool|null $is_atf2conll_diff_resolved
 * @property string|null $comments
 * @property string|null $structure
 * @property string|null $translation
 * @property string|null $transcription
 * @property int|null $update_event_id
 * @property string|null $inscription_comments
 * @property bool $is_latest
 *
 * @property \App\Model\Entity\Artifact $artifact
 * @property \App\Model\Entity\UpdateEvent $update_event
 */
class Inscription extends Entity
{
    use LinkedDataTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'artifact_id' => true,
        'atf' => true,
        'jtf' => true,
        'transliteration' => true,
        'transliteration_clean' => true,
        'transliteration_sign_names' => true,
        'transliteration_for_search' => true,
        'annotation' => true,
        'is_atf2conll_diff_resolved' => true,
        'comments' => true,
        'structure' => true,
        'translation' => true,
        'transcription' => true,
        'update_event_id' => true,
        'inscription_comments' => true,
        'is_latest' => true,
        'artifact' => true,
        'update_event' => true,
    ];

    /**
     * Fields that should be included in the JSON export.
     *
     * @var array
     */
    public array $jsonSchema = [
        'id',
        'artifact_id',
        'atf',
        'jtf',
        'transliteration',
        'transliteration_clean',
        'transliteration_sign_names',
        'transliteration_for_search',
        'annotation',
        'is_atf2conll_diff_resolved',
        'comments',
        'structure',
        'translation',
        'transcription',
        'inscription_comments',
        'is_latest',
        'artifact'
    ];

    /**
     * @var array|null
     */
    private $tokenWarnings = null;

    public function isFieldDifferent($other, $field)
    {
        return !$this->has($field) || !$other->has($field) || $this->get($field) !== $other->get($field);
    }

    public function setTokenWarnings($period)
    {
        if (is_null($this->tokenWarnings)) {
            $atfWarnings = [];
            foreach (['words', 'signs'] as $kind) {
                $list = TokenList::getTokenList($period, $kind);
                array_push($atfWarnings, ...TokenList::compareText($this->atf, $kind, $list));
            }
            $this->tokenWarnings = $atfWarnings;
        }
    }

    public function hasTokenWarnings()
    {
        if (is_null($this->tokenWarnings)) {
            return null;
        }

        return count($this->tokenWarnings) > 0;
    }

    public function getTokenWarnings()
    {
        return $this->tokenWarnings;
    }

    public function setAnnotationWarnings()
    {
        if (is_null($this->annotationWarnings)) {
            $this->annotationWarnings = array_filter(Check::checkCdliConll($this->annotation), function ($error) {
                return $error[0] === 'danger';
            });
        }
    }

    public function hasAnnotationWarnings()
    {
        if (is_null($this->annotationWarnings)) {
            return null;
        }

        return count($this->annotationWarnings) > 0;
    }

    public function getAnnotationWarnings()
    {
        return $this->annotationWarnings;
    }

    public function getPreferredType($types)
    {
        foreach ($types as $type) {
            if ($type === 'atf' && !$this->has('atf')) {
                continue;
            }

            if ($type === 'cdli-conll' && !$this->has('annotation')) {
                continue;
            }

            if ($type === 'conll-u' && !$this->has('annotation')) {
                continue;
            }

            if ($type === 'jtf' && !$this->has('atf')) {
                continue;
            }

            return $type;
        }

        return null;
    }

    public function getCidocCrm()
    {
        $inscription = [
            '@id' => $this->getUri(),
            '@type' => [
                'crm:E34_Inscription',
                'dcmitype:Text'
            ],
            'crm:P128i_is_carried_by' => self::getEntity($this->artifact),
            'crm:P3_has_note' => $this->atf
            // TODO
        ];

        if (!empty($this->artifact)) {
            $inscription['crm:P72_has_language'] = self::getEntities($this->artifact->languages);
            $inscription['crm:P148i_is_component_of'] = self::getEntities($this->artifact->composites);
            $inscription['crm:P67_refers_to'] = self::getEntities($this->artifact->dates);
        }

        return $inscription;
    }

    public function getProcessedAtf()
    {
        if (!$this->has('atf')) {
            return null;
        }

        // Process line by line
        $processed = '';
        foreach (preg_split('/\r\n|\r|\n/', $this->atf) as $line) {
            if (preg_match('/^[0-9]/', $line)) {
                // Transliterations
                $processed .= h($line) . '<br>';
            } elseif (preg_match('/^#tr\./', $line)) {
                // Translations
                $processed .= '<i>&emsp;' . h(substr($line, 4)) . '</i> <br>';
            } elseif (preg_match('/^[#$] /', $line)) {
                // Comments
                $processed .= '<i>&emsp;' . h(substr($line, 1)) . '</i><br>';
            } elseif (preg_match('/^@/', $line)) {
                // Structure
                $processed .= '<b>' . h(substr($line, 1)) . '</b><br>';
            }
        }

        return $processed;
    }
}
