<?php

declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * OjsPublication Entity
 *
 * @property int $publication_id
 * @property int|null $access_status
 * @property \Cake\I18n\FrozenDate|null $date_published
 * @property \Cake\I18n\FrozenTime|null $last_modified
 * @property string|null $locale
 * @property int|null $primary_contact_id
 * @property int|null $section_id
 * @property float $seq
 * @property int $submission_id
 * @property int $status
 * @property string|null $url_path
 * @property int|null $version
 *
 * @property \App\Model\Entity\PrimaryContact $primary_contact
 * @property \App\Model\Entity\OjsSection $section
 * @property \App\Model\Entity\OjsSubmission $submission
 */
class OjsPublication extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'publication_id' => true,
        'access_status' => true,
        'date_published' => true,
        'last_modified' => true,
        'locale' => true,
        'primary_contact_id' => true,
        'section_id' => true,
        'seq' => true,
        'submission_id' => true,
        'status' => true,
        'url_path' => true,
        'version' => true,
        'primary_contact' => true,
        'section' => true,
        'submission' => true,
    ];
}
