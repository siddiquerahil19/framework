<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Archive Entity
 *
 * @property int $id
 * @property string|null $archive
 * @property int|null $provenience_id
 *
 * @property \App\Model\Entity\Provenience $provenience
 * @property \App\Model\Entity\Artifact[] $artifacts
 */
class Archive extends Entity
{
    use LinkedDataTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'archive' => true,
        'provenience_id' => true,
        'provenience' => true,
        'artifacts' => true
    ];

    /**
     * Fields that should be included in the JSON export.
     *
     * @var array
     */
    public array $jsonSchema = [
        'id',
        'archive',
        'provenience_id',
        'provenience'
    ];

    public function getTableRow()
    {
        return [
            'id' => $this->id,
            'archive' => $this->archive,
            'provenience' => $this->has('provenience') ? $this->provenience->provenience : ''
        ];
    }

    public function getCidocCrm()
    {
        return [
            '@id' => $this->getUri(),
            '@type' => 'crm:E53_Place',
            'crm:P87_is_identified_by' => [
                '@type' => 'crm:E48_Place_Name',
                'rdfs:label' => $this->archive
            ],
            'crm:P89_falls_within' => self::getEntity($this->provenience),
            'crm:P53i_is_former_or_current_location_of' => self::getEntities($this->artifacts)
        ];
    }
}
