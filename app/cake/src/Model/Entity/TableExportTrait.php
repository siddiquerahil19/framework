<?php

namespace App\Model\Entity;

trait TableExportTrait
{
    public function serializeValue($variable)
    {
        return is_bool($variable) ? (string) (int) $variable : (string) $variable;
    }

    public function serializeDisplay($variable, $display)
    {
        return empty($variable) ? null : $this->serializeValue($variable[$display]);
    }

    public function serializeList($list, $key)
    {
        return $this->serializeListMap($list, function ($item) use ($key) {
            return $this->serializeValue($item[$key]);
        });
    }

    public function serializeListMap($list, $map)
    {
        return empty($list) ? null : implode('; ', array_map($map, $list));
    }
}
