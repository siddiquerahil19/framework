<?php

declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ArtifactAsset Entity
 *
 * @property int $id
 * @property int $artifact_id
 * @property string|null $artifact_aspect
 * @property bool $is_public
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property string $asset_type
 * @property string $file_format
 * @property float $file_size
 * @property int|null $image_width
 * @property int|null $image_height
 * @property float|null $image_scale
 * @property string|null $image_color_average
 * @property int|null $image_color_depth
 *
 * @property \App\Model\Entity\Artifact $artifact
 */
class ArtifactAsset extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'artifact_id' => true,
        'artifact_aspect' => true,
        'is_public' => true,
        'created' => true,
        'modified' => true,
        'asset_type' => true,
        'file_format' => true,
        'file_size' => true,
        'image_width' => true,
        'image_height' => true,
        'image_scale' => true,
        'image_color_average' => true,
        'image_color_depth' => true,
        'artifact' => true,
        'annotations' => true
    ];

    /**
     * Fields that should be included in the JSON export.
     *
     * @var array
     */
    public array $jsonSchema = [
        'id',
        'artifact_id',
        'artifact_aspect',
        'created',
        'modified',
        'asset_type',
        'file_format',
        'file_size',
        'image_width',
        'image_height',
        'image_scale',
        'image_color_average',
        'image_color_depth',
        'artifact',
        'annotations'
    ];

    public function getPath()
    {
        // Base directory
        $directory = $this->asset_type;
        if ($this->asset_type === '3D model') {
            $directory = 'vcmodels';
        } elseif ($this->asset_type === 'rti') {
            $directory = 'ptm';
        } elseif (in_array($this->file_format, ['svg', 'eps', 'pdf'])) {
            $directory = $this->file_format;
        } elseif ($this->asset_type === 'info' && $this->file_format === 'html') {
            $directory = 'long_translit';
        }

        // File name
        $file = 'P' . str_pad(strval($this->artifact_id), 6, '0', STR_PAD_LEFT);

        // File name suffix(es)
        $suffix = $this->asset_type === 'lineart' ? 'l' : '';

        if ($this->has('artifact_aspect')) {
            foreach (explode(', ', $this->artifact_aspect) as $part) {
                if (array_key_exists($part, self::$aspectToSuffix)) {
                    $suffix .= self::$aspectToSuffix[$part];
                } elseif (str_starts_with($part, 'column ')) {
                    $suffix .= substr($part, strlen('column '));
                } elseif (str_starts_with($part, 'detail')) {
                    $suffix .= 'd' . substr($part, strlen('detail '));
                }
            }
        }

        if ($suffix !== '') {
            $file .= '_' . $suffix;
        }

        if ($this->asset_type === '3D model') {
            $file .= DS . $file;
        } elseif ($this->asset_type === 'rti') {
            $file .= DS . 'info';
        } elseif ($this->asset_type === 'info' && $this->file_format === 'html') {
            $file .= DS . 'index';
        }

        return 'dl' . DS . $directory . DS . $file . '.' . $this->file_format;
    }

    public function getThumbnailPath()
    {
        if ($this->asset_type === '3D model') {
            return 'images/thumbnail-3d.png';
        }

        $path = null;
        if ($this->asset_type === 'rti') {
            $path = substr($this->getPath(), 0, -strlen('/info.xml')) . '/1_3.jpg';
        } elseif ($this->file_format === 'jpg') {
            $path = $this->getPath();
            $path = 'dl' . DS . 'tn_' . substr($path, strlen('dl' . DS));
        }

        if (!is_null($path) && file_exists(WWW_ROOT . $path)) {
            return $path;
        }

        return null;
    }

    public function getDescription()
    {
        $description = $this->asset_type === 'rti' ? 'RTI' : ucfirst($this->asset_type);
        if ($this->has('artifact_aspect')) {
            $description .= " ({$this->artifact_aspect})";
        }
        return $description;
    }

    public static function getEncompassingPath(string $path)
    {
        $prefix = WWW_ROOT . 'dl' . DS;
        $parts = explode(DS, substr($path, strlen($prefix)));
        return $prefix . $parts[0] . DS . $parts[1];
    }

    public static function parsePath(string $path, bool $strict = true)
    {
        $data = [];

        $directory = explode(DS, $path)[1];
        $file = basename($path);

        // [$artifact_id, $is_lineart, $artifact_aspect, $file_format]
        //  P0*([1-9]\d*)     l?           [a-z1-9]*   (ply|xml|html|.+)
        $parts = [];
        $matches = false;
        if ($directory === 'vcmodels') {
            $relativePath = substr($path, strlen('dl' . DS . 'vcmodels' . DS));
            if ($strict) {
                $matches = preg_match('/^P0*([1-9]\d*)(?:_()([a-z1-9]*))?\/P0*\1(?:_\2\3)?\.(ply)$/', $relativePath, $parts);
            } else {
                $matches = preg_match('/^P0*([1-9]\d*)(?:_()([a-z1-9]*))?\/(.+)/', $relativePath, $parts);
                $parts[4] = 'ply';
            }
        } elseif ($directory === 'ptm') {
            $relativePath = substr($path, strlen('dl' . DS . 'ptm' . DS));
            if ($strict) {
                $matches = preg_match('/^P0*([1-9]\d*)(?:_()([a-z1-9]*))?\/info\.(xml)$/', $relativePath, $parts);
            } else {
                $matches = preg_match('/^P0*([1-9]\d*)(?:_()([a-z1-9]*))?\/(.+)/', $relativePath, $parts);
                $parts[4] = 'xml';
            }
        } elseif ($directory === 'long_translit') {
            $relativePath = substr($path, strlen('dl' . DS . 'long_translit' . DS));
            if ($strict) {
                $matches = preg_match('/^P0*([1-9]\d*)(?:_()([a-z1-9]*))?\/index\.(html)$/', $relativePath, $parts);
            } else {
                $matches = preg_match('/^P0*([1-9]\d*)(?:_()([a-z1-9]*))?\/(.+)/', $relativePath, $parts);
                $parts[4] = 'html';
            }
        } else {
            $matches = preg_match('/^P0*([1-9]\d*)(?:_(l?)([a-z1-9]*))?\.(.+)$/', $file, $parts);
        }

        if (!$matches) {
            return null;
        }

        $data['artifact_id'] = $parts[1];

        if ($directory === 'vcmodels') {
            $data['asset_type'] = '3D model';
        } elseif ($directory === 'ptm') {
            $data['asset_type'] = 'rti';
        } elseif ($directory === 'lineart' || (!$strict && $directory === 'tn_lineart')) {
            $data['asset_type'] = 'lineart';
        } elseif ($directory === 'photo' || (!$strict && $directory === 'tn_photo')) {
            $data['asset_type'] = 'photo';
        } else {
            $data['asset_type'] = 'info';
        }

        // Error if lineart does not have _l in name; error if non-lineart has _l in name
        if (($parts[2] === 'l') !== ($data['asset_type'] === 'lineart')) {
            return null;
        }

        // [$aspect, $column, $detail]
        $suffixParts = [];
        $suffixMatches = preg_match('/^(?:(s[a-d]|[trlb]e|[eors]|)([1-9][a-d]?|))(d[1-9]?|)$/', $parts[3], $suffixParts);
        if (!$suffixMatches) {
            return null;
        }
        if ($suffixParts[1] !== '') {
            $data['artifact_aspect'] = self::$suffixToAspect[$suffixParts[1]];
            if ($suffixParts[2] !== '') {
                $data['artifact_aspect'] .= ', column ' . $suffixParts[2];
            }
        }
        if ($suffixParts[3] !== '') {
            $detail = $suffixParts[3] === 'd' ? 'detail' : 'detail ' . substr($suffixParts[3], 1);
            $data['artifact_aspect'] = $data['artifact_aspect'] ? $data['artifact_aspect'] . ', ' . $detail : $detail;
        }

        $data['file_format'] = $parts[4];

        return $data;
    }

    private static $suffixToAspect = [
        'e' => 'envelope',
        'o' => 'obverse',
        'r' => 'reverse',
        's' => 'seal impression',

        're' => 'right edge',
        'be' => 'bottom edge',
        'te' => 'top edge',
        'le' => 'left edge',

        'sa' => 'surface a',
        'sb' => 'surface b',
        'sc' => 'surface c',
        'sd' => 'surface d'
    ];

    private static $aspectToSuffix = [
        'envelope' => 'e',
        'obverse' => 'o',
        'reverse' => 'r',
        'seal impression' => 's',

        'right edge' => 're',
        'bottom edge' => 'be',
        'top edge' => 'te',
        'left edge' => 'le',

        'surface a' => 'sa',
        'surface b' => 'sb',
        'surface c' => 'sc',
        'surface d' => 'sd'
    ];

    public function getTableRow()
    {
        return [
            'path' => $this->getPath(),
            'thumbnail' => $this->getThumbnailPath(),
            'artifact_id' => $this->artifact_id,
            'artifact_aspect' => $this->artifact_aspect,
            'asset_type' => $this->asset_type,
            'file_format' => $this->file_format
        ];
    }

    public function _getPath()
    {
        return $this->getPath();
    }
}
