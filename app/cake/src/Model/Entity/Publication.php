<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;
use App\Model\Entity\TableExportTrait;
use Cake\Utility\Inflector;

/**
 * Publication Entity
 *
 * @property int $id
 * @property string|null $bibtexkey
 * @property string|null $year
 * @property int|null $entry_type_id
 * @property string|null $address
 * @property string|null $annote
 * @property string|null $book_title
 * @property string|null $chapter
 * @property string|null $crossref
 * @property string|null $edition
 * @property string|null $editor
 * @property string|null $how_published
 * @property string|null $institution
 * @property int|null $journal_id
 * @property string|null $month
 * @property string|null $note
 * @property string|null $number
 * @property string|null $organization
 * @property string|null $pages
 * @property string|null $publisher
 * @property string|null $school
 * @property string|null $title
 * @property string|null $volume
 * @property string|null $publication_history
 * @property string|null $series
 * @property int|null $oclc
 * @property string|null $designation
 * @property int $accepted_by
 * @property bool $accepted
 *
 * @property \App\Model\Entity\EntryType $entry_type
 * @property \App\Model\Entity\Journal $journal
 * @property \App\Model\Entity\Abbreviation[] $abbreviations
 * @property \App\Model\Entity\Article[] $articles
 * @property \App\Model\Entity\Artifact[] $artifacts
 * @property \App\Model\Entity\Provenience[] $proveniences
 * @property \App\Model\Entity\Author[] $authors
 * @property \App\Model\Entity\Editor[] $editors
 */
class Publication extends Entity
{
    use TableExportTrait;
    use LinkedDataTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'bibtexkey' => true,
        'year' => true,
        'entry_type_id' => true,
        'address' => true,
        'annote' => true,
        'book_title' => true,
        'chapter' => true,
        'crossref' => true,
        'edition' => true,
        'editor' => true,
        'how_published' => true,
        'institution' => true,
        'journal_id' => true,
        'month' => true,
        'note' => true,
        'number' => true,
        'organization' => true,
        'pages' => true,
        'publisher' => true,
        'school' => true,
        'title' => true,
        'volume' => true,
        'publication_history' => true,
        'series' => true,
        'oclc' => true,
        'designation' => true,
        'accepted_by' => true,
        'accepted' => true,
        'entry_type' => true,
        'journal' => true,
        'abbreviations' => true,
        'articles' => true,
        'artifacts' => true,
        'proveniences' => true,
        'authors' => true,
        'editors' => true
    ];

    /**
     * Fields that should be included in the JSON export.
     *
     * @var array
     */
    public array $jsonSchema = [
        'id',
        'bibtexkey',
        'year',
        'entry_type_id',
        'address',
        'annote',
        'book_title',
        'chapter',
        'crossref',
        'edition',
        'editor',
        'how_published',
        'institution',
        'journal_id',
        'month',
        'note',
        'number',
        'organization',
        'pages',
        'publisher',
        'school',
        'title',
        'volume',
        'publication_history',
        'series',
        'oclc',
        'designation',
        'entry_type',
        'journal',
        'abbreviations',
        'articles',
        'artifacts',
        'proveniences',
        'authors',
        'editors'
    ];

    public function getTableRow()
    {
        return [
            'bibtexkey' => $this->bibtexkey,
            'authors' => $this->serializeList($this->authors, 'author'),
            'year' => $this->year,
            'entry_type' => $this->serializeDisplay($this->entry_type, 'label'),
            'address' => $this->address,
            'annote' => $this->annote,
            'book_title' => $this->book_title,
            'chapter' => $this->chapter,
            'crossref' => $this->crossref,
            'edition' => $this->edition,
            'editors' => $this->serializeList($this->editors, 'author'),
            'how_published' => $this->how_published,
            'institution' => $this->institution,
            'journal' => $this->serializeDisplay($this->journal, 'journal'),
            'month' => $this->month,
            'note' => $this->note,
            'number' => $this->number,
            'organization' => $this->organization,
            'pages' => $this->pages,
            'publisher' => $this->publisher,
            'school' => $this->school,
            'title' => $this->title,
            'volume' => $this->volume,
            'publication_history' => $this->publication_history,
            'series' => $this->series,
            'designation' => $this->designation
        ];
    }

    private function getEntryType()
    {
        if (!empty($this->entry_type)) {
            return 'bibtex:' . ucfirst($this->entry_type->label);
        } else {
            return 'bibtex:Entry';
        }
    }

    private static function getCreators($creators)
    {
        if (!is_array($creators)) {
            return null;
        }

        return implode(' and ', array_map(function ($creator) {
            return $creator->author;
        }, $creators));
    }

    public function getCidocCrm()
    {
        if (!empty($this->publisher)) {
            $publisher = $this->publisher;
        } elseif (!empty($this->institution)) {
            $publisher = $this->institution;
        } elseif (!empty($this->organization)) {
            $publisher = $this->organization;
        } elseif (!empty($this->school)) {
            $publisher = $this->school;
        }

        if (!empty($this->year)) {
            $date = empty($this->month) ? $this->year : $this->month . ' ' . $this->year;
        }

        $entryType = $this->getEntryType();

        $graph = [
            'work' => [
                '@id' => $this->getUri(),
                '@type' => 'frbroo:F1_Work'
            ],
            'expressionCreation' => [
                '@id' => $this->getUri() . '#expressionCreation',
                '@type' => 'frbroo:F28_Expression_Creation',
                // author
                'frbroo:P14_carried_out_by' => self::getEntities($this->authors)
            ],
            'expression' => [
                '@id' => $this->getUri() . '#expression',
                '@type' => 'frbroo:F2_Expression',
                // title
                'crm:P1_is_identified_by' => [
                    '@type' => 'crm:E41_Appellation',
                    'rdfs:label' => $entryType === 'bibtex:Chapter' ? $this->chapter : $this->title
                ]
            ],
            'manifestationCreation' => [
                '@id' => $this->getUri() . '#manifestationCreation',
                '@type' => 'frbroo:F30_Manifestation_Creation',
                // editor
                'frbroo:P15_was_influenced_by' => self::getEntities($this->editors)
            ],
            'manifestation' => [
                '@id' => $this->getUri() . '#manifestation',
                '@type' => 'frbroo:F3_Manifestation',
                'crm:P1_is_identified_by' => [
                    // designation
                    ['@type' => 'crm:E41_Appellation', 'rdfs:label' => $this->designation],
                    // oclc
                    ['@type' => 'crm:E41_Appellation', 'rdfs:label' => $this->oclc]
                ],
                'crm:P2_has_type' => $entryType
            ],
            'itemCreation' => [
                '@id' => $this->getUri() . '#itemCreation',
                '@type' => 'frbroo:F32_Carrier_Production_Event',
                // publisher
                'frbroo:P14_carried_out_by' => (empty($publisher) ? null : [
                    '@type' => 'crm:E39_Actor',
                    'crm:P1_is_identified_by' => [
                        '@type' => 'crm:E41_Appellation',
                        'rdfs:label' => $publisher
                    ],
                    // address
                    'crm:P74_has_current_or_former_residence' => (empty($this->address) ? null : [
                        '@type' => 'E53 Place',
                        'crm:P1_is_identified_by' => [
                            '@type' => 'crm:E41_Appellation',
                            'rdfs:label' => $this->address
                        ]
                    ])
                ]),
                // year & month
                'crm:P4_has_time-span' => (empty($date) ? null : [
                    '@type' => 'crm:E52_Time-Span',
                    'crm:P79_beginning_is_qualified_by' => ['@type' => 'crm:E62_String', 'rdfs:value' => $date],
                    'crm:P80_end_is_qualified_by' => ['@type' => 'crm:E62_String', 'rdfs:value' => $date]
                ])
            ]
        ];

        if ($this->has('journal')) {
            $container = [
                '@type' => 'frbroo:F2_Expression',
                'frbroo:R3i_realised' => self::getEntity($this->journal)
            ];

            if (!empty($this->volume)) {
                $container = [
                    '@type' => 'frbroo:F2_Expression',
                    'frbroo:R5i_is_component_of' => $container,
                    'crm:P1_is_identified_by' => [
                        '@type' => 'crm:E41_Appellation',
                        'rdfs:label' => 'Volume ' . $this->volume
                    ]
                ];
            }
            if (!empty($this->number)) {
                $container = [
                    '@type' => 'frbroo:F2_Expression',
                    'frbroo:R5i_is_component_of' => $container,
                    'crm:P1_is_identified_by' => [
                        '@type' => 'crm:E41_Appellation',
                        'rdfs:label' => 'Issue' . $this->number
                    ]
                ];
            }
        } elseif ($entryType === 'bibtex:Chapter') {
            $container = [
                '@type' => 'frbroo:F2_Expression',
                'crm:P1_is_identified_by' => [
                    '@type' => 'crm:E41_Appellation',
                    'rdfs:label' => $this->title
                ]
            ];
        } elseif (!empty($this->booktitle)) {
            $container = [
                '@type' => 'frbroo:F2_Expression',
                'crm:P1_is_identified_by' => [
                    '@type' => 'crm:E41_Appellation',
                    'rdfs:label' => $this->booktitle
                ]
            ];
        }

        if (!empty($this->series) && empty($this->journal)) {
            $container['R5i_is_component_of'] = [
                '@type' => 'frbroo:F2_Expression',
                'frbroo:R3i_realised' => [
                    '@type' => 'frbroo:F1_Work',
                    'crm:P1_is_identified_by' => [
                        '@type' => 'crm:E41_Appellation',
                        'rdfs:label' => $this->series
                    ]
                ]
            ];
        }

        if (isset($container)) {
            $graph['expression']['frbroo:R5i_is_component_of'] = $container;
        }

        $bibtex = [
            '@id' => $this->getUri() . '#bibtex',
            '@type' => ['crm:E89_Propositional_Object', $entryType],
            'crm:P129_is_about' => self::getEntity($this),
            'bibtex:hasKey' => $this->bibtexkey,
            'bibtex:hasAddress' => $this->address,
            'bibtex:hasAnnotation' => $this->annote,
            'bibtex:hasAuthor' => self::getCreators($this->authors),
            'bibtex:hasBooktitle' => $this->book_title,
            'bibtex:hasChapter' => $this->chapter,
            'bibtex:hasCrossref' => $this->crossref,
            'bibtex:hasEdition' => $this->edition,
            'bibtex:hasEditor' => self::getCreators($this->editors),
            'bibtex:howPublished' => $this->how_published,
            'bibtex:hasInstitution' => $this->institution,
            'bibtex:hasJournal' => $this->serializeDisplay($this->journal, 'journal'),
            'bibtex:hasMonth' => $this->month,
            'bibtex:hasNote' => $this->note,
            'bibtex:hasNumber' => $this->number,
            'bibtex:hasOrganization' => $this->organization,
            'bibtex:hasPages' => $this->pages,
            'bibtex:hasPublisher' => $this->publisher,
            'bibtex:hasSchool' => $this->school,
            'bibtex:hasSeries' => $this->series,
            'bibtex:hasTitle' => $this->title,
            'bibtex:hasType' => $this->type,
            'bibtex:hasVolume' => $this->volume,
            'bibtex:hasYear' => $this->year
        ];

        foreach ($bibtex as $key => $value) {
            if (empty($value)) {
                unset($bibtex[$key]);
            }
        }

        $graph[] = $bibtex;

        return self::linkFrbrData($graph);
    }
}
