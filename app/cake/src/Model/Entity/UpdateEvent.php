<?php

namespace App\Model\Entity;

use App\Model\Entity\User;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

/**
 * UpdateEvent Entity
 *
 * @property int $id
 * @property \Cake\I18n\FrozenTime $created
 * @property int|null $created_by
 * @property int|null $external_resource_id
 * @property string|null $update_type
 * @property string|null $event_comments
 * @property \Cake\I18n\FrozenTime $approved
 * @property int|null $approved_by
 * @property string $status
 *
 * @property \App\Model\Entity\User $creator
 * @property \App\Model\Entity\User $reviewer
 * @property \App\Model\Entity\Author[] $authors
 */
class UpdateEvent extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'external_resource_id' => true,
        'update_type' => true,
        'event_comments' => true,
        'authors' => true,
    ];

    /**
     * Fields that should be included in the JSON export.
     *
     * @var array
     */
    public array $jsonSchema = [
        'id',
        'external_resource_id',
        'update_type',
        'event_comments',
        'created_by',
        'approved_by',
        'creator',
        'reviewer',
        'authors'
    ];

    public function apply()
    {
        if ($this->update_type === 'artifact') {
            TableRegistry::get('UpdateEvents')->loadInto($this, ['ArtifactsUpdates.Artifacts']);
            $Artifacts = TableRegistry::get('Artifacts');
            $ArtifactsShadow = TableRegistry::get('ArtifactsShadow');
            $ArtifactsUpdates = TableRegistry::get('ArtifactsUpdates');

            foreach ($this->artifacts_updates as $artifacts_update) {
                $artifact = $artifacts_update->artifact;
                if (empty($artifact)) {
                    $artifacts_update->artifact = $artifact = $Artifacts->newEmptyEntity();
                }

                $success = $artifacts_update->apply($artifact);
                if ($artifact->has('artifacts_shadow')) {
                    $success = $success && $ArtifactsShadow->saveMany($artifact->artifacts_shadow);
                }
                $success = $success && $Artifacts->save($artifact);
                $success = $success && $ArtifactsUpdates->save($artifacts_update);

                if (!$success) {
                    $errors = $artifacts_update->getErrors();
                    $message = __(
                        'Failed to update artifact #{0}. Reason(s): {1}',
                        $artifacts_update->artifact_id,
                        implode('; ', array_map(function ($field, $error) {
                            return $field . ': ' . implode(', ', $error);
                        }, array_keys($errors), array_values($errors)))
                    );
                    $this->setError('artifacts_updates', $message, false);
                }
            }
        } elseif ($this->update_type === 'visual_annotation') {
            TableRegistry::get('UpdateEvents')->loadInto($this, ['ArtifactAssetAnnotations']);
            $Annotations = TableRegistry::get('ArtifactAssetAnnotations');

            foreach ($this->artifact_asset_annotations as $annotation) {
                $success = true;

                if ($annotation->has('annotation_id')) {
                    $previousAnnotation = $Annotations->find()->where([
                        'is_active' => true,
                        'OR' => [
                            'id' => $annotation->annotation_id,
                            'annotation_id' => $annotation->annotation_id
                        ]
                    ])->first();
                    $previousAnnotation->is_active = true;
                    $success = $success && $Annotations->save($previousAnnotation);
                }

                $annotation->is_active = true;
                $success = $success && $Annotations->save($annotation);

                if (!$success) {
                    $this->setError('artifact_asset_annotations', __('Failed to save annotation #{0}', $annotation->id), false);
                }
            }
        } else {
            TableRegistry::get('UpdateEvents')->loadInto($this, ['Inscriptions.Artifacts']);
            $Inscriptions = TableRegistry::get('Inscriptions');
            $Artifacts = TableRegistry::get('Artifacts');

            foreach ($this->inscriptions as $inscription) {
                $Artifacts->loadInto($inscription->artifact, ['Inscriptions']);

                $success = true;
                if ($inscription->artifact->has('inscription')) {
                    $inscription->artifact->inscription->is_latest = false;
                    $success = $success && $Inscriptions->save($inscription->artifact->inscription);
                }

                $inscription->is_latest = true;
                $success = $success && $Inscriptions->save($inscription);

                if (!$success) {
                    $this->setError('inscriptions', __('Failed to update inscription #{0}', $inscription->id), false);
                }
            }
        }

        return !$this->hasErrors();
    }

    public function isCreatedBy(User $user)
    {
        if (empty($user) || !$user->has('author_id')) {
            return false;
        }
        return $this->created_by == $user->author_id;
    }

    public function isApprovableByEditor()
    {
        // Temporarily ignored; see https://gitlab.com/cdli/framework/-/issues/1375
        // if ($this->update_type === 'atf' && is_array($this->inscriptions)) {
        //     foreach ($this->inscriptions as $inscription) {
        //         $inscription->setTokenWarnings($inscription->artifact->period_id);
        //         $warnings = $inscription->getTokenWarnings();
        //         if (in_array('danger', array_column($warnings, 0))) {
        //             return false;
        //         }
        //     }
        // }

        if ($this->update_type === 'annotation' && is_array($this->inscriptions)) {
            foreach ($this->inscriptions as $inscription) {
                $inscription->setAnnotationWarnings($inscription->artifact->period_id);
                $warnings = $inscription->getAnnotationWarnings();
                if (in_array('danger', array_column($warnings, 0))) {
                    return false;
                }
            }
        }

        return true;
    }
}
