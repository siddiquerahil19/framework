<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ArtifactsSeal Entity
 *
 * @property int $id
 * @property string|null $seal_no
 * @property int|null $artifact_id
 *
 * @property \App\Model\Entity\Artifact $seal
 * @property \App\Model\Entity\Artifact $impression
 */
class ArtifactsSeal extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'seal_no' => true,
        'artifact_id' => true,
        'seal' => true,
        'impression' => true,
    ];

    /**
     * Fields that should be included in the JSON export.
     *
     * @var array
     */
    public array $jsonSchema = [
        'id',
        'seal_no',
        'artifact_id',
        'seal',
        'impression'
    ];
}
