<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Abbreviation Entity
 *
 * @property int $id
 * @property string $abbreviation
 * @property string|null $fullform
 * @property string|null $type
 * @property int|null $publication_id
 *
 * @property \App\Model\Entity\Publication $publication
 */
class Abbreviation extends Entity
{
    use LinkedDataTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'abbreviation' => true,
        'fullform' => true,
        'type' => true,
        'publication_id' => true,
        'publication' => true
    ];

    /**
     * Fields that should be included in the JSON export.
     *
     * @var array
     */
    public array $jsonSchema = [
        'id',
        'abbreviation',
        'fullform',
        'type',
        'publication_id',
        'publication'
    ];

    public function getTableRow()
    {
        return [
            'id' => $this->id,
            'abbreviation' => $this->abbreviation,
            'fullform' => $this->fullform,
            'type' => $this->type,
            'publication' => $this->has('publication') ? $this->publication->publication : ''
        ];
    }

    public function getCidocCrm()
    {
        $entity = [
            '@id' => $this->getUri(),
            '@type' => 'crm:E41_Appellation',
            'rdfs:label' => $this->abbreviation
        ];

        if ($this->has('publication')) {
            $entity['crm:P1i_identifies'] = ['@id' => $this->publication->getUri() . '#manifestation'];
        } elseif ($this->has('fullform')) {
            $entity['crm:P1i_identifies'] = [
                'crm:P1_is_identified_by' => [
                    '@type' => 'crm:E41_Appellation',
                    'rdfs:label' => trim($this->fullform)
                ]
            ];
        }

        return $entity;
    }
}
