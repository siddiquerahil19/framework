<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Period Entity
 *
 * @property int $id
 * @property int|null $sequence
 * @property string $period
 * @property string $name
 * @property string $time_range
 *
 * @property \App\Model\Entity\Artifact[] $artifacts
 * @property \App\Model\Entity\Ruler[] $rulers
 * @property \App\Model\Entity\SignReading[] $sign_readings
 * @property \App\Model\Entity\Year[] $years
 */
class Period extends Entity
{
    use LinkedDataTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'sequence' => true,
        'period' => true,
        'name' => true,
        'time_range' => true,
        'artifacts' => true,
        'rulers' => true,
        'sign_readings' => true,
        'years' => true
    ];

    /**
     * Fields that should be included in the JSON export.
     *
     * @var array
     */
    public array $jsonSchema = [
        'id',
        'sequence',
        'period',
        'name',
        'time_range',
        'rulers'
    ];

    public function getTableRow()
    {
        return [
            'id' => $this->id,
            'sequence' => $this->sequence,
            'period' => $this->period,
            'name' => $this->name,
            'time_range' => $this->time_range
        ];
    }

    public function getCidocCrm()
    {
        if ($this->period == 'uncertain') {
            return null;
        }

        $period = [
            '@id' => $this->getUri(),
            '@type' => 'crm:E4_Period',
            'crm:P1_is_identified_by' => [
                'rdfs:label' => $this->period,
                '@type' => 'crm:E41_Appellation'
            ],
            'crm:P10i_contains' => !is_array($this->artifacts) ? null : array_map(function ($artifact) {
                return [
                    '@type' => 'crm:E12_Production',
                    'crm:P108_has_produced' => $artifact->getCidocCrm()
                ];
            }, $this->artifacts)
            // TODO rulers
            // TODO sign-readings
            // TODO years
        ];

        preg_match('/(.+) \\(((?:ca\\. )?)(.+?)\\)/', $this->period, $matches);
        if (count($matches) > 0) {
            $period['crm:P1_is_identified_by']['rdfs:label'] = $matches[1];
            if ($matches[2] == 'ca. ') {
                $period['crm:P79_end_is_qualified_by'] = 'circa';
                $period['crm:P79_beginning_is_qualified_by'] = 'circa';
            }
            $period['crm:P4_has_time-span'] = [
                '@type' => 'crm:E52_Time-Span',
                'crm:P78_is_identified_by' => [
                    'rdfs:label' => $matches[3],
                    '@type' => 'crm:E49_Time_Appellation'
                ]
            ];
        }

        return $period;
    }
}
