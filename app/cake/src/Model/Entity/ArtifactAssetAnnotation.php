<?php

declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ArtifactAssetAnnotation Entity
 *
 * @property int $id
 * @property int $annotation_id
 * @property int $artifact_asset_id
 * @property int $update_event_id
 * @property string|null $license
 * @property string $target_selector
 * @property bool $is_active
 *
 * @property \App\Model\Entity\Annotation $annotation
 * @property \App\Model\Entity\ArtifactAsset $artifact_asset
 * @property \App\Model\Entity\UpdateEvent $update_event
 */
class ArtifactAssetAnnotation extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'annotation_id' => true,
        'artifact_asset_id' => true,
        'update_event_id' => true,
        'license' => true,
        'target_selector' => true,
        'is_active' => true,
        'original_annotation' => true,
        'artifact_asset' => true,
        'update_event' => true,
    ];

    public function getW3cJson()
    {
        $creation = $this->update_event;
        if ($this->has('original_annotation')) {
            $creation = $this->original_annotation->update_event;
        }
        $creators = [$creation->creator, ...($creation->authors ?? [])];

        return [
            '@context' => 'http://www.w3.org/ns/anno.jsonld',
            'id' => 'artifact-asset-annotations/' . $this->id,
            'type' => 'Annotation',
            'body' => array_map(function ($body) {
                return [
                    'type' => $body->type,
                    'purpose' => $body->purpose,
                    'value' => $body->value,
                    'source' => $body->source,
                    'rights' => $body->license
                ];
            }, $this->bodies),
            'target' => [
                'source' => [
                    'id' => 'artifact-assets/' . $this->artifact_asset_id
                ],
                'selector' => [
                    'type' => 'SvgSelector',
                    'value' => $this->target_selector
                ]
            ],
            'created' => $creation->approved,
            'creator' => array_map(function ($author) {
                return [
                    'id' => $author->getUri(),
                    'type' => $author->id === 820 ? 'Organization' : 'Person',
                    'name' => $author->author
                ];
            }, $creators),
            'modified' => $this->update_event->approved,
            'rights' => $this->license
        ];
    }
}
