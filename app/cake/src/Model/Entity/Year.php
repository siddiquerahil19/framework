<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Year Entity
 *
 * @property int $id
 * @property string|null $year_no
 * @property int|null $sequence
 * @property string|null $composite_year_name
 * @property int|null $period_id
 *
 * @property \App\Model\Entity\Period $period
 * @property \App\Model\Entity\Date[] $dates
 */
class Year extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'year_no' => true,
        'sequence' => true,
        'composite_year_name' => true,
        'period_id' => true,
        'period' => true,
        'dates' => true
    ];

    /**
     * Fields that should be included in the JSON export.
     *
     * @var array
     */
    public array $jsonSchema = [
        'id',
        'year_no',
        'sequence',
        'composite_year_name',
        'period_id',
        'period'
    ];
}
