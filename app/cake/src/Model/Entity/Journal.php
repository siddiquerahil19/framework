<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Journal Entity
 *
 * @property int $id
 * @property string $journal
 *
 * @property \App\Model\Entity\Publication[] $publications
 */
class Journal extends Entity
{
    use LinkedDataTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'journal' => true,
        'publications' => true
    ];

    /**
     * Fields that should be included in the JSON export.
     *
     * @var array
     */
    public array $jsonSchema = [
        'id',
        'journal',
        'publications'
    ];

    public function getTableRow()
    {
        return [
            'id' => $this->id,
            'journal' => $this->journal
        ];
    }

    public function getCidocCrm()
    {
        return [
            '@id' => $this->getUri(),
            '@type' => 'frbroo:F18_Serial_Work',
            'crm:P1_is_identified_by' => [
                [
                    '@type' => 'crm:E41_Appellation',
                    'rdfs:label' => $this->journal
                ]
            ]
        ];
    }
}
