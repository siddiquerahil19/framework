<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Author Entity
 *
 * @property int $id
 * @property string $author
 * @property string|null $last
 * @property string|null $first
 * @property bool $east_asian_order
 * @property string|null $email
 * @property string|null $institution
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property string|null $orcid_id
 * @property bool $deceased
 * @property string|null $biography
 *
 * @property \App\Model\Entity\SignReadingsComment[] $sign_readings_comments
 * @property \App\Model\Entity\Staff[] $staff
 * @property \App\Model\Entity\User[] $users
 * @property \App\Model\Entity\Article[] $articles
 * @property \App\Model\Entity\Publication[] $publications
 * @property \App\Model\Entity\UpdateEvent[] $update_events
 */
class Author extends Entity
{
    use LinkedDataTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'author' => true,
        'last' => true,
        'first' => true,
        'east_asian_order' => true,
        'email' => true,
        'institution' => true,
        'modified' => true,
        'orcid_id' => true,
        'deceased' => true,
        'biography' => true,
        'sign_readings_comments' => true,
        'staff' => true,
        'users' => true,
        'articles' => true,
        'publications' => true,
        'update_events' => true
    ];

    /**
     * Fields that should be included in the JSON export.
     *
     * @var array
     */
    public array $jsonSchema = [
        'id',
        'author',
        'last',
        'first',
        'east_asian_order',
        'email',
        'institution',
        'orcid_id',
        'deceased',
        'sign_readings_comments',
        'staff',
        'articles',
        'publications',
        'update_events'
    ];

    public function getTableRow()
    {
        return [
            'id' => $this->id,
            'author' => $this->author,
            'last' => $this->last,
            'first' => $this->first,
            'east_asian_order' => $this->east_asian_order,
            'email' => $this->email,
            'institution' => $this->institution,
            'biography' => $this->biography,
            'modified' => $this->modified,
            'orcid_id' => $this->orcid_id,
            'deceased' => $this->deceased,
            'ojs_author_id' => $this->ojs_author_id,
            'org_script_name' => $this->org_script_name
        ];
    }

    protected function _setEmail($value)
    {
        if ($value == '') {
            $value = null;
        }

        return $value;
    }

    public function getCidocCrm()
    {
        return [
            '@id' => $this->getUri(),
            '@type' => 'crm:E21_Person',
            'crm:P1_is_identified_by' => [
                [
                    '@type' => 'crm:E41_Appellation',
                    'rdfs:label' => $this->author
                ],
                (empty($this->org_script_name) ? null : [
                    '@type' => 'crm:E41_Appellation',
                    'rdfs:label' => $this->org_script_name
                ]),
                (empty($this->org_script_name) ? null : [
                    '@type' => 'crm:E41_Appellation',
                    'rdfs:label' => $this->orcid_id
                ])
            ],
            'crm:P14i_performed' => $this->has('publications') ? array_map(function ($publication) {
                return ['@id' => $publication->getUri() . '#expressionCreation'];
            }, $this->publications) : []
        ];
    }
}
