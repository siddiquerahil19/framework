<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * MaterialAspect Entity
 *
 * @property int $id
 * @property string|null $material_aspect
 *
 * @property \App\Model\Entity\Artifact[] $artifacts
 */
class MaterialAspect extends Entity
{
    use LinkedDataTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'material_aspect' => true,
        'artifacts' => true
    ];

    /**
     * Fields that should be included in the JSON export.
     *
     * @var array
     */
    public array $jsonSchema = [
        'id',
        'material_aspect'
    ];

    public function getTableRow()
    {
        return [
            'id' => $this->id,
            'material_aspect' => $this->material_aspect
        ];
    }

    public function getCidocCrm()
    {
        return [
            '@id' => $this->getUri(),
            '@type' => 'cdli:type_aspect',
            'crm:P1_is_identified_by' => [
                '@type' => 'crm:E41_Appellation',
                'rdfs:label' => $this->material_aspect
            ],
            'crm:P56i_is_found_on' => self::getEntities($this->artifacts)
        ];
    }
}
