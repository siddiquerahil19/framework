<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ExternalResource Entity
 *
 * @property int $id
 * @property string|null $external_resource
 * @property string|null $base_url
 * @property string|null $project_url
 * @property string|null $abbrev
 *
 * @property \App\Model\Entity\Artifact[] $artifacts
 */
class ExternalResource extends Entity
{
    use LinkedDataTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'external_resource' => true,
        'base_url' => true,
        'project_url' => true,
        'abbrev' => true,
        'artifacts' => true
    ];

    /**
     * Fields that should be included in the JSON export.
     *
     * @var array
     */
    public array $jsonSchema = [
        'id',
        'external_resource',
        'base_url',
        'project_url',
        'abbrev'
    ];

    public function getTableRow()
    {
        return [
            'id' => $this->id,
            'external_resource' => $this->external_resource,
            'base_url' => $this->base_url,
            'project_url' => $this->project_url,
            'abbrev' => $this->abbrev
        ];
    }

    public function getCidocCrm()
    {
        return [
            '@id' => $this->getUri(),
            '@type' => 'crm:E73_Information_Object',
            'crm:P1_is_identified_by' => [
                [
                    '@type' => 'crm:E41_Appellation',
                    'rdfs:label' => $this->external_resource
                ],
                (empty($this->abbrev) ? null : [
                    '@type' => 'crm:E41_Appellation',
                    'rdfs:label' => $this->abbrev
                ]),
                (empty($this->project_url) ? null : [
                    '@type' => 'crm:E41_Appellation',
                    'rdfs:label' => $this->project_url
                ])
            ]
        ];
    }
}
