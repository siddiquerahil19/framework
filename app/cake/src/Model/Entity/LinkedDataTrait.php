<?php

namespace App\Model\Entity;

use Cake\Utility\Inflector;
use Cake\ORM\TableRegistry;

trait LinkedDataTrait
{
    public function getUri($id = null)
    {
        if (empty($id)) {
            $id = $this->id;
        }

        $table = TableRegistry::get($this->getSource());
        $key = Inflector::variable($table->getTable());

        // Following would be for routes like /artifacts_materials/1
        // which are not used, apparently.
        // $key = Inflector::underscore($table->getTable());

        return $key . '/' . $id;
    }

    protected static function getUris($array)
    {
        if (!is_array($array)) {
            return [];
        }

        return array_map(function ($entity) {
            return $entity->getUri();
        }, $array);
    }

    protected static function getEntity($entity, ...$args)
    {
        if (empty($entity)) {
            return null;
        }

        // return $entity->getCidocCrm(...$args);
        return ['@id' => $entity->getUri()];
    }

    protected static function getEntities($array, ...$args)
    {
        if (!is_array($array)) {
            return [];
        }

        return array_map(function ($entity) use ($args) {
            return self::getEntity($entity, ...$args);
        }, $array);
    }

    protected static function getIdentifier($value, $type)
    {
        if (empty($value)) {
            return null;
        }

        return [
            'rdfs:label' => $value,
            '@type' => 'cdli:identifier_' . $type
        ];
    }

    protected static function getDimension($value, $dimension, $unit)
    {
        if (empty($value)) {
            return null;
        }

        return [
            '@type' => 'cdli:dimension_' . $dimension,
            'crm:P90_has_value' => $value,
            'crm:P91_has_unit' => ['@id' => 'cdli:unit_' . $unit]
        ];
    }

    protected static function addEntityClass(&$entity, $type)
    {
        if (array_key_exists($entity, '@type')) {
            if (is_array($entity['@type'])) {
                array_push($entity['@type'], $type);
            } else {
                $entity['@type'] = [$entity['@type'], $type];
            }
        }
        $entity['@type'] = $type;
    }

    protected static $frbrRelations = [
        ['frbroo:R3_is_realised_in', 'frbroo:R3i_realised', 'work', 'expression'],
        ['frbroo:R19_created_a_realisation_of', 'frbroo:R19i_was_realised_through', 'expressionCreation', 'work'],
        ['frbroo:R17_created', 'frbroo:R17i_was_created_by', 'expressionCreation', 'expression'],
        ['frbroo:R4_embodies', 'frbroo:R4i_is_embodied_in', 'manifestation', 'expression'],
        ['frbroo:R19_created_a_realisation_of', 'frbroo:R19i_was_realised_through', 'manifestationCreation', 'expression'],
        ['frbroo:R24_created', 'frbroo:R24i_was_created_through', 'manifestationCreation', 'manifestation'],
        ['frbroo:R27_materialized', 'frbroo:R27i_was_materialized_by', 'itemCreation', 'manifestation'],
    ];

    protected static function linkFrbrData($entities = [])
    {
        foreach (self::$frbrRelations as $relation) {
            if (array_key_exists($relation[2], $entities) && array_key_exists($relation[3], $entities)) {
                $entities[$relation[2]][$relation[0]] = ['@id' => $entities[$relation[3]]['@id']];
                $entities[$relation[3]][$relation[1]] = ['@id' => $entities[$relation[2]]['@id']];
            }
        }

        return ['@graph' => array_values($entities)];
    }

    abstract public function getCidocCrm();
}
