<?php

declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * AnnotationType Entity
 *
 * @property int $id
 * @property string $annotation_type
 *
 * @property \App\Model\Entity\ImageAnnotation[] $image_annotations
 */
class AnnotationType extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'annotation_type' => true,
        'image_annotations' => true,
    ];
}
