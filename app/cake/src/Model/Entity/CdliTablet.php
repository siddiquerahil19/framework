<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CdliTablet Entity
 *
 * @property int $id
 * @property \Cake\I18n\FrozenDate|null $date
 * @property string|null $name
 * @property string|null $path
 * @property string|null $theme
 * @property string|null $title
 * @property string|null $blurb
 * @property string|null $full_info
 */
class CdliTablet extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'date' => true,
        'name' => true,
        'path' => true,
        'theme' => true,
        'title' => true,
        'blurb' => true,
        'full_info' => true
    ];

    /**
     * Fields that should be included in the JSON export.
     *
     * @var array
     */
    public array $jsonSchema = [
        'id',
        'date',
        'name',
        'path',
        'theme',
        'title',
        'blurb',
        'full_info'
    ];
}
