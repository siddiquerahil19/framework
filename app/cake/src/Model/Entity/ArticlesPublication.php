<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ArticlesPublication Entity
 *
 * @property int $id
 * @property int $article_id
 * @property int $publication_id
 *
 * @property \App\Model\Entity\Article $article
 * @property \App\Model\Entity\Publication $publication
 */
class ArticlesPublication extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'article_id' => true,
        'publication_id' => true,
        'article' => true,
        'publication' => true
    ];

    /**
     * Fields that should be included in the JSON export.
     *
     * @var array
     */
    public array $jsonSchema = [
        'id',
        'article_id',
        'publication_id',
        'article',
        'publication'
    ];
}
