<?php

declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * OjsReviewRound Entity
 *
 * @property int $review_round_id
 * @property int $submission_id
 * @property int|null $stage_id
 * @property int $round
 * @property int|null $review_revision
 * @property int|null $status
 *
 * @property \App\Model\Entity\OjsSubmission $submission
 * @property \App\Model\Entity\Stage $stage
 * @property \App\Model\Entity\OjsReviewAssignment[] $review_assignments;
 */
class OjsReviewRound extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'submission_id' => true,
        'stage_id' => true,
        'round' => true,
        'review_revision' => true,
        'status' => true,
        'submission' => true,
        'stage' => true,
        'review_assignments' => true,
    ];
}
