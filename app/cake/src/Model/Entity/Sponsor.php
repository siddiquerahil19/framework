<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Sponsor Entity
 *
 * @property int $id
 * @property int $sponsor_type_id
 * @property string $sponsor
 * @property string $url
 * @property string $contribution
 * @property string $sequence
 *
 * @property \App\Model\Entity\SponsorType $staff_type
 */
class Sponsor extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'sponsor_type_id' => true,
        'sponsor' => true,
        'url' => true,
        'contribution' => true,
        'sequence' => true,
        'sponsor_type' => true
    ];

    /**
     * Fields that should be included in the JSON export.
     *
     * @var array
     */
    public array $jsonSchema = [
        'id',
        'sponsor_type_id',
        'sponsor',
        'url',
        'contribution',
        'sequence',
        'sponsor_type'
    ];
}
