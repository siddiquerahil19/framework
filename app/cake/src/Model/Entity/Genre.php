<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Genre Entity
 *
 * @property int $id
 * @property string $genre
 * @property int|null $parent_id
 * @property string|null $genre_description
 *
 * @property \App\Model\Entity\Genre $parent_genre
 * @property \App\Model\Entity\Genre[] $child_genres
 * @property \App\Model\Entity\Artifact[] $artifacts
 */
class Genre extends Entity
{
    use LinkedDataTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'genre' => true,
        'parent_id' => true,
        'genre_description' => true,
        'parent_genre' => true,
        'child_genres' => true,
        'artifacts' => true
    ];

    /**
     * Fields that should be included in the JSON export.
     *
     * @var array
     */
    public array $jsonSchema = [
        'id',
        'genre',
        'parent_id',
        'genre_description',
        'parent_genre',
        'child_genres'
    ];

    public function getTableRow()
    {
        return [
            'id' => $this->id,
            'genre' => $this->genre,
            'parent' => $this->has('parent_genre') ? $this->parent_genre->genre : '',
            'genre_description' => $this->genre_description
        ];
    }

    public function getCidocCrm()
    {
        return [
            '@id' => $this->getUri(),
            '@type' => 'cdli:type_genre',
            'rdfs:label' => $this->genre,
            'crm:P3_has_note' => $this->genre_description,
            'crm:P127_has_broader_term' => self::getEntity($this->parent_genre),
            'crm:P127i_has_narrower_term' => self::getEntities($this->child_genres),
            'crm:P2i_is_type_of' => self::getEntities($this->artifacts)
        ];
    }
}
