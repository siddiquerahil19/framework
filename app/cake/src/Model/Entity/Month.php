<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Month Entity
 *
 * @property int $id
 * @property string|null $composite_month_name
 * @property string|null $month_no
 * @property int|null $sequence
 *
 * @property \App\Model\Entity\Date[] $dates
 */
class Month extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'composite_month_name' => true,
        'month_no' => true,
        'sequence' => true,
        'dates' => true
    ];

    /**
     * Fields that should be included in the JSON export.
     *
     * @var array
     */
    public array $jsonSchema = [
        'id',
        'composite_month_name',
        'month_no',
        'sequence'
    ];
}
