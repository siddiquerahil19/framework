<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Ruler Entity
 *
 * @property int $id
 * @property int|null $sequence
 * @property string|null $ruler
 * @property int|null $period_id
 * @property int|null $dynasty_id
 *
 * @property \App\Model\Entity\Period $period
 * @property \App\Model\Entity\Dynasty $dynasty
 * @property \App\Model\Entity\Date[] $dates
 */
class Ruler extends Entity
{
    use LinkedDataTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'sequence' => true,
        'ruler' => true,
        'period_id' => true,
        'dynasty_id' => true,
        'period' => true,
        'dynasty' => true,
        'dates' => true
    ];

    /**
     * Fields that should be included in the JSON export.
     *
     * @var array
     */
    public array $jsonSchema = [
        'id',
        'region',
        'sequence',
        'ruler',
        'period_id',
        'dynasty_id',
        'period',
        'dynasty',
        'dates'
    ];

    public function getTableRow()
    {
        return [
            'id' => $this->id,
            'sequence' => $this->sequence,
            'ruler' => $this->ruler,
            'period' => $this->has('period') ? $this->period->period : '',
            'dynasty' => $this->has('dynasty') ? $this->dynasty->dynasty : '',
        ];
    }

    public function getCidocCrm()
    {
        return [
            '@id' => $this->getUri(),
            '@type' => 'crm:E7_Activity',
            'crm:P14_carried_out_by' => [
                '@type' => 'crm:E21_Person',
                'crm:P1_is_identified_by' => [
                    '@type' => 'crm:E82_Actor_Appellation',
                    'rdfs:label' => $this->ruler
                ]
            ],
            'crm:P4_has_time-span' => [
                '@type' => 'crm:E52_Time-Span',
                'crm:P86i_contains' => self::getEntities($this->dates),
            ],
            'crm:P10_falls_within' => self::getEntity($this->dynasty)
            // also falls within period
        ];
    }
}
