<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Material Entity
 *
 * @property int $id
 * @property string $material
 * @property int|null $parent_id
 *
 * @property \App\Model\Entity\Material $parent_material
 * @property \App\Model\Entity\Material[] $child_materials
 * @property \App\Model\Entity\Artifact[] $artifacts
 */
class Material extends Entity
{
    use LinkedDataTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'material' => true,
        'parent_id' => true,
        'parent_material' => true,
        'child_materials' => true,
        'artifacts' => true,
        '_joinData' => true
    ];

    /**
     * Fields that should be included in the JSON export.
     *
     * @var array
     */
    public array $jsonSchema = [
        'id',
        'material',
        'parent_id',
        'parent_material',
        'child_materials'
    ];

    public function getTableRow()
    {
        return [
            'id' => $this->id,
            'material' => $this->material,
            'parent' => $this->has('parent_material') ? $this->parent_material->material : ''
        ];
    }

    public function getCidocCrm()
    {
        return [
            '@id' => $this->getUri(),
            '@type' => 'crm:E57_Material',
            'crm:P1_is_identified_by' => [
                '@type' => 'crm:E41_Appellation',
                'rdfs:label' => $this->material
            ],
            'crm:P127_has_broader_term' => $this->getUri($this->parent_id),
            'crm:P127i_has_narrower_term' => self::getUris($this->child_materials),
            'crm:P45i_is_incorporated_in' => self::getEntities($this->artifacts)
        ];
    }
}
