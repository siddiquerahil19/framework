<?php

declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ArtifactAssetAnnotations Model
 *
 * @property \App\Model\Table\AnnotationsTable&\Cake\ORM\Association\BelongsTo $Annotations
 * @property \App\Model\Table\ArtifactAssetsTable&\Cake\ORM\Association\BelongsTo $ArtifactAssets
 * @property \App\Model\Table\UpdateEventsTable&\Cake\ORM\Association\BelongsTo $UpdateEvents
 *
 * @method \App\Model\Entity\ArtifactAssetAnnotation newEmptyEntity()
 * @method \App\Model\Entity\ArtifactAssetAnnotation newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactAssetAnnotation[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactAssetAnnotation get($primaryKey, $options = [])
 * @method \App\Model\Entity\ArtifactAssetAnnotation findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\ArtifactAssetAnnotation patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactAssetAnnotation[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactAssetAnnotation|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ArtifactAssetAnnotation saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ArtifactAssetAnnotation[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ArtifactAssetAnnotation[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\ArtifactAssetAnnotation[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ArtifactAssetAnnotation[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class ArtifactAssetAnnotationsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('artifact_asset_annotations');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('Bodies', [
            'className' => 'ArtifactAssetAnnotationBodies',
            'foreignKey' => 'annotation_id'
        ]);
        $this->belongsTo('OriginalAnnotations', [
            'className' => 'ArtifactAssetAnnotations',
            'foreignKey' => 'annotation_id'
        ]);
        $this->belongsTo('ArtifactAssets', [
            'foreignKey' => 'artifact_asset_id'
        ]);
        $this->belongsTo('UpdateEvents', [
            'foreignKey' => 'update_event_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('license')
            ->allowEmptyString('license');

        $validator
            ->scalar('target_selector')
            ->requirePresence('target_selector', 'create')
            ->notEmptyString('target_selector');

        $validator
            ->boolean('is_active')
            ->notEmptyString('is_active');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['annotation_id'], 'OriginalAnnotations'), ['errorField' => 'annotation_id']);
        $rules->add($rules->existsIn(['artifact_asset_id'], 'ArtifactAssets'), ['errorField' => 'artifact_asset_id']);
        $rules->add($rules->existsIn(['update_event_id'], 'UpdateEvents'), ['errorField' => 'update_event_id']);

        return $rules;
    }

    public function newEntityFromJson(array $data)
    {
        $annotation = $this->newEmptyEntity();

        if (isset($data['target'])) {
            $target = $data['target'];

            // Target selector
            if (!isset($target['selector'])) {
                $annotation->setError('target_selector', 'Annotation should have an embedded SVG selector');
            }
            $selector = isset($target['selector']['type']) ? $target['selector'] : (isset($target['selector'][0]) ? $target['selector'][0] : null);
            if (!is_null($selector) && $selector['type'] === 'SvgSelector' && isset($selector['value'])) {
                $annotation->target_selector = $selector['value'];
            } else {
                $annotation->setError('target_selector', 'Annotation should have an embedded SVG selector');
                return $annotation;
            }

            // ID of visual asset
            $asset = null;
            if (isset($target['source'])) {
                $source = substr(parse_url($target['source'], PHP_URL_PATH), 1);
                $asset = $this->ArtifactAssets->find('path', [$source])->contain(['Artifacts'])->first();
            }
            if (is_null($asset)) {
                $annotation->setError('artifact_asset_id', 'Image could not be identified from target source');
                return $annotation;
            } else {
                $annotation->artifact_asset_id = $asset->id;
                $annotation->artifact_asset = $asset;
            }
        } else {
            $annotation->setError('target_selector', 'Annotation should have a target');
            return $annotation;
        }

        if (isset($data['id'])) {
            if (preg_match('/\/artifact-asset-annotations\/([1-9]\d*)/', $data['id'], $match)) {
                $annotation->annotation_id = $match[1];
            }
        }

        $bodies = null;
        if (isset($data['body'])) {
            if (isset($data['body']['type'])) {
                $bodies = [$data['body']];
            } elseif (isset($data['body'][0])) {
                $bodies = $data['body'];
            }
        }
        if (!is_null($bodies)) {
            $annotation->bodies = [];
            foreach ($bodies as $body) {
                $annotation->bodies[] = $this->Bodies->newEntityFromJson($body);
            }
        } else {
            $annotation->setError('bodies', 'Annotation should have at least one body');
        }

        if (isset($data['rights']) && is_string($data['rights'])) {
            $annotation->license = $data['rights'];
        }

        return $annotation;
    }
}
