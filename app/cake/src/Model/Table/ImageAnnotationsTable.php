<?php

declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ImageAnnotations Model
 *
 * @property \App\Model\Table\AnnotationsTable&\Cake\ORM\Association\BelongsTo $Annotations
 * @property \App\Model\Table\AnnotationTypesTable&\Cake\ORM\Association\BelongsTo $AnnotationTypes
 * @property \App\Model\Table\ImagesTable&\Cake\ORM\Association\BelongsTo $Images
 * @property \App\Model\Table\ArtifactsTable&\Cake\ORM\Association\BelongsTo $Artifacts
 *
 * @method \App\Model\Entity\ImageAnnotation newEmptyEntity()
 * @method \App\Model\Entity\ImageAnnotation newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\ImageAnnotation[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ImageAnnotation get($primaryKey, $options = [])
 * @method \App\Model\Entity\ImageAnnotation findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\ImageAnnotation patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ImageAnnotation[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\ImageAnnotation|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ImageAnnotation saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ImageAnnotation[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ImageAnnotation[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\ImageAnnotation[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ImageAnnotation[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class ImageAnnotationsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('image_annotations');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Annotations', [
            'foreignKey' => 'annotation_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('AnnotationTypes', [
            'foreignKey' => 'annotation_type_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Images', [
            'foreignKey' => 'image_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Artifacts', [
            'foreignKey' => 'artifact_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('creator')
            ->allowEmptyString('creator');

        $validator
            ->dateTime('created_at')
            ->allowEmptyDateTime('created_at');

        $validator
            ->scalar('annotation_body_type')
            ->requirePresence('annotation_body_type', 'create')
            ->notEmptyString('annotation_body_type');

        $validator
            ->scalar('annotation_body_value')
            ->requirePresence('annotation_body_value', 'create')
            ->notEmptyString('annotation_body_value');

        $validator
            ->scalar('annotation_body_purpose')
            ->requirePresence('annotation_body_purpose', 'create')
            ->notEmptyString('annotation_body_purpose');

        $validator
            ->scalar('tag_label')
            ->allowEmptyString('tag_label');

        $validator
            ->scalar('tag_uri')
            ->allowEmptyString('tag_uri');

        $validator
            ->scalar('annotation_target_source')
            ->requirePresence('annotation_target_source', 'create')
            ->notEmptyString('annotation_target_source');

        $validator
            ->scalar('annotation_target_selector')
            ->requirePresence('annotation_target_selector', 'create')
            ->notEmptyString('annotation_target_selector');

        $validator
            ->scalar('surface')
            ->requirePresence('surface', 'create')
            ->notEmptyString('surface');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['annotation_id'], 'Annotations'), ['errorField' => 'annotation_id']);
        $rules->add($rules->existsIn(['annotation_type_id'], 'AnnotationTypes'), ['errorField' => 'annotation_type_id']);
        $rules->add($rules->existsIn(['image_id'], 'Images'), ['errorField' => 'image_id']);
        $rules->add($rules->existsIn(['artifact_id'], 'Artifacts'), ['errorField' => 'artifact_id']);

        return $rules;
    }
}
