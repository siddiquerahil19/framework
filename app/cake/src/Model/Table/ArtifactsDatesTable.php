<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ArtifactsDates Model
 *
 * @property \App\Model\Table\ArtifactsTable&\Cake\ORM\Association\BelongsTo $Artifacts
 * @property \App\Model\Table\DatesTable&\Cake\ORM\Association\BelongsTo $Dates
 *
 * @method \App\Model\Entity\ArtifactsDate newEmptyEntity()
 * @method \App\Model\Entity\ArtifactsDate newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactsDate[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactsDate get($primaryKey, $options = [])
 * @method \App\Model\Entity\ArtifactsDate findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\ArtifactsDate patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactsDate[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactsDate|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ArtifactsDate saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ArtifactsDate[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ArtifactsDate[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\ArtifactsDate[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ArtifactsDate[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class ArtifactsDatesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('artifacts_dates');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Artifacts', [
            'foreignKey' => 'artifact_id'
        ]);
        $this->belongsTo('Dates', [
            'foreignKey' => 'date_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['artifact_id'], 'Artifacts'), ['errorField' => 'artifact_id']);
        $rules->add($rules->existsIn(['date_id'], 'Dates'), ['errorField' => 'date_id']);

        return $rules;
    }
}
