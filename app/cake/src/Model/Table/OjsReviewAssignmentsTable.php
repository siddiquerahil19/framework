<?php

declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OjsReviewAssignments Model
 *
 * @property \App\Model\Table\OjsSubmissionsTable&\Cake\ORM\Association\BelongsTo $OjsSubmissions
 * @property \App\Model\Table\ReviewerFilesTable&\Cake\ORM\Association\BelongsTo $ReviewerFiles
 * @property \App\Model\Table\OjsReviewRoundsTable&\Cake\ORM\Association\BelongsTo $OjsReviewRounds
 * @property \App\Model\Table\UsrSettingsTable&\Cake\ORM\Association\HasMany $OjsUserSettings
 * @property \App\Model\Table\OjsReviewRoundsTable&\Cake\ORM\Association\HasMany $OjsSubmissionComments
 *
 * @method \App\Model\Entity\OjsReviewAssignment newEmptyEntity()
 * @method \App\Model\Entity\OjsReviewAssignment newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\OjsReviewAssignment[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OjsReviewAssignment get($primaryKey, $options = [])
 * @method \App\Model\Entity\OjsReviewAssignment findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\OjsReviewAssignment patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OjsReviewAssignment[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\OjsReviewAssignment|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OjsReviewAssignment saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OjsReviewAssignment[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\OjsReviewAssignment[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\OjsReviewAssignment[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\OjsReviewAssignment[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class OjsReviewAssignmentsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('review_assignments');
        $this->setDisplayField('review_id');
        $this->setPrimaryKey('review_id');

        $this->belongsTo('OjsSubmissions', [
            'foreignKey' => 'submission_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('OjsReviewRounds', [
            'foreignKey' => 'review_round_id',
        ]);
        $this->hasMany('OjsUserSettings', [
            'foreignKey' => 'user_id',
            'bindingKey' => 'reviewer_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('OjsSubmissionComments', [
            'foreignKey' => 'assoc_id',
            'bindingKey' => 'review_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('review_id', null, 'create');

        $validator
            ->scalar('competing_interests')
            ->allowEmptyString('competing_interests');

        $validator
            ->allowEmptyString('recommendation');

        $validator
            ->dateTime('date_assigned')
            ->allowEmptyDateTime('date_assigned');

        $validator
            ->dateTime('date_notified')
            ->allowEmptyDateTime('date_notified');

        $validator
            ->dateTime('date_confirmed')
            ->allowEmptyDateTime('date_confirmed');

        $validator
            ->dateTime('date_completed')
            ->allowEmptyDateTime('date_completed');

        $validator
            ->dateTime('date_acknowledged')
            ->allowEmptyDateTime('date_acknowledged');

        $validator
            ->dateTime('date_due')
            ->allowEmptyDateTime('date_due');

        $validator
            ->dateTime('date_response_due')
            ->allowEmptyDateTime('date_response_due');

        $validator
            ->dateTime('last_modified')
            ->allowEmptyDateTime('last_modified');

        $validator
            ->notEmptyString('reminder_was_automatic');

        $validator
            ->notEmptyString('declined');

        $validator
            ->notEmptyString('cancelled');

        $validator
            ->dateTime('date_rated')
            ->allowEmptyDateTime('date_rated');

        $validator
            ->dateTime('date_reminded')
            ->allowEmptyDateTime('date_reminded');

        $validator
            ->allowEmptyString('quality');

        $validator
            ->notEmptyString('review_method');

        $validator
            ->notEmptyString('round');

        $validator
            ->notEmptyString('step');

        $validator
            ->allowEmptyString('unconsidered');

        return $validator;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName(): string
    {
        return 'ojsdb';
    }
}
