<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use ArrayObject;
use Cake\ORM\TableRegistry;

/**
 * EditorsPublications Model
 *
 * @property \App\Model\Table\PublicationsTable&\Cake\ORM\Association\BelongsTo $Publications
 * @property \App\Model\Table\EditorsTable&\Cake\ORM\Association\BelongsTo $Editors
 *
 * @method \App\Model\Entity\EditorsPublication newEmptyEntity()
 * @method \App\Model\Entity\EditorsPublication newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\EditorsPublication[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\EditorsPublication get($primaryKey, $options = [])
 * @method \App\Model\Entity\EditorsPublication findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\EditorsPublication patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\EditorsPublication[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\EditorsPublication|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\EditorsPublication saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\EditorsPublication[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\EditorsPublication[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\EditorsPublication[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\EditorsPublication[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class EditorsPublicationsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('editors_publications');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Publications', [
            'foreignKey' => 'publication_id'
        ]);
        $this->belongsTo('Editors', [
            'foreignKey' => 'editor_id'
        ]);
    }

    /**
     * Convert input data to required format.
     */
    public function beforeMarshal(Event $event, ArrayObject $data)
    {
        if (isset($data['publication_id'])) {
            // Removing leading and trailing whitespaces
            foreach ($data as $key => $value) {
                $data[$key] = trim($value);
            }

            // Conversion from publication bibtexkey to id
            $publications = TableRegistry::getTableLocator()->get('Publications');
            $publication = $publications->find('all', ['conditions' => ['bibtexkey' => $data['publication_id']]])->first();
            $data['publication_id'] = (isset($publication)) ? $publication->id : 0;   // 0 indicates that the publication doesn't exist

            // Conversion from editor name to author (as editor) id
            $editors = TableRegistry::getTableLocator()->get('Editors');
            $editor = $editors->find('all', ['conditions' => ['author' => $data['editor_id']]])->first();
            $data['editor_id'] = (isset($editor)) ? $editor->id : 0;   // 0 indicates that the author doesn't exist
        }
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->integer('sequence')
            ->allowEmptyString('sequence');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['publication_id'], 'Publications'), ['errorField' => 'publication_id']);
        $rules->add($rules->existsIn(['editor_id'], 'Editors'), ['errorField' => 'editor_id']);

        return $rules;
    }
}
