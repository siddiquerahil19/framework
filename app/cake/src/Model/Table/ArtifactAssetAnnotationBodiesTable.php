<?php

declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ArtifactAssetAnnotationBodies Model
 *
 * @property \App\Model\Table\AnnotationsTable&\Cake\ORM\Association\BelongsTo $Annotations
 *
 * @method \App\Model\Entity\ArtifactAssetAnnotationBody newEmptyEntity()
 * @method \App\Model\Entity\ArtifactAssetAnnotationBody newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactAssetAnnotationBody[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactAssetAnnotationBody get($primaryKey, $options = [])
 * @method \App\Model\Entity\ArtifactAssetAnnotationBody findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\ArtifactAssetAnnotationBody patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactAssetAnnotationBody[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactAssetAnnotationBody|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ArtifactAssetAnnotationBody saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ArtifactAssetAnnotationBody[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ArtifactAssetAnnotationBody[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\ArtifactAssetAnnotationBody[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ArtifactAssetAnnotationBody[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class ArtifactAssetAnnotationBodiesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('artifact_asset_annotation_bodies');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Annotations', [
            'className' => 'ArtifactAssetAnnotations',
            'foreignKey' => 'annotation_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('type')
            ->allowEmptyString('type');

        $validator
            ->scalar('value')
            ->allowEmptyString('value');

        $validator
            ->scalar('source')
            ->allowEmptyString('source');

        $validator
            ->scalar('purpose')
            ->allowEmptyString('purpose');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['annotation_id'], 'Annotations'), ['errorField' => 'annotation_id']);

        return $rules;
    }

    public function newEntityFromJson(array $data)
    {
        $body = $this->newEmptyEntity();
        if (isset($data['type']) && in_array($data['type'], ['SpecialResource', 'TextualBody'])) {
            $body->type = $data['type'];
        } else {
            $this->setError('type', 'Annotation body type should be SpecialResource or TextualBody');
        }

        if (isset($data['value'])) {
            $body->value = $data['value'];
        }

        if (isset($data['source'])) {
            $body->source = $data['source'];
        }

        if (!isset($body->value) && !isset($body->purpose)) {
            $body->setError('value', 'Annotation body should have at least either value or source');
        }

        if (isset($data['purpose'])) {
            $body->purpose = $data['purpose'];
        }

        return $body;
    }
}
