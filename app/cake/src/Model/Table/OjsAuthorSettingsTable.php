<?php

declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OjsAuthorSettings Model
 *
 * @property \App\Model\Table\AuthorsTable&\Cake\ORM\Association\BelongsTo $Authors
 *
 * @method \App\Model\Entity\OjsAuthorSetting newEmptyEntity()
 * @method \App\Model\Entity\OjsAuthorSetting newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\OjsAuthorSetting[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OjsAuthorSetting get($primaryKey, $options = [])
 * @method \App\Model\Entity\OjsAuthorSetting findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\OjsAuthorSetting patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OjsAuthorSetting[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\OjsAuthorSetting|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OjsAuthorSetting saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OjsAuthorSetting[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\OjsAuthorSetting[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\OjsAuthorSetting[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\OjsAuthorSetting[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class OjsAuthorSettingsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('author_settings');

        $this->belongsTo('Authors', [
            'foreignKey' => 'author_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->scalar('locale')
            ->maxLength('locale', 14)
            ->notEmptyString('locale');

        $validator
            ->scalar('setting_name')
            ->maxLength('setting_name', 255)
            ->requirePresence('setting_name', 'create')
            ->notEmptyString('setting_name');

        $validator
            ->scalar('setting_value')
            ->allowEmptyString('setting_value');

        $validator
            ->scalar('setting_type')
            ->maxLength('setting_type', 6)
            ->allowEmptyString('setting_type');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['author_id'], 'Authors'), ['errorField' => 'author_id']);

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName(): string
    {
        return 'ojsdb';
    }
}
