<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Proveniences Model
 *
 * @property \App\Model\Table\RegionsTable&\Cake\ORM\Association\BelongsTo $Regions
 * @property \App\Model\Table\ArchivesTable&\Cake\ORM\Association\HasMany $Archives
 * @property \App\Model\Table\ArtifactsTable&\Cake\ORM\Association\HasMany $Artifacts
 * @property \App\Model\Table\DynastiesTable&\Cake\ORM\Association\HasMany $Dynasties
 * @property \App\Model\Table\SignReadingsTable&\Cake\ORM\Association\HasMany $SignReadings
 *
 * @method \App\Model\Entity\Provenience newEmptyEntity()
 * @method \App\Model\Entity\Provenience newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Provenience[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Provenience get($primaryKey, $options = [])
 * @method \App\Model\Entity\Provenience findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Provenience patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Provenience[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Provenience|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Provenience saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Provenience[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Provenience[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Provenience[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Provenience[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class ProveniencesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('proveniences');
        $this->setDisplayField('provenience');
        $this->setPrimaryKey('id');

        $this->belongsTo('Regions', [
            'foreignKey' => 'region_id'
        ]);
        $this->hasMany('Archives', [
            'foreignKey' => 'provenience_id'
        ]);
        $this->hasMany('Artifacts', [
            'foreignKey' => 'provenience_id'
        ]);
        $this->hasMany('Dynasties', [
            'foreignKey' => 'provenience_id'
        ]);
        $this->hasMany('SignReadings', [
            'foreignKey' => 'provenience_id'
        ]);
        $this->belongsToMany('Publications', [
            'foreignKey' => 'entity_id',
            'targetForeignKey' => 'publication_id',
            'joinTable' => 'entities_publications',
            'conditions' => 'table_name = "proveniences"',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('provenience')
            ->maxLength('provenience', 300)
            ->allowEmptyString('provenience');

        $validator
            ->scalar('geo_coordinates')
            ->maxLength('geo_coordinates', 50)
            ->allowEmptyString('geo_coordinates');

        $validator
            ->scalar('anc_name')
            ->maxLength('anc_name', 255)
            ->allowEmptyString('anc_name');

        $validator
            ->scalar('transc_name')
            ->maxLength('transc_name', 255)
            ->allowEmptyString('transc_name');

        $validator
            ->integer('pleiades_id')
            ->maxLength('pleiades_id', 11)
            ->allowEmptyString('pleiades_id');

        $validator
            ->scalar('site_id')
            ->maxLength('site_id', 3)
            ->allowEmptyString('site_id');

        $validator
            ->scalar('ara_name')
            ->maxLength('ara_name', 200)
            ->allowEmptyString('ara_name');

        $validator
            ->scalar('arm_name')
            ->maxLength('arm_name', 200)
            ->allowEmptyString('arm_name');

        $validator
            ->scalar('fas_name')
            ->maxLength('fas_name', 200)
            ->allowEmptyString('fas_name');

        $validator
            ->scalar('geo_name')
            ->maxLength('geo_name', 200)
            ->allowEmptyString('geo_name');

        $validator
            ->scalar('gre_name')
            ->maxLength('gre_name', 200)
            ->allowEmptyString('gre_name');

        $validator
            ->scalar('heb_name')
            ->maxLength('heb_name', 200)
            ->allowEmptyString('heb_name');

        $validator
            ->scalar('rus_name')
            ->maxLength('rus_name', 200)
            ->allowEmptyString('rus_name');

        $validator
            ->scalar('osm_id')
            ->maxLength('osm_id', 10)
            ->allowEmptyString('osm_id');

        $validator
            ->scalar('osm_type')
            ->allowEmptyString('osm_type');

        $validator
            ->integer('geonames_id')
            ->maxLength('geonames_id', 11)
            ->allowEmptyString('geonames_id');

        $validator
            ->scalar('lon_wgs1984')
            ->maxLength('lon_wgs1984', 200)
            ->allowEmptyString('lon_wgs1984');

        $validator
            ->scalar('lat_wgs1984')
            ->maxLength('lat_wgs1984', 200)
            ->allowEmptyString('lat_wgs1984');

        $validator
            ->integer('accuracy')
            ->maxLength('accuracy', 11)
            ->allowEmptyString('accuracy');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['id']), ['errorField' => 'id']);
        $rules->add($rules->existsIn(['region_id'], 'Regions'), ['errorField' => 'region_id']);

        return $rules;
    }
}
