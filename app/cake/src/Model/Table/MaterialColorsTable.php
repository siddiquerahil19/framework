<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MaterialColors Model
 *
 * @property \App\Model\Table\ArtifactsTable&\Cake\ORM\Association\HasMany $Artifacts
 *
 * @method \App\Model\Entity\MaterialColor newEmptyEntity()
 * @method \App\Model\Entity\MaterialColor newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\MaterialColor[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\MaterialColor get($primaryKey, $options = [])
 * @method \App\Model\Entity\MaterialColor findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\MaterialColor patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\MaterialColor[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\MaterialColor|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MaterialColor saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MaterialColor[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\MaterialColor[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\MaterialColor[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\MaterialColor[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class MaterialColorsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('material_colors');
        $this->setDisplayField('material_color');
        $this->setPrimaryKey('id');

        $this->belongsToMany('Artifacts', [
            'foreignKey' => 'material_color_id',
            'targetForeignKey' => 'artifact_id',
            'joinTable' => 'artifacts_materials'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('material_color')
            ->maxLength('material_color', 45)
            ->allowEmptyString('material_color');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['id']), ['errorField' => 'id']);

        return $rules;
    }
}
