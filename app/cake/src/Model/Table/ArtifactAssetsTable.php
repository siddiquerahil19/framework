<?php

declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\ArtifactAsset;
use Cake\I18n\FrozenTime;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ArtifactAssets Model
 *
 * @property \App\Model\Table\ArtifactsTable&\Cake\ORM\Association\BelongsTo $Artifacts
 *
 * @method \App\Model\Entity\ArtifactAsset newEmptyEntity()
 * @method \App\Model\Entity\ArtifactAsset newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactAsset[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactAsset get($primaryKey, $options = [])
 * @method \App\Model\Entity\ArtifactAsset findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\ArtifactAsset patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactAsset[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactAsset|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ArtifactAsset saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ArtifactAsset[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ArtifactAsset[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\ArtifactAsset[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ArtifactAsset[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ArtifactAssetsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('artifact_assets');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Artifacts', [
            'foreignKey' => 'artifact_id'
        ]);
        $this->hasMany('Annotations', [
            'className' => 'ArtifactAssetAnnotations',
            'foreignKey' => 'artifact_asset_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('artifact_aspect')
            ->maxLength('artifact_aspect', 200)
            ->allowEmptyString('artifact_aspect');

        $validator
            ->boolean('is_public')
            ->notEmptyString('is_public');

        $validator
            ->scalar('asset_type')
            ->requirePresence('asset_type', 'create')
            ->notEmptyString('asset_type');

        $validator
            ->scalar('file_format')
            ->maxLength('file_format', 10)
            ->requirePresence('file_format', 'create')
            ->notEmptyFile('file_format');

        $validator
            ->nonNegativeInteger('file_size')
            ->requirePresence('file_size', 'create')
            ->notEmptyFile('file_size');

        $validator
            ->integer('image_width')
            ->allowEmptyFile('image_width');

        $validator
            ->integer('image_height')
            ->allowEmptyFile('image_height');

        $validator
            ->numeric('image_scale')
            ->allowEmptyFile('image_scale');

        $validator
            ->scalar('image_color_average')
            ->maxLength('image_color_average', 20)
            ->allowEmptyFile('image_color_average');

        $validator
            ->integer('image_color_depth')
            ->allowEmptyFile('image_color_depth');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['artifact_id'], 'Artifacts'), ['errorField' => 'artifact_id']);

        return $rules;
    }

    public function findPath(Query $query, array $options)
    {
        $conditions = ArtifactAsset::parsePath(substr($options[0], 1), false);
        if (is_null($conditions)) {
            return $query->where(['artifact_id IS' => null]);
        } else {
            return $query->where($conditions);
        }
    }

    public function newEntityFromFile(string $path)
    {
        $asset = $this->newEmptyEntity();

        $pathPrefix = WWW_ROOT . 'dl' . DS;
        if (!str_starts_with($path, $pathPrefix)) {
            $asset->setError('id', "Artifact assets should be in the $pathPrefix directory");
            return $asset;
        } elseif (!file_exists($path)) {
            $asset->setError('id', "Artifact assets should exist");
            return $asset;
        }

        $parsedPath = ArtifactAsset::parsePath(substr($path, strlen(WWW_ROOT)));
        if (!$parsedPath) {
            $path = substr($path, strlen(WWW_ROOT));
            $asset->setError('artifact_id', "File name $path does not follow expected format");
            return $asset;
        }
        $this->patchEntity($asset, $parsedPath);

        $asset->is_public = 1;
        $asset->file_size = $this->getAssetSize(ArtifactAsset::getEncompassingPath($path));

        $asset->created = new FrozenTime(filectime($path));
        $asset->modified = new FrozenTime(filemtime($path));

        // Additional info for jpg images
        if ($asset->file_format === 'jpg') {
            // width, height, color depth
            $imageInfo = getimagesize($path);
            $asset->image_width = $imageInfo[0];
            $asset->image_height = $imageInfo[1];
            $asset->image_color_depth = $imageInfo['bits'];

            // average color
            $image = imagecreatefromjpeg($path);
            $scaledImage = imagecreatetruecolor(1, 1);
            imagecopyresampled($scaledImage, $image, 0, 0, 0, 0, 1, 1, $asset->image_width, $asset->image_height);
            $color = imagecolorsforindex($scaledImage, imagecolorat($scaledImage, 0, 0));
            $asset->image_color_average = sprintf('rgb(%d, %d, %d)', $color['red'], $color['green'], $color['blue']);

            // creation time from exif
            $tags = exif_read_data($path, null, true);
            if (array_key_exists('IFD0', $tags) && array_key_exists('DateTime', $tags['IFD0'])) {
                $asset->created = new FrozenTime($tags['IFD0']['DateTime']);
            }
        }

        return $asset;
    }

    private function getAssetSize($path)
    {
        if (is_dir($path)) {
            $size = 0;
            foreach (array_diff(scandir($path), ['.', '..']) as $child) {
                $size += $this->getAssetSize($path . DS . $child);
            }
            return $size;
        } else {
            return filesize($path);
        }
    }
}
