<?php

declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AnnotationTypes Model
 *
 * @property \App\Model\Table\ImageAnnotationsTable&\Cake\ORM\Association\HasMany $ImageAnnotations
 *
 * @method \App\Model\Entity\AnnotationType newEmptyEntity()
 * @method \App\Model\Entity\AnnotationType newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\AnnotationType[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\AnnotationType get($primaryKey, $options = [])
 * @method \App\Model\Entity\AnnotationType findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\AnnotationType patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\AnnotationType[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\AnnotationType|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AnnotationType saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AnnotationType[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\AnnotationType[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\AnnotationType[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\AnnotationType[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class AnnotationTypesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('annotation_types');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('ImageAnnotations', [
            'foreignKey' => 'annotation_type_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('annotation_type')
            ->requirePresence('annotation_type', 'create')
            ->notEmptyString('annotation_type');

        return $validator;
    }
}
