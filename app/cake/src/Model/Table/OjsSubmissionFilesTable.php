<?php

declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OjsSubmissionFiles Model
 *
 * @property \App\Model\Table\SourceFilesTable&\Cake\ORM\Association\BelongsTo $SourceFiles
 * @property \App\Model\Table\OjsSubmissionsTable&\Cake\ORM\Association\BelongsTo $OjsSubmissions
 * @property \App\Model\Table\GenresTable&\Cake\ORM\Association\BelongsTo $Genres
 * @property \App\Model\Table\UploaderUsersTable&\Cake\ORM\Association\BelongsTo $UploaderUsers
 * @property \App\Model\Table\AssocsTable&\Cake\ORM\Association\BelongsTo $Assocs
 *
 * @method \App\Model\Entity\OjsSubmissionFile newEmptyEntity()
 * @method \App\Model\Entity\OjsSubmissionFile newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\OjsSubmissionFile[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OjsSubmissionFile get($primaryKey, $options = [])
 * @method \App\Model\Entity\OjsSubmissionFile findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\OjsSubmissionFile patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OjsSubmissionFile[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\OjsSubmissionFile|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OjsSubmissionFile saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OjsSubmissionFile[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\OjsSubmissionFile[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\OjsSubmissionFile[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\OjsSubmissionFile[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class OjsSubmissionFilesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('submission_files');
        $this->setDisplayField('file_id');
        $this->setPrimaryKey(['file_id', 'revision']);

        $this->belongsTo('SourceFiles', [
            'foreignKey' => 'source_file_id',
        ]);
        $this->belongsTo('OjsSubmissions', [
            'foreignKey' => 'submission_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Genres', [
            'foreignKey' => 'genre_id',
        ]);
        $this->belongsTo('UploaderUsers', [
            'foreignKey' => 'uploader_user_id',
        ]);
        $this->belongsTo('Assocs', [
            'foreignKey' => 'assoc_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('revision', null, 'create');

        $validator
            ->allowEmptyString('source_revision');

        $validator
            ->scalar('file_type')
            ->maxLength('file_type', 255)
            ->requirePresence('file_type', 'create')
            ->notEmptyFile('file_type');

        $validator
            ->requirePresence('file_size', 'create')
            ->notEmptyFile('file_size');

        $validator
            ->scalar('original_file_name')
            ->maxLength('original_file_name', 127)
            ->allowEmptyFile('original_file_name');

        $validator
            ->requirePresence('file_stage', 'create')
            ->notEmptyFile('file_stage');

        $validator
            ->scalar('direct_sales_price')
            ->maxLength('direct_sales_price', 255)
            ->allowEmptyString('direct_sales_price');

        $validator
            ->scalar('sales_type')
            ->maxLength('sales_type', 255)
            ->allowEmptyString('sales_type');

        $validator
            ->allowEmptyString('viewable');

        $validator
            ->dateTime('date_uploaded')
            ->requirePresence('date_uploaded', 'create')
            ->notEmptyDateTime('date_uploaded');

        $validator
            ->dateTime('date_modified')
            ->requirePresence('date_modified', 'create')
            ->notEmptyDateTime('date_modified');

        $validator
            ->allowEmptyString('assoc_type');

        return $validator;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName(): string
    {
        return 'ojsdb';
    }
}
