<?php

declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OjsReviewRoundFiles Model
 *
 * @property \App\Model\Table\OjsSubmissionsTable&\Cake\ORM\Association\BelongsTo $OjsSubmissions
 * @property \App\Model\Table\OjsReviewRoundsTable&\Cake\ORM\Association\BelongsTo $OjsReviewRounds
 * @property \App\Model\Table\StagesTable&\Cake\ORM\Association\BelongsTo $Stages
 * @property \App\Model\Table\OjsSubmissionFilesTable&\Cake\ORM\Association\BelongsTo $OjsSubmissionFiles
 *
 * @method \App\Model\Entity\OjsReviewRoundFile newEmptyEntity()
 * @method \App\Model\Entity\OjsReviewRoundFile newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\OjsReviewRoundFile[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OjsReviewRoundFile get($primaryKey, $options = [])
 * @method \App\Model\Entity\OjsReviewRoundFile findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\OjsReviewRoundFile patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OjsReviewRoundFile[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\OjsReviewRoundFile|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OjsReviewRoundFile saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OjsReviewRoundFile[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\OjsReviewRoundFile[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\OjsReviewRoundFile[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\OjsReviewRoundFile[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class OjsReviewRoundFilesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('review_round_files');

        $this->belongsTo('OjsSubmissions', [
            'foreignKey' => 'submission_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('OjsReviewRounds', [
            'foreignKey' => 'review_round_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Stages', [
            'foreignKey' => 'stage_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('OjsSubmissionFiles', [
            'foreignKey' => 'file_id',
            'bindingKey' => 'file_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->notEmptyString('revision');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['submission_id'], 'OjsSubmission'), ['errorField' => 'submission_id']);
        $rules->add($rules->existsIn(['review_round_id'], 'OjsReviewRounds'), ['errorField' => 'review_round_id']);
        $rules->add($rules->existsIn(['stage_id'], 'Stages'), ['errorField' => 'stage_id']);
        $rules->add($rules->existsIn(['file_id'], 'Files'), ['errorField' => 'file_id']);

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName(): string
    {
        return 'ojsdb';
    }
}
