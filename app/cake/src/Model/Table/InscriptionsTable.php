<?php

namespace App\Model\Table;

use App\Model\Entity\Inscription;
use App\Utility\CdliProcessor\Convert;
use App\Utility\CdliProcessor\Parse;
use App\Utility\CdliProcessor\TokenList;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Inscriptions Model
 *
 * @property \App\Model\Table\ArtifactsTable&\Cake\ORM\Association\BelongsTo $Artifacts
 * @property \App\Model\Table\UpdateEventsTable&\Cake\ORM\Association\BelongsTo $UpdateEvents
 *
 * @method \App\Model\Entity\Inscription newEmptyEntity()
 * @method \App\Model\Entity\Inscription newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Inscription[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Inscription get($primaryKey, $options = [])
 * @method \App\Model\Entity\Inscription findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Inscription patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Inscription[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Inscription|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Inscription saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Inscription[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Inscription[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Inscription[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Inscription[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class InscriptionsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('inscriptions');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Artifacts', [
            'foreignKey' => 'artifact_id'
        ]);
        $this->belongsTo('UpdateEvents', [
            'foreignKey' => 'update_event_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('atf')
            ->maxLength('atf', 4294967295)
            ->allowEmptyString('atf');

        $validator
            ->scalar('jtf')
            ->maxLength('jtf', 4294967295)
            ->allowEmptyString('jtf');

        $validator
            ->scalar('transliteration')
            ->maxLength('transliteration', 4294967295)
            ->allowEmptyString('transliteration');

        $validator
            ->scalar('transliteration_clean')
            ->maxLength('transliteration_clean', 4294967295)
            ->allowEmptyString('transliteration_clean');

        $validator
            ->scalar('transliteration_sign_names')
            ->maxLength('transliteration_sign_names', 4294967295)
            ->allowEmptyString('transliteration_sign_names');

        $validator
            ->scalar('transliteration_for_search')
            ->maxLength('transliteration_for_search', 4294967295)
            ->allowEmptyString('transliteration_for_search');

        $validator
            ->scalar('annotation')
            ->maxLength('annotation', 4294967295)
            ->allowEmptyString('annotation');

        $validator
            ->boolean('is_atf2conll_diff_resolved')
            ->allowEmptyString('is_atf2conll_diff_resolved');

        $validator
            ->scalar('comments')
            ->allowEmptyString('comments');

        $validator
            ->scalar('structure')
            ->allowEmptyString('structure');

        $validator
            ->scalar('translation')
            ->allowEmptyString('translation');

        $validator
            ->scalar('transcription')
            ->allowEmptyString('transcription');

        $validator
            ->scalar('inscription_comments')
            ->allowEmptyString('inscription_comments');

        $validator
            ->boolean('is_latest')
            ->notEmptyString('is_latest');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['id']), ['errorField' => 'id']);
        $rules->add($rules->existsIn(['artifact_id'], 'Artifacts'), ['errorField' => 'artifact_id']);
        $rules->add($rules->existsIn(['update_event_id'], 'UpdateEvents'), ['errorField' => 'update_event_id']);

        return $rules;
    }

    public function newEntityFromInput(string $input, string $inputField): Inscription
    {
        if ($inputField === 'atf') {
            preg_match('/^&P0*(\d+)/', $input, $matches);
        } elseif ($inputField === 'annotation') {
            preg_match('/^#new_text=P0*(\d+)/', $input, $matches);
        }

        if (array_key_exists(0, $matches)) {
            $artifactId = $matches[1];
        } else {
            throw new \Exception('Artifact could not be determined from input');
        }

        $artifact = $this->Artifacts->get($artifactId, ['contain' => ['Inscriptions']]);
        if (empty($artifact->inscription)) {
            $inscription = ['artifact_id' => $artifact->id];
        } else {
            $inscription = $artifact->inscription->toArray();
            unset($inscription['id']);
            unset($inscription['update_event_id']);
        }

        $inscription = $this->newEntity($inscription);
        $inscription->is_latest = false;
        $inscription->set($inputField, $input);

        if ($inputField === 'atf') {
            $parsed = Convert::cAtfToJtf($input);
            if (!empty($parsed->errors)) {
                foreach ($parsed->errors as $error) {
                    if (property_exists($error, 'text')) {
                        $inscription->setError('atf', $error->text);
                    } else {
                        $text = sprintf('Parsing failed on line %d near character %d', $error->line, $error->column);
                        $inscription->setError('atf', $text);
                    }
                }
            }

            // Set JTF
            if (!empty($parsed->objects)) {
                $inscription->jtf = json_encode($parsed->objects);
            }

            // Update annotations
            if (!empty($inscription->annotation) && empty($parsed->errors)) {
                $atfConll = Convert::cAtfToCconll($input);
                $inscription->annotation = Convert::mergeCConll($atfConll, $inscription->annotation);
                $inscription->is_atf2conll_diff_resolved = false;
            }

            // Compare to token lists
            if (!is_null($artifact->period_id)) {
                $inscription->setTokenWarnings($artifact->period_id);
            }

            // Extract parts of ATF
            $inscription->structure = Parse::getStructure($input);
            $inscription->translation = Parse::getTranslations($input);
            $inscription->transcription = Parse::getTranscriptions($input);
            $inscription->transliteration_clean = Parse::getTransliterationsClean($input);
            $inscription->comments = Parse::getComments($input);
            $inscription->transliteration = Parse::getTransliterations($input);
            $inscription->transliteration_sign_names = Parse::getJtfSignTokens($input);
        } elseif ($inputField === 'annotation') {
            $inscription->setAnnotationWarnings();
            $inscription->is_atf2conll_diff_resolved = true;
        }

        return $inscription;
    }

    public function getPreviousInscriptions(Inscription $inscription)
    {
        if ($inscription->update_event->status == 'approved') {
            return $this->find()
                ->contain('UpdateEvents')
                ->where([
                    'Inscriptions.artifact_id' => $inscription->artifact_id,
                    'UpdateEvents.approved <' => $inscription->update_event->approved
                ])
                ->order(['UpdateEvents.approved' => 'DESC'])
                ->first();
        } else {
            return $this->find()
                ->where(['artifact_id' => $inscription->artifact_id, 'is_latest' => 1])
                ->first();
        }
    }
}
