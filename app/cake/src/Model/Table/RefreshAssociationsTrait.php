<?php

namespace App\Model\Table;

use Cake\ORM\Entity;

// See https://gist.github.com/berarma/5ea4ca9ff909df0a1017ef73250ef647
trait RefreshAssociationsTrait
{
    public function refreshAssociations(Entity $entity, array $associations)
    {
        foreach ($associations as $table => $options) {
            if (is_int($table)) {
                $table = $options;
                $options = [];
            } else {
                if (strpos($table, '.') !== false) {
                    list($table, $secondary) = explode('.', $table, 2);
                    if (empty($options)) {
                        $options = [$secondary];
                    } else {
                        $options = [$secondary => $options];
                    }
                }
            }

            $association = $this->getAssociation($table);
            $type = $association->type();
            $foreignKey = $association->getForeignKey();
            $property = $association->getProperty();
            $values = $entity->get($property);
            $target = $association->getTarget();

            if (in_array($type, [$association::ONE_TO_ONE, $association::MANY_TO_ONE])) {
                if (!$entity->isDirty($foreignKey)) {
                    continue;
                }
                $foreignId = $entity->get($foreignKey);
                if ($foreignId === null) {
                    $value = null;
                } else {
                    $value = $target->get($foreignId, ['contain' => $options]);
                }
                $entity->set($property, $value);
            } elseif (!empty($values) && !empty($options)) {
                $target->loadInto($values, $options);
            }
        }
    }
}
