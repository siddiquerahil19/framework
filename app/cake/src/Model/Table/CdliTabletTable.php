<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CdliTablet Model
 *
 * @method \App\Model\Entity\CdliTablet newEmptyEntity()
 * @method \App\Model\Entity\CdliTablet newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\CdliTablet[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CdliTablet get($primaryKey, $options = [])
 * @method \App\Model\Entity\CdliTablet findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\CdliTablet patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CdliTablet[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\CdliTablet|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CdliTablet saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CdliTablet[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\CdliTablet[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\CdliTablet[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\CdliTablet[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class CdliTabletTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('cdli_tablet');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->date('date')
            ->allowEmptyDate('date');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->allowEmptyString('name');

        $validator
            ->scalar('path')
            ->maxLength('path', 255)
            ->allowEmptyString('path');

        $validator
            ->scalar('theme')
            ->maxLength('theme', 70)
            ->allowEmptyString('theme');

        $validator
            ->scalar('title')
            ->maxLength('title', 70)
            ->allowEmptyString('title');

        $validator
            ->scalar('blurb')
            ->maxLength('blurb', 7500)
            ->allowEmptyString('blurb');

        $validator
            ->scalar('full_info')
            ->maxLength('full_info', 7500)
            ->allowEmptyString('full_info');

        return $validator;
    }
}
