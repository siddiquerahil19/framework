<?php

declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ChemicalData Model
 *
 * @property \App\Model\Table\ArtifactsTable&\Cake\ORM\Association\BelongsTo $Artifacts
 * @property \App\Model\Table\CalibrationTable&\Cake\ORM\Association\BelongsTo $Calibration
 *
 * @method \App\Model\Entity\ChemicalData newEmptyEntity()
 * @method \App\Model\Entity\ChemicalData newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\ChemicalData[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ChemicalData get($primaryKey, $options = [])
 * @method \App\Model\Entity\ChemicalData findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\ChemicalData patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ChemicalData[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\ChemicalData|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ChemicalData saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ChemicalData[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ChemicalData[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\ChemicalData[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ChemicalData[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class ChemicalDataTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('chemical_data');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Artifacts', [
            'foreignKey' => 'artifact_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Calibration', [
            'foreignKey' => 'calibration_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('chemistry')
            ->maxLength('chemistry', 4294967295)
            ->requirePresence('chemistry', 'create')
            ->notEmptyString('chemistry');

        $validator
            ->scalar('area')
            ->requirePresence('area', 'create')
            ->notEmptyString('area');

        $validator
            ->scalar('area_description')
            ->allowEmptyString('area_description');

        $validator
            ->scalar('reading')
            ->maxLength('reading', 10)
            ->requirePresence('reading', 'create')
            ->notEmptyString('reading');

        $validator
            ->scalar('time')
            ->maxLength('time', 20)
            ->requirePresence('time', 'create')
            ->notEmptyString('time');

        $validator
            ->scalar('duration')
            ->maxLength('duration', 10)
            ->requirePresence('duration', 'create')
            ->notEmptyString('duration');

        $validator
            ->scalar('date')
            ->maxLength('date', 20)
            ->requirePresence('date', 'create')
            ->notEmptyString('date');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['artifact_id'], 'Artifacts'), ['errorField' => 'artifact_id']);
        $rules->add($rules->existsIn(['calibration_id'], 'Calibration'), ['errorField' => 'calibration_id']);

        return $rules;
    }
}
