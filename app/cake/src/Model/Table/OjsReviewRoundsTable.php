<?php

declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OjsReviewRounds Model
 *
 * @property \App\Model\Table\OjsSubmissionsTable&\Cake\ORM\Association\BelongsTo $OjsSubmissions
 * @property \App\Model\Table\StagesTable&\Cake\ORM\Association\BelongsTo $Stages
 * @property \App\Model\Table\OjsReviewAssignmentsTable&\Cake\ORM\Association\HasMany $OjsReviewAssignments
 *
 * @method \App\Model\Entity\OjsReviewRound newEmptyEntity()
 * @method \App\Model\Entity\OjsReviewRound newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\OjsReviewRound[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OjsReviewRound get($primaryKey, $options = [])
 * @method \App\Model\Entity\OjsReviewRound findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\OjsReviewRound patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OjsReviewRound[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\OjsReviewRound|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OjsReviewRound saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OjsReviewRound[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\OjsReviewRound[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\OjsReviewRound[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\OjsReviewRound[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class OjsReviewRoundsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('review_rounds');
        $this->setDisplayField('review_round_id');
        $this->setPrimaryKey('review_round_id');

        $this->belongsTo('OjsSubmissions', [
            'foreignKey' => 'submission_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Stages', [
            'foreignKey' => 'stage_id',
        ]);
        $this->hasMany('OjsReviewAssignments');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('review_round_id', null, 'create');

        $validator
            ->requirePresence('round', 'create')
            ->notEmptyString('round');

        $validator
            ->allowEmptyString('review_revision');

        $validator
            ->allowEmptyString('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['submission_id'], 'OjsSubmission'), ['errorField' => 'submission_id']);
        $rules->add($rules->existsIn(['stage_id'], 'Stages'), ['errorField' => 'stage_id']);

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName(): string
    {
        return 'ojsdb';
    }
}
