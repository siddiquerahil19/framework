<?php

declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OjsSubmissions Model
 *
 * @property \App\Model\Table\OjsSectionsTable&\Cake\ORM\Association\BelongsTo $OjsSections
 * @property \App\Model\Table\OjsPublicationsTable&\Cake\ORM\Association\BelongsTo $OjsPublications
 * @property \App\Model\Table\OjsReviewAssignmentsTable&\Cake\ORM\Association\HasMany $OjsReviewAssignments
 * @property \App\Model\Table\QueriesTable&\Cake\ORM\Association\HasMany $Queries
 * @property \App\Model\Table\OjsPublicationSettingsTable&\Cake\ORM\Association\HasMany $OjsPublicationSettings
 *
 * @method \App\Model\Entity\OjsSubmission newEmptyEntity()
 * @method \App\Model\Entity\OjsSubmission newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\OjsSubmission[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OjsSubmission get($primaryKey, $options = [])
 * @method \App\Model\Entity\OjsSubmission findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\OjsSubmission patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OjsSubmission[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\OjsSubmission|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OjsSubmission saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OjsSubmission[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\OjsSubmission[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\OjsSubmission[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\OjsSubmission[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class OjsSubmissionsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('submissions');
        $this->setDisplayField('submission_id');
        $this->setPrimaryKey('submission_id');
        $this->belongsTo('OjsSections', [
            'foreignKey' => 'section_id',
        ]);
        $this->hasMany('OjsReviewAssignments');
        $this->hasMany('Queries', [
            'foreignKey' => 'assoc_id',
            'bindingKey' => 'submission_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('OjsPublicationSettings', [
            'foreignKey' => 'publication_id',
            'bindingKey' => 'current_publication_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('OjsPublications');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('submission_id', null, 'create');

        $validator
            ->scalar('locale')
            ->maxLength('locale', 14)
            ->allowEmptyString('locale');

        $validator
            ->dateTime('date_last_activity')
            ->allowEmptyDateTime('date_last_activity');

        $validator
            ->dateTime('date_submitted')
            ->allowEmptyDateTime('date_submitted');

        $validator
            ->dateTime('last_modified')
            ->allowEmptyDateTime('last_modified');

        $validator
            ->notEmptyString('status');

        $validator
            ->notEmptyString('submission_progress');

        $validator
            ->allowEmptyString('work_type');

        return $validator;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName(): string
    {
        return 'ojsdb';
    }
}
