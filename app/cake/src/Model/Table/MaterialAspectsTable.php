<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MaterialAspects Model
 *
 * @property \App\Model\Table\ArtifactsTable&\Cake\ORM\Association\HasMany $Artifacts
 *
 * @method \App\Model\Entity\MaterialAspect newEmptyEntity()
 * @method \App\Model\Entity\MaterialAspect newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\MaterialAspect[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\MaterialAspect get($primaryKey, $options = [])
 * @method \App\Model\Entity\MaterialAspect findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\MaterialAspect patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\MaterialAspect[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\MaterialAspect|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MaterialAspect saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MaterialAspect[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\MaterialAspect[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\MaterialAspect[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\MaterialAspect[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class MaterialAspectsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('material_aspects');
        $this->setDisplayField('material_aspect');
        $this->setPrimaryKey('id');

        $this->belongsToMany('Artifacts', [
            'foreignKey' => 'material_aspect_id',
            'targetForeignKey' => 'artifact_id',
            'joinTable' => 'artifacts_materials'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('material_aspect')
            ->maxLength('material_aspect', 45)
            ->allowEmptyString('material_aspect');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['id']), ['errorField' => 'id']);

        return $rules;
    }
}
