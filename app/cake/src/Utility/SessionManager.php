<?php

namespace App\Utility;

use App\Model\Entity\User;

class SessionManager
{
    private static $sessionPath = 'tmp/sessions/';

    public static function updateUserSessions(User $user)
    {
        foreach (self::listSessions() as $id) {
            $session = self::getSession($id);

            if (array_key_exists('Auth', $session) && array_key_exists('User', $session['Auth'])) {
                if ($session['Auth']['User']['username'] === $user->username) {
                    $session['Auth']['User']['author_id'] = $user->author_id;
                    $session['Auth']['User']['active'] = $user->active;
                    $session['Auth']['User']['hd_images_collection_id'] = $user->hd_images_collection_id;
                    $session['Auth']['User']['roles'] = array_column($user->roles, 'id');

                    self::writeSession($id, $session);
                }
            }
        }
    }

    private static function listSessions()
    {
        return array_diff(scandir(self::$sessionPath), ['..', '.']);
    }

    private static function getSession(string $id)
    {
        return self::unserializeSession(file_get_contents(self::$sessionPath . $id));
    }

    private static function unserializeSession(string $session)
    {
        $data = [];
        $index = 0;

        while ($nameEnd = strpos($session, '|', $index)) {
            $name = substr($session, $index, $nameEnd - $index);
            $rest = substr($session, $nameEnd + 1);

            $value = unserialize($rest);
            $data[$name] = $value;

            $index = $nameEnd + 1 + strlen(serialize($value));
        }

        return $data;
    }

    private static function writeSession(string $id, array $data)
    {
        file_put_contents(self::$sessionPath . $id, self::serializeSession($data), LOCK_EX);
    }

    private static function serializeSession(array $data)
    {
        $output = '';
        foreach ($data as $key => $value) {
            $output .= $key . '|' . serialize($value);
        }
        return $output;
    }
}
