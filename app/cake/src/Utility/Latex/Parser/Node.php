<?php

namespace App\Utility\Latex\Parser;

use PhpLatex_Node;

class Node extends PhpLatex_Node
{
    public static function fromPhpLatexNode($node)
    {
        $new = new Node($node->_type);
        if (!empty($node->_props)) {
            $new->setProps(array_slice($node->_props, 0));
        }
        $new->_children = array_slice($node->_children, 0);
        return $new;
    }

    public function removeArguments($numArgs)
    {
        array_splice($this->_children, 0, $numArgs);
        return $this;
    }

    public function removeChild($child)
    {
        $index = array_search($child, $this->_children);
        array_splice($this->_children, $index, 1);
        return $this;
    }

    public function prependChild($node)
    {
        array_unshift($this->_children, $node);
        return $this;
    }

    public function setChildren($children)
    {
        $this->_children = $children;
    }
}
