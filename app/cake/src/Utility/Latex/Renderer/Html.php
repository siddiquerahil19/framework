<?php

namespace App\Utility\Latex\Renderer;

use App\Utility\Latex\Parser;
use App\Utility\Latex\Parser\Node;
use PhpLatex_Node;
use PhpLatex_Renderer_Html;
use PhpLatex_Renderer_Typestyle;

class Html extends PhpLatex_Renderer_Html
{
    protected $_data;

    protected $_switchCommands = [
        // Style
        '\\it' => '\\textit',
        '\\itshape' => '\\textit',
        '\\sl' => '\\textsl',
        '\\slshape' => '\\textsl',

        // Flags
        '\\bf' => '\\textbf',
        '\\bfseries' => '\\textbf',
        '\\em' => '\\emph',
        '\\sc' => '\\textsc',
        '\\scshape' => '\\textsc',

        // Font selection
        '\\rm' => '\\textrm',
        '\\sf' => '\\textsf',
        '\\tt' => '\\texttt'
    ];

    protected $_environRefLabels = [
        'figure' => 'Figure',
        'lstlisting' => 'Listing',
        'table' => 'Table'
    ];

    public function __construct(array $data)
    {
        $this->_data = $data;
        $citations = $data['citations'];
        $footnotes = array_values($data['footnotes']);
        $labels = $data['labels'];
        $images = array_map(function ($index, $image) {
            return [$index, $image];
        }, array_keys($data['images']), $data['images']);
        $images = array_merge($images, $images);

        // Text styling
        $this->addCommandRenderer('\\subscript', function ($node) {
            return '<sub>' . $this->_renderNodeChildren($node) . '</sub>';
        });
        $this->addCommandRenderer('\\superscript', function ($node) {
            return '<sup>' . $this->_renderNodeChildren($node) . '</sup>';
        });

        // Special characters
        $this->addCommandRenderer('\\everymodeprime', function () {
            return '&prime;';
        });
        $this->addCommandRenderer('\\hith', function () {
            return 'h&#x32E;';
        });
        $this->addCommandRenderer('\\Hith', function () {
            return 'H&#x32E;';
        });
        $this->addCommandRenderer('\\textcorner', function () {
            return '&#x231D;';
        });
        $this->addCommandRenderer('\\textopencorner', function () {
            return '&#x231C;';
        });

        // Bibliography
        $this->addCommandRenderer('\\printbibliography', function () use ($citations) {
            if ($citations) {
                $entries = array_map(function ($entry) {
                    return $entry[1];
                }, $citations['bibliography']);
                return '<div class="csl-bibliography">' . implode("\n", $entries) . '</div>';
            } else {
                return '';
            }
        });
        $this->addCommandRenderer('\\nocite', function () {
            return '';
        });
        $this->addCommandRenderer('\\cite', function () use (&$citations) {
            if ($citations) {
                return substr(array_shift($citations['citation']), 1, -1);
            } else {
                return '';
            }
        });
        $this->addCommandRenderer('\\citep', function () use (&$citations) {
            if ($citations) {
                return array_shift($citations['citation']);
            } else {
                return '';
            }
        });
        $this->addCommandRenderer('\\parencite', function () use (&$citations) {
            if ($citations) {
                return array_shift($citations['citation']);
            } else {
                return '';
            }
        });

        // Footnotes
        $this->addCommandRenderer('\\footnote', function () use (&$footnotes) {
            $index = array_shift($footnotes)['index'];
            return '<sup><a href="#f' . $index . '" id="r' . $index . '">[' . $index . ']</a></sup>';
        });

        // Labels
        $this->addCommandRenderer('\\label', function ($node) {
            $label = $this->toLatex($node->getChildren()[0]->getChildren());
            return '<a name="' . $label . '"></a>';
        });
        $this->addCommandRenderer('\\Cref', function ($node) use ($labels) {
            $keys = $this->toLatex($node->getChildren()[0]->getChildren());
            $keys = explode(',', $keys);
            $refs = [];

            foreach ($keys as $key) {
                $text = $labels[$key]['index'];
                if (array_key_exists($labels[$key]['type'], $this->_environRefLabels)) {
                    $text = $this->_environRefLabels[$labels[$key]['type']] . ' ' . $text;
                }
                $refs[] = '<a href="' . $key . '">' . $text . '</a>';
            }

            return implode(', ', $refs);
        });

        // Figures
        $this->addCommandRenderer('\\caption', function ($node) {
            if ($node->getProp('environ') == 'longtable') {
                return '<caption align="bottom">' . $this->_renderNodeChildren($node) . '</caption>';
            } else {
                return '<figcaption>' . $this->_renderNodeChildren($node) . '</figcaption>';
            }
        });
        $this->addCommandRenderer('\\includegraphics', function ($node) use (&$images) {
            $image = array_shift($images); // [$index, ['url' => $url, 'caption' => $caption]]
            $caption = $image[1]['caption'];

            $fileExtension = pathinfo($image[1]['url'], PATHINFO_EXTENSION);
            $fileExtension = empty($fileExtension) ? '' : '.' . $fileExtension;
            $url = '/pubs/figs/%%ARTICLE_ID%%-' . str_pad($image[0] + 1, 3, '0', STR_PAD_LEFT) . $fileExtension;
            return '<img src="' . $url . '" alt="' . strip_tags($caption) . '"></img>';
        });
        $this->addCommandRenderer('\\captionsetup', function ($node) {
            return '';
        });

        // Other
        $this->addCommandRenderer('\\today', function () {
            return date("d.m.Y");
        });
        $this->addCommandRenderer('\\thispagestyle', function () {
            return '';
        });
        $this->addCommandRenderer('\\vspace', function () {
            return '';
        });
        $this->addCommandRenderer('\\hspace', function () {
            return '';
        });
    }

    protected function _renderEnvironComment($node)
    {
        return '';
    }

    protected function _renderEnvironDescription($node)
    {
        return parent::_renderEnvironList($node);
    }

    protected function _renderEnvironFigure($node)
    {
        $node = Node::fromPhpLatexNode($node);

        // Check for optional argument
        if ($node->getChildren()[0]->getChildren()[0]->getProp('value') == '[') {
            $node->removeArguments(2);
        }

        return $this->_renderFigure($node);
    }

    protected function _renderEnvironLstlisting($node)
    {
        [$params, $code] = Parser::parseListingEnviron($node->getChildren()[0]->value);
        $language = h($params['language']);
        $code = "<pre style=\"text-align: left;\"><code class=\"language-$language\">$code</code></pre>";
        $caption = h(substr($params['caption'], 1, -1));
        return "<figure>$code<figcaption>$caption</figcaption></figure>";
    }

    protected function _renderEnvironSubfigure($node)
    {
        $node = Node::fromPhpLatexNode($node);

        // Check for optional argument
        if ($node->getChildren()[0]->getChildren()[0]->getProp('value') == '[') {
            $node->removeArguments(2);
        }

        $node->removeArguments(1);

        return $this->_renderFigure($node);
    }

    protected function _renderEnvironFlushedleft($node)
    {
        return $this->_renderNodeChildren($node);
    }

    protected function _renderEnvironLongtable($node)
    {
        $node = Node::fromPhpLatexNode($node);

        // Check for optional argument
        if ($node->getChildren()[0]->getChildren()[0]->getProp('value') == '[') {
            $node->removeArguments(4);
        } else {
            $node->removeArguments(1);
        }

        $node->prependChild(new Node(Parser::TYPE_TEXT));

        $caption;
        foreach ($node->getChildren() as $child) {
            if ($child->getType() == Parser::TYPE_COMMAND && $child->getProp('value') == '\\caption') {
                $caption = $this->_renderNode($child);
                $node->removeChild($child);
            }
        }

        $table = $this->_renderEnvironTabular($node);

        if (isset($caption)) {
            $table = preg_replace('/<\/table>$/', $caption . '</table>', $table);
        }

        return $table;
    }

    protected function _renderEnvironMulticols($node)
    {
        $node = Node::fromPhpLatexNode($node);
        $node->removeArguments(1);
        return $this->_renderNodeChildren($node);
    }

    protected function _renderEnvironTabular($node)
    {
        $node = Node::fromPhpLatexNode($node);

        // Fix alignment parsing (i.e. parsing "p{3.0cm}" as "c" for "center")
        $alignment = $this->toLatex($node->getChildren()[0]->getChildren());
        $alignment = preg_replace('/p{.*?}/', 'p', $alignment);

        // Re-insert alignment params
        $params = new Node(Parser::TYPE_GROUP, ['mode' => 1]);
        $params->appendChild(new Node(Parser::TYPE_TEXT, ['value' => $alignment]));
        $node->removeArguments(1);
        $node->prependChild($params);

        return parent::_renderEnvironTabular($node);
    }

    protected function _renderFigure($node)
    {
        return '<figure>' . $this->_renderNodeChildren($node) . '</figure>';
    }

    protected function _renderNodeChildren($node)
    {
        $html = '';
        foreach ($node->getChildren() as $child) {
            $html .= $this->_renderNode($child, 0);
        }
        return $html;
    }

    protected function _collectFragments($node)
    {
        $node = Node::fromPhpLatexNode($node);
        $children = $node->getChildren();
        $fragments = [];
        $fragment = new PhpLatex_Node(Parser::TYPE_DOCUMENT);

        foreach ($children as $index => $child) {
            if ($child->getType() == Parser::TYPE_COMMAND) {
                $command = $child->getProp('value');

                if (array_key_exists($command, $this->_switchCommands)) {
                    $fragments[] = $fragment;
                    $fragment = new PhpLatex_Node(Parser::TYPE_COMMAND, [
                        'value' => $this->_switchCommands[$command],
                        'switchCommand' => true,
                        'fragmented' => true
                    ]);
                    continue;
                }
            }

            $fragment->appendChild($child);
        }

        $fragments[] = $fragment;
        array_splice($fragments, 0, 1, $fragments[0]->getChildren());
        $node->setChildren($fragments);

        return $node;
    }

    /**
     * @param PhpLatex_Node|string $document
     * @return mixed|string
     */
    public function render($document)
    {
        if (!$document instanceof PhpLatex_Node) {
            $document = $this->getParser()->parse($document);
        }

        $result = parent::render($this->_collectFragments($document));

        // Fix whitespace issues
        $result = preg_replace('/<(h1|h2|h3|h4|h5|h6|pre|ul|ol|table|figure)/i', '</p><\1', $result);
        $result = preg_replace('/<\/(h1|h2|h3|h4|h5|h6|pre|ul|ol|table|figure)>/i', '</\1><p>', $result);
        $result = preg_replace('/<br\/>(<br\/>)+/', '</p><p>', $result);
        $result = preg_replace('/(^|<p>)\s*($|<\/p>)/', '', $result);

        return $result;
    }

    /**
     * @param PhpLatex_Node|string $part
     * @return mixed|string
     */
    public function renderPart($document)
    {
        if (is_array($document)) {
            $children = $document;
            $document = new Node(Parser::TYPE_GROUP);
            foreach ($children as $child) {
                $document->appendChild($child);
            }
        }

        if (!$document instanceof PhpLatex_Node) {
            $document = $this->getParser()->parse($document);
        }

        return $this->_renderNode($document);
    }

    protected function _renderNode($node, $flags = 0)
    {
        if (!$node->getProp('fragmented')) {
            $node = $this->_collectFragments($node);
        }

        return parent::_renderNode($node, $flags);
    }
}
