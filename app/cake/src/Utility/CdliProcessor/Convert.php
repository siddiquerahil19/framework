<?php

namespace App\Utility\CdliProcessor;

use Cake\Http\Client;
use Cake\Http\Exception\HttpException;

class Convert
{
    public static function cAtfToJtf($text)
    {
        $http = new Client();
        $response = $http->post('http://jtf-lib:3003/jtf-lib/api/getJTF', json_encode(['atf' => $text]), ['type' => 'json']);

        if ($response->getStatusCode() >= 400) {
            $htmlError = $response->getStringBody();
            $textError = html_entity_decode(strip_tags($htmlError), ENT_QUOTES | ENT_HTML5, 'UTF-8');
            throw new HttpException($textError);
        } else {
            return json_decode($response->getStringBody());
        }
    }

    public static function cAtfToCconll($text)
    {
        // Check atf first, if fail return error [TODO]
        $http = new Client();
        $response = $http->post('http://node-tools:3032/convert/'. substr($text, 1, 7) . '.atf', $text, ['type' => 'text/plain']);
        return $response->getStringBody();
    }

    public static function cAtfToCConllMulti($texts)
    {
        // Check atf first, if fail return error [TODO]
        $result="";
        foreach (array_slice(preg_split("@(?=&)@", $texts), 1) as $text) {
            $result .= self::cAtfToCconll($text);
        }
        return $result;
    }

    public function cConllToConllU($inscription_object)
    {
        $http = new Client();
        $response = $http->post('http://node-tools:3002/format/u', json_encode($inscription_object), ['type' => 'json']);
        return $response->getStringBody();
    }

    public function cConllToConllUMulti($texts)
    {
        $result="";
        $split_texts = preg_split("@#new_text@", $texts['annotation']);
        foreach ($split_texts as $text['annotation']) {
            if ($text['annotation'] === '') {
                continue;
            }
            $text['annotation'] = '#new_text'.$text['annotation'];
            $result .= self::cConllToConllU($text);
        }
        return $result;
    }

    public function cAtfToOracc($text)
    {
        return $oracc;
    }


    public function oraccAtfTo2CAtf($text)
    {
        return $atf;
    }

    public function cConllToBrat($text)
    {
        return $brat;
    }

    public function cAtfToConcordance($text)
    {
        return $text;
    }

    public static function mergeCConll($a, $b)
    {
        return self::mergeConll($a, $b, /* FORM */ 1);
    }

    public static function mergeConll($a, $b, $alignCol)
    {
        $data = ['files' => [$a, $b], 'alignCol' => $alignCol];
        $http = new Client();
        $response = $http->post('http://node-tools:3005/merge', json_encode($data), ['type' => 'json']);
        return $response->getStringBody();
    }
}
