<?php

namespace App\Utility\CdliProcessor;

use Cake\Http\Client;
use Cake\Filesystem\File;

class Parse
{
    public static function parseATF($atf, $field)
    {
        $comments='';
        $structure='';
        $transcription = '';
        $translations = '';
        $transliteration = '';
        $transliteration_clean = '';
        $lines = preg_split("/\r\n|\r|\n/", $atf);
        foreach ($lines as $line) {
            if ($line == "" || substr($line, 0, 2) == ">>") {
                continue;
            } elseif (preg_match("/^@/", $line)) {
                $structure .=  substr($line, 1);
            } elseif (preg_match("/^(\$ )/", $line) || preg_match("/^(# )/", $line)) {
                $comments .=  substr($line, 2);
            } elseif (preg_match("/^(#tr)/", $line)) {
                $translations .=  substr($line, 4);
            } elseif (preg_match("/^[0-9]+/", $line)) {
                $transliteration_clean .=  substr($line, 2);
                $transliteration .=  substr($line, 2)."\n";
            } elseif (preg_match("/#ts/", $line)) {
                $transcription .= $line;
            }
        }

        if ($field == "structure") {
            return $structure;
        } elseif ($field == "comment") {
            return $comments;
        } elseif ($field == "transcription") {
            return $transcription;
        } elseif ($field == "translation") {
            return $translations;
        } elseif ($field == "transliteration") {
            return $transliteration;
        } elseif ($field == "transliteration_clean") {
            return $transliteration_clean;
        }
    }

    public static function getStructure($atf)
    {
        $structure = Parse::parseATF($atf, "structure");
        return $structure;
    }

    public static function getComments($atf)
    {
        $comments = Parse::parseATF($atf, "comment");
        return $comments;
    }

    public static function getTranslations($atf)
    {
        $translations = Parse::parseATF($atf, "translation");
        return $translations;
    }

    public static function getTransliterationsClean($atf)
    {
        $transliteration_clean = Parse::parseATF($atf, "transliteration_clean");
        return $transliteration_clean;
    }

    public static function getTransliterations($atf)
    {
        $transliteration = Parse::parseATF($atf, "transliteration");
        return $transliteration;
    }

    public static function getTranscriptions($atf)
    {
        $transcription = Parse::parseATF($atf, "transcription");
        return $transcription;
    }

    // getWordTokens(), getSignTokens() adapted from signfilter.pl
    // https://gitlab.com/cdli/framework/-/blob/develop/app/search/signfilter.pl
    public static function getWordTokens($text)
    {
        $words = [];
        $mathMode = false;

        foreach (preg_split('/[\r\n]+/', $text) as $line) {
            $line = trim($line, $characters = " \n\r\t\v\x00'\"");

            if ($line === '') {
                continue;
            }

            // Handle math mode
            if ($line === '#atf: use math') {
                $mathMode = true;
            } elseif ($line[0] === '&') {
                $mathMode = false;
            }

            // Remove non-transliteration lines
            // TODO when ATF is fixed, second path in this pattern
            if (!preg_match('/^([^\s#&$<>|]\S*\.|\d+\'?,)\s+/', $line)) {
                continue;
            }

            // Remove line numbers
            // TODO when ATF is fixed, remove comma in this pattern
            $line = preg_replace('/^\S+[.,] ?/', '', $line);
            // Remove ($ ... $) sequences
            $line = preg_replace('/\(\$.*?\$\)/', '', $line);
            // Remove <( ... )> sequences
            $line = preg_replace('/<\(.*?\)>/', '', $line);
            // Remove exact character sequences
            $line = preg_replace('/[#?*<>_[\]]/', '', $line);
            // Replace tabs (TODO remove when ATF is fixed)
            $line = preg_replace('/\t/', ' ', $line);
            // Unwrap (...) sequences
            // (be careful not to remove qualified signs and number signs)
            $line = preg_replace('/(^|[} -])\(([^()]*)\)(?=[{ -]|$)/', '$1$2', $line);

            foreach (preg_split('/[ \"]/', $line) as $word) {
                // Remove language switches, lexical & other symbols, "..."
                if (preg_match('/^(%\S+|[:*\/]|:[.:\'"]|[&,~|=^@]|\.{3})$/', $word)) {
                    continue;
                }

                // Process math mode
                if ($mathMode) {
                    array_push($words, ...self::_convertMathMode($word));
                    continue;
                }

                $words[] = $word;
            }
        }
        return array_filter($words);
    }

    private static function _convertMathMode($token_)
    {
        $token = preg_replace('/(?<=^|\.)(\d)([1-9])(?=\.|$)/', '${1}0.$2', $token_);
        return array_map(function ($token) {
            $token = preg_replace('/^(\d)((@v)?)$/', '$1(disz)$2', $token);
            $token = preg_replace('/^(\d)0$/', '$1(u)', $token);
            return $token;
        }, preg_split('/(?<=\d|\d@v)\./', $token));
    }

    public static function getSignTokens($atf)
    {
        $signs = [];

        foreach (self::getWordTokens($atf) as $word) {
            foreach (preg_split('/(-|\{\+?|\})/', $word) as $token) {
                if (preg_match('/^(|\.\.\.)$/', $token)) {
                    continue;
                }
                $signs[] = $token;
            }
        }

        return $signs;
    }

    public static function getJtfSignTokens($text)
    {
        $http = new Client();
        $errorText = '';
        $atf = ['atf' => $text];
        $response = $http->post(
            'http://jtf-lib:3003/jtf-lib/api/getSignnamesATF',
            json_encode($atf),
            ['type' => 'json']
        );
        if ($response->getStatusCode() >= 400) {
            $htmlError = $response->getStringBody();
            $textError = html_entity_decode(strip_tags($htmlError), ENT_QUOTES | ENT_HTML5, 'UTF-8');
            throw new HttpException($textError);
        }
        return $response->getStringBody();
    }

    public static function getJtf($text)
    {
        $http = new Client();
        $errorText = '';
        $response = $http->post('http://jtf-lib:3003/jtf-lib/api/getJTF', json_encode(['atf' => $text]), ['type' => 'json']);
        return $response;
    }
}
