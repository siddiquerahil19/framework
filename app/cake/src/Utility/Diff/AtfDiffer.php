<?php

namespace App\Utility\Diff;

use SebastianBergmann\Diff\Differ;

class AtfDiffer
{
    private $parent;

    /**
     * @param DiffOutputBuilderInterface $outputBuilder
     *
     * @throws InvalidArgumentException
     */
    public function __construct($builder)
    {
        $this->parent = new Differ($builder);
    }

    /**
     * Returns the diff between two arrays or strings as string.
     *
     * @param array|string $from
     * @param array|string $to
     */
    public function diff($from, $to, LongestCommonSubsequenceCalculator $lcs = null): string
    {
        return $this->parent->diff(
            $this->normalizeDiffInput($from),
            $this->normalizeDiffInput($to),
            $lcs
        );
    }


    public function normalizeDiffInput($input)
    {
        // Ignore changes to trailing whitespace
        return h(preg_replace('/\s+$/m', '', (string) $input));
    }
}
