<?php

namespace App\Utility\Diff;

use SebastianBergmann\Diff\Differ;
use SebastianBergmann\Diff\Output\DiffOutputBuilderInterface;

class HtmlDiffOutputBuilder implements DiffOutputBuilderInterface
{
    private $delimiter = "\n";

    public function __construct($delimiter = null)
    {
        if (!is_null($delimiter)) {
            $this->delimiter = $delimiter;
        }
    }

    public function getDiff(array $diff): string
    {
        $buffer = fopen('php://memory', 'r+b');

        foreach ($diff as $index => $diffEntry) {
            $diffEntryText = $diffEntry[0];
            if ($index === array_key_last($diff) && str_ends_with($diffEntryText, $this->delimiter)) {
                $diffEntryText = substr($diffEntryText, 0, -strlen($this->delimiter));
            }

            if ($diffEntry[1] === Differ::ADDED) {
                fwrite($buffer, '<ins>' . $diffEntryText . '</ins>');
            } elseif ($diffEntry[1] === Differ::REMOVED) {
                fwrite($buffer, '<del>' . $diffEntryText . '</del>');
            } elseif ($diffEntry[1] === Differ::DIFF_LINE_END_WARNING) {
                fwrite($buffer, $diffEntryText . '<span class="fas fa-exclamation-circle" title="Different line ends"></span>');

                continue; // Warnings should not be tested for line break, it will always be there
            } else { /* Not changed (old) 0 */
                fwrite($buffer, $diffEntryText);
            }

            if ($index !== array_key_last($diff) && !str_ends_with($diffEntryText, $this->delimiter)) {
                fwrite($buffer, $this->delimiter); // \No newline at end of file
            }
        }

        $diff = stream_get_contents($buffer, -1, 0);
        fclose($buffer);

        return $diff;
    }
}
