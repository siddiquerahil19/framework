<?php

namespace App\View;

use App\Model\Entity\Inscription;
use App\Utility\CdliProcessor\Convert;
use Cake\View\SerializedView;

class JtfView extends SerializedView
{
    use SerializeTrait;

    /**
     * List of special view vars.
     *
     * @var array
     */
    protected $_specialVars = ['_serialize'];

    /**
     * @var string
     */
    protected $_responseType = 'jtf';

    protected function _serialize($serialize): string
    {
        $inscription = $this->_dataToSerialize($serialize);

        if (is_array($inscription)) {
            $inscription = $inscription[0];
        }

        if ($inscription instanceof Inscription) {
            $this->response = $this->response->withDownload(
                $inscription->artifact->getCdliNumber() . '-jtf.json'
            );

            if ($inscription->has('jtf')) {
                return $inscription->jtf;
            }

            return json_encode(Convert::cAtfToJtf($inscription->atf)->objects);
        } else {
            return null;
        }
    }
}
