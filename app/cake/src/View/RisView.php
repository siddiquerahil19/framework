<?php

namespace App\View;

use Cake\View\SerializedView;

class RisView extends SerializedView
{
    use SerializeTrait;

    /**
     * List of special view vars.
     *
     * @var array
     */
    protected $_specialVars = ['_serialize'];

    /**
     * @var string
     */
    protected $_responseType = 'ris';

    public function initialize(): void
    {
        parent::initialize();
        $this->loadHelper('Scripts');
    }

    protected function _serialize($serialize): string
    {
        return $this->Citation->formatReference(
            $this->_dataToSerialize($serialize),
            'ris',
            []
        );
    }
}
