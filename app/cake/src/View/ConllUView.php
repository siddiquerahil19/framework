<?php

namespace App\View;

use App\Model\Entity\Inscription;
use Cake\View\SerializedView;

class ConllUView extends SerializedView
{
    use SerializeTrait;

    /**
     * List of special view vars.
     *
     * @var array
     */
    protected $_specialVars = ['_serialize'];

    /**
     * @var string
     */
    protected $_responseType = 'conll-u';

    public function initialize(): void
    {
        parent::initialize();
        $this->loadHelper('Scripts');
    }

    protected function _serialize($serialize): string
    {
        $inscriptions = $this->_dataToSerialize($serialize);
        $fileName = 'inscriptions';

        if (!is_array($inscriptions)) {
            if ($inscriptions instanceof Inscription) {
                $fileName = $inscriptions->artifact->getCdliNumber();
            }
            $inscriptions = [$inscriptions];
        }

        $response = '';

        foreach ($inscriptions as $inscription) {
            if ($inscription instanceof Inscription && !is_null($inscription->annotation)) {
                $response .= $this->Scripts->formatConllU($inscription);
            }
        }

        $this->response = $this->response->withDownload($fileName . '.conll');
        return $response;
    }
}
