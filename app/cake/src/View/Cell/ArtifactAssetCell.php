<?php

namespace App\View\Cell;

use Cake\View\Cell;

class ArtifactAssetCell extends Cell
{
    public function initialize(): void
    {
        $this->loadModel('ArtifactAssets');
        $this->viewBuilder()->addHelper('Html');
    }

    public function display($id)
    {
        $asset = $this->ArtifactAssets->get($id, [
            'contain' => [
                'Artifacts' => [
                    'Collections',
                    'Periods',
                    'Proveniences',
                    'ArtifactTypes',
                    'Materials',
                    'Genres',
                    'Languages'
                ],
                'Annotations' => [
                    'Bodies',
                    'OriginalAnnotations.UpdateEvents' => ['Creators', 'Authors'],
                    'UpdateEvents' => ['Creators', 'Authors']
                ]
            ]
        ]);
        $this->set('asset', $asset);

        if ($asset->asset_type === '3D model') {
            $this->viewBuilder()->setTemplate('3d_model');
        } elseif ($asset->asset_type === 'rti') {
            $this->viewBuilder()->setTemplate('rti_viewer');
        } elseif ($asset->file_format === 'pdf') {
            $this->viewBuilder()->setTemplate('pdf');
        }
    }
}
