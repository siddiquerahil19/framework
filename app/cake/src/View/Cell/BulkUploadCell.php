<?php

namespace App\View\Cell;

use Cake\View\Cell;

class BulkUploadCell extends Cell
{
    /**
     * Show error report for all errors after saving data.
     */
    public function display($table, $error_list, $header, $entities)
    {
        $this->set(compact('table', 'error_list', 'header', 'entities'));
    }

    /**
     * Display validation errors and ask confirmation for saving.
     */
    public function confirmation($table, $error_list_validation, $header, $entities)
    {
        $this->set(compact('table', 'error_list_validation', 'header', 'entities'));
    }
}
