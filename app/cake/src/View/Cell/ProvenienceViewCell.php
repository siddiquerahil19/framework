<?php

namespace App\View\Cell;

use Cake\View\Cell;

class ProvenienceViewCell extends Cell
{
    public function display($id)
    {
        $this->loadModel('Proveniences');
        $provenience = $this->Proveniences->get($id, [
            'contain' => ['Regions']
        ]);
        $this->set(compact('provenience'));
    }
}
