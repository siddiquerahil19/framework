<?php

namespace App\View;

use CsvView\View\CsvView;

class XlsxView extends CsvView
{
    use TableTrait;

    protected $_responseType = 'xlsx';

    /**
     * Returns data to be serialized.
     *
     * @param array|string|bool $serialize
     * @return string
     */
    protected function _serialize($serialize): string
    {
        $data = $this->_rowsToSerialize($this->viewVars['_serialize']);
        $this->set('table', $this->prepareTableData($data));
        $this->setConfig([
            'header' => $this->prepareTableHeader($data),
            'serialize' => 'table'
        ]);
        return parent::_serialize(null);
    }
}
