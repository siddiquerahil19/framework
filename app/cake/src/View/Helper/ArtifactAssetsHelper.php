<?php

namespace App\View\Helper;

use App\Model\Entity\Artifact;
use App\Model\Entity\ArtifactAsset;
use Cake\View\Helper;

class ArtifactAssetsHelper extends Helper
{
    public function getImage($assets)
    {
        $main = $this->_selectMain($assets);

        if (!is_null($main)) {
            $main = DS . $main->getPath();
        }

        return $main;
    }

    public function getThumbnail($assets)
    {
        $main = $this->_selectMain($assets);

        if (!is_null($main)) {
            $main = $main->getThumbnailPath();
        }

        return is_null($main) ? '/images/no-image.jpg' : DS . $main;
    }

    public function orderAssets($assets)
    {
        if ($assets instanceof Artifact) {
            $assets = $assets->artifact_assets;
        }
        $assets = is_null($assets) ? [] : array_slice($assets, 0);

        usort($assets, function ($a, $b) {
            if ($a->file_format === 'jpg' && $b->file_format !== 'jpg') {
                return -1;
            } elseif ($a->file_format !== 'jpg' && $b->file_format === 'jpg') {
                return 1;
            }

            if (is_null($a->artifact_aspect) && !is_null($b->artifact_aspect)) {
                return -1;
            } elseif (!is_null($a->artifact_aspect) && is_null($b->artifact_aspect)) {
                return 1;
            }

            if ($a->asset_type === 'photo' && $b->asset_type === 'lineart') {
                return -1;
            } elseif ($a->asset_type === 'lineart' && $b->asset_type === 'photo') {
                return 1;
            } elseif ($a->asset_type === 'lineart' && $b->asset_type !== 'lineart') {
                return -1;
            } elseif ($a->asset_type !== 'lineart' && $b->asset_type === 'lineart') {
                return 1;
            }
        });

        return $assets;
    }

    public function newEntityFromData(array $data)
    {
        $entity = new ArtifactAsset();
        foreach ($data as $key => $value) {
            if ($value !== '') {
                $entity->set($key, $value);
            }
        }
        return $entity;
    }

    private function _selectMain($assets)
    {
        $assets = $this->orderAssets($assets);
        if (count($assets) === 0) {
            return null;
        }

        $main = $assets[0];
        if ($main->file_format !== 'jpg' && $main->asset_type !== 'photo' && $main->asset_type !== 'lineart') {
            return null;
        }

        return $main;
    }
}
