<?php

namespace App\View\Helper;

use Cake\View\Helper;

class GridHelper extends Helper
{
    public $helpers = ['Html'];

    public function Alphabetical($arr)
    {
        $prev = array_values($arr)[0][0];
        $render = ["<div class='alphabetical-anchors'><div><span>".$prev."</span>"];
        foreach ($arr as $element => $key) {
            if ($prev != $key[0]) {
                $prev = $key[0];
                array_push($render, "</div><div><span>".$key[0]."</span>");
            }
            array_push($render, $this->Html->link(
                h($element),
                ['controller' => $key, 'action' => 'index']
            ));
        }

        array_push($render, "</div></div>");
        foreach ($render as $r) {
            echo $r;
        }
    }

    public function AlphabeticalHtmlView($arr)
    {
        $render = ["<div class='container'><div class='row'>"];
        for ($c = 65; $c <= 90; ++$c) {
            $alpha = chr($c);
            array_push($render, "<div class='col-sm-3 p-4'><h4>$alpha</h4><br>");
            foreach ($arr as $key => $value) {
                if ($key[0] == chr($c) || $key[0] == chr($c+32)) {
                    array_push($render, "<a align='left' href='$value' target='_blank'>$key</a><br>");
                }
            }
            array_push($render, "</div>");
        }
        array_push($render, "</div></div>");
        foreach ($render as $r) {
            echo $r;
        }
    }
}
