<?php

namespace App\View\Helper;

use Cake\View\Helper;

class ActionsHelper extends Helper
{
    public $helpers = array('Html', 'Form');

    public function manageRelation($controller, $id)
    {
        return implode("\n", [
            $this->Html->link('View', ['controller' => $controller, 'action' => 'view', $id]),
            $this->Html->link('Edit', ['controller' => $controller, 'action' => 'edit', $id]),
            $this->Form->postLink('Delete', ['controller' => $controller, 'action' => 'delete', $id], ['confirm' => __('Are you sure you want to delete # {0}?', $id)])
        ]);
    }
}
