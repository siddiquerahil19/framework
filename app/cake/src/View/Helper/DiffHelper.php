<?php

namespace App\View\Helper;

use App\Utility\Diff\AtfDiffer;
use App\Utility\Diff\HtmlDiffOutputBuilder;
use Cake\View\Helper;
use SebastianBergmann\Diff\Differ;

class DiffHelper extends Helper
{
    public function diffAtf($old, $new)
    {
        $differ = new AtfDiffer(new HtmlDiffOutputBuilder());
        return $differ->diff($old, $new);
    }

    public function diff($old, $new, $delimiter)
    {
        $differ = new Differ(new HtmlDiffOutputBuilder($delimiter));
        return $differ->diff(
            $this->splitLines($old, $delimiter),
            $this->splitLines($new, $delimiter)
        );
    }

    public function diffInscription($old, $new, $field)
    {
        $new = $new->get($field);
        $old = is_null($old) ? '' : $old->get($field);
        return $field === 'atf' ? $this->diffAtf($old, $new) : $this->diff($old, $new, "\n");
    }

    private function splitLines($text, $delimiter)
    {
        return array_map(function ($line) use ($delimiter) {
            return $line . $delimiter;
        }, explode($delimiter, $text));
    }
}
