<?php

namespace App\View\Helper;

use Cake\View\Helper\FormHelper as CakeFormHelper;

class FormHelper extends CakeFormHelper
{
    public function iconButton(string $title, string $icon, array $options = []): string
    {
        $options += [
            'type' => 'button',
            'escapeTitle' => true,
            'class' => 'btn cdli-btn-light'
        ];

        if ($options['escapeTitle']) {
            $title = h($title);
            $options['escapeTitle'] = false;
        }

        $title = '<span class="fa fa-' . h($icon) . ' plus-icon" aria-hidden="true"></span> ' . $title;

        return '<div class="mb-4">' . $this->button($title, $options) . '</div>';
    }

    public function toggle(string $fieldName, array $options = [])
    {
        $options += ['hiddenField' => true, 'value' => 1];

        // Work around value=>val translations.
        $value = $options['value'];
        unset($options['value']);
        $options = $this->_initInputField($fieldName, $options);
        $options['value'] = $value;

        $output = '';
        if ($options['hiddenField'] !== false && is_scalar($options['hiddenField'])) {
            $hiddenOptions = [
                'name' => $options['name'],
                'value' => $options['hiddenField'] !== true
                    && $options['hiddenField'] !== '_split'
                    ? (string)$options['hiddenField'] : '0',
                'form' => $options['form'] ?? null,
                'secure' => false,
            ];
            if (isset($options['disabled']) && $options['disabled']) {
                $hiddenOptions['disabled'] = 'disabled';
            }
            $output = $this->hidden($fieldName, $hiddenOptions);
        }

        if ($options['hiddenField'] === '_split') {
            unset($options['hiddenField'], $options['type']);

            return ['hidden' => $output, 'input' => $this->widget('toggle', $options)];
        }
        unset($options['hiddenField'], $options['type']);

        return $output . $this->widget('toggle', $options);
    }

    public function hideQueryParameters(string $params, $exludePattern = null)
    {
        if ($params === '') {
            return '';
        }
        $output = '';
        foreach (explode('&', $params) as $pair) {
            [$key, $value] = explode('=', $pair);
            if (!is_null($exludePattern) && preg_match($exludePattern, urldecode($key))) {
                continue;
            }
            $output .= $this->hidden(urldecode($key), ['value' => urldecode($value)]);
        }
        return $output;
    }
}
