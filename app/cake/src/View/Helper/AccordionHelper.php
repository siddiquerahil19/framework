<?php

namespace App\View\Helper;

use Cake\View\Helper;

class AccordionHelper extends Helper
{
    public function partOpen($id, $title, string $tag = 'p', bool $open = false)
    {
        return implode("\n", [
            '<section id="' . h($id) . '">',
            '<input type="checkbox" id="' . h($id) . '-toggle" class="accordion-textbox"' . ($open ? ' checked' : '') . '>',
            '<'.$tag.' class="accordion-title"><label class="w-100" for="' . h($id) . '-toggle">' . h($title) . '</label></'.$tag.'>',
            '<div class="detail-leaf">'
        ]);
    }

    public function partOuterOpen($id, $title, $tag="p")
    {
        return implode("\n", [
            '<section id="' . h($id) . '">',
            '<input type="checkbox" id="' . h($id) . '-toggle" class="accordion-textbox" checked>',
            '<'.$tag.' class="accordion-title"><label class="w-100" for="' . h($id) . '-toggle">' . h($title) . '</label></'.$tag.'>',
            '<div class="detail-leaf outer-accordion">'
        ]);
    }

    public function partClose()
    {
        return '</div></section>';
    }
}
