<?php

namespace App\View\Helper;

use App\Model\Entity\Article;
use App\Model\Entity\Artifact;
use App\Model\Entity\Publication;
use Cake\Database\Query;
use Cake\Http\Exception\HttpException;
use Cake\ORM\Entity;
use Cake\ORM\Resultset;
use Cake\View\Helper;

class CitationHelper extends Helper
{
    public $helpers = ['Scripts'];

    /**
     * Format any data into a reference.
     *
     * Option and format documentation: see /app/tools/citation.js/README.md.
     *
     * @param array $data
     * @param string $format
     * @param array $options
     * @return string
     * @throws \Cake\Http\Exception\HttpException
     */
    public function formatReference($data, string $format, array $options): string
    {
        return $this->Scripts->formatReference($this->removeFields($data), $format, $options);
    }

    /**
     * Get the default data for citing a webpage of the framework.
     *
     * @return array
     */
    public function getDefaultData()
    {
        $view = $this->getView();
        return [
            'type' => 'webpage',
            'title' => $view->fetch('title'),
            'author' => [['literal' => 'CDLI contributors']],
            'container-title' => 'Cuneiform Digital Library Initiative',
            'issued' => [['date-parts' => explode('-', date("Y-m-d"))]],
            'accessed' => [['date-parts' => explode('-', date("Y-m-d"))]],
            'URL' => 'https://cdli.mpiwg-berlin.mpg.de' . $view->getRequest()->getRequestTarget()
        ];
    }

    /**
     * Remove certain fields from associative arrays.
     */
    private function removeFields($data): array
    {
        return array_map(function ($entry) {
            if ($entry instanceof Entity) {
                if ($entry instanceof Article) {
                    $entry = $this->extractData($entry, [
                        'title',
                        'authors' => [
                            'first',
                            'last',
                            'author',
                            '_joinData' => ['sequence']
                        ],
                        'article_type',
                        'created',
                        'year',
                        'article_no'
                    ]);
                } elseif ($entry instanceof Artifact) {
                    $entry = $this->extractData($entry, [
                        'designation',
                        'artifacts_updates' => [
                            'update_event' => ['created', 'approved']
                        ],
                        'inscription' => ['transliteration_clean'],
                        'id',
                        'has_fragments' // for recognition
                    ]);
                } elseif ($entry instanceof Publication) {
                    $entry = $this->extractData($entry, [
                        'address',
                        'annote',
                        'bibtexkey',
                        'book_title',
                        'chapter',
                        'crossref',
                        'edition',
                        'editor',
                        'how_published',
                        'institution',
                        'journal_id',
                        'month',
                        'note',
                        'number',
                        'organization',
                        'pages',
                        'publisher',
                        'school',
                        'title',
                        'volume',
                        'year',
                        'entry_type' => ['label'],
                        'authors' => ['author'],
                        'editors' => ['author'],
                        'journal' => ['journal']
                    ]);
                }
            }

            return $entry;
        }, $this->getDataArray($data));
    }

    private function extractData(Entity $entity, $shape)
    {
        $data = [];
        foreach ($shape as $key => $value) {
            if (is_int($key)) {
                if ($entity->has($value)) {
                    $data[$value] = $entity->get($value);
                }
            } elseif ($entity->has($key)) {
                $value = $entity->get($key);
                if ($value instanceof Query) {
                    $value = $value->all();
                }
                if ($value instanceof ResultSet) {
                    $value = $value->toArray();
                }

                if (is_array($value)) {
                    $data[$key] = array_map(function ($entity) use ($shape, $key) {
                        return $this->extractData($entity, $shape[$key]);
                    }, $value);
                } else {
                    $data[$key] = $this->extractData($value, $shape[$key]);
                }
            }
        }
        return $data;
    }

    private function getDataArray($value)
    {
        if ($value instanceof Query) {
            $value = $value->all();
        }
        if ($value instanceof ResultSet) {
            $value = $value->toArray();
        }
        if ($value instanceof Entity) {
            $value = [$value];
        }
        return $value;
    }
}
