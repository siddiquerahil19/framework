<?php

namespace App\View\Helper;

use Cake\View\Helper;

class URLEncodingHelper extends Helper
{
    public $baseURL = "/cdli-accounting-viz/?";

    /* Get a link to the commodity viz page with a compressed
     * list of artifact ids as a URL parameter.
     *
     * @param : $ids
     * ids -> array of artifact ids to encode in the url
     *
     * @return : a URL pointing to the commodity viz page
     */
    public function getEncodedCommodityURL($ids)
    {
        $map = array(
            ","=>"A", "0"=>"B", "1"=>"C", "2"=>"D", "3"=>"E",
            "4"=>"F", "5"=>"G", "6"=>"H", "7"=>"I", "8"=>"J",
            "9"=>"K", "1,1,"=>"L", "0,"=>"M", "1,"=>"N", "2,"=>"O",
            "3,"=>"P", "4,"=>"Q", "5,"=>"R", "6,"=>"S", "7,"=>"T",
            "8,"=>"U", "9,"=>"V", ",0"=>"W", ",1"=>"X", ",2"=>"Y",
            ",3"=>"Z", ",4"=>"a", ",5"=>"b", ",6"=>"c", ",7"=>"d",
            ",8"=>"e", ",9"=>"f", "00"=>"g", "11"=>"h", "22"=>"i",
            "33"=>"j", "44"=>"k", "55"=>"l", "66"=>"m", "77"=>"n",
            "88"=>"o", "99"=>"p", "0,1"=>"q", "1,1"=>"r", "2,1"=>"s",
            "3,1"=>"t", "4,1"=>"u", "5,1"=>"v", "6,1"=>"w", "7,1"=>"x",
            "8,1"=>"y", "9,1"=>"z", ",0,"=>"0", ",1,"=>"1", ",2,"=>"2",
            ",3,"=>"3", ",4,"=>"4", ",5,"=>"5", ",6,"=>"6", ",7,"=>"7",
            ",8,"=>"8", ",9,"=>"9");

        if (count($ids) == 0) {
            return "";
        }

        // Compute the difference between successive
        // ids, because these differences will be much
        // smaller than the ids themselves:
        sort($ids);
        $deltas = array( $ids[0] );
        for ($i = 1; $i < count($ids); $i++) {
            $deltas[$i] = $ids[$i] - $ids[$i-1];
        }

        $delta_string = join(",", $deltas);

        // Length of the longest string which
        // can be encoded by a single character:
        // For our encoding this is "1,1,"
        $MAX_ENCODING_LENGTH = 4;

        // Iterate over the string. Every iteration,
        // greedily convert the string's prefix into
        // an encoded letter.
        $encoding = "";
        $i = 0;
        $keys = array_map("strval", array_keys($map));
        while ($i < strlen($delta_string)) {
            for ($length = $MAX_ENCODING_LENGTH; $length > 0; $length--) {
                $key = substr($delta_string, $i, $length);
                if (in_array($key, $keys, true)) {
                    $encoding = $encoding . $map[$key];
                    $i += $length;
                    break;
                }
            }
        }

        return $this->baseURL . $encoding;
    }
}
