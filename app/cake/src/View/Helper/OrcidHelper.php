<?php

namespace App\View\Helper;

use Cake\View\Helper;

class OrcidHelper extends Helper
{
    /**
     * @param string $orcid
     * @param string $format
     * @return string
     */
    public function format($orcid, string $format): string
    {
        if (empty($orcid)) {
            return '';
        }

        $orcid = h($orcid);
        $linkContent = '';
        if ($format === 'full') {
            $linkContent = 'https://orcid.org/' . $orcid;
        } elseif ($format === 'compact') {
            $linkContent = $orcid;
        } elseif ($format === 'inline') {
            $linkContent = '';
        }

        return "
            <a href=\"https://orcid.org/$orcid\">
                <img alt=\"ORCID logo\" src=\"https://info.orcid.org/wp-content/uploads/2019/11/orcid_16x16.png\" width=\"16\" height=\"16\" />
                $linkContent
            </a>
        ";
    }
}
