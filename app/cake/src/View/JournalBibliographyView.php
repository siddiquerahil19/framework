<?php

namespace App\View;

use Cake\View\SerializedView;

class JournalBibliographyView extends SerializedView
{
    use SerializeTrait;

    /**
     * @var array
     */
    protected static $defaultOptions = [
        'format' => 'html',
        'template' => 'chicago-author-date',
        'asEntryArray' => true
    ];

    /**
     * List of special view vars.
     *
     * @var array
     */
    protected $_specialVars = ['_serialize', '_citations'];

    /**
     * @var string
     */
    protected $_responseType = 'json';

    public function initialize(): void
    {
        parent::initialize();
        $this->loadHelper('Scripts');
    }

    protected function _serialize($serialize): string
    {
        $data = [
            'data' => $this->_dataToSerialize($serialize),
            'bibliographyEntries' => [],
            'citationEntries' => []
        ];

        foreach ($this->viewVars['_citations'] as $citation) {
            if (preg_match('/\\\\([A-Za-z]+)(?:\\[(.+)\\])?\\{(.+)\\}/', $citation, $match)) {
                $ids = explode(',', $match[3]);
                $locator = $match[2];

                if (empty($locator)) {
                    $ids = array_map(function ($id) {
                        return ['id' => $id];
                    }, $ids);
                } else {
                    $locator = preg_replace('/\{|\}/', '', $locator);
                    $ids = array_map(function ($id) use ($locator) {
                        return ['id' => $id, 'locator' => $locator];
                    }, $ids);
                }

                if ($match[1] != 'nocite') {
                    array_push($data['citationEntries'], $ids);
                }
                array_push($data['bibliographyEntries'], ...$ids);
            }
        }

        return $this->Citation->formatReference(
            $data,
            'bibliographyandcitations',
            self::$defaultOptions
        );
    }
}
