<?php

namespace App\Datasource;

use App\Datasource\ElasticSearchQuery;

class ElasticSearchResultSet extends ArrayResultSet
{
    public array $_buckets = [];
    public array $_metrics = [];

    public function __construct(array $results)
    {
        parent::__construct($results['hits']['hits']);
        $this->_buckets = $results['aggregations'];
        $this->_metrics = [
            'count' => $results['hits']['total'],
            'duration' => $results['took']
        ];

        // Simplify buckets of nested fields
        foreach ($this->_buckets as $filter => $bucket) {
            $field = ElasticSearchQuery::transformFieldName($filter);
            if (ElasticSearchQuery::isNestedField($field) && array_key_exists('nested', $bucket)) {
                $this->_buckets[$filter] = [
                    'doc_count_error_upper_bound' => $bucket['nested']['doc_count_error_upper_bound'],
                    'sum_other_doc_count' => $bucket['nested']['sum_other_doc_count'],
                    'buckets' => array_map(function ($bucket) {
                        return [
                            'key' => $bucket['key'],
                            'doc_count' => $bucket['root']['doc_count']
                        ];
                    }, $bucket['nested']['buckets'])
                ];
            }
        }
    }
}
