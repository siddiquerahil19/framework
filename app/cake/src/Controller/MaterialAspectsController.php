<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * MaterialAspects Controller
 *
 * @property \App\Model\Table\MaterialAspectsTable $MaterialAspects
 *
 * @method \App\Model\Entity\MaterialAspect[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MaterialAspectsController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('Api', ['features' => ['LinkedData', 'TableExport']]);
        $this->loadComponent('GeneralFunctions');

        // Set access for public.
        $this->Auth->allow(['index', 'view']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'maxLimit' => $this->MaterialAspects->find()->count()
        ];
        $materialAspects = $this->paginate($this->MaterialAspects);
        $access_granted = $this->GeneralFunctions->checkIfRolesExists([1]);

        $this->set(compact('materialAspects', 'access_granted'));
        $this->set('_serialize', 'materialAspects');
    }

    /**
     * View method
     *
     * @param string|null $id Material Aspect id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $materialAspect = $this->MaterialAspects->get($id);
        $artifacts = $this->loadModel('ArtifactsMaterials');
        $access_granted = $this->GeneralFunctions->checkIfRolesExists([1]);
        $count = $artifacts->find('list', ['conditions' => ['material_aspect_id' => $id]])->count();

        $this->set(compact('access_granted', 'count'));
        $this->set('materialAspect', $materialAspect);
        $this->set('_serialize', 'materialAspect');
    }
}
