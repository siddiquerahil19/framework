<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Sponsor Controller
 *
 * @property \App\Model\Table\SponsorsTable $Sponsor
 *
 * @method \App\Model\Entity\Sponsor[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SponsorsController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('GeneralFunctions');
        $this->loadModel('SponsorTypes');
        // Set access for public.
        $this->Auth->allow(['index']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $sponsors = $this->Sponsors->find('all');
        $sponsor_types = $this->SponsorTypes->find('all');
        $access_granted = $this->GeneralFunctions->checkIfRolesExists([1]);
        $this->set(compact('sponsor_types', 'sponsors', 'access_granted'));
        $this->set('_serialize', 'sponsors');
    }
    public function view($id = null)
    {
        $sponsor = $this->Sponsors->get($id, [
            'contain' => ['SponsorTypes']
        ]);
        $access_granted = $this->GeneralFunctions->checkIfRolesExists([1]);
        $this->set(compact('sponsor', 'access_granted'));
        $this->set('_serialize', 'sponsor');
    }
}
