<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Viz Controller
 */

class VizController extends AppController
{
    public function initialize(): void
    {
        parent::initialize();
        $this->loadModel('Artifacts');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $data = $this->Artifacts->getLanguagesWithCount();
        $this->set('data', json_encode($data));
    }

    public function donut()
    {
        $data = $this->Artifacts->getLanguagesWithCount();
        $this->set('data', json_encode($data));
    }

    public function radar()
    {
        $data = $this->Artifacts->getRadarChartData();
        $legend = $this->Artifacts->getRadarChartLegend($data);

        // Required data format without Legend
        $data = $this->Artifacts->formatRadarChartData($data);

        $this->set('data', json_encode($data));
        $this->set('legendOptions', json_encode($legend));
    }

    public function line()
    {
        $data = $this->Artifacts->getLineChartData();

        $this->set('data', json_encode($data));
    }

    public function dendrogram()
    {
        $data = $this->Artifacts->getDendrogramChartData();

        $this->set('data', json_encode($data));
    }

    public function choropleth()
    {
        $data = $this->Artifacts->getChoroplethMapData();

        // Uncomment below line to plot the map for proveniences
//        $data = $this->Artifacts->getChoroplethMapData('proveniences');

        $this->set('data', json_encode($data));
    }
}
