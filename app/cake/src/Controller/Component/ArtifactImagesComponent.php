<?php

namespace App\Controller\Component;

use Cake\Controller\Component;

class ArtifactImagesComponent extends Component
{
    public function getImageLinks($CDLI_NO)
    {
        $image_types = ['photo', 'lineart', 'photo_detail', 'photo_envelope', 'lineart_detail'];
        $extension = '';
        $foldername = '';
        $filepath = '';
        $finalpath = '';
        $imagesLinks = [];

        foreach ($image_types as $type) {
            switch ($type) {
                case 'photo': {
                    $foldername = 'photo';
                    $extension = '.jpg';
                    break;
                }

                case 'lineart': {
                    $foldername = 'lineart';
                    $extension = '_l.jpg';
                    break;
                }

                case 'photo_detail': {
                    $foldername = 'photo';
                    $extension = '_d.jpg';
                    break;
                }

                case 'photo_envelope': {
                    $foldername = 'photo';
                    $extension = '_e.jpg';
                    break;
                }

                case 'lineart_detail': {
                    $foldername = 'lineart';
                    $extension = '_ld.jpg';
                    break;
                }
            }

            $filepath = WWW_ROOT . 'dl' . DS . $foldername . DS . $CDLI_NO . $extension;
            if (file_exists($filepath)) {
                $imagesLinks[$type] = DS . 'dl' . DS . $foldername . DS . $CDLI_NO . $extension;
            }
        }
        return $imagesLinks;
    }
}
