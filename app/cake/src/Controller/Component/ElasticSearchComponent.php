<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Http\Client;
use Cake\ORM\TableRegistry;
use Cake\Utility\Inflector;

class ElasticSearchComponent extends Component
{
    public $components = ['GeneralFunctions'];
    /**
     * elasticSearchCredentials method
     *
     * @return array [username, password]
     */
    private function elasticSearchCredentials()
    {
        $username = 'elastic';
        $password = 'elasticchangeme';
        return [$username, $password];
    }

    /**
     * filterESQuery method
     *
     * @return ElasticSearch query for filters. (string)
     */
    public function filterESQuery($filterData)
    {
        /**
            $filterForSameField = '
                {
                    \"match_phrase\":  {
                        \"materials\": \"limestone\"
                    }
                }';

            $filterForCombinedInnerField = '        {
                "bool": {
                    "should": [
                        {
                            "match_phrase":  {
                                "atype": "barrel"
                            }
                        },
                        {
                            "match_phrase":  {
                                "atype": "other (see object remarks)"
                            }
                        }
                    ]
                }
            }';
        */


        $hasTranslationArray = [];
        $hasTransliterationArray = [];
        $filterWiseCompleteQueryArray = [];
        $filterwiseArray = [];
        foreach ($filterData as $filter => $valueAray) {
            if ($filter === 'transliteration') {
                $hasTransliterationArray = [
                    'exists' => [
                        'field' => 'atf'
                    ]
                ];
                array_push($filterwiseArray, $hasTransliterationArray);
                continue;
            }
            if ($filter === 'translation') {
                $hasTranslationArray = [
                    'exists' => [
                        'field' => 'translation'
                    ]
                ];
                array_push($filterwiseArray, $hasTranslationArray);
                continue;
            }

            foreach ($valueAray as $value) {
                $tempInnerArray = [
                    'match_phrase' => [
                         $filter => $value
                    ]
                ];
            }

            if (!empty($tempInnerArray)) {
                $filterWiseCompleteQueryArray = [
                    'bool' => [
                        'should' => $tempInnerArray
                    ]
                ];
                array_push($filterwiseArray, $filterWiseCompleteQueryArray);
            }
        }
        return $filterwiseArray;
    }

    /**
     * inscriptionSearchQuery method
     *
     * @return ElasticSearch query for transliterations search. (string)
     */
    public function inscriptionSearchQuery($fields, $value, $settings)
    {
        $fieldsQuery = [];
        $fieldsQueryArray = [];
        foreach ($fields as $field) {
            if ($field === 'transliteration_permutation') {
                $signNames = $this->getSignNames($value);
                if (!empty($signNames)) {
                    $innerQueryArray = [
                        'bool' => [
                            'must' => [
                                'match_phrase' => [
                                    'transliteration_sign_names' => $signNames
                                ]
                            ]
                        ]
                    ];
                    array_push($fieldsQueryArray, $innerQueryArray);
                }
            }

            $innerQueryArray = [
                'bool' => [
                    'must' => [
                        'match_phrase' => [
                            'transliteration_for_search' => $value
                        ]
                    ]
                ]
            ];
            array_push($fieldsQueryArray, $innerQueryArray);
        }
        if (!empty($fieldsQueryArray)) {
            $finalQueryArray = [
                ['bool' => [
                    'should' => $fieldsQueryArray
                ]]
            ];
            if (!$settings['canViewPrivateArtifacts']) {
                $updatefinalQueryArray = [
                    'match_phrase' => [
                        'is_public' => 'true'
                    ]
                ];
                array_push($finalQueryArray, $updatefinalQueryArray);
            }
        }
        return $finalQueryArray;
    }
    /**
     * ES query for buckets aggregations
     *
     * @return BucketsQueryArray that is to be appended into final Query
     */
    public function bucketsQuery()
    {
        $filtersarray =["period","languages","materials","genres","atype","provenience","authors","collection","year","image_type"];
        $bucketsQueryArray = [];
        foreach ($filtersarray as $filter) {
            $bucketsQueryArray[$filter] = [
                    'terms' => [
                        'field' => $filter.'.keyword',
                        'size' => '1000'
                    ]
                ];
        }
        return $bucketsQueryArray;
    }

    public function stripMuseumId($museumId)
    {
        $museumIdPattern = '/[A-Za-z]+\s*\d+/';
        $strippedMuseumId = '';
        if (preg_match($museumIdPattern, $museumId)) {
            $strippedMuseumId = preg_replace_callback(
                $museumIdPattern,
                function ($matches) {
                    return preg_filter('/[0-9]+/', '*$0', $matches[0]);
                },
                $museumId
            );
        }
        return $strippedMuseumId;
    }

    public function stripId($id)
    {
        //Regex that matches letters,spaces and frontal zeroes.
        $idPattern = '/[a-zA-Z]\s*0*/';
        $strippedId = '';
        if (preg_match($idPattern, $id)) {
            //Strip letters and frontal zeroes
            $strippedId = preg_replace($idPattern, '', $id);
        }
        return $strippedId;
    }

    public function multiIdSearch($field, $value)
    {
        $value = preg_replace('/,/', " OR ", $value);
        $keywords = preg_split("/(\s)/", $value);
        $query = $field.':(';
        foreach ($keywords as $keyword) {
            if ($keyword !== 'OR' || $keyword !== 'AND') {
                $strippedData = ($field === 'museum_no') ? $this->stripMuseumId($keyword) : $this->stripId($keyword);
                $query .= (empty($strippedData)) ? $keyword.' ' : $keyword.' OR '.$strippedData.' ';
            } else {
                $query .= ' '.$keyword.' ';
            }
        }
        $query .= ')';
        return $query;
    }

    public function idSearchQuery($field, $value)
    {
        $tempString = '';
        if ($field === 'museum_no') {
            //Multiple search fields
            if (preg_match('/(OR|AND|,)/', $value)) {
                $tempString = $this->multiIdSearch($field, $value);
            } else {
                $strippedMuseumId = $this->stripMuseumId($value);
                $tempString = (empty($strippedMuseumId)) ? $field.':('.$value.')' : $field.':('.$value.' OR '.$strippedMuseumId.')';
            }
        } else {
            //Multiple search fields
            if (preg_match('/(OR|AND|,)/', $value)) {
                $tempString = $this->multiIdSearch($field, $value);
            } else {
                $strippedId = $this->stripId($value);
                $tempString = (empty($strippedId) || preg_match('/^[\\\]/', $strippedId)) ? $field.':('.$value.')' : $field.':('.$value.' OR '.$strippedId.')';
            }
        }
        if ($field === 'composite_no' || $field === 'seal_no') {
            // "composite_no:(Q000002)" -> "composite_no:(Q000002) OR acomposite_no:(Q000002)"
            $tempString = $tempString . ' OR a' . $tempString;
        }

        return $tempString;
    }

    public function getSignNames($value)
    {
        $http = new Client();
        $atf = ['atf' => $value];
        $response = $http->post(
            'http://jtf-lib:3003/jtf-lib/api/getSignnamesATFLINE/',
            json_encode($atf),
            ['type' => 'json']
        );
        return $response->getStringBody();
    }

    /**
     * searchAdvancedArtifacts method
     *
     * Advanced Search.
     * Refer: https://gitlab.com/cdli/framework/-/issues/298
     *
     * @param
     *
     * @return
     */
    public function searchAdvancedArtifacts($queryData, $settings, $filterData = [])
    {
        //searchTranslitQuery --> transliteration query for elasticsearch
        $searchTranslitQuery = '';
        //translitQuery --> transliteration query from user
        $translitQuery = '';
        //signName --> used in permutation search
        $signName = '';
        $projectQuery = '';
        $creditQuery = '';
        $searchTranslitPermutation = false;
        $searchTranslit = false;

        if (array_key_exists('transliteration_permutation', $queryData)) {
            $translitQuery = $queryData['transliteration_permutation'];
            $searchTranslitQuery = $this->inscriptionSearchQuery(['transliteration_permutation'], $translitQuery, $settings);
            $searchTranslitPermutation = true;
            $signName = $this->getSignNames($translitQuery);
            unset($queryData['transliteration_permutation']);
        } elseif (array_key_exists('transliteration', $queryData)) {
            $translitQuery = $queryData['transliteration'];
            $searchTranslit = true;
            $searchTranslitQuery = $this->inscriptionSearchQuery(['transliteration'], $translitQuery, $settings);
            unset($queryData['transliteration']);
        }

        $keyword = implode(' AND ', array_map(
            function ($value, $field) {
                $fuzzyIdFields = [
                    'composite_no',
                    'seal_no',
                    'museum_no',
                    'id'
                ];
                $tempString = '';
                // Processing input for Id's field
                if (in_array($field, $fuzzyIdFields)) {
                    $tempString = $this->idSearchQuery($field, $value);
                } elseif ($field === 'project') {
                    $tempString = 'artifact_project:('.$value.') OR inscription_project:('.$value.')';
                } elseif ($field === 'credit') {
                    $tempString = 'artifact_credit:('.$value.') OR inscription_credit:('.$value.')';
                } else {
                    $tempString = $field.':('.$value.')';
                }
                return $tempString;
            },
            $queryData,
            array_keys($queryData)
        ));



        $parentChildString = '';

        if (array_key_exists('materials', $queryData)) {
            $parentChildString = $this->queryParentChild($queryData['materials']);
        }

        if (array_key_exists('genres', $queryData)) {
            $parentChildString = $this->queryParentChild($queryData['genres']);
        }

        if (array_key_exists('atype', $queryData)) {
            $parentChildString = $this->queryParentChild($queryData['atype']);
        }

        if (!$settings['canViewPrivateArtifacts'] && !empty($keyword)) {
            $keyword = '('.$keyword.$parentChildString.')'.' AND is_public:true';
        }
        if (!empty($filterData)) {
            $filterWiseArray = $this->filterESQuery($filterData);
        }

        $bucketsquery = $this->bucketsQuery();

        $fieldsToBeDisplayedInSearchReuslts = ["id", "adesignation", "collection", "materials", "artifact_type_id", "atype", "period_id", "period", "provenience_id", "provenience", "genres", "languages", "authors", "year", "image_type", "atf", "translation", "dates", "artifact_credit", "inscription_credit", "artifact_project", "inscription_project", "archive", "museum_no", "accession_no", "written_in", "excavation_no", "composite_no", "seal_no", "acomposite_no", "aseal_no"];
        $ESquery['_source'] = [
            'includes' => $fieldsToBeDisplayedInSearchReuslts
        ];

        $ESquery['aggs'] = $bucketsquery;
        $ESquery['query'] = [
            'bool' =>[
                'filter' =>[
                    'term' => [
                        'retired' => 'false'
                    ]
                ],
                'must' =>[
                    ['query_string' => [
                        'query' => $keyword,
                        'default_operator' => 'and'
                    ]]
                ]
            ]

        ];
        $ESquery['sort'] = [
            '_score' => [
                'order' => 'desc'
            ]

        ];
        $ESquery['size'] = 10000;

        if (!empty($filterData)) {
            $ESquery['query']['bool']['must'] = array_merge($ESquery['query']['bool']['must'], $filterWiseArray);
        }
        if (!empty($searchTranslitQuery)) {
            $ESquery['query']['bool']['must'] = array_merge($ESquery['query']['bool']['must'], $searchTranslitQuery);
        }

        $result = [];

        $filters = [
            'materials' => [],
            'collection' => [],
            'atype' => [],
            'period' => [],
            'provenience' => [],
            'genres' => [],
            'languages' => [],
            'authors' => [],
            'year' => [],
            'image_type' => [],
            'transliteration' => [],
            'translation' => [],
            'archive' => []
        ];

        $resultArray = $this->getDataFromES('advanced_artifacts', json_encode($ESquery), 0);

        $totalHits = $resultArray['total'];

        // Calculating results size till previous pages.
        $totalPreviousResults = ($settings['Page'] == 1) ? 0 : ($settings['Page'] - 1) * $settings['PageSize'];

        // Set No. of result to be stored in session from overall results.
        $resultSet = (int)floor($totalPreviousResults / 10000);

        // Result Size stored in Session.
        $resultSize = 10000;

        // If there is any need for extra result which will be required on certain page where the set of results stored in session is not enough to display results (as set by page size) on that page.
        $checkIfExtraResultsRequired = 10000/$settings['PageSize'];

        if ($checkIfExtraResultsRequired != floor($checkIfExtraResultsRequired)) {
            $changeResultSet = floor(($totalPreviousResults + $settings['PageSize'])/10000) == $resultSet ? 0 : 1;

            if ($changeResultSet) {
                $resultSize += ($settings['PageSize'] - ($resultSize - $totalPreviousResults));
            }
        }

        $lowerBound = $resultSet * 10000;
        $upperBound = $lowerBound + $resultSize - 1;

        $trackingCount = 0;

        foreach ($resultArray['hits'] as $resultRow) {
            if ($lowerBound <= $trackingCount && $trackingCount <= $upperBound) {
                if ($searchTranslitPermutation || $searchTranslit) {
                    //without any non-sign data
                    $cleanInscription = preg_replace('/[^\w\s]/i', '', $resultRow['atf']);
                    $cleanTranslitQuery = preg_replace('/[^\w\s]/i', '', $translitQuery);
                    $regexPattern = ($searchTranslitPermutation) ? '/'.$cleanTranslitQuery.'|'.$signName.'/i' : '/'.$cleanTranslitQuery.'/i';

                    //Do not consider the result which does not contains the required translit.
                    if (!preg_match($regexPattern, $cleanInscription)) {
                        continue;
                    }
                }

                $resultRow['artifact_credit'] = (!$resultRow['artifact_credit']) ? [] : explode(', ', $resultRow['artifact_credit']);
                $resultRow['inscription_credit'] = (!$resultRow['inscription_credit']) ? [] : explode(', ', $resultRow['inscription_credit']);
                $resultRow['artifact_project'] = (!$resultRow['artifact_project']) ? [] : explode(', ', $resultRow['artifact_project']);
                $resultRow['inscription_project'] = (!$resultRow['inscription_project']) ? [] : explode(', ', $resultRow['inscription_project']);

                $project = array_unique(array_merge($resultRow['artifact_project'], $resultRow['inscription_project']));
                $credit = array_unique(array_merge($resultRow['artifact_credit'], $resultRow['inscription_credit']));
                $project = implode(', ', $project);
                $credit = implode(', ', $credit);

                $resultRow['acomposite_no'] = '';
                $resultRow['aseal_no'] = '';
                $resultRow['museum_no'] = '';
                $resultRow['accession_no'] = '';
                $resultRow['written_in'] = '';
                $resultRow['excavation_no'] = '';

                $result[$resultRow['id']] = [
                    'artifact' => [
                        'acomposite_no' => $resultRow['acomposite_no'],
                        'aseal_no' => $resultRow['aseal_no'],
                        'composite_no' => $resultRow['composite_no'],
                        'seal_no' => $resultRow['seal_no'],
                        'designation' => $resultRow['adesignation'],
                        'museum_no' => $resultRow['museum_no']
                    ],
                    'collection' => [
                        'collection' => $resultRow['collection']
                    ],
                    'material' => [
                        'material' => $resultRow['materials']
                    ],
                    'artifact_type' => [
                        'id' => $resultRow['artifact_type_id'],
                        'artifact_type' => $resultRow['atype']
                    ],
                    'period' => [
                        'period_id' => $resultRow['period_id'],
                        'period' => $resultRow['period']
                    ],
                    'provenience' => [
                        'provenience_id' => $resultRow['provenience_id'],
                        'provenience' => $resultRow['provenience']
                    ],
                    'genres' => [
                        'genres' => $resultRow['genres']
                    ],
                    'languages' => [
                        'languages' => $resultRow['languages']
                    ],
                    'dates' => [
                        'dates' => array_key_exists('dates', $resultRow) ? $resultRow['dates'] : 'N.A'
                    ],
                    'project' => [
                        'project' => $project
                    ],
                    'credit' => [
                        'credit' => $credit
                    ],
                    'accession_no' => [
                        'accession_no' => $resultRow['accession_no']
                    ],
                    'written_in' => [
                        'written_in' => $resultRow['written_in']
                    ],
                    'excavation_no' => [
                        'excavation_no' => $resultRow['excavation_no']
                    ],
                    'archive' => [
                        'archive' => $resultRow['archive']
                    ]
                ];
            }

            $filters['genres'] = array_merge($filters['genres'], array_fill_keys(array_filter(explode(' ; ', $resultRow['genres']), 'strlen'), 0));

            $filters['languages'] = array_merge($filters['languages'], array_fill_keys(array_filter(explode(' ; ', $resultRow['languages']), 'strlen'), 0));

            $filters['materials'] = array_merge($filters['materials'], array_fill_keys(array_filter(explode(', ', $resultRow['materials']), 'strlen'), 0));

            $filters['collection'] = $resultRow['collection'] != '' ? array_merge($filters['collection'], [$resultRow['collection'] => 0]) : $filters['collection'];

            $filters['atype'] = $resultRow['atype'] != '' ? array_merge($filters['atype'], [$resultRow['atype'] => 0]) : $filters['atype'];

            $filters['provenience'] = $resultRow['provenience'] != '' ? array_merge($filters['provenience'], [$resultRow['provenience'] => 0]) : $filters['provenience'];

            $filters['period'] = $resultRow['period'] != '' ? array_merge($filters['period'], [$resultRow['period'] => 0]) : $filters['period'];

            $filters['authors'] = array_merge($filters['authors'], array_fill_keys(array_filter(explode(PHP_EOL, $resultRow['authors']), 'strlen'), 0));

            $filters['year'] = $filters['year'] + array_fill_keys(array_filter(explode(' ', $resultRow['year']), 'strlen'), 0);

            $filters['archive'] = $resultRow['archive'] != '' ? array_merge($filters['archive'], [$resultRow['archive'] => 0]) : ($filters['archive'] === null ? [] : $filters['archive']);

            $trackingCount += 1;
        }

        $customResult = [
            'result' => $result,
            'totalHits' => count($result),
            'resultSet' => $resultSet,
            'filters' => $filters
        ];
        $customResult = $customResult + $resultArray;
        return $customResult;
    }

    /**
    * ReservedCharacterforES method
    *
    * @param keywords that are being searched
    *
    * @return updated keyword with added '\\' for every special character that occurs
    */
    public function ReservedCharactersforES($queryData)
    {
        foreach ($queryData as $field => $value) {
            $reservedcharacters = ['+','-','=','&&','||','>','<','!','(', ')','{','}','[',']','^','"','~','*','?',':','/'];
            $newkeyword = '';
            if (!empty($field)) {
                $newkeyword =  $queryData[$field];
                for ($i = 0; $i < strlen($newkeyword); $i++) {
                    $char = $newkeyword[$i];
                    $updatekeyword = '';
                    foreach ($reservedcharacters as $val) {
                        if ($char == $val) { // checks if any character in keyword matches the reserved character
                            $updatekeyword = substr_replace($newkeyword, '\\\\', $i, 0);
                            break;
                        }
                    }
                    if (!empty($updatekeyword)) {
                        $newkeyword = $updatekeyword;
                        $i = $i+2;
                    }
                }
            }
        }

        return $newkeyword;
    }

    /**
    * parentChild method
    *
    * @param name of the table for which you want the following array
    *
    * @return Array mapped as 'ParentEntity' => array of name of that entity's children
    */
    public function parentChild($tablename)
    {
        $Table = TableRegistry::get(Inflector::pluralize(Inflector::classify($tablename)))->find()->toArray();
        $parentChild = [];
        $IDwithField = [];
        foreach ($Table as $field) {
            $IDwithField[$field['id']] = $field[$tablename];
            if (!empty($field['parent_id'])) {
                if ($tablename == 'artifact_type' && $field['parent_id']==27) {
                    $IDwithField[27] = 'tablet or envelope';
                    $parentChild[$IDwithField[$field['parent_id']]] = [];
                } else {
                    $parentChild[$IDwithField[$field['parent_id']]] = [];
                }
            }
        }
        foreach ($Table as $field) {
            if (!empty($field['parent_id'])) {
                array_push($parentChild[$IDwithField[$field['parent_id']]], $field[$tablename]);
            }
        }
        return $parentChild;
    }

    /**
    * queryParentChild method
    *
    * @param  string that is being searched
    *
    * @return string query that is supposed to be added to the queries to also include children results
    */
    public function queryParentChild($requiredkeyword)
    {
        $parentChildMaterial = $this->parentChild('material');
        $parentChildGenre = $this->parentChild('genre');
        $parentChildAtype = $this->parentChild('artifact_type');
        $parentChildArray = $parentChildGenre + $parentChildMaterial + $parentChildAtype;

        $replacestring = '';
        foreach (array_keys($parentChildArray) as $parent) {
            if (strcasecmp($requiredkeyword, $parent) == 0) {
                foreach ($parentChildMaterial as $materialparent=>$value) {
                    if (strcasecmp($requiredkeyword, $materialparent) == 0) {
                        foreach ($parentChildMaterial[$materialparent] as $children) {
                            $replacestring .= ' OR materials:('.$children.')';
                        }
                        break;
                    }
                }
                foreach ($parentChildGenre as $genreparent => $value1) {
                    if (strcasecmp($requiredkeyword, $genreparent) == 0) {
                        foreach ($parentChildGenre[$genreparent] as $children) {
                            $replacestring .= ' OR genres:('.$children.')';
                        }
                        break;
                    }
                }
                foreach ($parentChildAtype as $Atypeparent => $value2) {
                    if (strcasecmp($requiredkeyword, $Atypeparent) == 0) {
                        foreach ($parentChildAtype[$Atypeparent] as $children) {
                            $replacestring .= ' OR atype:('.$children.')';
                        }
                        break;
                    }
                }
            }
        }
        return $replacestring;
    }

    public function expandQuery($queryData)
    {
        $mappingQueryFields = [
            'publication' => [
                'designation_exactref',
                'pdesignation',
                'bibtexkey',
                'year',
                'publisher',
                'school',
                'series',
                'title',
                'book_title',
                'chapter',
                'journal',
                'editors',
                'authors'
            ],
            'collection' => [
                'collection'
            ],
            'provenience' => [
                'provenience',
                'written_in',
                'region'
            ],
            'period' => [
                'period'
            ],
            'inscription' => [
                'atf',
                'translation',
                'transliteration_for_search'
            ],
            'id' => [
                'pdesignation',
                'adesignation',
                'excavation_no',
                'bibtexkey',
                'designation_exactref',
                'composite_no',
                'seal_no',
                'acomposite_no',
                'aseal_no',
                'museum_no',
                'id',
                'accession_no'
            ],
            'keyword' => [
                'designation_exactref',
                'pdesignation',
                'adesignation',
                'excavation_no',
                'bibtexkey',
                'year',
                'publisher',
                'school',
                'series',
                'title',
                'book_title',
                'chapter',
                'journal',
                'editors',
                'authors',
                'collection',
                'provenience',
                'written_in',
                'region',
                'period',
                'atf',
                'translation',
                'composite_no',
                'seal_no',
                'acomposite_no',
                'aseal_no',
                'museum_no',
                'id',
                'dates',
                'artifact_project',
                'inscription_project',
                'artifact_credit',
                'inscription_credit',
                'archive',
                'languages',
                'genres',
                'atype',
                'materials',
                'acomments',
                'condition_description',
                'elevation',
                'findspot_comments',
                'findspot_square',
                'artifact_preservation',
                'surface_preservation',
                'provenience_comments',
                'period_comments',
                'stratigraphic_level',
                'material_aspect',
                'material_color',
                'oclc',
                'volume',
                'pages',
                'organization',
                'address',
                'dates',
                'alternative_years'

            ]
        ];

        $fuzzyIdFields = [
            'composite_no',
            'seal_no',
            'acomposite_no',
            'aseal_no',
            'museum_no',
            'id'
        ];

        foreach ($queryData as $numberofkeywords => $value1) {
            foreach ($queryData[$numberofkeywords] as $field => $value) {
                if ($queryData[$numberofkeywords][$field] != 'AND' || $queryData[$numberofkeywords][$field] != 'OR') {
                    $updatedkeyword = $this->ReservedCharactersforES($queryData[$numberofkeywords]);
                    $queryData[$numberofkeywords][$field] = $updatedkeyword;
                }
            }
        }
        $queryStringIndividual = '';

        foreach ($queryData as $keyValue) {
            $key = array_keys($keyValue)[0];
            $value = array_values($keyValue)[0];

            if ($key !== 'operator') {
                $tempQueryArray = [];

                foreach ($mappingQueryFields[$key] as $field) {
                    $tempString = '';
                    // Processing input for Id's field
                    if (in_array($field, $fuzzyIdFields)) {
                        $tempString = $this->idSearchQuery($field, $value);
                    } else {
                        $tempString = $field.':('.$value.')';
                    }
                    array_push($tempQueryArray, $tempString);
                }
                $queryStringIndividual .= '('.implode(' OR ', $tempQueryArray).')';
            } else {
                $queryStringIndividual .= ' '.$value.' ';
            }
        }

        if (!empty($queryData[0]['keyword'])) {
            $parentChildString = $this->queryParentChild($queryData[0]['keyword']);
            $queryStringIndividual = substr_replace($queryStringIndividual, $parentChildString, -1, 0);
        }


        return $queryStringIndividual;
    }


    /**
     * searchAdvancedArtifacts method
     *
     * Advanced Search.
     * Refer: https://gitlab.com/cdli/framework/-/issues/298
     *
     * @param
     *
     * @return
     */
    public function simpleSearch($queryData, $settings, $filterData = [])
    {
        $keyword = $this->expandQuery($queryData);

        if (!$settings['canViewPrivateArtifacts']) {
            $keyword = '('.$keyword.')'.' AND is_public:true';
        }

        $bucketsquery = $this->bucketsQuery();

        $fieldsToBeDisplayedInSearchReuslts = ["id", "adesignation", "collection", "materials", "artifact_type_id", "atype", "period_id", "period", "provenience_id", "provenience", "genres", "languages", "authors", "year", "image_type", "atf", "translation", "dates", "artifact_credit", "inscription_credit", "artifact_project", "inscription_project", "archive", "museum_no", "accession_no", "written_in", "excavation_no", "composite_no", "seal_no", "acomposite_no", "aseal_no"];
        $ESquery['_source'] = [
            'includes' => $fieldsToBeDisplayedInSearchReuslts
        ];

        $ESquery['aggs'] = $bucketsquery;
        $ESquery['query'] = [
            'bool' =>[
                'filter' =>[
                    'term' => [
                        'retired' => 'false'
                    ]
                ],
                'must' =>[
                    ['query_string' => [
                        'query' => $keyword,
                        'default_operator' => 'and'
                    ]]
                ]
            ]

        ];
        $ESquery['sort'] = [
            '_score' => [
                'order' => 'desc'
            ]

        ];
        $ESquery['size'] = 10000;

        if (!empty($filterData)) {
            $filterWiseArray = $this->filterESQuery($filterData);
            $ESquery['query']['bool']['must'] = array_merge($ESquery['query']['bool']['must'], $filterWiseArray);
        }

        $result = [];

        $filters = [
            'materials' => [],
            'collection' => [],
            'museum_no' => [],
            'atype' => [],
            'period' => [],
            'provenience' => [],
            'publication' => [],
            'genres' => [],
            'languages' => [],
            'authors' => [],
            'year' => [],
            'image_type' => [],
            'transliteration' => [],
            'translation' => [],
            'accession_no' => [],
            'dates_referenced' => [],
            'written_in' => [],
            'archive' => [],
            'excavation_no' => []
        ];

        $resultArray = $this->getDataFromES('advanced_artifacts', json_encode($ESquery), 0);

        $totalHits = $resultArray['total'];

        // Calculating results size till previous pages.
        $totalPreviousResults = ($settings['Page'] == 1) ? 0 : ($settings['Page'] - 1) * $settings['PageSize'];

        // Set No. of result to be stored in session from overall results.
        $resultSet = (int)floor($totalPreviousResults / 10000);

        // Result Size stored in Session.
        $resultSize = 10000;

        // If there is any need for extra result which will be required on certain page where the set of results stored in session is not enough to display results (as set by page size) on that page.
        $checkIfExtraResultsRequired = 10000/$settings['PageSize'];

        if ($checkIfExtraResultsRequired != floor($checkIfExtraResultsRequired)) {
            $changeResultSet = floor(($totalPreviousResults + $settings['PageSize'])/10000) == $resultSet ? 0 : 1;

            if ($changeResultSet) {
                $resultSize += ($settings['PageSize'] - ($resultSize - $totalPreviousResults));
            }
        }

        $lowerBound = $resultSet * 10000;
        $upperBound = $lowerBound + $resultSize - 1;

        $trackingCount = 0;

        foreach ($resultArray['hits'] as $resultRow) {
            if ($lowerBound <= $trackingCount && $trackingCount <= $upperBound) {
                $resultRow['artifact_credit'] = (!$resultRow['artifact_credit']) ? [] : explode(', ', $resultRow['artifact_credit']);
                $resultRow['inscription_credit'] = (!$resultRow['inscription_credit']) ? [] : explode(', ', $resultRow['inscription_credit']);
                $resultRow['artifact_project'] = (!$resultRow['artifact_project']) ? [] : explode(', ', $resultRow['artifact_project']);
                $resultRow['inscription_project'] = (!$resultRow['inscription_project']) ? [] : explode(', ', $resultRow['inscription_project']);

                $project = array_unique(array_merge($resultRow['artifact_project'], $resultRow['inscription_project']));
                $credit = array_unique(array_merge($resultRow['artifact_credit'], $resultRow['inscription_credit']));
                $project = implode(', ', $project);
                $credit = implode(', ', $credit);

                $resultRow['acomposite_no'] = '';
                $resultRow['aseal_no'] = '';

                $result[$resultRow['id']] = [
                    'artifact' => [
                        'acomposite_no' => $resultRow['acomposite_no'],
                        'aseal_no' => $resultRow['aseal_no'],
                        'composite_no' => $resultRow['composite_no'],
                        'seal_no' => $resultRow['seal_no'],
                        'designation' => $resultRow['adesignation'],
                        'museum_no' => $resultRow['museum_no']
                    ],
                    'collection' => [
                        'collection' => $resultRow['collection']
                    ],
                    'material' => [
                        'material' => $resultRow['materials']
                    ],
                    'artifact_type' => [
                        'id' => $resultRow['artifact_type_id'],
                        'artifact_type' => $resultRow['atype']
                    ],
                    'period' => [
                        'period_id' => $resultRow['period_id'],
                        'period' => $resultRow['period']
                    ],
                    'provenience' => [
                        'provenience_id' => $resultRow['provenience_id'],
                        'provenience' => $resultRow['provenience']
                    ],
                    'genres' => [
                        'genres' => $resultRow['genres']
                    ],
                    'languages' => [
                        'languages' => $resultRow['languages']
                    ],
                    'dates' => [
                        'dates' => array_key_exists('dates', $resultRow) ? $resultRow['dates'] : 'N.A'
                    ],
                    'project' => [
                        'project' => $project
                    ],
                    'credit' => [
                        'credit' => $credit
                    ],
                    'accession_no' => [
                        'accession_no' => $resultRow['accession_no']
                    ],
                    'written_in' => [
                        'written_in' => $resultRow['written_in']
                    ],
                    'excavation_no' => [
                        'excavation_no' => $resultRow['excavation_no']
                    ],
                    'archive' => [
                        'archive' => $resultRow['archive']
                    ]
                ];
            }

            $filters['genres'] = array_merge($filters['genres'], array_fill_keys(array_filter(explode(' ; ', $resultRow['genres']), 'strlen'), 0));

            $filters['languages'] = array_merge($filters['languages'], array_fill_keys(array_filter(explode(' ; ', $resultRow['languages']), 'strlen'), 0));

            $filters['materials'] = array_merge($filters['materials'], array_fill_keys(array_filter(explode(', ', $resultRow['materials']), 'strlen'), 0));

            $filters['collection'] = $resultRow['collection'] != '' ? array_merge($filters['collection'], [$resultRow['collection'] => 0]) : $filters['collection'];

            $filters['atype'] = $resultRow['atype'] != '' ? array_merge($filters['atype'], [$resultRow['atype'] => 0]) : $filters['atype'];

            $filters['accession_no'] = $resultRow['accession_no'] != '' ? array_merge($filters['accession_no'], [$resultRow['accession_no'] => 0]) : $filters['accession_no'];

            $filters['archive'] = $resultRow['archive'] != '' ? array_merge($filters['archive'], [$resultRow['archive'] => 0]) : $filters['archive'];

            $filters['written_in'] = $resultRow['written_in'] != '' ? array_merge($filters['written_in'], [$resultRow['written_in'] => 0]) : $filters['written_in'];

            $filters['excavation_no'] = $resultRow['excavation_no'] != '' ? array_merge($filters['excavation_no'], [$resultRow['excavation_no'] => 0]) : $filters['excavation_no'];

            $filters['provenience'] = $resultRow['provenience'] != '' ? array_merge($filters['provenience'], [$resultRow['provenience'] => 0]) : $filters['provenience'];

            $filters['period'] = $resultRow['period'] != '' ? array_merge($filters['period'], [$resultRow['period'] => 0]) : $filters['period'];

            $filters['authors'] = array_merge($filters['authors'], array_fill_keys(array_filter(explode(PHP_EOL, $resultRow['authors']), 'strlen'), 0));

            $filters['year'] = $filters['year'] + array_fill_keys(array_filter(explode(' ', $resultRow['year']), 'strlen'), 0);

            $filters['archive'] = $resultRow['archive'] != '' ? array_merge($filters['archive'], [$resultRow['archive'] => 0]) : ($filters['archive'] === null ? [] : $filters['archive']);

            $trackingCount += 1;
        }

        $customResult = [
            'result' => $result,
            'totalHits' => $totalHits,
            'resultSet' => $resultSet,
            'filters' => $filters
        ];

        $customResult = $customResult + $resultArray;
        return $customResult;
    }

    /**
     * getInscriptionWithArtifactId method
     *
     * To get latest Inscription for specified artifact ID.
     *
     * @param
     * artifactId : Artifact ID
     *
     * @return array of Inscription for specific artifact ID. (Size = 1 if presesnt else empty array)
     */
    public function getInscriptionWithArtifactId($artifactId, $canViewPrivateInscriptions)
    {
        $ESquery['query'] = [
            'bool' => [
                'must' => [
                    [
                        'term' => [
                            'artifact_id' => $artifactId
                        ]
                    ],
                    [
                        'term' => [
                            'is_latest' => 'true'
                        ]
                    ]
                ]
            ]
        ];
        if (!$canViewPrivateInscriptions) {
            $PrivateImagesQueryArray = [
                [
                    'term' => [
                        'is_atf_public' => 'true'
                    ]
                ]
            ];
            $ESquery['query']['bool']['must'] = array_merge($ESquery['query']['bool']['must'], $PrivateImagesQueryArray);
        }

        $resultArray = $this->getDataFromES('inscription', json_encode($ESquery), 1);

        $result = [];

        if (!empty($resultArray['hits'])) {
            $inscription = $resultArray['hits'][0];
            $result = [
                'id' => $inscription['id'],
                'atf' => $inscription['atf'],
                'translation' => $inscription['translation'],
                'is_atf2conll_diff_resolved' => $inscription['is_atf2conll_diff_resolved']
            ];
        }

        return $result;
    }

    public function filterImages($artifactId, $canViewPrivateImages)
    {
        $ESquery['_source'] = [
            'includes' => 'asset_type'
        ];
        $ESquery['query'] = [
            'bool' => [
                'must' => [
                    [
                        'term' => [
                            'artifact_id' => $artifactId
                        ]
                    ]
                ]
            ]
        ];

        if (!$canViewPrivateImages) {
            $PrivateImagesQueryArray = [
                [
                    'term' => [
                        'are_artifact_images_public' => 'true'
                    ]
                ],
                [
                    'term' => [
                        'is_public' => 'true'
                    ]
                ]
            ];
            $ESquery['query']['bool']['must'] = array_merge($ESquery['query']['bool']['must'], $PrivateImagesQueryArray);
        }

        $resultArray = $this->getDataFromES('images', json_encode($ESquery), 1);

        return $resultArray['hits'];
    }

    /**
     * getPublicationWithArtifactId method
     *
     * To get Publication for specified artifact ID.
     *
     * @param
     * artifactId : Artifact ID
     *
     * @return array of Publication for specific artifact ID.
     */
    public function getPublicationWithArtifactId($artifactId)
    {
        $ESquery = [
            'query' => [
                'bool' => [
                    'must' => [
                        [
                            'term' => [
                                'artifact_id' => $artifactId
                            ]
                        ],
                        [
                            'term' => [
                                'accepted' => 'false'
                            ]
                        ]
                    ]
                ]
            ]
        ];

        $resultArray = $this->getDataFromES('entities_publications', json_encode($ESquery), 1);

        $resultArray = $resultArray['hits'];

        $result = [];

        if (!empty($resultArray)) {
            $publication = $resultArray[0];
            $result = [
                'publication' => [
                    'publication_id' => $publication['publication_id'],
                    'year' => $publication['year']
                ],
                'author' => [
                    'id' => $publication['authors_id'],
                    'authors' => $publication['author_authors']
                ]
            ];
        }

        return $result;
    }
    /**
     * BucketsData Method
     *
     * get data to print number of hits into parentheses besides the filter category
     *
     * @return array mapped as 'filtername' => number of hits
     */

    public function BucketsData($result = [])
    {
        $filtersarray =["period","languages","materials","genres","atype","provenience","year","collection","authors","image_type"];
        $mappedBucketsArray = [];
        foreach ($filtersarray as $filter) {
            if (!empty($result['_scroll_id'])) {
                $aggregationsresult = $result['aggregations'][$filter]['buckets'];

                if (!empty($aggregationsresult)) {
                    foreach ($aggregationsresult as $key => $value1) {
                        $filtercategory=$aggregationsresult[$key]['key'];
                        $filtercategorycount=$aggregationsresult[$key]['doc_count'];
                        $mappedBucketsArray[strval($filtercategory)] = $filtercategorycount;
                    }
                }
            }
        }
        foreach ($mappedBucketsArray as $filtername => $doccount) {
            if (!empty($mappedBucketsArray)) {
                if (strpos($filtername, ";") != false) {
                    $languagesarray = explode(" ; ", $filtername);
                    foreach ($languagesarray as $language) {
                        if (array_key_exists($language, $mappedBucketsArray) == false) {
                            $mappedBucketsArray[$language] = $doccount;
                        } else {
                            $mappedBucketsArray[$language] = ($mappedBucketsArray[$language] + $doccount);
                        }
                    }
                }
                if (strpos($filtername, "\n") != false) {
                    $authorsarray = explode("\n", $filtername);

                    foreach ($authorsarray as $author) {
                        if (array_key_exists($author, $mappedBucketsArray) == false) {
                            $mappedBucketsArray[$author] = $doccount;
                        } else {
                            $mappedBucketsArray[$author] = ($mappedBucketsArray[$author] + $doccount);
                        }
                    }
                }
            }
        }

        if (!empty($result['_scroll_id'])) {
            $numberofhits = count($result['hits']['hits']);
            $numberoftransliteration = 0;
            for ($i=0;$i<=$numberofhits;$i++) {
                if (!empty($result['hits']['hits'][$i]['_source']['atf'])) {
                    $numberoftransliteration++;
                } else {
                    continue;
                }
            }
            $mappedBucketsArray['transliteration'] = $numberoftransliteration;
        }
        return $mappedBucketsArray;
    }

    /**
     * getDataFromES method
     *
     * This functions fetched data for specific ElasticSearch query with required parameters using http from ElasticSearch Server.
     *
     * @param
     * esIndex : Elastic Search Index to be used
     * data : Query parameters with respective values
     *
     * @return array of total results count and array of results.
     */
    private function getDataFromES($esIndex, $data, $type)
    {
        [$elasticUser, $elasticPassword] = $this->elasticSearchCredentials();

        $resultArray = [];

        if ($type == 0) {
            $url = 'http://'.$elasticUser.':'.$elasticPassword.'@elasticsearch:9200/'.$esIndex.'/_search?scroll=5m';
        } else {
            $url = 'http://'.$elasticUser.':'.$elasticPassword.'@elasticsearch:9200/'.$esIndex.'/_search';
        }

        $result = $this->httpModule($url, $data);

        $resultArray['hits'] = array_column($result['hits']['hits'], '_source');

        if ($type == 0) {
            $totalResults = $result['hits']['total']['value'];
            $scrollId = $result['_scroll_id'];

            $scrollUrl = 'http://'.$elasticUser.':'.$elasticPassword.'@elasticsearch:9200/_search/scroll?';

            $scrollData = '
                {
                    "scroll": "5m",
                    "scroll_id": "'.$scrollId.'"
                }
            ';

            for ($loop = 0; $loop < ($totalResults/10000); $loop++) {
                $scrollResults = $this->httpModule($scrollUrl, $scrollData);

                $tempResult = array_column($scrollResults['hits']['hits'], '_source');

                array_push($resultArray['hits'], ...$tempResult);
            }

            $deleteScrollData = '
                {
                    "scroll_id": "'.$scrollId.'"
                }
            ';
            $deletedScroll = $this->httpModule($scrollUrl, $deleteScrollData, 'DELETE');
        }

        $resultArray['total'] = sizeof($resultArray['hits']);

        $mappedBucketsArray = $this -> BucketsData($result);

        $resultArray = $resultArray + $mappedBucketsArray; // appending buckets data with results

        return $resultArray;
    }

    private function httpModule($url, $data, $requestType = 'GET')
    {
        $http = new Client();

        if ($requestType === 'GET') {
            /**
             * Request body search using HTTP can only be done using POST request
             * Reference:
             * 1. https://stackoverflow.com/questions/978061/http-get-with-request-body,
             * 2. https://stackoverflow.com/questions/36939748/elasticsearch-get-request-with-request-body
            */
            $response = $http->post(
                $url,
                $data,
                ['type' => 'json']
            );
        } elseif ($requestType === 'DELETE') {
            $response = $http->delete(
                $url,
                $data,
                ['type' => 'json']
            );
        }
        $result = $response->getJson();

        return $result;
    }
}
