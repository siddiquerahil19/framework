<?php

namespace App\Controller\Component;

use Cake\Controller\Component\RequestHandlerComponent;
use Cake\Event\EventInterface;
use Cake\Http\Response;
use Cake\ORM\Entity;
use Cake\ORM\ResultSet;
use Cake\Routing\Router;
use Cake\Utility\Inflector;

class ApiComponent extends RequestHandlerComponent
{
    public $types = [
        // Bibliographic content types (MIME types from https://citation.crosscite.org/docs.html#sec-4)
        'Bibliography' => [
            // w/ accepted file extension
            'bib' => 'application/x-bibtex',
            'ris' => 'application/x-research-info-systems',

            // no defined file extension
            'csljson' => 'application/vnd.citationstyles.csl+json',
            'biblatex' => 'application/x-biblatex',
            'bibliography' => 'text/x-bibliography',
        ],
        // No official MIME types, but we need some anyway
        'Inscription' => [
            'atf' => 'text/x-c-atf',
            'cdli-conll' => 'text/x-cdli-conll',
            'conll-u' => 'text/x-conll-u',
            'jtf' => 'application/x-jtf+json',
        ],
        'LinkedData' => [
            'jsonld' => 'application/ld+json',
            'rdfjson' => 'application/rdf+json',
            'rdf' => 'application/rdf+xml',
            'nt' => 'application/n-triples',
            'ttl' => [
                'text/turtle',
                'application/turtle',
                'application/x-turtle'
            ],
        ],
        'TableExport' => [
            'csv' => 'text/csv',
            'tsv' => 'text/tab-separated-values',
            'xlsx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'txt' => 'text',
        ],
    ];

    public $viewClassMap = [
        'viewClassMap.json' => 'Json',

        // Bibliography
        'viewClassMap.bib' => 'Bibtex',
        'viewClassMap.ris' => 'Ris',
        'viewClassMap.csljson' => 'CslJson',
        'viewClassMap.biblatex' => 'Biblatex',
        'viewClassMap.bibliography' => 'Bibliography',

        // Inscription
        'viewClassMap.atf' => 'Atf',
        'viewClassMap.cdli-conll' => 'CdliConll',
        'viewClassMap.conll-u' => 'ConllU',
        'viewClassMap.jtf' => 'Jtf',

        // Linked data
        'viewClassMap.jsonld' => 'JsonLd',
        'viewClassMap.rdfjson' => 'RdfJson',
        'viewClassMap.xml' => 'Xml',
        'viewClassMap.rdf' => 'Xml',
        'viewClassMap.ttl' => 'Turtle',
        'viewClassMap.nt' => 'NTriples',

        // Table export
        'viewClassMap.csv' => 'Csv',
        'viewClassMap.tsv' => 'Tsv',
        'viewClassMap.xlsx' => 'Xlsx',
        'viewClassMap.txt' => 'Txt',
    ];

    public function initialize(array $config): void
    {
        $this->features = array_key_exists('features', $config) ? $config['features'] : [];

        $controller = $this->_registry->getController();
        foreach ($this->features as $feature) {
            foreach ($this->types[$feature] as $ext => $header) {
                $controller->getResponse()->setTypeMap($ext, $header);
            }
        }

        $this->setConfig($this->viewClassMap);
    }

    // =======
    // METHODS
    // =======

    /**
     * @param string $type
     * @return array
     */
    public function accepts($type = null): array
    {
        if (!empty($this->ext)) {
            // Treat the ?format= URL param as a file extension
            return [$this->ext];
        } else {
            return parent::accepts($type);
        }
    }

    /**
     * Check whether the request is being handled by the API.
     *
     * @param string $type
     * @return bool
     */
    public function requestsApi($type = null)
    {
        if (is_null($type)) {
            $type = $this->prefers();
        }

        if ($type === 'json') {
            return true;
        }

        return $this->requestsFormat($this->features, $type);
    }

    /**
     * Check whether the request is being handled by the API.
     *
     * @param string[] $features
     * @param string $type
     * @return bool
     */
    public function requestsFormat($features, $type = null)
    {
        if (is_null($type)) {
            $type = $this->prefers();
        }

        foreach ($features as $feature) {
            if (isset($this->types[$feature][$type])) {
                return true;
            }
        }

        return false;
    }

    // ======
    // EVENTS
    // ======

    /**
     * @param \Cake\Event\EventInterface $event Event.
     * @return \Cake\Http\Response|null|void
     */
    public function startup(EventInterface $event): void
    {
        parent::startup($event);

        // Treat the ?format= URL param as a file extension
        $format = $this->getController()->getRequest()->getParam('format');

        if (is_null($format)) {
            $format = $this->getController()->getRequest()->getQuery('format');

            if ($format) {
                $this->ext = $format;
            }
        } else {
            $this->ext = $format;
        }
    }

    /**
     * @param \Cake\Event\EventInterface $event
     * @return \Cake\Http\Response|null
     */
    public function beforeRender(EventInterface $event): void
    {
        $type = $this->prefers();
        if (!$this->requestsApi($type)) {
            return;
        }

        $controller = $event->getSubject();
        $response = $controller->getResponse()->withHeader('Access-Control-Allow-Origin', '*')->withType($type);
        $controller->setResponse($response);

        if ($this->requestsFormat(['Bibliography'], $type)) {
            $options = ['format' => 'text'];
            if ($type == 'bibliography') {
                $options['format'] = 'html';
                $style = $controller->getRequest()->getQuery('style');
                if (!empty($style)) {
                    $options['template'] = $style;
                }
            }

            $controller->set('_format', $type);
            $controller->set('_options', $options);
        } elseif ($this->requestsFormat(['TableExport'], $type)) {
            $controller->set('_tableType', $type);
        }

        parent::beforeRender($event);
    }

    /**
     * @param \Cake\Event\EventInterface $event
     * @param array $parts
     * @param \Cake\Http\Response $response
     * @return \Cake\Http\Response|null
     */
    public function beforeRedirect(EventInterface $event, $parts, Response $response)
    {
        if (!$this->requestsApi()) {
            return;
        }

        $response = $response->withHeader('Access-Control-Allow-Origin', '*');

        $request = $this->getController()->getRequest();
        if ($request->getMethod() === 'OPTIONS') {
            return $response->withoutHeader('Location')->withStatus(204);
        }

        $format = $request->getParam('format');
        if (!is_null($format)) {
            $parts['format'] = $format;
            return $response->withLocation(Router::url($parts));
        }

        return $response;
    }

    /**
     * @param \Cake\Event\EventInterface $event
     * @return \Cake\Http\Response|null
     */
    public function afterFilter(EventInterface $event)
    {
        $controller = $event->getSubject();
        if ($controller->getResponse()->hasHeader('Content-Disposition')) {
            return;
        }

        $type = $this->prefers();

        if ($this->requestsApi($type) && $this->requestsFormat(['Inscription', 'LinkedData', 'TableExport'], $type)) {
            $vars = $controller->viewBuilder()->getVars();

            if (array_key_exists('_serialize', $vars)) {
                $serialized = $vars['_serialize'];
                $serialized = is_array($serialized) ? $serialized : [$serialized];
                $serialized = array_map(function ($key) use ($vars) {
                    $var = $vars[$key];
                    if ($var instanceof ResultSet) {
                        $var = (array) $var;
                    } elseif (!is_array($var)) {
                        $var = [$var];
                    }
                    return $var;
                }, $serialized);
                $serialized = array_merge(...$serialized);

                if (is_array($serialized) && array_key_exists(0, $serialized) && $serialized[0] instanceof Entity) {
                    $file = Inflector::dasherize($serialized[0]->getSource());

                    if (count($serialized) == 1 && $serialized[0]->has('id')) {
                        $file .= '_' . $serialized[0]->id;
                    }

                    $file .= '.' . $type;

                    $controller->setResponse($controller->getResponse()->withDownload($file));
                }
            }
        }
    }

    /**
     * Events supported by this component.
     *
     * @return array<string, mixed>
     */
    public function implementedEvents(): array
    {
        return [
            'Controller.startup' => 'startup',
            'Controller.beforeRender' => 'beforeRender',
            'Controller.beforeRedirect' => 'beforeRedirect',
            'Controller.shutdown' => 'afterFilter'
        ];
    }
}
