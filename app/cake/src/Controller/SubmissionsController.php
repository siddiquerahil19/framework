<?php

declare(strict_types=1);

namespace App\Controller;

/**
 * Submissions Controller
 *
 * @property \App\Model\Table\OjsSubmissionsTable $OjsSubmissions
 * @method \App\Model\Entity\OjsSubmission[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SubmissionsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['OjsSections'],
        ];
        $submissions = $this->paginate($this->OjsSubmissions);

        $this->set(compact('submissions'));
    }

    /**
     * View method
     *
     * @param string|null $id Submission id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $submission = $this->OjsSubmissions->get($id, [
            'contain' => ['OjsSections',
                          'OjsReviewAssignments',
                          'OjsReviewAssignments.OjsUserSettings',
                          'OjsReviewAssignments.OjsSubmissionComments',
                          'OjsQueries',
                          'OjsQueries.OjsNotes',
                          'OjsQueries.OjsNotes.OjsUserSettings'],
        ]);

        $this->set(compact('submission'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $submission = $this->OjsSubmissions->newEmptyEntity();
        if ($this->request->is('post')) {
            $submission = $this->OjsSubmissions->patchEntity($submission, $this->request->getData());
            if ($this->OjsSubmissions->save($submission)) {
                $this->Flash->success(__('The submission has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The submission could not be saved. Please, try again.'));
        }
        $contexts = $this->OjsSubmissions->OjsContexts->find('list', ['limit' => 200]);
        $sections = $this->OjsSubmissions->OjsSections->find('list', ['limit' => 200]);
        $currentPublications = $this->OjsSubmissions->OjsCurrentPublications->find('list', ['limit' => 200]);
        $stages = $this->OjsSubmissions->OjsStages->find('list', ['limit' => 200]);
        $this->set(compact('submission', 'contexts', 'sections', 'currentPublications', 'stages'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Submission id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $submission = $this->OjsSubmissions->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $submission = $this->OjsSubmissions->patchEntity($submission, $this->request->getData());
            if ($this->OjsSubmissions->save($submission)) {
                $this->Flash->success(__('The submission has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The submission could not be saved. Please, try again.'));
        }
        $contexts = $this->OjsSubmissions->OjsContexts->find('list', ['limit' => 200]);
        $sections = $this->OjsSubmissions->OjsSections->find('list', ['limit' => 200]);
        $currentPublications = $this->OjsSubmissions->OjsCurrentPublications->find('list', ['limit' => 200]);
        $stages = $this->OjsSubmissions->OjsStages->find('list', ['limit' => 200]);
        $this->set(compact('submission', 'contexts', 'sections', 'currentPublications', 'stages'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Submission id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $submission = $this->OjsSubmissions->get($id);
        if ($this->OjsSubmissions->delete($submission)) {
            $this->Flash->success(__('The submission has been deleted.'));
        } else {
            $this->Flash->error(__('The submission could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
