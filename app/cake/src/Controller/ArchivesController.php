<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Archives Controller
 *
 * @property \App\Model\Table\ArchivesTable $Archives
 *
 * @method \App\Model\Entity\Archive[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ArchivesController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('GeneralFunctions');
        $this->loadComponent('Api', ['features' => ['LinkedData','TableExport']]);

        // Set access for public.
        $this->Auth->allow(['index', 'view']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Proveniences'],
            'maxLimit' => $this->Archives->find()->count()
        ];
        $archives = $this->paginate($this->Archives);

        $access_granted = $this->GeneralFunctions->checkIfRolesExists([1,2]);

        $this->set(compact('archives', 'access_granted'));
        $this->set('_serialize', 'archives');
    }

    /**
     * View method
     *
     * @param string|null $id Archive id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $archive = $this->Archives->get($id, [
            'contain' => ['Proveniences']
        ]);

        $access_granted = $this->GeneralFunctions->checkIfRolesExists([1,2]);

        $this->set(compact('access_granted'));
        $this->set('archive', $archive);
        $this->set('_serialize', 'archive');
    }
}
