<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * ExternalResources Controller
 *
 * @property \App\Model\Table\ExternalResourcesTable $ExternalResources
 *
 * @method \App\Model\Entity\ExternalResource[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ExternalResourcesController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('Api', ['features' => ['LinkedData', 'TableExport']]);
        $this->loadComponent('GeneralFunctions');

        // Set access for public.
        $this->Auth->allow(['index', 'view']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $externalResources = $this->paginate($this->ExternalResources, [
            'maxLimit' => $this->ExternalResources->find()->count()
        ]);
        $allAbbrev = $this->ExternalResources->find()->extract('abbrev')->toArray();
        $allLinks = $this->ExternalResources->find()->extract('project_url')->toArray();
        $abbrev_arr = array_combine($allAbbrev, $allLinks);
        ksort($abbrev_arr);
        $access_granted = $this->GeneralFunctions->checkIfRolesExists([1]);
        $this->set(compact('externalResources', 'access_granted', 'abbrev_arr'));
        $this->set('_serialize', 'externalResources');
    }

    /**
     * View method
     *
     * @param string|null $id External Resource id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $externalResource = $this->ExternalResources->get($id, [
            'contain' => []
        ]);

        $artifacts = $this->loadModel('ArtifactsExternalResources');
        $count = $artifacts->find('list', ['conditions' => ['external_resource_id' => $id]])->count();
        $access_granted = $this->GeneralFunctions->checkIfRolesExists([1]);

        $this->set(compact('externalResource', 'count', 'access_granted'));
        $this->set('_serialize', 'externalResource');
    }
}
