<?php

namespace App\Controller;

use App\Datasource\ArrayQuery;
use Cake\Http\Exception\RecordNotFoundException;

/**
 * ArtifactAssetAnnotations Controller
 *
 * @property \App\Model\Table\ArtifactAssetAnnotationsTable $ArtifactAssetAnnotations
 * @method \App\Model\Entity\ArtifactAssetAnnotation[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ArtifactAssetAnnotationsController extends AppController
{
    public function initialize(): void
    {
        parent::initialize();
        $this->loadModel('ArtifactAssetAnnotations');
        $this->loadComponent('GranularAccess');
        $this->Auth->allow(['add']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $canSubmitEdits = $this->GranularAccess->canSubmitEdits();

        $session = $this->getRequest()->getSession();
        $pendingUploads = 'ArtifactAssetAnnotations.uploads';
        $uploadErrors = 'ArtifactAssetAnnotations.errors';

        if (!$this->request->is(['post', 'put', 'patch'])) {
            // If there are already uploads pending, show them
            if ($session->check($pendingUploads)) {
                $annotations = $session->read($pendingUploads);

                if ($session->check($uploadErrors)) {
                    $errors = $session->read($uploadErrors);
                    return $this->setAction('errors', $annotations, $canSubmitEdits, $errors);
                }

                return $this->setAction('confirm', $annotations, $canSubmitEdits);
            }

            $this->set('canSubmitEdits', $canSubmitEdits);
            return;
        }

        $input = null;

        // Handle uploaded files
        $file = $this->getRequest()->getData('anno_file');
        if (!is_null($file) && $file->getError() != UPLOAD_ERR_NO_FILE) {
            $file = $file->getStream()->detach();
            $file = stream_get_contents($file);
            $input = json_decode($file, true);
        }

        if (is_null($input)) {
            // Overwrite with missing input ("cancel")
            $session->delete($pendingUploads);
            $session->delete($uploadErrors);
            $this->set('canSubmitEdits', $canSubmitEdits);
            return;
        }

        $annotations = [];
        $errors = [];
        $hasErrors = false;
        foreach ($input as $data) {
            try {
                $annotation = $this->ArtifactAssetAnnotations->newEntityFromJson($data);
                $hasErrors = $hasErrors || $annotation->hasErrors();
                $annotations[] = $annotation;
                if ($annotation->hasErrors()) {
                    $hasErrors = true;
                }
            } catch (\Throwable $error) {
                $errors[] = $error->getMessage();
                $hasErrors = true;
            }
        }

        // Store updates in sessions
        if (!empty($annotations)) {
            $session->write($pendingUploads, $annotations);
        }

        // Redirect to error page if necessary.
        if ($hasErrors) {
            $session->write($uploadErrors, $errors);
            return $this->setAction('errors', $annotations, $canSubmitEdits, $errors);
        } else {
            $session->delete($uploadErrors);
        }

        return $this->setAction('confirm', $annotations, $canSubmitEdits);
    }

    public function confirm(array $annotations, bool $canSubmitEdits)
    {
        $annotations = $this->paginate(new ArrayQuery($annotations));
        $this->set('annotations', $annotations);
        $this->set('canSubmitEdits', $canSubmitEdits);
    }

    public function errors(array $annotations, bool $canSubmitEdits, array $errors)
    {
        $annotations = array_filter($annotations, function ($annotation) {
            return $annotation->hasErrors();
        });
        $annotations = $this->paginate(new ArrayQuery($annotations));
        $this->set('annotations', $annotations);
        $this->set('errors', $errors);
        $this->set('canSubmitEdits', $canSubmitEdits);
    }

    // /**
    //  * Edit method
    //  *
    //  * @param string|null $id Artifact Asset Annotation id.
    //  * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
    //  * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    //  */
    // public function edit($id = null)
    // {
    //     $artifactAssetAnnotation = $this->ArtifactAssetAnnotations->get($id, [
    //         'contain' => [],
    //     ]);
    //     if ($this->request->is(['patch', 'post', 'put'])) {
    //         $artifactAssetAnnotation = $this->ArtifactAssetAnnotations->patchEntity($artifactAssetAnnotation, $this->request->getData());
    //         if ($this->ArtifactAssetAnnotations->save($artifactAssetAnnotation)) {
    //             $this->Flash->success(__('The artifact asset annotation has been saved.'));
    //
    //             return $this->redirect(['action' => 'index']);
    //         }
    //         $this->Flash->error(__('The artifact asset annotation could not be saved. Please, try again.'));
    //     }
    //     $annotations = $this->ArtifactAssetAnnotations->Annotations->find('list', ['limit' => 200]);
    //     $artifactAssets = $this->ArtifactAssetAnnotations->ArtifactAssets->find('list', ['limit' => 200]);
    //     $updateEvents = $this->ArtifactAssetAnnotations->UpdateEvents->find('list', ['limit' => 200]);
    //     $this->set(compact('artifactAssetAnnotation', 'annotations', 'artifactAssets', 'updateEvents'));
    // }
    //
    // /**
    //  * Delete method
    //  *
    //  * @param string|null $id Artifact Asset Annotation id.
    //  * @return \Cake\Http\Response|null|void Redirects to index.
    //  * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    //  */
    // public function delete($id = null)
    // {
    //     $this->request->allowMethod(['post', 'delete']);
    //     $artifactAssetAnnotation = $this->ArtifactAssetAnnotations->get($id);
    //     if ($this->ArtifactAssetAnnotations->delete($artifactAssetAnnotation)) {
    //         $this->Flash->success(__('The artifact asset annotation has been deleted.'));
    //     } else {
    //         $this->Flash->error(__('The artifact asset annotation could not be deleted. Please, try again.'));
    //     }
    //
    //     return $this->redirect(['action' => 'index']);
    // }
}
