<?php

namespace App\Controller;

use App\Controller\AppController;
use App\Datasource\ArrayQuery;
use App\Model\Entity\Artifact;
use Cake\Datasource\ConnectionManager;
use Cake\Datasource\Exception\RecordNotFoundException;

/**
 * ArtifactsUpdates Controller
 *
 * @property \App\Model\Table\ArtifactsUpdatesTable $ArtifactsUpdates
 *
 * @method \App\Model\Entity\artifactsUpdate[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ArtifactsUpdatesController extends AppController
{
    public function initialize(): void
    {
        parent::initialize();
        $this->loadModel('Artifacts');
        $this->loadModel('ArtifactsUpdates');
        $this->loadComponent('GranularAccess');

        $this->Auth->allow(['view', 'add', 'upload']);
    }

    /**
     * View method
     *
     * @param string|null $id Artifacts Date id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $artifactsUpdate = $this->ArtifactsUpdates->get($id, [
            'contain' => [
                'Artifacts',
                'UpdateEvents' => ['Creators', 'Authors', 'Reviewers']
            ]
        ]);

        if (!$this->GranularAccess->canAccessEdits($artifactsUpdate->update_event)) {
            throw new UnauthorizedException();
        }

        if (($artifactsUpdate->has('artifact_id') && !$artifactsUpdate->artifact->is_public) &&
            !$this->GranularAccess->canViewPrivateArtifact()) {
            throw new UnauthorizedException();
        }

        $old = $this->ArtifactsUpdates->getPreviousUpdates($artifactsUpdate, false);
        $this->GranularAccess->amendArtifactsUpdate([$artifactsUpdate, $old]);
        $this->set(['artifactsUpdate' => $artifactsUpdate, 'old' => $old]);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null
     */
    public function add()
    {
        ini_set('max_execution_time', 300);

        $canSubmitEdits = $this->GranularAccess->canSubmitEdits();
        $session = $this->getRequest()->getSession();
        $uploadErrors = 'ArtifactsUpdates.errors';
        $pendingUploads = 'ArtifactsUpdates.uploads';

        if (!$this->request->is('post')) {
            if ($session->check($pendingUploads)) {
                $updates = $session->read($pendingUploads);

                if ($session->check($uploadErrors)) {
                    $errors = $session->read($uploadErrors);
                    return $this->setAction('errors', $updates, $errors);
                }

                return $this->setAction('confirm', $updates, $canSubmitEdits);
            }

            $this->set('canSubmitEdits', $canSubmitEdits);
            return;
        }

        // Overwrite with empty file ("cancel")
        if (is_null($this->request->getData('csv'))) {
            $session->delete($pendingUploads);
            $session->delete($uploadErrors);
            $this->set('canSubmitEdits', $canSubmitEdits);
            return;
        }

        // Get options
        $concatenate = $this->request->getData('concatenate_data');

        // Validate file
        $file = $this->request->getData('csv');
        if ($file->getClientMediaType() != 'text/csv') {
            return $this->setAction('errors', [], ['Expected a CSV file']);
        }

        // Parse CSV file
        $handle = $file->getStream()->detach();
        if (fgets($handle, 4) !== "\u{FEFF}") {
            rewind($handle);
        }
        $header = fgetcsv($handle);
        $errors = [];
        $updates = [];
        $hasErrors = false;

        $connection = ConnectionManager::get('default');
        $connection->enableQueryLogging(false);
        while (($row = fgetcsv($handle)) !== false) {
            $new_data;

            try {
                $new_data = array_combine($header, $row);
            } catch (\Error $error) {
                $update = $this->ArtifactsUpdates->newEmptyEntity();
                $update->setError('row', __('This row does not contain the same number of values as expected from the header'));
                $updates[] = $update;
                $hasErrors = true;
                continue;
            }

            // Parse row
            try {
                if (!$this->GranularAccess->isAdmin()) {
                    $new_data = array_diff_key($new_data, Artifact::$privateFlatFields);
                }

                $update = $this->ArtifactsUpdates->newEntityFromFlatData($new_data, $concatenate);

                // Check whether user has access to artifact
                if ($update->has('artifact_id')) {
                    $artifact = $this->Artifacts->get($update->artifact_id);

                    if (!$artifact->is_public && !$this->GranularAccess->canViewPrivateArtifact()) {
                        throw new RecordNotFoundException();
                    }

                    $artifact = null;
                }

                if (empty($update->getChanged())) {
                    continue;
                }

                // Validate fields within update
                if (!$update->validate()) {
                    $hasErrors = true;
                }

                $updates[] = $update;
            } catch (RecordNotFoundException $error) {
                $update = $this->ArtifactsUpdates->newEntityFromFlatData(
                    array_diff_key($new_data, ['artifact_id' => null])
                );
                $update->setError('artifact_id', __('Record {0} not found in table "artifacts"', $new_data['artifact_id']));
                $updates[] = $update;
                $hasErrors = true;
            } catch (\Error $error) {
                $errors[] = $error->getMessage();
                $hasErrors = true;
            }
        }
        $connection->enableQueryLogging(true);

        // Store updates in sessions
        if (!empty($updates)) {
            $session->write($pendingUploads, $updates);
        }

        // Redirect to error page if necessary.
        if ($hasErrors) {
            $session->write($uploadErrors, $errors);
            return $this->setAction('errors', $updates, $errors);
        } else {
            $session->delete($uploadErrors);
        }

        return $this->setAction('confirm', $updates, $canSubmitEdits);
    }

    /**
     * @param \App\Model\Entity\ArtifactsUpdate[] $updates
     * @param string[] $errors
     * @return \Cake\Http\Response|null
     */
    public function errors(array $updates, array $errors)
    {
        $updates = array_filter($updates, function ($update) {
            return $update->hasErrors();
        });
        $updates = $this->paginate(new ArrayQuery($updates));

        $this->set('artifactUpdates', $updates);
        $this->set('errors', $errors);
    }

    /**
     * @param \App\Model\Entity\ArtifactsUpdate[] $updates
     * @return \Cake\Http\Response|null
     */
    public function confirm(array $updates, bool $canSubmitEdits)
    {
        $noChanges = empty($updates);
        $updates = $this->paginate(new ArrayQuery($updates));

        foreach ($updates as $update) {
            if ($update->has('artifact_id')) {
                $update->artifact = $this->Artifacts->get($update->artifact_id, [
                    'contain' => [
                        'Archives',
                        'ArtifactsShadow',
                        'ArtifactTypes',
                        'Collections',
                        'Composites',
                        'ExternalResources',
                        'Genres',
                        'Languages',
                        'Materials',
                        'Origins',
                        'Periods',
                        'Proveniences',
                        'Publications',
                        'Seals'
                    ]
                ]);
            }
        }

        $this->set('artifactUpdates', $updates);
        $this->set('canSubmitEdits', $canSubmitEdits);
        $this->set('noChanges', $noChanges);
    }
}
