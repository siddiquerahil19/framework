<?php

namespace App\Controller;

use App\Controller\AppController;
use App\Datasource\DiffPaginator;
use App\Model\Entity\Artifact;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Http\Exception\UnauthorizedException;
use Cake\ORM\TableRegistry;
use Cake\Http\Client;
use Ghunti\HighchartsPHP\Highchart;
use Ghunti\HighchartsPHP\HighchartJsExpr;
use App\Utility\CdliProcessor\Convert;
use Cake\Database\Query;

/**
 * Artifacts Controller
 *
 * @property \App\Model\Table\ArtifactsTable $Artifacts
 * @property \App\Controller\Component\RequestHandlerComponent $RequestHandler
 *
 * @method \App\Model\Entity\Artifact[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ArtifactsController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('GeneralFunctions');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('GranularAccess');
        $this->loadComponent('Api', ['features' => ['Bibliography', 'Inscription', 'LinkedData', 'TableExport']]);
        $this->loadComponent('Paginator');

        // Set access for public.
        $this->Auth->allow([
            'index',
            'view',
            'resolve',
            'resolveInscription',
            'history',
            'composites',
            'compositesScore',
            'reader',
            'add',
            'edit',
            'seals',
            'downloadChemistryCSV'
        ]);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        if ($this->RequestHandler->prefers() == 'html') {
            return $this->redirect([
                'controller' => 'Advancedsearch',
                'action' => 'index'
            ]);
        }

        if ($this->Api->requestsFormat(['TableExport'])) {
            $this->paginate = [
                'contain' => [
                    'ArtifactsUpdates' => [
                        'UpdateEvents',
                        'conditions' => ['UpdateEvents.status' => 'approved'],
                        'sort' => ['UpdateEvents.approved' => 'ASC']
                    ]
                ],
                'conditions' => ['Artifacts.is_public' => true],
                'order' => ['Artifacts.id' => 'ASC'],
            ];

            if ($this->GranularAccess->canViewPrivateArtifact()) {
                // Remove conditions for privileged users.
                unset($this->paginate['conditions']);
            }

            $artifacts = $this->paginate($this->Artifacts)->toArray();
            $artifacts = array_map(function ($artifact) {
                return $this->Artifacts->ArtifactsUpdates->mergeUpdates($artifact->artifacts_updates);
            }, $artifacts);

            $this->set(compact('artifacts'));
            $this->set('_serialize', 'artifacts');
            $this->set('_admin', $this->GranularAccess->isAdmin());

            return;
        }

        $this->paginate = [
            'contain' => [
                'Proveniences',
                'Origins',
                'Periods',
                'ArtifactTypes',
                'Archives',
                'Collections',
                'Dates',
                'ExternalResources',
                'Genres',
                'Languages',
                'Materials',
                'MaterialAspects',
                'MaterialColors',
                'Publications',
                // TODO RetiredArtifacts
            ],
            'conditions' => ['Artifacts.is_public' => true],
            'order' => ['Artifacts.id' => 'ASC'],
        ];

        if ($this->GranularAccess->canViewPrivateArtifact()) {
            // Remove conditions for privileged users.
            unset($this->paginate['conditions']);
        }

        $artifacts = $this->paginate($this->Artifacts);

        foreach ($artifacts as $artifact) {
            $this->GranularAccess->amendArtifact($artifact);

            $artifact->artifacts_updates = $this->Artifacts->ArtifactsUpdates->find()
                ->contain(['UpdateEvents' => ['Creators', 'Authors']])
                ->where(['UpdateEvents.status' => 'approved', 'artifact_id' => $artifact->id]);
        }

        $this->set(compact('artifacts'));
        $this->set('_serialize', 'artifacts');
        $this->set('_admin', $this->GranularAccess->isAdmin());
    }


    //composites controller
    /**
     *  Composites method
     *
     * @return \Cake\Http\Response|void
     */
    public function composites($compositetype = null)
    {
        $this->paginate = [
            'limit' => 20,
            'contain' => ['Composites', 'Witnesses', 'Witnesses.Inscriptions', 'Genres'],
            'sortableFields' => [
                'composite_no',
                'designation',
            ]
        ];

        $genreIdMap = [
            'Lexical' => 4,
            'Literary' => 5,
            'Royal' => 6,
            'Scientific' => 7
        ];

        if (!is_null($compositetype)) {
            $artifacts = $this->Artifacts->find('all')
                        ->contain(['Composites', 'Witnesses', 'Witnesses.Inscriptions'])
                        ->innerJoinWith('Genres', function (\Cake\ORM\Query $q) use ($genreIdMap, $compositetype) {
                            return $q->where(['Genres.id' => $genreIdMap[$compositetype]]);
                        })
                        ->where([
                            'Artifacts.composite_no !=' => '',
                            'Artifacts.composite_no NOT LIKE' => 'needed'
                        ]);

            $artifacts = $this->paginate($artifacts);
            $this->set(compact('artifacts', 'compositetype'));
        }
    }

    public function compositesScore($compositenumber = null)
    {
        $this->set('score', $compositenumber);
        $artifactscore = $this->Artifacts->find('all')
                        ->contain(['Composites', 'Witnesses', 'Witnesses.Inscriptions'])
                        ->where([
                            'Artifacts.composite_no =' => $compositenumber
                        ]);
        $show404 = false;
        $text = "";
        $designation = "";
        $artifactId = "";
        $incArr = array();

        foreach ($artifactscore as $artifactscore) {
            $artifactId = $artifactscore->id;
            $designation = $artifactscore->designation;
            for ($i = 1;$i < count($artifactscore->witnesses);$i++) {
                if ($artifactscore->witnesses[$i]->composite_no == '' && (!is_null($artifactscore->witnesses[$i]->inscription))) {
                    array_push($incArr, $artifactscore->witnesses[$i]->inscription);
                    $text = $text.$artifactscore->witnesses[$i]->inscription->atf;
                }
            }
        }

        if (count($artifactscore->witnesses)==0) {
            throw new UnauthorizedException();
        }

        $this->set(compact('text', 'artifactId', 'designation', 'incArr', 'show404'));
    }


    /**
     * Resolve identifiers
     *
     * @param string|null $id Artifact, composite or seal id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function resolve($id)
    {
        if (is_null($id)) {
            $this->setAction('index');
        }

        $artifact = $this->Artifacts
            ->find('number', ['id' => $id])
            ->select(['id', 'is_public'])
            ->first();

        if (is_null($artifact)) {
            throw new RecordNotFoundException($id . ' not found');
        }

        // Do not redirect private artifacts
        if (!$artifact->is_public &&
            !$this->GranularAccess->canViewPrivateArtifact()) {
            throw new UnauthorizedException();
        }

        $this->redirect([
            'action' => 'view',
            '_ext' => $this->getRequest()->getParam('_ext'),
            $artifact->id
        ]);
    }

    /**
     * Resolve inscription
     *
     * @param string|null $id Artifact, composite or seal id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function resolveInscription($id)
    {
        $inscription = $this->Artifacts->getLatestInscription($id);
        $this->redirect([
            'controller' => 'Inscriptions',
            'action' => 'view',
            '_ext' => $this->getRequest()->getParam('_ext'),
            $inscription
        ]);
    }

    public function setGraphData($type, $sealChemistry, $elementCount)
    {
        $sealArea = array();
        $dataXAxis = array();
        $headers = array();
        foreach ($sealChemistry as $sealdata) {
            $finalJson = json_decode(json_decode(json_encode($sealdata->chemistry, JSON_HEX_TAG)), true);
            $headers = array_merge($headers, array_keys($finalJson));
        }

        $headers = array_values(array_unique($headers));
        foreach ($sealChemistry as $sealdata) {
            $finalJson = json_decode(json_decode(json_encode($sealdata->chemistry, JSON_HEX_TAG)), true);
            $sealArea[$sealdata->reading]["XAxis"] = array();
            $sealArea[$sealdata->reading]["YAxis"] = array();

            foreach ($headers as $col) {
                array_push($sealArea[$sealdata->reading]["XAxis"], $col);
                if (isset($finalJson[$col])) {
                    array_push($sealArea[$sealdata->reading]["YAxis"], doubleval($finalJson[$col]['value']));
                } else {
                    array_push($sealArea[$sealdata->reading]["YAxis"], 0.0);
                }
            }
        }

        $chart = new Highchart();
        $chart->chart->renderTo = "graphcontainer";
        $chart->title->text = "";

        $chart->yAxis->min = 0;
        $chart->yAxis->title->text = "Percent";

        $chart->plotOptions->column->pointPadding = 0.2;
        $chart->plotOptions->column->borderWidth = 0;
        $chart->credits->enabled = false;

        $chart->tooltip->formatter = new HighchartJsExpr("function() {
            return '' + this.x +': '+ this.y +' %';}");

        foreach ($sealArea as $key=>$val) {
            if ($elementCount != 'All') {
                $dataXAxis = count($dataXAxis) > count(array_slice($val['XAxis'], 0, $elementCount)) ? $dataXAxis : array_slice($val['XAxis'], 0, $elementCount);
                $chart->series[] = array(
                    'type' => $type,
                    'name' => $key,
                    'data' => array_slice($val['YAxis'], 0, $elementCount)
                );
            } else {
                $dataXAxis = count($dataXAxis) > count($val['XAxis']) ? $dataXAxis : $val['XAxis'];
                $chart->series[] = array(
                    'type'=>$type,
                    'name'=> $key,
                    'data' => $val['YAxis']
                );
            }
        }
        $chart->xAxis->categories = $dataXAxis;

        return array($sealArea, $chart, $headers);
    }

    /**
     * View method
     *
     * @param string|null $id Artifact id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $associations = [
            'contain' => [
                'Proveniences',
                'Origins',
                'Periods',
                'ArtifactTypes',
                'Archives',
                'Collections',
                'Dates',
                'Dates.Years',
                'ExternalResources',
                'Genres',
                'Languages',
                'Materials',
                'MaterialAspects',
                'MaterialColors',
                'Publications',
                'Publications.Authors' => [
                    'sort' => ['AuthorsPublications.sequence' => 'ASC']
                    ],
                'Publications.Editors' => [
                    'sort' => ['EditorsPublications.sequence' => 'ASC']
                    ],
                'Publications.EntryTypes',
                'Publications.Journals',
                'Impressions'
            ]
        ];

        $artifact = $this->Artifacts->get($id, $associations);

        // Check whether artifact should be displayed:
        // ===========================================

        // Check for artifact retirement
        if ($artifact->retired == 1) {
            $retiredPhrase = __('The artifact {0} has been retired.', $artifact->getCDLINumber());
            if ($artifact->has('retired_comments') && $artifact->retired_comments !== '') {
                $retiredPhrase .= ' Reason: "' . $artifact->retired_comments . '"';
            }
            if (is_null($artifact->redirect_artifact_id)) {
                $this->Flash->default(__('You have been redirected to the home page. {0}', $retiredPhrase));
                return $this->redirect(['controller' => 'Home', 'action' => 'index']);
            } else {
                $redirectNumber = 'P' . str_pad($artifact->redirect_artifact_id, 6, '0', STR_PAD_LEFT);
                $this->Flash->default(__('You have been redirected to {0}. {1}', $redirectNumber, $retiredPhrase));
                return $this->redirect(['controller' => 'Artifacts', 'action' => 'view', $artifact->redirect_artifact_id]);
            }
        }

        // Check if artifact is private or public
        if (!$artifact->is_public &&
            !$this->GranularAccess->canViewPrivateArtifact()) {
            throw new UnauthorizedException();
        }

        // Artifact single view is a go, fetching additional info.
        // =======================================================

        // Additional authorization-dependent data is fetched
        $this->GranularAccess->amendArtifact($artifact);
        $this->GranularAccess->amendArtifactAssets($artifact);

        // Artifact updates (for citations)
        $artifact->artifacts_updates = $this->Artifacts->ArtifactsUpdates->find()
            ->contain(['UpdateEvents' => ['Creators', 'Authors']])
            ->where(['UpdateEvents.status' => 'approved', 'artifact_id' => $artifact->id]);

        // Update events (for display)
        $updateEvents = $this->Artifacts->Inscriptions->UpdateEvents
            ->find('artifact', ['artifact' => $artifact->id])
            ->contain([
                'ExternalResources',
                'Creators',
                'Reviewers',
                'Authors',
            ])
            ->order(['created' => 'DESC']);
        $this->set('updateEvents', $updateEvents);

        // Additional information on witnesses, seals, & impressions
        foreach ($artifact->witnesses as $witness) {
            $this->Artifacts->loadInto($witness, ['Periods', 'Proveniences']);
        }
        foreach ($artifact->seals as $seal) {
            $this->Artifacts->loadInto($seal, ['Periods', 'Proveniences']);
        }
        foreach ($artifact->impressions as $impression) {
            $this->Artifacts->loadInto($impression, ['Periods', 'Proveniences']);
        }

        // Edit access
        $this->set('canSubmitEdits', $this->GranularAccess->canSubmitEdits());

        // Additional annotation information
        if (!empty($artifact->inscription->annotation)) {
            $this->set('conll_u', (new Convert())->cConllToConllU($artifact->inscription));
        }

        // Additional information for retrieving parent materials, genres
        $this->set('artifactAllMaterials', $this->Artifacts->Materials->find()->all()->toArray());
        $this->set('artifactAllGenres', $this->Artifacts->Genres->find()->all()->toArray());

        // Chemistry data
        $data = $this->request->getQueryParams();
        $sealChemistry = $this->loadModel('ChemicalData')->find()
                        ->where([
                            'artifact_id' => $id,
                        ]);
        $this->paginate = [
            'limit' => 5
        ];
        $tableData = $this->paginate($sealChemistry);
        $type='column';
        $hasgraph = false;
        $elementCount = 'All';
        $sealArea = array();
        $chart = new Highchart();
        $headers = array();

        if (isset($data['type'])) {
            $type = $data['type'];
        }

        if (isset($data['elementCount'])) {
            $elementCount = (int)filter_var($data['elementCount'], FILTER_SANITIZE_NUMBER_INT)!=0 ? (int)filter_var($data['elementCount'], FILTER_SANITIZE_NUMBER_INT) : 'All';
        }


        list($sealArea, $chart, $headers) = $this->setGraphData($type, $sealChemistry, $elementCount);

        if (count($sealArea)>0) {
            $hasgraph = true;
        }

        $this->set(compact('chart', 'type', 'elementCount', 'tableData', 'hasgraph', 'headers'));

        $this->set('artifact', $artifact);
        $this->set('_serialize', 'artifact');
        $this->set('_admin', $this->GranularAccess->isAdmin());
    }

    public function reader($artifactId, $assetId)
    {
        $artifact = $this->Artifacts->get($artifactId, ['contain' => [
            'Inscriptions',

            // For description
            'Genres',
            'ArtifactTypes',
            'Origins',
            'Proveniences',
            'Periods',
            'Collections'
        ]]);
        $asset = $this->Artifacts->ArtifactAssets->get($assetId);
        if ($asset->artifact_id != $artifactId) {
            throw new RecordNotFoundException();
        }

        if (!$this->GranularAccess->canViewAsset($asset)) {
            throw new UnauthorizedException();
        }

        $this->set('artifact', $artifact);
        $this->set('asset', $asset);
    }

    public function downloadChemistryCSV($id=null)
    {
        $sealChemistry = $this->loadModel('ChemicalData')->find()
                        ->contain(['Calibration'])
                        ->where([
                            'artifact_id' => $id,
                        ]);
        $headers = array();
        foreach ($sealChemistry as $sealdata) {
            $finalJson = json_decode(json_decode(json_encode($sealdata->chemistry, JSON_HEX_TAG)), true);
            $headers = array_merge($headers, array_keys($finalJson));
        }

        $headers = array_values(array_unique($headers));

        $filename = 'artifact_'.$id.'_chemicalData.csv';
        $data = array();
        $tableHeaders = array();
        $tableRows = array();
        $count=0;
        foreach ($sealChemistry as $sealdata) {
            $finalJson = json_decode(json_decode(json_encode($sealdata->chemistry, JSON_HEX_TAG)), true);
            $row = array();
            foreach ($headers as $key=>$col) {
                if ($count==1) {
                    array_push($tableHeaders, $col);
                    array_push($tableHeaders, '+/-');
                }
                if (isset($finalJson[$col])) {
                    array_push($row, $finalJson[$col]['value']);
                    array_push($row, $finalJson[$col]['+/-']);
                } else {
                    array_push($row, 0.0);
                    array_push($row, 0.0);
                }
            }
            $count = $count+1;
            array_push($row, $sealdata->reading);
            array_push($row, $sealdata->area);
            array_push($row, $sealdata->area_description);
            array_push($row, $sealdata->artifact_id);
            array_push($row, $sealdata->calibration->human_number);
            array_push($row, $sealdata->date);
            array_push($row, $sealdata->time);
            array_push($row, $sealdata->duration);
            array_push($tableRows, $row);
        }
        array_push($tableHeaders, 'Reading');
        array_push($tableHeaders, 'Area');
        array_push($tableHeaders, 'Area description');
        array_push($tableHeaders, 'Artifact_id');
        array_push($tableHeaders, 'Calibration file name');
        array_push($tableHeaders, 'Date');
        array_push($tableHeaders, 'Time');
        array_push($tableHeaders, 'Duration');
        array_push($data, $tableHeaders);
        $data = array_merge($data, $tableRows);

        $this->setResponse($this->getResponse()->withDownload($filename));
        $this->set(compact('data'));
        $this->viewBuilder()
            ->setClassName('CsvView.Csv')
            ->setOption('serialize', 'data');
    }


    public function deleteChemistry($id=null, $artifactId=null)
    {
        $chemicalData = $this->loadModel('ChemicalData');
        $sealChemistry = $chemicalData->get($id);
        if ($chemicalData->delete($sealChemistry)) {
            $this->Flash->success('Entry has been deleted successfully.');
            $this->redirect(['controller'=>'Artifacts', 'action'=>'view', $artifactId]);
        } else {
            $this->Flash->error('Entry could not be deleted. Please, try again.');
        }
    }

    /**
     *
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function history($id, $type)
    {
        $artifact = $this->Artifacts->get($id);
        if (!$artifact->is_public && !$this->GranularAccess->canViewPrivateArtifact()) {
            throw new UnauthorizedException();
        }

        $this->set('artifact', $artifact);
        if ($type === 'atf' || $type === 'annotation') {
            if (!$artifact->is_atf_public && !$this->GranularAccess->canViewPrivateInscriptions()) {
                throw new UnauthorizedException();
            }

            $query = TableRegistry::get('Inscriptions')
                ->find()
                ->where([
                    'Inscriptions.artifact_id' => $id,
                    'UpdateEvents.status' => 'approved',
                    'UpdateEvents.update_type' => $type
                ])
                ->contain(['UpdateEvents.Creators', 'UpdateEvents.Authors' ])
                ->order(['UpdateEvents.approved' => 'DESC']);

            $this->Paginator->setPaginator(new DiffPaginator());
            $inscriptions = $this->paginate($query);

            $this->set('inscriptions', $inscriptions);
            $this->render("history/$type");
        } elseif ($type === 'artifact') {
            $query = $this->Artifacts->ArtifactsUpdates
                ->find()
                ->where([
                    'ArtifactsUpdates.artifact_id' => $id,
                    'UpdateEvents.status' => 'approved'
                ])
                ->contain(['UpdateEvents.Creators', 'UpdateEvents.Authors'])
                ->order(['UpdateEvents.approved' => 'DESC']);

            $updates = $this->paginate($query);
            $old = $this->Artifacts->ArtifactsUpdates->getPreviousUpdates($updates->last(), false);
            $updates = $updates->toArray();

            $this->GranularAccess->amendArtifactsUpdate([$updates, $old]);
            $updates = array_map(function ($index, $update) use ($updates, $old) {
                $older = array_slice($updates, $index + 1);
                $older = $this->Artifacts->ArtifactsUpdates->mergeUpdates([$old, ...$older]);
                return [$older, $update];
            }, array_keys($updates), array_values($updates));

            $this->set('updates', $updates);
            $this->render('history/artifact');
        } else {
            $this->redirect($this->referrer());
        }
    }

    /**
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $artifact = $this->Artifacts->newEmptyEntity();

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $artifact = $this->Artifacts->patchEntity($artifact, $this->_getFixedFormData());
            $this->Artifacts->refreshAssociations($artifact, [
                'ArtifactTypes',
                'Archives',
                'Collections',
                'Dates',
                'Dates.Years',
                'ExternalResources',
                'Genres',
                'Languages',
                'Materials',
                'MaterialAspects',
                'MaterialColors',
                'Origins',
                'Proveniences',
                'Periods'
            ]);

            $flatData = $artifact->getFlatData();
            if (!$this->GranularAccess->isAdmin()) {
                $flatData = array_diff_key($flatData, Artifact::$privateFlatFields);
            }

            $session = $this->getRequest()->getSession();
            $pendingUploads = 'ArtifactsUpdates.uploads';
            $uploadErrors = 'ArtifactsUpdates.errors';
            $session->delete($uploadErrors);

            try {
                $update = $this->Artifacts->ArtifactsUpdates->newEntityFromFlatData($flatData);
                $session->write($pendingUploads, [$update]);
            } catch (\Error $error) {
                $session->write($pendingUploads, [$this->Artifacts->ArtifactsUpdates->newEmptyEntity()]);
                $session->write($uploadErrors, [$error->getMessage()]);
            }

            return $this->redirect(['controller' => 'ArtifactsUpdates', 'action' => 'add']);
        }

        $this->Artifacts->patchEntity($artifact, [
            'is_public' => true,
            'is_atf_public' => true,
            'are_images_public' => true
        ]);
        $this->set('artifact', $artifact);
        $this->set('isAdmin', $this->GranularAccess->isAdmin());
        $this->_listRelatedEntities();
        $this->render('edit');
    }

    /**
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function edit($id)
    {
        $associations = [
            'contain' => [
                'ArtifactTypes',
                'Archives',
                'Collections',
                'Dates',
                'Dates.Years',
                'ExternalResources',
                'Genres',
                'Languages',
                'Materials',
                'MaterialAspects',
                'MaterialColors',
                'Origins',
                'Proveniences',
                'Periods'
            ]
        ];

        $artifact = $this->Artifacts->get($id, $associations);

        // Check if artifact is private or public
        if (!$artifact->is_public &&
            !$this->GranularAccess->canViewPrivateArtifact()) {
            throw new UnauthorizedException();
        }

        $this->GranularAccess->amendArtifact($artifact);

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $data = $this->getRequest()->getData();
            $artifact = $this->Artifacts->patchEntity($artifact, $this->_getFixedFormData());
            $this->Artifacts->refreshAssociations($artifact, $associations['contain']);

            $flatData = $artifact->getFlatData();
            if (!$this->GranularAccess->isAdmin()) {
                $flatData = array_diff_key($flatData, Artifact::$privateFlatFields);
            }

            $session = $this->getRequest()->getSession();
            $pendingUploads = 'ArtifactsUpdates.uploads';
            $uploadErrors = 'ArtifactsUpdates.errors';
            $session->delete($uploadErrors);

            try {
                $update = $this->Artifacts->ArtifactsUpdates->newEntityFromFlatData($flatData);
                $session->write($pendingUploads, [$update]);
            } catch (\Error $error) {
                $session->write($uploadErrors, [$error->getMessage()]);
            }

            $this->redirect(['controller' => 'ArtifactsUpdates', 'action' => 'add']);
        }

        $this->set('artifact', $artifact);
        $this->set('isAdmin', $this->GranularAccess->isAdmin());
        $this->_listRelatedEntities();
    }

    private function _getFixedFormData()
    {
        $data = $this->getRequest()->getData();
        foreach ($data as &$value) {
            if (is_array($value) && array_key_exists('template', $value)) {
                unset($value['template']);
            }
        }
        return $data;
    }

    private function _listRelatedEntities()
    {
        $proveniences = $this->Artifacts->Proveniences->find('list', ['order' => ['provenience' => 'ASC']]);
        $periods = $this->Artifacts->Periods->find('list', ['order' => ['period' => 'ASC']]);
        $artifactTypes = $this->Artifacts->ArtifactTypes->find('list', ['order' => ['artifact_type' => 'ASC']]);
        $archives = $this->Artifacts->Archives->find('list', ['order' => ['archive' => 'ASC']]);
        $collections = $this->Artifacts->Collections->find('list', ['order' => ['collection' => 'ASC']]);
        // $dates = $this->Artifacts->Dates->find('list', ['order' => ['period' => 'ASC']]);
        $genres = $this->Artifacts->Genres->find('list', ['order' => ['genre' => 'ASC']]);
        $languages = $this->Artifacts->Languages->find('list', ['order' => ['language' => 'ASC']]);
        $materials = $this->Artifacts->Materials->find('list', ['order' => ['material' => 'ASC']]);
        $materialColors = $this->Artifacts->MaterialColors->find('list', ['order' => ['material_color' => 'ASC']]);
        $materialAspects = $this->Artifacts->MaterialAspects->find('list', ['order' => ['material_aspect' => 'ASC']]);

        $this->set(compact(
            'proveniences',
            'periods',
            'artifactTypes',
            'archives',
            'collections',
            // 'dates',
            'genres',
            'languages',
            'materials',
            'materialColors',
            'materialAspects'
        ));
    }

    public function images($id)
    {
        $artifact = $this->Artifacts->get($id);
        $this->GranularAccess->amendArtifactAssets($artifact);
        $this->set('artifact', $artifact);
        $this->set('isAdmin', $this->GranularAccess->isAdmin());
    }

    //seal portal
    public function seals()
    {
        $this->paginate = [
            'sortableFields' => [
                'Seals',
                'Periods.sequence'
            ],
            'contain' => 'Periods',
            'limit' => 10
        ];

        $featuredSeals = $this->loadModel('FeaturedSeals')->find('all')->contain(['Artifacts']);
        foreach ($featuredSeals as $seal) {
            $this->GranularAccess->amendArtifactAssets($seal->artifact);
        }

        $isadmin = $this->GranularAccess->isAdmin();

        $posting = $this->loadModel('Postings')->find()
                   ->where([
                        'Postings.id' => 8,
                   ])->all();

        $query = $this->Artifacts->find();
        $sealPeriods = $query
            ->contain([
                'ArtifactTypes',
                'Periods'=>[
                    'fields' => [
                        'Periods.period',
                        'Periods.sequence'
                    ],
                ]
            ])
            ->select([
                'Seals' => $query->func()->count('Artifacts.period_id'),
                'Artifacts.seal_no',
                'Artifacts.period_id',
                'Artifacts.artifact_type_id',
                'ArtifactTypes.artifact_type'
            ])
            ->where([
                'Artifacts.period_id IS NOT' => null,
                'Artifacts.is_public' => 1,
                'OR' => [['Artifacts.artifact_type_id' => 12],['ArtifactTypes.parent_id'=>12]],
                'period IS NOT' => null
            ])
            ->group('Artifacts.period_id');
        $sealPeriods = $this->paginate($sealPeriods);

        $this->set(compact('featuredSeals', 'sealPeriods', 'posting', 'isadmin'));
    }
}
