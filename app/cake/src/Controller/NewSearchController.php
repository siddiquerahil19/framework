<?php

namespace App\Controller;

use App\Datasource\ElasticSearchQuery;
use App\Utility\CdliProcessor\TokenList;

/**
 * New Search Controller
 */
class NewSearchController extends AppController
{
    /**
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('Api', ['features' => ['Bibliography', 'Inscription', 'LinkedData', 'TableExport']]);
        $this->loadComponent('GeneralFunctions');
        $this->loadComponent('GranularAccess');
        $this->loadModel('Artifacts');

        $this->Auth->allow(['index', 'view', 'resolve', 'tokenList']);

        if (is_null($this->getRequest()->getSession()->read('searchSettings2'))) {
            $this->initializeSearchSettings();
        }
    }

    /**
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        // Load search settings
        $searchSettings = $this->getRequest()->getSession()->read('searchSettings2');
        $this->set('searchSettings', $searchSettings);

        // Determine default layout
        $layout = $this->getRequest()->getQuery('layout');
        if (!in_array($layout, ['full', 'compact'])) {
            $layout = $searchSettings['layout_type'];
        }
        $this->set('layout', $layout);

        // Prepare query data
        $queryData = array_filter(array_intersect_key($this->getRequest()->getQueryParams(), ElasticSearchQuery::$searchFields));
        $queryType = array_key_exists('simple-value', $queryData) || count($queryData) === 0 ? 'simple' : 'advanced';
        $orderData = $layout === 'full' ? [$searchSettings['full:sort']] : [
            $searchSettings['compact:sort_1'],
            $searchSettings['compact:sort_2'],
            $searchSettings['compact:sort_3']
        ];
        $orderData = array_intersect($this->getRequest()->getQuery('order') ?? $orderData, array_keys(ElasticSearchQuery::$sortFields));
        $filterData = array_intersect_key($this->getRequest()->getQuery('f') ?? [], ElasticSearchQuery::$filters);
        $access = [
            'artifact' => $this->GranularAccess->canViewPrivateArtifact(),
            'atf' => $this->GranularAccess->canViewPrivateInscriptions(),
            'asset' => $this->GranularAccess->canViewPrivateImage()
        ];

        // Perform query
        $query = (new ElasticSearchQuery('artifacts', $access))
            ->find($queryType, ['query' => $queryData])
            ->where($filterData)
            ->order($orderData, true)
            ->aggregate(ElasticSearchQuery::$filters);

        // Paginate results
        $this->paginate = ['limit' => $searchSettings[$layout . ':page_size'], 'maxLimit' => 1000];
        $results = $this->paginate($query);
        if ($this->Api->requestsApi()) {
            return $this->_setApiVariables($results);
        }
        $this->set('results', $results->toArray());
        $this->set('filterBuckets', $results->_buckets);

        // Pass additional info to the view
        $this->set('query', [
            'data' => $queryData,
            'type' => $queryType,
            'order' => $orderData,
            'filters' => $filterData,
            'highlight' => $query->atfQueries
        ]);
        $this->set('sortFields', ElasticSearchQuery::$sortFields);
        $this->set('filters', ElasticSearchQuery::$filters);
        $this->set('metrics', $results->_metrics);
        $this->set('access', $access);
        $this->set('canSubmitEdits', $this->GranularAccess->canSubmitEdits());
    }

    /**
     * @param string $path
     * @return \Cake\Http\Response|null
     */
    public function view($path)
    {
        if ($path === 'settings') {
            $request = $this->getRequest();
            if ($request->is(['post', 'patch', 'put'])) {
                $data = $request->getData();
                if (array_key_exists('reset', $data)) {
                    $this->initializeSearchSettings();
                    $this->Flash->success('Search settings reset successfully!');
                } else {
                    $request->getSession()->write('searchSettings2', $data);
                    $this->Flash->success('Search settings updated successfully!');
                }
            }
            $settings = $request->getSession()->read('searchSettings2');
            $this->set('settings', $settings);
            $this->set('sortFields', ElasticSearchQuery::$sortFields);
            $this->render('settings');
        } elseif ($path === 'advanced') {
            $this->set('query', $this->getRequest()->getQueryParams());
            $this->render('advanced');
        } elseif ($path === 'commodity-viz') {
            // Prepare query data
            $queryData = array_filter(array_intersect_key($this->getRequest()->getQueryParams(), ElasticSearchQuery::$searchFields));
            $queryType = array_key_exists('simple-value', $queryData) || count($queryData) === 0 ? 'simple' : 'advanced';
            $filterData = array_intersect_key($this->getRequest()->getQuery('f') ?? [], ElasticSearchQuery::$filters);
            $access = [
                'artifact' => $this->GranularAccess->canViewPrivateArtifact(),
                'atf' => $this->GranularAccess->canViewPrivateInscriptions(),
                'asset' => $this->GranularAccess->canViewPrivateImage()
            ];

            // Perform query
            $query = (new ElasticSearchQuery('artifacts', $access))
                ->find($queryType, ['query' => $queryData])
                ->where($filterData)
                ->order(['id'], true)
                ->aggregate(ElasticSearchQuery::$filters);
            $query->limit(1000);
            $results = $query->all();

            if ($results->_metrics['count']['relation'] !== 'gte') {
                $url = '/cdli-accounting-viz/?' . $this->_encodeIdSequence($results->toArray());
                if (strlen($url) <= 2048) {
                    return $this->redirect($url);
                }
            }

            $this->Flash->error(__('Too many results to show.'));
            $this->render('empty');
        }
    }

    /**
     * @param string $kind
     * @return \Cake\Http\Response|null
     */
    public function resolve($id)
    {
        $this->redirect(['action' => 'index', '?' => ['cdli_id' => $id] + $this->getRequest()->getQueryParams()]);
    }

    /**
     * @param string $kind
     * @return \Cake\Http\Response|null
     */
    public function tokenList($kind)
    {
        if ($kind !== 'signs' && $kind !== 'words') {
            throw new \Cake\Http\Exception\NotFoundException();
        }

        // Prepare query data
        $queryData = array_filter(array_intersect_key($this->getRequest()->getQueryParams(), ElasticSearchQuery::$searchFields));
        $queryType = array_key_exists('simple-value', $queryData) || count($queryData) === 0 ? 'simple' : 'advanced';
        $filterData = array_intersect_key($this->getRequest()->getQuery('f') ?? [], ElasticSearchQuery::$filters);
        $access = [
            'artifact' => $this->GranularAccess->canViewPrivateArtifact(),
            'atf' => $this->GranularAccess->canViewPrivateInscriptions(),
            'asset' => $this->GranularAccess->canViewPrivateImage()
        ];

        // Perform query
        $query = (new ElasticSearchQuery('artifacts', $access))
            ->find($queryType, ['query' => $queryData])
            ->where($filterData)
            ->order(['id'], true)
            ->aggregate(ElasticSearchQuery::$filters);
        $query->limit(1000);
        $results = $query->all();
        $artifactIds = array_column(array_column($results->toArray(), '_source'), 'id');

        // Compose token list
        $type = $this->Api->prefers() === 'json' ? 'json' : 'txt';

        $inscription_count = 0;
        $token_count = 0;
        $token_instance_count = 0;

        $list = [];
        foreach ($artifactIds as $artifactId) {
            $inscription_count += 1;

            $inscription = $this->Artifacts->Inscriptions->find()
                ->where(['Inscriptions.artifact_id' => $artifactId, 'Inscriptions.is_latest' => true])
                ->select(['atf']);
            if (!$access['atf']) {
                $inscription = $inscription->matching('Artifacts', function ($query) {
                    return $query->where(['Artifacts.is_atf_public' => true]);
                });
            }
            $inscription = $inscription->first();

            if (empty($inscription->atf)) {
                continue;
            }

            $tokens = TokenList::processText($inscription->atf, $kind);

            foreach ($tokens as $token) {
                if (array_key_exists($token, $list)) {
                    $list[$token] += 1;
                } else {
                    $token_count += 1;
                    $list[$token] = 1;
                }
                $token_instance_count += 1;
            }
        }
        ksort($list);

        $data;
        if ($type === 'json') {
            $data = json_encode([
                'metadata' => [
                    'token_kind' => $kind,
                    'inscription_count' => $inscription_count,
                    'token_count' => $token_count,
                    'token_instance_count' => $token_instance_count
                ],
                'list' => $list
            ]);
        } else {
            $data = implode("\n", array_keys($list));
        }

        $this->Api->respondAs($type);
        return $this->getResponse()->withStringBody($data);
    }

    /**
     * @return null;
     */
    protected function initializeSearchSettings()
    {
        $this->getRequest()->getSession()->write('searchSettings2', [
            'layout_type' => 'full',

            'full:page_size' => 25,
            'full:sidebar' => true,
            'full:sort' => '_score',
            'full:field:publication' => true,
            'full:field:collection' => true,
            'full:field:museum_no' => true,
            'full:field:provenience' => true,
            'full:field:period' => true,
            'full:field:artifact_type' => true,
            'full:field:material' => true,
            'full:field:dates_referenced' => true,
            'full:field:atf' => true,

            'compact:page_size' => 25,
            'compact:sort_1' => 'id',
            'compact:sort_2' => '',
            'compact:sort_3' => '',
            'compact:sidebar' => false,
            'compact:field:publication' => true,
            'compact:field:collection' => false,
            'compact:field:period' => true,
            'compact:field:provenience' => true,
            'compact:field:artifact_type' => false,
            'compact:field:material' => false,
            'compact:field:accession_no' => false,
            'compact:field:museum_no' => true,
            'compact:field:written_in' => false,
            'compact:field:excavation_no' => false,
            'compact:field:archive' => false,
            'compact:field:dates_referenced' => true,
            'compact:field:genre' => true,
            'compact:field:language' => false,

            'filter:period' => true,
            'filter:provenience' => true,
            'filter:artifact_type' => true,
            'filter:material' => true,
            'filter:genre' => true,
            'filter:language' => true,
            'filter:dates_referenced' => true,
            'filter:collection' => true,
            'filter:publication_authors' => true,
            'filter:publication_year' => true,
            'filter:publication_journal' => true,
            'filter:publication_editors' => true,
            'filter:publication_series' => true,
            'filter:publication_publisher' => true,
            'filter:asset_type' => true,
            'filter:atf_transliteration' => true,
            'filter:annotation' => true,
            'filter:atf_translation' => true
        ]);
    }

    /**
     * @param \App\Datasource\ElasticSearchResultSet $results
     * @return \Cake\Http\Response|null
     */
    protected function _setApiVariables($results)
    {
        $ids = array_column(array_column($results->toArray(), '_source'), 'id');
        $aspect = $this->getRequest()->getQuery('aspect');
        $entities;

        if ($aspect == 'inscriptions') {
            $entities = [];
            $contain = ['Artifacts'];

            if ($this->Api->requestsFormat(['LinkedData'])) {
                array_push($contain, 'Artifacts.Dates', 'Artifacts.Languages', 'Artifacts.Composites');
            }

            foreach ($ids as $id) {
                $inscription = $this->Artifacts->Inscriptions
                    ->findByArtifactId($id)
                    ->contain($contain)
                    ->where(['is_latest' => 1])
                    ->first();
                if ($inscription) {
                    $entities[] = $inscription;
                }
            }
        } elseif ($aspect == 'publications') {
            $entities = $this->Artifacts->Publications
                ->find()
                ->contain(['EntryTypes', 'Authors', 'Editors', 'Journals'])
                ->matching('Artifacts', function ($q) use ($ids) {
                    return $q->whereInList('Artifacts.id', $ids);
                })
                ->all();
        } else {
            $entities = array_map(function ($id) {
                $artifact = $this->Artifacts->get($id, [
                    'contain' => [
                        'Proveniences',
                        'Origins',
                        'Periods',
                        'ArtifactTypes',
                        'Archives',
                        'Collections',
                        'Dates',
                        'ExternalResources',
                        'Genres',
                        'Languages',
                        'Materials',
                        'MaterialAspects',
                        'MaterialColors',
                        'Publications',
                        // TODO RetiredArtifacts
                    ]
                ]);

                $this->GranularAccess->amendArtifact($artifact);
                $artifact->artifacts_updates = $this->Artifacts->ArtifactsUpdates->find()
                    ->contain(['UpdateEvents' => ['Creators', 'Authors']])
                    ->where(['UpdateEvents.status' => 'approved', 'artifact_id' => $artifact->id]);

                return $artifact;
            }, $ids);
        }

        $this->set([
            'entities' => $entities,
            '_admin' => $this->GranularAccess->isAdmin(),
            '_serialize' => 'entities'
        ]);
    }

    /**
     * @param array $results
     * @return string
     */
    protected function _encodeIdSequence($results)
    {
        if (count($results) === 0) {
            return '';
        }

        $map = [
            ',' => 'A', '0' => 'B', '1' => 'C', '2' => 'D', '3' => 'E',
            '4' => 'F', '5' => 'G', '6' => 'H', '7' => 'I', '8' => 'J',
            '9' => 'K', '1,1,' => 'L', '0,' => 'M', '1,' => 'N', '2,' => 'O',
            '3,' => 'P', '4,' => 'Q', '5,' => 'R', '6,' => 'S', '7,' => 'T',
            '8,' => 'U', '9,' => 'V', ',0' => 'W', ',1' => 'X', ',2' => 'Y',
            ',3' => 'Z', ',4' => 'a', ',5' => 'b', ',6' => 'c', ',7' => 'd',
            ',8' => 'e', ',9' => 'f', '00' => 'g', '11' => 'h', '22' => 'i',
            '33' => 'j', '44' => 'k', '55' => 'l', '66' => 'm', '77' => 'n',
            '88' => 'o', '99' => 'p', '0,1' => 'q', '1,1' => 'r', '2,1' => 's',
            '3,1' => 't', '4,1' => 'u', '5,1' => 'v', '6,1' => 'w', '7,1' => 'x',
            '8,1' => 'y', '9,1' => 'z', ',0,' => '0', ',1,' => '1', ',2,' => '2',
            ',3,' => '3', ',4,' => '4', ',5,' => '5', ',6,' => '6', ',7,' => '7',
            ',8,' => '8', ',9,' => '9'
        ];

        // Compute the difference between successive
        // ids, because these differences will be much
        // smaller than the ids themselves:
        $deltas = [$results[0]['_source']['id']];
        for ($i = 1; $i < count($results); $i++) {
            $deltas[] = $results[$i]['_source']['id'] - $results[$i - 1]['_source']['id'];
        }
        $delta_string = implode(',', $deltas);

        // Length of the longest string which
        // can be encoded by a single character:
        // For our encoding this is '1,1,'
        $MAX_ENCODING_LENGTH = 4;

        // Iterate over the string. Every iteration,
        // greedily convert the string's prefix into
        // an encoded letter.
        $encoding = '';
        $i = 0;
        $keys = array_map('strval', array_keys($map));
        while ($i < strlen($delta_string)) {
            for ($length = $MAX_ENCODING_LENGTH; $length > 0; $length--) {
                $key = substr($delta_string, $i, $length);
                if (in_array($key, $keys, true)) {
                    $encoding .= $map[$key];
                    $i += $length;
                    break;
                }
            }
        }

        return $encoding;
    }
}
