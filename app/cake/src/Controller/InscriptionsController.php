<?php

namespace App\Controller;

use App\Controller\AppController;
use App\Datasource\ArrayQuery;
use Cake\Http\Exception\NotAcceptableException;
use Cake\Http\Exception\RecordNotFoundException;
use Cake\Http\Exception\UnauthorizedException;

/**
 * Inscriptions Controller
 *
 * @property \App\Model\Table\InscriptionsTable $Inscriptions
 *
 * @method \App\Model\Entity\Inscription[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class InscriptionsController extends AppController
{
    public function initialize(): void
    {
        parent::initialize();
        $this->loadModel('Artifacts');
        $this->loadComponent('GeneralFunctions');
        $this->loadComponent('GranularAccess');
        $this->loadComponent('Api', ['features' => ['Inscription', 'LinkedData']]);
        $this->Auth->allow(['index', 'view', 'add', 'edit']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => [
                'Artifacts',
                'Artifacts.Dates',
                'Artifacts.Languages',
                'Artifacts.Composites' => [
                    'conditions' => ['Composites.is_public' => true]
                ]
            ],
            'conditions' => [
                'Artifacts.is_public' => true,
                'Artifacts.is_atf_public' => true,
                'Inscriptions.is_latest' => true
            ]
        ];

        if ($this->GranularAccess->canViewPrivateInscriptions()) {
            unset($this->paginate['conditions']['Artifacts.is_public']);
            unset($this->paginate['conditions']['Artifacts.is_atf_public']);
            unset($this->paginate['contain']['Artifacts.Composites']['conditions']);
        }

        $inscriptions = $this->paginate($this->Inscriptions);
        $access_granted = $this->GeneralFunctions->checkIfRolesExists([1]);
        $this->set(compact('inscriptions', 'access_granted'));
        $this->set('_serialize', 'inscriptions');
    }

    /**
     * View method
     *
     * @param string|null $id Inscription id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $compositeConditions = [];
        if ($this->GranularAccess->canViewPrivateInscriptions()) {
            $compositeConditions = [
                'conditions' => ['Composites.is_public' => true]
            ];
        }

        $inscription = $this->Inscriptions->get($id, [
            'contain' => [
                'Artifacts',
                'Artifacts.Languages',
                'Artifacts.Dates',
                'Artifacts.Composites' => $compositeConditions,
                'UpdateEvents',
                'UpdateEvents.Creators',
                'UpdateEvents.Authors',
            ]
        ]);

        $artifact = $inscription->artifact;

        if (!$this->GranularAccess->canViewPrivateInscriptions() &&
            (!$artifact->is_public || !$artifact->is_atf_public)) {
            throw new UnauthorizedException();
        }

        if (!$this->GranularAccess->canAccessEdits($inscription->update_event)) {
            throw new UnauthorizedException();
        }

        $type = $inscription->getPreferredType($this->Api->accepts());
        if (empty($type)) {
            throw new NotAcceptableException();
        }

        if ($type == 'html') {
            $oldInscription;
            if ($inscription->update_event->status == 'approved') {
                $oldInscription = $this->Inscriptions->find()
                    ->contain('UpdateEvents')
                    ->where([
                        'Inscriptions.artifact_id' => $inscription->artifact_id,
                        'UpdateEvents.approved <' => $inscription->update_event->approved
                    ])
                    ->order(['UpdateEvents.approved' => 'DESC'])
                    ->first();
            } else {
                $oldInscription = $oldInscription = $this->Inscriptions->find()
                    ->where(['artifact_id' => $inscription->artifact_id, 'is_latest' => 1])
                    ->first();
            }
            $this->set('oldInscription', $oldInscription);
        } else {
            $this->Api->renderAs($this, $type);
        }

        $this->set('inscription', $inscription);
        $this->set('_serialize', 'inscription');
    }

    private function _getInput($prefix)
    {
        $file = $this->request->getData("${prefix}_file");
        $paste = $this->request->getData("${prefix}_paste");
        $input;

        // Choose input method
        if (!is_null($file) && $file->getError() != UPLOAD_ERR_NO_FILE) {
            $handle = $file->getStream()->detach();
            $input = stream_get_contents($handle);
        } else {
            $input = $paste;
        }

        return is_null($input) ? $input : preg_replace('/\r\n?/', "\n", $input);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null
     */
    public function add()
    {
        $canSubmitEdits = $this->GranularAccess->canSubmitEdits();
        $session = $this->getRequest()->getSession();
        $uploadErrors = 'Inscriptions.errors';
        $uploadType = 'Inscriptions.uploadType';
        $pendingUploads = 'Inscriptions.uploads';

        if (!$this->request->is(['post', 'put', 'patch'])) {
            if ($session->check($pendingUploads)) {
                $inscriptions = $session->read($pendingUploads);
                $inputField = $session->read($uploadType);

                if ($session->check($uploadErrors)) {
                    $errors = $session->read($uploadErrors);
                    return $this->setAction('errors', $inscriptions, $errors);
                }

                return $this->setAction('confirm', $inscriptions, $inputField, $canSubmitEdits);
            }

            if (!is_null($this->request->getQuery('artifact'))) {
                $this->setAction('edit');
            }

            $this->set('canSubmitEdits', $canSubmitEdits);
            return;
        }

        $input = $this->_getInput('atf');
        $inputField = 'atf';

        $conll = $this->_getInput('conll');
        if (!empty($conll)) {
            $input = $conll;
            $inputField = 'annotation';
        }

        if (is_null($input)) {
            // Overwrite with missing input ("cancel")
            $session->delete($pendingUploads);
            $session->delete($uploadType);
            $session->delete($uploadErrors);
            $this->set('canSubmitEdits', $canSubmitEdits);
            return;
        } elseif (empty($input)) {
            // Empty input (distinct from missing input) does nothing
            $this->set('canSubmitEdits', $canSubmitEdits);
            return;
        }

        if ($inputField === 'atf') {
            $records = preg_split('/(?<=\n)(?=&)/', $input);
            $records = array_map(function ($record) {
                return preg_replace('/\n+$/', "\n\n", $record);
            }, $records);
        } elseif ($inputField === 'annotation') {
            $records = preg_split('/(?<=\n)(?=#new_text=)/', $input);
        }

        $inscriptions = [];
        $errors = [];
        $hasErrors = false;

        foreach ($records as $record) {
            try {
                $inscription = $this->Inscriptions->newEntityFromInput($record, $inputField);

                // Check whether user has access to inscription
                if ($inscription->has('artifact_id')) {
                    $artifact = $this->Artifacts->get($inscription->artifact_id, ['contain' => ['Inscriptions']]);

                    if (!$artifact->is_public && !$this->GranularAccess->canViewPrivateArtifact()) {
                        throw new RecordNotFoundException($inscription->artifact_id);
                    }

                    if (!$artifact->is_atf_public && !$this->GranularAccess->canViewPrivateInscriptions()) {
                        $inscription->setError('atf', __('Cannot add or change the transliteration/annotation. Please contact an administrator.'));
                    }

                    if (!empty($artifact->inscription) && !$inscription->isFieldDifferent($artifact->inscription, $inputField)) {
                        continue;
                    }

                    $artifact = null;
                }

                // Cannot add/change annotations if there is no atf
                if ($inputField === 'annotation' && !$inscription->has('atf')) {
                    $inscription->setError('atf', __('Not transliteration, cannot add or change the annotation.'));
                }

                // Check for errors
                if ($inscription->hasErrors()) {
                    $hasErrors = true;
                }

                $inscriptions[] = $inscription;
            } catch (RecordNotFoundException $error) {
                $inscription = $this->Inscriptions->newEmptyEntity();
                $inscription->setError('artifact_id', __('Record {0} not found in table "artifacts"', $error->getMessage()));
                $inscriptions[] = $inscription;
                $hasErrors = true;
            } catch (\Throwable $error) {
                $errors[] = $error->getMessage();
                $hasErrors = true;
            }
        }

        // Store updates in sessions
        if (!empty($inscriptions)) {
            $session->write($pendingUploads, $inscriptions);
            $session->write($uploadType, $inputField);
        }

        // Redirect to error page if necessary.
        if ($hasErrors) {
            $session->write($uploadErrors, $errors);
            return $this->setAction('errors', $inscriptions, $errors);
        } else {
            $session->delete($uploadErrors);
        }

        return $this->setAction('confirm', $inscriptions, $inputField, $canSubmitEdits);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null
     */
    public function edit($id = null)
    {
        // Do not display the form if the user cannot submit edits anyway (there
        // are other ways to check validity of ATF so no need to make this public).
        if (!$this->GranularAccess->canSubmitEdits()) {
            $this->Flash->error('Only logged in users who have an author associated to their account can edit text. Create an account and / or  contact an administrator to associate an author with your account.');
            return $this->redirect($this->referer());
        }

        $inscription = is_null($id) ? $this->Inscriptions->newEmptyEntity() : $this->Inscriptions->get($id);
        $this->set('inscription', $inscription);

        // Get artifact ID from
        //   1. Existing inscription, or, if that does not exist
        //   2. Query parameter (i.e. ?artifact=123)
        $artifactId = $inscription->artifact_id ?? $this->getRequest()->getQuery('artifact');

        // If still no artifact is given, return
        if (is_null($artifactId)) {
            return;
        }

        // Else, get artifact info to check for access
        $artifact = $this->Inscriptions->Artifacts->get($artifactId);

        // If the visitor is authorized to view artifact metadata or atf, deny access
        $canViewArtifact = $artifact->is_public || $this->GranularAccess->canViewPrivateArtifact();
        $canViewAtf = $artifact->is_atf_public || $this->GranularAccess->canViewPrivateInscriptions();
        if (!$canViewArtifact || !$canViewAtf) {
            throw new UnauthorizedException();
        }

        // Else, provide artifact metadata and if authorized, images
        $this->set('artifact', $artifact);
        $this->GranularAccess->amendArtifactAssets($artifact);

        // Check which tab to display
        $this->set('tab', $this->getRequest()->getQuery('tab') ?? 'atf');

        // More info for the template
        $this->set('isAdmin', $this->GranularAccess->isAdmin());
    }

    /**
     * @param \App\Model\Entity\Inscriptions[] $inscriptions
     * @param string $type
     * @return \Cake\Http\Response|null
     */
    public function confirm(array $inscriptions, string $type, bool $canSubmitEdits)
    {
        $noChanges = empty($inscriptions);
        $inscriptions = $this->paginate(new ArrayQuery($inscriptions));

        foreach ($inscriptions as $inscription) {
            if ($inscription->has('artifact_id')) {
                $inscription->artifact = $this->Artifacts->get($inscription->artifact_id, ['contain' => ['Inscriptions']]);
            }
        }

        $this->set('inscriptions', $inscriptions);
        $this->set('updateType', $type);
        $this->set('canSubmitEdits', $canSubmitEdits);
        $this->set('noChanges', $noChanges);
    }

    /**
     * @param \App\Model\Entity\Inscriptions[] $inscriptions
     * @param string[] $errors
     * @return \Cake\Http\Response|null
     */
    public function errors(array $inscriptions, array $errors)
    {
        $inscriptions = array_filter($inscriptions, function ($inscription) {
            return $inscription->hasErrors();
        });
        $inscriptions = $this->paginate(new ArrayQuery($inscriptions));

        $this->set('inscriptions', $inscriptions);
        $this->set('errors', $errors);
    }
}
