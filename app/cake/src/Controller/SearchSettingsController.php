<?php

namespace App\Controller;

use App\Controller\AppController;

class SearchSettingsController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();

        // Set access for public.
        $this->Auth->allow(['index']);
    }

    /**
     * beforeFilter method
     *
     * To set up access before this contoller is executed.
     *
     * @return \Cake\Http\Response|void
     */
    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);

        // Search Setting Form options
        $this->SearchSettingOption = [
            'PageSize' => [
                '10' => 10,
                '25' => 25,
                '100' => 100,
                '500' => 500,
                '1000' => 1000
            ],
            'Filter' => [
                'Sidebar' => [
                    '1' => 'Show filter sidebar',
                    '0' => 'Hide filter sidebar'
                ],
                'Object' => [
                    'collection' => 'Museum Collections',
                    'period' => 'Period',
                    'provenience' => 'Provenience',
                    'atype' => 'Artifact type',
                    'materials' => 'Material(s)',
                    'image_type' => 'Image type'
                ],
                'Textual' => [
                    'genres' => 'Genre(s)/sub-genre',
                    'languages' => 'Language'
                ],
                'Publication' => [
                    'authors' => 'Authors',
                    'year' => 'Date of publication'
                ],
                'Inscription' => [
                    'transliteration' => 'Transliteration/translation'
                ]
            ],
            'CompactFilter' => [
                'Object' => [
                    'collection' => 'Museum Collections',
                    'period' => 'Period',
                    'provenience' => 'Provenience',
                    'atype' => 'Artifact type',
                    'materials' => 'Material(s)',
                    'accession_no' => 'Accession number',
                    'written_in' => 'Written in',
                    'excavation_no' => 'Excavation number',
                    'archive' => 'Archive',
                    'dates_referenced' => 'Date(s) referenced'
                ]
            ]
        ];

        // Fetch Search Settings from Session Variable
        $this->searchSettings = $this->getRequest()->getSession()->read('searchSettings');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->isSettingsUpdated = false;
        if ($this->request->is('post')) {
            if ($this->request->getData('Reset')==='') {
                $this->loadComponent('GeneralFunctions')->initializeSearchSettings();
                $this->searchSettings = $this->getRequest()->getSession()->read('searchSettings');
            } else {
                $this->searchSettings = $this->request->getData();

                foreach ($this->searchSettings as $key => $value) {
                    if ($value === '') {
                        $this->searchSettings[$key] = [];
                    }
                }

                $this->getRequest()->getSession()->write('searchSettings', $this->searchSettings);
            }
            $this->isSettingsUpdated = true;
        }
        $this->set([
            'searchSettings' => $this->searchSettings,
            'SearchSettingOption' => $this->SearchSettingOption,
            'isSettingsUpdated' => $this->isSettingsUpdated
            ]);
    }
}
