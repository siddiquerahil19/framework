<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Authors Controller
 *
 * @property \App\Model\Table\AuthorsTable $Authors
 *
 * @method \App\Model\Entity\Author[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AuthorsController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('Api', ['features' => ['LinkedData','TableExport']]);
        $this->loadComponent('GeneralFunctions');

        // Set access for public.
        $this->Auth->allow(['index', 'view', 'edit']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'limit' => 10,
            'order' => [
                'author' => 'ASC'
            ],
            'maxLimit' => $this->Authors->find()->count()
        ];

        $data = $this->request->getQueryParams();

        if (isset($data['letter'])) {
            $authors = $this->Authors->find('all', array('conditions' => array('Authors.author LIKE' => $data['letter'] . '%')));
        } else {
            $authors = $this->Authors;
        }

        $authors = $this->paginate($authors);
        $access_granted = $this->GeneralFunctions->checkIfRolesExists([1, 2, 12]);
        $this->set(compact('authors', 'access_granted'));
        $this->set('_serialize', 'authors');
    }

    /**
     * View method
     *
     * @param string|null $id Author id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->paginate = [
            'limit' => 10,
            'Pub' => ['scope' => 'pub']
        ];

        $author = $this->Authors->get($id, ['contain' => 'Publications']);

        $this->loadModel('Staff');
        $staff = $this->Staff
            ->find()
            ->where(['author_id' => $id])
            ->first();


        $this->loadModel('UpdateEvents');
        $updateEvents_q = $this->UpdateEvents
            ->find()
            ->contain([
                'Reviewers',
                'Creators',
                'ExternalResources'
            ])
            ->matching('Authors', function ($q) use ($id) {
                return $q->where(['OR' => [
                    'UpdateEvents.created_by' => $id, //creator
                    'UpdateEvents.approved_by' => $id,// approver
                    'Authors.id' => $id // author -> editor
                ]]);
            })
            ->order([
                'UpdateEvents.created' => 'DESC'
            ]);

        $updateEvents = $this->paginate($updateEvents_q);

        $this->loadModel('Publications');
        $query = $this->Publications->find('all', [
            'contain' => [
                'EntryTypes',
                'Journals',
                'Authors' => [
                    'sort' => ['AuthorsPublications.sequence' => 'ASC']
                ],
                'Editors' => [
                    'sort' => ['EditorsPublications.sequence' => 'ASC']
                ]
            ],
            'sort' => [
                'COALESCE(Publications.bibtexkey, "zz") ASC',
                'COALESCE(Publications.title, "zz") ASC',
                'COALESCE(Publications.designation, "zz") ASC'
            ]
        ]);

        $filter = ['Authors.author' => $author->author];
        $query = $query->innerJoinWith(
            'Authors',
            function ($q) use ($filter) {
                return $q->where($filter);
            }
        );
        $publications = $this->paginate($query, ['scope' => 'pub']);
        $access_granted = $this->GeneralFunctions->checkIfRolesExists([1]);
        $this->set(compact('staff', 'author', 'access_granted', 'publications', 'updateEvents'));
        $this->set('_serialize', 'author');
    }

    public function search()
    {
        $search_key = $this->request->getBody()->getContents();

        $authors =  $this->Authors->find('all', [
            'fields' => ['id', 'author'],
            'order' => ['author' => 'asc'],
            'limit' => 10
        ])->where([
            'author LIKE' => '%' . $search_key . '%'
        ])->all();
        $authors = json_encode(array_map(function ($author) {
            $author['author'] = h($author['author']);
            return $author;
        }, $authors->toArray()));

        return $this->getResponse()->withStringBody($authors)->withType('json');
    }

    /**
     * Edit method
     *
     * @param string|null $id Author id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit()
    {
        // variable to store the author_id of the current logged in user
        $id = $this->Auth->user()['author_id'];

        if (!isset($id)) {
            $this->Flash->error('You are not authorized to access that location');
            return $this->redirect(['action' => 'index']);
        }

        $author = $this->Authors->get($id);

        if ($this->getRequest()->is(['patch', 'post', 'put'])) {
            $data = $this->getRequest()->getData();
            $author = $this->Authors->patchEntity($author, $data);
            if ($this->Authors->save($author)) {
                $this->Flash->success(__('The author details have been updated.'));

                return $this->redirect(['controller' => 'Authors', 'action' => 'view', $id]);
            }
            $this->Flash->error(__('The author details could not be updated. Please, try again.'));
        }
        $this->set(compact('author'));
    }
}
