<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Core\Configure;
use elFinder;
use elFinderConnector;

/**
 * Home Controller
 *
 * @method \App\Model\Entity\Artifact[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class HomeController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();

        // Load Component 'GeneralFunctions'
        $this->loadComponent('GeneralFunctions');
    }

    /**
     * dashboard method
     *
     * @return \Cake\Http\Response|void
     */
    public function dashboard()
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1, 2, 12])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }
    }
}
