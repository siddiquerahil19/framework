<?php

namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Proveniences Controller
 *
 * @property \App\Model\Table\ProveniencesTable $Proveniences
 *
 * @method \App\Model\Entity\Provenience[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ProveniencesController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();

        // Load Component 'GeneralFunctions'
        $this->loadComponent('GeneralFunctions');
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1,2])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $provenience = $this->Proveniences->newEmptyEntity();
        if ($this->getRequest()->is('post')) {
            $provenience = $this->Proveniences->patchEntity($provenience, $this->getRequest()->getData());
            $provenience["transc_name"] = $this->getRequest()->getData()["transc_name"];
            if ($this->getRequest()->getData()["osm_type"] == "null") {
                $provenience["osm_type"] = null;
            }
            if ($this->Proveniences->save($provenience)) {
                $this->Flash->success(__('The provenience has been saved.'));

                return $this->redirect(['prefix' => false,'action' => 'index']);
            }

            $this->Flash->error(__('The provenience could not be saved. Please, try again.'));
        }
        $regions = $this->Proveniences->Regions->find('list', ['limit' => 200]);
        $this->set(compact('provenience', 'regions'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Provenience id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1,2])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $provenience = $this->Proveniences->get($id, [
            'contain' => []
        ]);
        if ($this->getRequest()->is(['patch', 'post', 'put'])) {
            $provenience = $this->Proveniences->patchEntity($provenience, $this->getRequest()->getData());
            $provenience["transc_name"] = $this->getRequest()->getData()["transc_name"];
            if ($this->getRequest()->getData()["osm_type"] == "null") {
                $provenience["osm_type"] = null;
            }
            if ($this->Proveniences->save($provenience)) {
                $this->Flash->success(__('The provenience has been saved.'));

                return $this->redirect(['prefix' => false,'action' => 'index']);
            }
            $this->Flash->error(__('The provenience could not be saved. Please, try again.'));
        }
        $regions = $this->Proveniences->Regions->find('list', ['limit' => 200]);
        $this->set(compact('provenience', 'regions'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Provenience id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $this->getRequest()->allowMethod(['post', 'delete']);
        $provenience = $this->Proveniences->get($id);
        if ($this->Proveniences->delete($provenience)) {
            $this->Flash->success(__('The provenience has been deleted.'));
        } else {
            $this->Flash->error(__('The provenience could not be deleted. Please, try again.'));
        }

        return $this->redirect(['prefix' => false,'action' => 'index']);
    }
}
