<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Http\Exception\NotFoundException;
use elFinder;
use elFinderConnector;

/**
 * CKE connector Controller
 */
class CkeconnectorController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();

        // Load Component 'GeneralFunctions'
        $this->loadComponent('GeneralFunctions');
    }

    public function index()
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1, 2])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $this->request->allowMethod(['post', 'get']);
        // is_readable('./vendor/autoload.php') && require './vendor/autoload.php';
        // require WWW_ROOT.'assets/js/elfinder/php/autoload.php';
        elFinder::$netDrivers['ftp'] = 'FTP';

        $opts = [
          'debug' => true,
          'roots' => [
            // Items volume
            [
              'driver'        => 'LocalFileSystem',           // driver for accessing file system (REQUIRED)
              'path'          =>  WWW_ROOT . 'files-up',      // path to files (REQUIRED)
              'URL'           => '/' . 'files-up',            // URL to files (REQUIRED)
              'winHashFix'    => DIRECTORY_SEPARATOR !== '/', // to make hash same to Linux one on windows too
              'uploadDeny'    => array('all'),                // All Mimetypes not allowed to upload
              'uploadAllow'   => [
                'image/x-ms-bmp', 'image/gif', 'image/jpeg', 'image/png', 'image/x-icon',
                'text/plain', 'application/pdf',
                'application/msword',
                'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                'application/vnd.ms-excel',
                'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                'application/vnd.ms-powerpoint',
                'application/vnd.openxmlformats-officedocument.presentationml.presentation',
                'application/vnd.oasis.opendocument.text',
                'application/vnd.oasis.opendocument.spreadsheet',
                'application/vnd.oasis.opendocument.presentation',
              ],
              'uploadOrder'   => array('deny', 'allow'),      // allowed Mimetype `image` and `text/plain` only
              'accessControl' => 'access'                     // disable and hide dot starting files (OPTIONAL)
            ],
            [
              'driver'        => 'LocalFileSystem',           // driver for accessing file system (REQUIRED)
              'path'          =>  WWW_ROOT . 'pubs',          // path to files (REQUIRED)
              'URL'           => '/' . 'pubs',                // URL to files (REQUIRED)
              'winHashFix'    => DIRECTORY_SEPARATOR !== '/', // to make hash same to Linux one on windows too
              'uploadDeny'    => array('all'),                // All Mimetypes not allowed to upload
              'uploadOrder'   => array('deny', 'allow'),      // allowed Mimetype `image` and `text/plain` only
              'accessControl' => 'access'                     // disable and hide dot starting files (OPTIONAL)
            ]
          ]
        ];

        // run elFinder
        $this->autoRender = false;
        $connector = new elFinderConnector(new elFinder($opts));
        $connector->run();
    }

    public function view($id)
    {
        if ($id !== 'elfinder') {
            $path = implode('/', $this->getRequest()->getParam('pass'));
            return $this->response->withFile(ROOT . '/vendor/studio-42/elfinder/' . $path);
        }
    }
}
