<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\Utility\Inflector;

/**
 * EntitiesPublications Controller
 *
 * @property \App\Model\Table\EntitiesPublicationsTable $EntitiesPublications
 * @property \App\Model\Table\PublicationsTable $Publications
 *
 * @method \App\Model\Entity\ArtifactsPublication[]
 * |\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class EntitiesPublicationsController extends AppController
{
    /**
     * initialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();

        // Load Component 'GeneralFunctions'
        $this->loadComponent('GeneralFunctions');
    }

    /**
     * Add method.
     *
     * @param string $flag type of add operation.
     * '' => Normal add,
     * 'entity' => Add link to a selected publication,
     * 'publication' => Add link to a selected entity,
     * 'bulk' => Bulk add links.
     * @param int $id Id of the selected publication or selected entity
     * for $flag = 'entity' and $flag = 'publication' respectively.
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($flag = '', $entity_type = null, $id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        if ($flag == '') {
            $entitiesPublication = $this->EntitiesPublications->newEmptyEntity();
            if ($this->getRequest()->is('post')) {
                $entitiesPublication =
                      $this->EntitiesPublications->patchEntity(
                          $entitiesPublication,
                          $this->getRequest()->getData()
                      );
                if ($this->EntitiesPublications->save($entitiesPublication)) {
                    $this->Flash->success('New link has been saved.');
                    return $this->redirect(['action' => 'add']);
                }
                $this->Flash->error('New link could not be saved. Please, try again.');
            }
            $this->set(compact('entitiesPublication', 'flag'));
        } elseif ($flag == 'entity') {
            // Check if publication id is provided
            if (!isset($id)) {
                $this->Flash->error('No Publication selected');
                return $this->redirect(['action' => 'add']);
            }

            $this->paginate = [
                'limit' => 10
            ];
            $entitiesPublication = $this->EntitiesPublications->newEmptyEntity();
            if ($this->getRequest()->is('post')) {
                $entitiesPublication = $this->EntitiesPublications->patchEntity(
                    $entitiesPublication,
                    $this->getRequest()->getData()
                );
                if ($this->EntitiesPublications->save($entitiesPublication)) {
                    $this->Flash->success(__('The entity has been linked.'));
                    return $this->redirect(['action' => 'add', $flag, $entity_type, $id]);
                }
                $this->Flash->error(__('The entity could not be linked. Please, try again.'));
            }
            $entitiesPublications = $this->paginate(
                $this->EntitiesPublications->find(
                    'all',
                    ['order' => 'entity_id',
                    'contain' => ['Publications', Inflector::camelize($entity_type)]]
                )->where(['publication_id' => $id])
            );
            $pub = TableRegistry::getTableLocator()->get("Publications")->find()->where(['id' => $id])->firstOrFail();
            $bibtexkey = $pub->bibtexkey;
            $this->set(compact(
                'id',
                'entitiesPublications',
                'entitiesPublication',
                'flag',
                'entity_type',
                'bibtexkey'
            ));
        } elseif ($flag == 'publication') {
            // Check if artifact id is provided
            if (!isset($id)) {
                $this->Flash->error('No entity selected');
                return $this->redirect(['action' => 'add']);
            }

            $this->paginate = [
                'limit' => 10
            ];
            $entitiesPublication = $this->EntitiesPublications->newEmptyEntity();
            if ($this->getRequest()->is('post')) {
                $entitiesPublication = $this->EntitiesPublications->patchEntity(
                    $entitiesPublication,
                    $this->getRequest()->getData()
                );
                if ($this->EntitiesPublications->save($entitiesPublication)) {
                    $this->Flash->success(__('The publication has been linked.'));
                    return $this->redirect(['action' => 'add', $flag, $entity_type, $id]);
                }
                $this->Flash->error(__('The publication could not be linked. Please, try again.'));
            }
            $entitiesPublications = $this->paginate($this->EntitiesPublications->find('all', [
                'order' => 'publication_id',
                'contain' => [
                    'Publications' => [
                        'EntryTypes',
                        'Journals',
                        'Editors' => [
                            'sort' => ['EditorsPublications.sequence' => 'ASC']
                            ],
                        'Authors' => [
                            'sort' => ['AuthorsPublications.sequence' => 'ASC']
                            ]
                        ],
                    Inflector::camelize($entity_type)
                    ]
                ])->where(['entity_id' => $id]));
            $this->set(compact('id', 'entitiesPublications', 'entitiesPublication', 'flag', 'entity_type'));
        } elseif ($flag == 'bulk') {
            $this->loadComponent('BulkUpload', ['table' => 'EntitiesPublications']);
            $this->BulkUpload->upload();

            $this->set(compact('flag'));
        }
    }

    /**
     * Edit method.
     *
     * @param int $id Entity Publication Link id.
     * @param int $flag page to redirect to.
     * '' => Normal edit,
     * 'entity' => Edit page for a selected publication,
     * 'publication' => Edit page for a selected entity.
     * @param int $parent_id Id of the selected publication or selected artifact
     * for $flag = 'artifact' and $flag = 'publication' respectively.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null, $flag = '', $entity_type = null, $parent_id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        // Check if id is provided
        if (!isset($id)) {
            $this->Flash->error('No Entity-Publication link selected');
            return $this->redirect(['prefix' =>'admin', 'action' => 'add']);
        }

        $entitiesPublication = $this->EntitiesPublications->get($id, [
            'contain' => [Inflector::camelize($entity_type), 'Publications']
        ]);
        if ($this->getRequest()->is(['patch', 'post', 'put'])) {
            $entitiesPublication = $this->EntitiesPublications->patchEntity(
                $entitiesPublication,
                $this->getRequest()->getData()
            );
            if ($this->EntitiesPublications->save($entitiesPublication)) {
                $this->Flash->success(__('Changes has been saved.'));

                if (!isset($parent_id)) {
                    return $this->redirect(['controller' => 'EntitiesPublications', 'action' => 'index']);
                } else {
                    return $this->redirect(['action' => 'add', $flag, $entity_type, $parent_id]);
                }
            }
            $this->Flash->error(__('Changes could not be saved. Please, try again.'));
        }
        $this->set(compact('entitiesPublication', 'flag', 'parent_id', 'entity_type'));
    }

    /**
     * Delete method.
     *
     * @param int $id Artifact Publication Link id.
     * @param int $flag page to redirect to.
     * '' => Index page,
     * 'artifact' => Add page for a selected publication,
     * 'publication' => Add page for a selected artifact.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null, $flag = '', $entity_type = null, $parent_id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $this->getRequest()->allowMethod(['post', 'delete']);
        $entitiesPublication = $this->EntitiesPublications->get($id);
        if ($this->EntitiesPublications->delete($entitiesPublication)) {
            $this->Flash->success(__('The link has been deleted.'));
        } else {
            $this->Flash->error(__('The link could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'add', $flag, $entity_type, $parent_id]);
    }
}
