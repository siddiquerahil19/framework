<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use App\Utility\SessionManager;
use Cake\I18n\Time;
use Cake\Routing\Router;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();

        // Load Component 'GeneralFunctions'
        $this->loadComponent('GeneralFunctions');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $users = $this->paginate($this->Users->find('all')->contain(['Authors']));

        $this->set(compact('users'));
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($username = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $user = $this->Users->findByUsername($username)->contain(['Authors', 'Roles'])->first();

        $this->set('user', $user);
        $this->set('roles', $this->Users->Roles->find()->all());
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($username)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->getConfig('authError'));
            return $this->redirect($this->referer());
        }

        $user = $this->Users->findByUsername($username)->contain(['Roles'])->first();

        if ($this->getRequest()->is(['post', 'put'])) {
            $formData = $this->getRequest()->getData();

            if ($this->Auth->user('id') === $user['id']) {
                if (!$formData->active) {
                    $this->Flash->error(__('You cannot deactivate your own account.'));
                    return $this->redirect($this->referer());
                }
                if (!$formData->{'2fa_status'}) {
                    $this->Flash->error(__('You cannot reset 2FA on your own account.'));
                    return $this->redirect($this->referer());
                }
            }

            $formData['roles'] = array_filter($formData['roles'], function ($role) {
                return $role['id'] !== '0';
            });
            $user = $this->Users->patchEntity($user, $formData, [
                'fields' => ['active', '2fa_status', 'author_id', 'roles'],
                'associated' => ['Roles']
            ]);

            if ($this->Users->save($user)) {
                SessionManager::updateUserSessions($user);
                $this->Flash->success(__('Successfully edited'));
                return $this->redirect(['action' => 'view', $user->username]);
            } else {
                $this->Flash->error(__('Editing user failed'));
                return $this->redirect([]);
            }
        }

        $this->set('user', $user);
        $this->set('roles', $this->Users->Roles->find()->all());
    }

    /**
     * Create a password reset link
     *
     * @param string|null $username
     * @return \Cake\Http\Response|null Redirects back to view. Displays reset link when successful.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function forgotPassword($username)
    {
        $time = Time::now();
        $unixTime = $time->toUnixString();

        $user = $this->Users->findByUsername($username)->contain(['Roles'])->first();

        // Check if reset password requested witin span of 5 minutes
        if (!is_null($user->generated_at) && ($unixTime - $user->generated_at->toUnixString() < 5*60)) {
            $this->Flash->error(__('Last reset link generated less than 5 minutes ago. Please try again later.'));
        } else {
            $randomKey = uniqid();
            $uniqueKey = substr($unixTime, 0, 5) . $randomKey . substr($unixTime, 5);
            $url = Router::url('/forgot/password/reset/' . $uniqueKey, true);

            $user = $this->Users->patchEntity($user, [
                'token_pass' => $randomKey,
                'generated_at' => $time
            ]);

            if ($this->Users->save($user)) {
                $this->Flash->success(__('The password reset link is - {0}', $url));
            } else {
                $this->Flash->error(__('Creating reset link failed.'));
            }
        }

        $this->redirect(['action' => 'view', $username]);
    }
}
