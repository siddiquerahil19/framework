<?php

namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Archives Controller
 *
 * @property \App\Model\Table\UpdateEventsTable $UpdateEvents
 *
 * @method \App\Model\Entity\UpdateEvent[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UpdateEventsController extends AppController
{
    public $paginate = [
        'conditions' => ['OR' => [['status' => 'created'], ['status' => 'submitted']]],
        'contain' => ['Creators', 'ExternalResources'],
        'order' => ['UpdateEvents.created' => 'desc'],
    ];

    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();

        // Load Component 'GeneralFunctions'
        $this->loadComponent('GranularAccess');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        if (!$this->GranularAccess->canReviewEdits()) {
            throw ForbiddenException();
        }

        $updateEvents = $this->paginate($this->UpdateEvents);
        $this->set('updateEvents', $updateEvents);
    }
}
