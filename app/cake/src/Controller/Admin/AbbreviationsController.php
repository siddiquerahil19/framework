<?php

namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Abbreviations Controller
 *
 * @property \App\Model\Table\AbbreviationsTable $Abbreviations
 *
 * @method \App\Model\Entity\Abbreviation[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AbbreviationsController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();

        // Load Component 'GeneralFunctions'
        $this->loadComponent('GeneralFunctions');
    }

    /**
     * View method
     *
     * @param string|null $id Abbreviation id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $abbreviation = $this->Abbreviations->get($id, [
            'contain' => ['Publications'],
        ]);

        $this->set('abbreviation', $abbreviation);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $abbreviation = $this->Abbreviations->newEmptyEntity();
        if ($this->getRequest()->is('post')) {
            $data = $this->getRequest()->getData();

            if ($data['bibtexkey']) {
                $bibtexkeyQuery = $this->Abbreviations->Publications
                    ->find()
                    ->select(['id', 'bibtexkey'])
                    ->where(['bibtexkey =' => trim($data['bibtexkey'])])
                    ->first();
                if ($bibtexkeyQuery) {
                    $data['publication_id'] = $bibtexkeyQuery->id;
                }
            } else {
                $data['publication_id'] = null;
            }

            $abbreviation = $this->Abbreviations->patchEntity($abbreviation, $data);

            if ($this->Abbreviations->save($abbreviation)) {
                $this->Flash->success(__('The abbreviation has been saved.'));

                return $this->redirect(['prefix' => false,'action' => 'index']);
            }
            $this->Flash->error(__('The abbreviation could not be saved. Please, try again.'));
        }
        $publications = $this->Abbreviations->Publications->find('list', ['limit' => 200]);
        $this->set(compact('abbreviation', 'publications'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Abbreviation id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $abbreviation = $this->Abbreviations->get($id, [
            'contain' => [],
        ]);

        $abbreviationsTableId = $this->Abbreviations
            ->find()
            ->select(['id', 'publication_id'])
            ->where(['id =' => $id])
            ->first();

        if ($abbreviationsTableId->publication_id) {
            $publicationId = $abbreviationsTableId->publication_id;
            $publicationsTableId = $this->Abbreviations->Publications
                ->find()
                ->select(['id', 'bibtexkey'])
                ->where(['id =' => $publicationId])
                ->first();
        }

        $bibtexkey = '';

        if ($this->getRequest()->is(['patch', 'post', 'put'])) {
            $data = $this->getRequest()->getData();

            if ($data['bibtexkey']) {
                $bibtexkey = $data['bibtexkey'];
                $bibtexkeyQuery = $this->Abbreviations->Publications
                    ->find()
                    ->select(['id', 'bibtexkey'])
                    ->where(['bibtexkey =' => trim($data['bibtexkey'])])
                    ->first();
                if ($bibtexkeyQuery) {
                    $data['publication_id'] = $bibtexkeyQuery->id;
                }
            } else {
                $data['publication_id'] = null;
            }

            $abbreviation = $this->Abbreviations->patchEntity($abbreviation, $data);

            if ($this->Abbreviations->save($abbreviation)) {
                $this->Flash->success(__('The abbreviation has been saved.'));
                return $this->redirect(['prefix' => false,'action' => 'index']);
            }
            $this->Flash->error(__('The abbreviation could not be saved. Please, try again.'));
        }

        $publications = $this->Abbreviations->Publications->find('list', ['limit' => 200]);

        if ($abbreviationsTableId->publication_id && !$bibtexkey) {
            $bibtexkey = $publicationsTableId->bibtexkey;
        }

        $this->set(compact('abbreviation', 'publications', 'bibtexkey'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Abbreviation id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $this->getRequest()->allowMethod(['post', 'delete']);
        $abbreviation = $this->Abbreviations->get($id);
        if ($this->Abbreviations->delete($abbreviation)) {
            $this->Flash->success(__('The abbreviation has been deleted.'));
        } else {
            $this->Flash->error(__('The abbreviation could not be deleted. Please, try again.'));
        }

        return $this->redirect(['prefix' => false,'action' => 'index']);
    }
}
