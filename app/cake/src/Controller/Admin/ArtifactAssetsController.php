<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * ArtifactAssets Controller
 *
 * @property \App\Model\Table\ArtifactAssetsTable $ArtifactAssets
 * @method \App\Model\Entity\ArtifactAsset[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ArtifactAssetsController extends AppController
{
    /**
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('GeneralFunctions');
    }

    public function index()
    {
        $status = $this->_checkIndex();
        $fileOnly = array_values(array_diff_key($status['files'], $status['index']));
        $indexOnly = array_values(array_diff_key($status['index'], $status['files']));
        $this->set(compact('fileOnly', 'indexOnly'));
    }

    public function updateIndex()
    {
        $status = $this->_checkIndex();
        $unindexed = array_diff_key($status['files'], $status['index']);
        $this->set('unindexed', $unindexed);
    }

    public function missingFiles()
    {
        $status = $this->_checkIndex();
        $missingFiles = array_diff_key($status['index'], $status['files']);
        $this->set('missingFiles', $missingFiles);
    }

    private function _checkIndex()
    {
        $assets = $this->ArtifactAssets->find()
            ->select(['id', 'asset_type', 'file_format', 'artifact_aspect', 'artifact_id'])
            ->disableBufferedResults()
            ->all();

        $index = [];
        foreach ($assets as $asset) {
            $index[WWW_ROOT . $asset->getPath()] = $asset->id;
        }

        $searchPatterns = [
            // Single files
            'photo/*', 'lineart/*', 'eps/*.eps.zip', 'pdf/*', 'svg/*',
            // 3D models
            'vcmodels/*/*.ply',
            // RTI
            'ptm/*/info.xml'
        ];

        $files = [];
        foreach ($searchPatterns as $pattern) {
            foreach (glob(WWW_ROOT . 'dl' . DS . $pattern) as $file) {
                $files[$file] = true;
            }
        }

        return ['index' => $index, 'files' => $files];
    }

    public function add()
    {
        if ($this->getRequest()->is('post')) {
            $data = $this->getRequest()->getData('asset');
            $entities = [];
            foreach ($data as $asset) {
                if (array_key_exists('path', $asset)) {
                    $entities[] = $this->ArtifactAssets->newEntityFromFile($asset['path']);
                } else {
                    $entity = $this->ArtifactAssets->newEmptyEntity();
                    $this->ArtifactAssets->patchEntity($entity, $asset);
                    $entities[] = $entity;
                }
            }

            $success = $this->ArtifactAssets->saveMany($entities);
            if (!$success) {
                $message = 'Encountered errors: ';
                foreach ($entities as $index => $entity) {
                    if (!$entity->hasErrors()) {
                        continue;
                    }

                    $asset = $data[$index];
                    if (array_key_exists('path', $asset)) {
                        $message .= $asset['path'];
                    } else {
                        $message .= 'visual asset #' . ($index + 1);
                    }
                    $message .= ': ';
                    foreach ($entity->getErrors() as $field => $errors) {
                        foreach ($errors as $error) {
                            $message .= $field . ': ' . $error . ', ';
                        }
                    }
                }
                $message = preg_replace('/, $/', '', $message);
            }

            if ($this->RequestHandler->prefers() === 'html') {
                if (!$success) {
                    $this->Flash->error($message);
                }
                return $this->redirect(['action' => 'index']);
            } else {
                if ($success) {
                    return $this->getResponse()->withStatus(200);
                } else {
                    return $this->getResponse()->withStatus(500)->withStringBody($message);
                }
            }
        }
    }

    public function delete($id)
    {
        if (!$this->getRequest()->is('post')) {
            return $this->getResponse()->withStatus(405);
        }

        $asset = $this->ArtifactAssets->get($id);
        $success = $this->ArtifactAssets->delete($asset);
        $message = 'Deleting entry of ' . $asset->getPath() . ' failed.';


        if ($this->RequestHandler->prefers() === 'html') {
            if (!$success) {
                $this->Flash->error($message);
            }
            return $this->redirect(['action' => 'index']);
        } elseif ($success) {
            return $this->getResponse()->withStatus(200);
        } else {
            return $this->getResponse()->withStatus(500)->withStringBody($message);
        }
    }

    public function edit($id)
    {
        $asset = $this->ArtifactAssets->get($id, ['contain' => ['Artifacts']]);
        if ($this->getRequest()->is(['patch', 'post', 'put'])) {
            $this->ArtifactAssets->patchEntity($asset, $this->getRequest()->getData(), [
                'fields' => ['is_public']
            ]);
            if (!$this->ArtifactAssets->save($asset)) {
                $this->Flash->error(__('Error saving visual asset.'));
            }
            $this->redirect(['prefix' => false, 'controller' => 'ArtifactAssets', 'action' => 'view', $id]);
        }

        $this->set('asset', $asset);
    }
}
