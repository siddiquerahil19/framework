<?php

namespace App\Controller\Admin;

use App\Controller\AppController;

use ArrayObject;
use Cake\ORM\TableRegistry;

/**
 * Publications Controller
 *
 * @property \App\Model\Table\PublicationsTable $Publications
 *
 * @method \App\Model\Entity\Publication[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PublicationsController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();

        // Load Component 'GeneralFunctions'
        $this->loadComponent('GeneralFunctions');
    }

    /**
    * Add method
    *
    * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
    */
    public function add($flag = '')
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1,2,12])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        if ($flag == '') {
            $publication = $this->Publications->newEmptyEntity();
            if ($this->getRequest()->is('post')) {
                $data = $this->getRequest()->getData();
                $publication = $this->Publications->patchEntity(
                    $publication,
                    $data,
                    ['associated' => ['Authors',
                    'Editors']]
                );
                if ($this->Publications->save($publication)) {
                    $this->Flash->success(__('The publication has been saved.'));

                    return $this->redirect(['prefix' => false, 'action' => 'index']);
                }
                $this->Flash->error(__('The publication could not be saved. Please, try again.'));
            }
            $entryTypes = $this->Publications->EntryTypes->find('list', [
                'keyField' => 'id',
                'valueField' => 'label',
                'order' => 'label'
            ])->toArray();
            $journals = $this->Publications->Journals->find('list', [
                'keyField' => 'id',
                'valueField' => 'journal',
                'order' => 'journal'
            ])->toArray();
            $this->set(compact('publication', 'entryTypes', 'journals', 'flag'));
        } elseif ($flag == 'bulk') {
            $this->loadComponent('BulkUpload', ['table' => 'Publications']);
            $this->BulkUpload->upload();

            $this->set(compact('flag'));
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Publication id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1,2,12])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        // Check if id is provided
        if (!isset($id)) {
            $this->Flash->error('No Publication selected');
            return $this->redirect(['prefix' =>false, 'action' => 'index']);
        }

        $publication = $this->Publications->get($id, [
            'contain' => ['Authors', 'Editors']
        ]);
        if ($this->getRequest()->is(['patch', 'post', 'put'])) {
            $publication = $this->Publications->patchEntity($publication, $this->getRequest()->getData());
            if ($this->Publications->save($publication)) {
                $this->Flash->success(__('The publication has been saved.'));

                return $this->redirect(['prefix' =>false,'action' => 'index']);
            }
            $this->Flash->error(__('The publication could not be saved. Please, try again.'));
        }
        $entryTypes = $this->Publications->EntryTypes->find('list', [
            'keyField' => 'id',
            'valueField' => 'label',
            'order' => 'label'
        ])->toArray();
        $journals = $this->Publications->Journals->find('list', [
            'keyField' => 'id',
            'valueField' => 'journal',
            'order' => 'journal'
        ])->toArray();
        $this->set(compact('publication', 'entryTypes', 'journals'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Publication id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $this->getRequest()->allowMethod(['post', 'delete']);
        $publication = $this->Publications->get($id);
        if ($this->Publications->delete($publication)) {
            $this->Flash->success(__('The publication has been deleted.'));
        } else {
            $this->Flash->error(__('The publication could not be deleted. Please, try again.'));
        }

        return $this->redirect(['prefix' => false, 'action' => 'index']);
    }

    public function autocomplete()
    {
        $this->autoRender = false;
        $terms = $this->Publications->find('list', [
            'conditions' => [
                        'bibtexkey LIKE' => trim($this->getRequest()->getQuery('term')) . '%'
                ],
                'valueField' => ['bibtexkey']
            ])->toArray();
        echo json_encode($terms);
    }

    /**
     * Export method for downloading the entries containing errors.
     */
    public function export()
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1,2,12])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $this->loadComponent('BulkUpload', ['table' => 'Publications']);
        $this->BulkUpload->export();
    }

    /**
     * Index page for selecting publications for merging.
     */
    public function mergeSelect()
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $publications = null;   // Default value
        $data = $this->getRequest()->getQueryParams();
        if (!empty($data)) {
            $searchFilter = array();
            $publications = $this->Publications->find('all', [
                'contain' => [
                    'EntryTypes',
                    'Journals',
                    'Editors' => [
                        'sort' => ['EditorsPublications.sequence' => 'ASC']
                        ],
                    'Authors' => [
                        'sort' => ['AuthorsPublications.sequence' => 'ASC']
                        ]
                    ],
                'order' => [
                    'COALESCE(Publications.bibtexkey, "zz") ASC',
                    'COALESCE(Publications.title, "zz") ASC',
                    'COALESCE(Publications.designation, "zz") ASC'
                ],
                'limit' => 50
            ])
            ->distinct(['Publications.id']);

            foreach ($data as $field => $value) {
                $value = trim($value);
                if (!empty($value) and $field !== 'page') {
                    if ($field == 'entry_type_id' or $field == 'journal_id') {
                        $searchFilter["Publications.{$field}"] = $value;
                    } elseif ($field == 'from') {
                        $searchFilter["Publications.year >="] = $value;
                    } elseif ($field == 'to') {
                        $searchFilter["Publications.year <="] = $value;
                    } elseif ($field == 'author') {
                        $filter = ['Authors.author LIKE' => "%{$value}%"];
                        $publications = $publications->innerJoinWith(
                            'Authors',
                            function ($q) use ($filter) {
                                return $q->where($filter);
                            }
                        );
                    } else {
                        $searchFilter["Publications.{$field} LIKE"] = "%{$value}%";
                    }
                }
            }

            $publications = $publications->where($searchFilter);
            if ($publications->count() > 50) {
                $this->Flash->error(__('Search results exceed the limit of 50 results.
                 Displaying only the top 50 search results. Please further narrow the search criteria.'));
            }
        }

        $entryTypes = $this->Publications->EntryTypes->find('list', [
            'keyField' => 'id',
            'valueField' => 'label',
            'order' => 'label'
        ])->toArray();
        $journals = $this->Publications->Journals->find('list', [
            'keyField' => 'id',
            'valueField' => 'journal',
            'order' => 'journal'
        ])->toArray();

        $this->set(compact('publications', 'entryTypes', 'journals', 'data'));
    }

    /**
     * Merging selected publications.
     */
    public function merge($id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1,2,12])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            if ((!isset($data['ids'])) or count($data['ids']) < 2) {
                $this->Flash->error(__('Atleast two publications need to be selected for merging.'));
                return $this->redirect(['action' => 'mergeSelect']);
            }
            /**
            * check if data came from merge form, not merge-select form
            */
            $publication_merge = $this->Publications->get($data['ids'][0], [
                    'contain' => [
                        'Artifacts' => [
                            'Proveniences',
                            'Periods',
                            'ArtifactTypes',
                            'Collections',
                            'sort' => 'Artifacts.id'
                        ],
                        'EntryTypes',
                        'Journals',
                        'Editors' => [
                            'sort' => ['EditorsPublications.sequence' => 'ASC']
                            ],
                        'Authors' => [
                            'sort' => ['AuthorsPublications.sequence' => 'ASC']
                            ]
                        ]
                ]);
            $publications = $this->Publications->find('all', [
                    'conditions' => ['Publications.id IN' => $data['ids']],
                    'contain' => [
                        'Artifacts' => [
                            'Proveniences',
                            'Periods',
                            'ArtifactTypes',
                            'Collections',
                            'sort' => 'Artifacts.id'
                        ],
                        'EntryTypes',
                        'Journals',
                        'Editors' => [
                            'sort' => ['EditorsPublications.sequence' => 'ASC']
                            ],
                        'Authors' => [
                            'sort' => ['AuthorsPublications.sequence' => 'ASC']
                            ]
                        ]
                ]);
            unset($data['ids']);
            $artifacts_objects = [];
            $artifacts_values = [];

            foreach ($publications as $publication) {
                foreach ($publication->artifacts as $artifact) {
                    // To pre-fill the shortened publication, we need to find the real designation.
                    preg_match('/\w+ \d+/', $publication->designation, $matches);
                    $new_designation = $matches[0];
                    $new_exact_ref = trim(preg_replace('/' . $new_designation . '/', '', $publication->designation), '., ');
                    $artifact->_joinData->exact_reference = $new_exact_ref;
                    $value = [
                        $artifact->id,
                        $artifact->_joinData->publication_type,
                        $artifact->_joinData->exact_reference
                    ];
                    if (!in_array($value, $artifacts_values)) {
                        array_push($artifacts_values, $value);
                        array_push($artifacts_objects, $artifact);
                    }
                }
            }
            $publication_merge->artifacts = $artifacts_objects;
            if (strpos($this->referer(), 'merge-select') == false) {
                /**
                 * Fetch data of the first of the publications to fill in
                 * the edit form of the future merged publication
                 */
                $publication_merge = $this->Publications->patchEntity($publication_merge, $data, [
                'associated' => ['Artifacts', 'Authors', 'Editors']
                ]);

                // Merging all associated artifacts to the final entity
                $publication_merge->artifacts = [];
                foreach ($data['artifacts'] as $key => $artifact_link) {
                    array_push($publication_merge->artifacts, $this->Publications->Artifacts->get(
                        $artifact_link['id']
                    ));

                    $publication_merge->artifacts[$key]->_joinData = $this->Publications->EntitiesPublications->
                    patchEntity($this->Publications->EntitiesPublications->get(
                        $artifact_link['_joinData']['id']
                    ), $artifact_link['_joinData']);
                }
                if ($this->Publications->save($publication_merge)) {
                    foreach (array_slice($publications->toArray(), 1) as $entity) {
                        $this->Publications->delete($entity);
                    }
                    $this->Flash->success('Successfully merged all publications.');
                    $this->redirect(['action' => 'mergeSelect']);
                } else {
                    $this->Flash->error('Could not merge.');
                }
            }
            $entryTypes = $this->Publications->EntryTypes->find('list', [
                'keyField' => 'id',
                'valueField' => 'label',
                'order' => 'label'
            ])->toArray();
            $journals = $this->Publications->Journals->find('list', [
                'keyField' => 'id',
                'valueField' => 'journal',
                'order' => 'journal'
            ])->toArray();
            $publication_type_options = [
                'primary' => 'primary',
                'electronic' => 'electronic',
                'citation' => 'citation',
                'collation' => 'collation',
                'history' => 'history',
                'other' => 'other'
            ];
            $this->set(compact(
                'publications',
                'publication_merge',
                'entryTypes',
                'journals',
                'publication_type_options'
            ));
        } else {
            $this->Flash->error('Please first select publications for merging.');
            return $this->redirect(['action' => 'mergeSelect']);
        }
    }
}
