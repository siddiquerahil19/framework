<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\Query;

/**
 * Artifacts Controller
 *
 * @property \App\Model\Table\ArtifactsTable $Artifacts
 *
 * @method \App\Model\Entity\Artifact[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ArtifactsController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();

        // Load Component 'GeneralFunctions'
        $this->loadComponent('GeneralFunctions');
        $this->loadComponent('GranularAccess');
    }

    // Method for Retired Artifacts Index
    public function retired()
    {
        //Access check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $retired = $this->Artifacts->find('all', ['conditions'=> ['Artifacts.retired = 1']]);

        $this->paginate = [
            'contain' => ['RedirectArtifacts'],
            'limit' => 20,
            'order' => [
                'id' => 'ASC'
            ]
        ];

        $retired = $this->paginate($retired);
        $this->set(compact('retired'));
    }

    public function sealChemistryAdd()
    {
        //Access check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $sealChemistry = $this->loadModel('ChemicalData');
        $calibration = $this->loadModel('Calibration');
        $calibrationData = $calibration->find('all')->select(['human_number','id']);
        $fileNames = array();
        $fileIds = array();
        foreach ($calibrationData as $calData) {
            array_push($fileNames, $calData->human_number);
            array_push($fileIds, $calData->id);
        }

        $sealChemistryAddData = $sealChemistry->newEmptyEntity();
        $data_to_push = array();

        if ($this->getRequest()->is('post')) {
            $data = $this->getRequest()->getData();
            $header = array();
            $row=1;
            if (($handle = fopen($_FILES['chemicalSheet']['tmp_name'], "r")) !== false) {
                while (($chemdata = fgetcsv($handle, 1000, ",")) !== false) {
                    if ($row==1) {
                        foreach ($chemdata as $columnName) {
                            if ($columnName === '+/-') {
                                $element = $header[count($header) - 1];
                                $header[] = $element . '_' . $columnName;
                            } else {
                                $header[] = $columnName;
                            }
                        }
                        $row=$row+1;
                        continue;
                    }
                    $currentData = array();
                    $chemical_data = array();
                    $currentData = array_filter(array_combine($header, $chemdata));
                    $chemical_data['artifact_id'] = (int)filter_var($currentData['P Number'], FILTER_SANITIZE_NUMBER_INT);
                    $chemical_data['area'] = strtolower($currentData['Area']);
                    $chemical_data['area_description'] = isset($currentData['Area description']) ? $currentData['Area description'] : '';
                    $chemical_data['reading'] = $currentData['Reading'];
                    $chemical_data['date'] = $currentData['Date'];
                    $chemical_data['time'] = $currentData['Time'];
                    $chemical_data['duration'] = $currentData['Duration'];
                    $chemical_data['chemistry'] = array();
                    foreach (array_slice($currentData, 6) as $label=>$value) {
                        if (str_contains($label, '+/-')) {
                            $chemical_data['chemistry'][str_replace('_+/-', '', $label)]['+/-'] = $value;
                        } else {
                            $chemical_data['chemistry'][$label] = array(
                                'value' => $value
                            );
                        }
                    }
                    $chemical_data['chemistry'] = json_encode($chemical_data['chemistry']);
                    $chemical_data['calibration_id'] = $fileIds[intval($data['Calibration_file'])];
                    array_push($data_to_push, $chemical_data);
                }
                fclose($handle);
            }
            $entities = $sealChemistry->newEntities($data_to_push);
            if ($sealChemistry->saveMany($entities)) {
                $this->Flash->success(__('The chemical data has been saved.'));

                return  $this->redirect(
                    ['action' => 'sealChemistryAdd']
                );
            }
            $this->Flash->error(__('The chemical data could not be saved. Please, try again.'));
        }
        $this->set(compact('sealChemistryAddData', 'fileNames'));
    }

    public function calibrationAdd()
    {
        $calibration = $this->loadModel('Calibration');

        $calibrationAddData = $calibration->newEmptyEntity();


        if ($this->getRequest()->is('post')) {
            $data = $this->getRequest()->getData();
            $datafinal = '';
            if (($handle = fopen($_FILES['calibrationSheet']['tmp_name'], "r")) !== false) {
                while (($cal = fgetcsv($handle, 1000, ",")) !== false) {
                    $datafinal = $datafinal.implode(",", $cal)."\n";
                }
                fclose($handle);
            }
            $data['content'] = $datafinal;
            $data['human_number'] = $data['humanId'];
            unset($data['humanId']);
            unset($data['calibrationSheet']);
            $entities = $calibration->patchEntity($calibrationAddData, $data);
            if ($calibration->save($entities)) {
                $this->Flash->success(__('The calibration has been saved.'));
                return $this->redirect(
                    ['action' => 'calibrationAdd']
                );
            }
            $this->Flash->error(__('The calibration could not be saved. Please, try again.'));
        }
        $this->set(compact('calibrationAddData'));
    }


    public function featuredSealsAdd()
    {
        //Access check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $featuredSeals = $this->loadModel('FeaturedSeals');
        $featuredSealsAddData = $featuredSeals->newEmptyEntity();

        if ($this->getRequest()->is('post')) {
            $data = $this->getRequest()->getData();
            $data['artifact_id'] = (int)$data['Artifact_id'];
            unset($data['Artifact_id']);
            unset($data['Image_type']);
            $featuredSealsAddData = $featuredSeals->patchEntity($featuredSealsAddData, $data);

            if ($featuredSeals->save($featuredSealsAddData)) {
                $this->Flash->success(__('The featured seal has been saved.'));

                return  $this->redirect(
                    ['action' => 'featuredSealsAdd']
                );
            }
            $this->Flash->error(__('The featured seal could not be saved. Please, try again.'));
        }


        $this->set(compact('featuredSealsAddData'));
    }

    public function featuredSealsEdit($id = null)
    {
        //Access check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $featuredSeals = $this->loadModel('FeaturedSeals');

        $featuredSealsEditData = $featuredSeals->get($id, [
            'contain' => ['Artifacts','Artifacts.Periods'],
        ]);

        $this->GranularAccess->amendArtifactAssets($featuredSealsEditData->artifact);

        if ($this->getRequest()->is(['patch', 'post', 'put'])) {
            $data = $this->getRequest()->getData();
            unset($data['period']);
            unset($data['designation']);
            unset($data['Image_type']);

            $featuredSealsEditData = $featuredSeals->patchEntity($featuredSealsEditData, $data);

            if ($featuredSeals->save($featuredSealsEditData)) {
                $this->Flash->success(__('The featured seal has been saved.'));
                return $this->redirect(
                    ['action' => 'featuredSealsEdit',$id]
                );
            }
            $this->Flash->error(__('The featured seal could not be saved. Please, try again.'));
        }

        $this->set(compact('featuredSealsEditData'));
    }
}
