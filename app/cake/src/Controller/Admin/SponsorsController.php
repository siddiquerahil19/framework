<?php

namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Sponsor Controller
 *
 * @property \App\Model\Table\SponsorTable $Sponsor
 *
 * @method \App\Model\Entity\Sponsor[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SponsorsController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();

        // Load Component 'GeneralFunctions'
        $this->loadComponent('GeneralFunctions');
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $sponsor = $this->Sponsors->newEmptyEntity();
        if ($this->getRequest()->is('post')) {
            $sponsor = $this->Sponsors->patchEntity($sponsor, $this->getRequest()->getData());
            if ($this->Sponsors->save($sponsor)) {
                $id = $sponsor->id;
                $image = $this->request->getUploadedFile('image_file');
                print_r($image);
                if ($image !== null && $image->getError() !== \UPLOAD_ERR_NO_FILE) {
                    $name = $image->getClientFilename();
                    $extension = substr(strrchr($name, '.'), 1);
                    $newname = $id.'.'.$extension;

                    if (1) {
                        $ImagePath = WWW_ROOT.'files-up/images/sponsor-img/'.$newname;

                        try {
                            $image->moveTo($ImagePath);
                        } catch (\Exception $e) {
                            echo 'Image is not uploaded.';
                        }
                    } else {
                        throw new ForbiddenException(__('Only images files are allowed.'));
                    }
                }
                $this->Flash->success(__('The sponsor has been saved.'));
                return $this->redirect(['prefix' => false,'action' => 'index']);
            }
            $this->Flash->error(__('The sponsor could not be saved. Please, try again.'));
        }
        $sponsorTypes = $this->Sponsors->SponsorTypes->find('list', ['limit' => 200]);
        $this->set(compact('sponsor', 'sponsorTypes'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Sponsor id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $sponsor = $this->Sponsors->get($id, [
            'contain' => []
        ]);
        if ($this->getRequest()->is(['patch', 'post', 'put'])) {
            $sponsor = $this->Sponsors->patchEntity($sponsor, $this->getRequest()->getData());
            $image = $this->request->getUploadedFile('image_file');
            if ($image !== null && $image->getError() !== \UPLOAD_ERR_NO_FILE) {
                $name = $image->getClientFilename();
                $extension = substr(strrchr($name, '.'), 1);
                $id = h($sponsor->id);
                $newname = $id.'.'.$extension;
                if (1) {
                    $ImagePath = WWW_ROOT.'files-up/images/sponsor-img/'.$newname;
                    $files = glob(WWW_ROOT . 'files-up/images/sponsor-img/' . $id . ".*");
                    if (count($files) > 0) {
                        unlink($files[0]);
                    }
                    $image->moveTo($ImagePath);
                } else {
                    throw new ForbiddenException(__('Only images files are allowed.'));
                }
            }
            if ($this->Sponsors->save($sponsor)) {
                $this->Flash->success(__('The sponsor has been saved.'));
                return $this->redirect(['prefix' => false,'action' => 'index']);
            }
            $this->Flash->error(__('The sponsor could not be saved. Please, try again.'));
        }
        if (glob(WWW_ROOT.'files-up/images/sponsor-img/'.$id.".*")) {
            $file = glob(WWW_ROOT.'files-up/images/sponsor-img/'.$id.".*");
            $extension = substr(strrchr($file[0], '.'), 1);
            $id = h($sponsor->id);
        } else {
            $id = 'default';
            $extension = 'png';
        }
        $sponsorTypes = $this->Sponsors->SponsorTypes->find('list');
        $this->set(compact('sponsor', 'sponsorTypes', 'id', 'extension'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Sponsor id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1,2])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }
        $this->getRequest()->allowMethod(['post', 'delete']);
        $sponsor = $this->Sponsors->get($id);
        if ($this->Sponsors->delete($sponsor)) {
            if (glob(WWW_ROOT.'files-up/images/sponsor-img/'.$id.".*")) {
                $file = glob(WWW_ROOT.'files-up/images/sponsor-img/'.$id.".*");
                if ($file[0]) {
                    unlink($file[0]);
                }
            }
            $this->Flash->success(__('The sponsor has been deleted.'));
        } else {
            $this->Flash->error(__('The sponsor could not be deleted. Please, try again.'));
        }

        return $this->redirect(['prefix' => false,'action' => 'index']);
    }
}
