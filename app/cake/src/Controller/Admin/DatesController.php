<?php

namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Dates Controller
 *
 * @property \App\Model\Table\DatesTable $Dates
 *
 * @method \App\Model\Entity\Date[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DatesController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();

        // Load Component 'GeneralFunctions'
        $this->loadComponent('GeneralFunctions');
    }


    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $date = $this->Dates->newEmptyEntity();
        if ($this->getRequest()->is('post')) {
            $date = $this->Dates->patchEntity($date, $this->getRequest()->getData());
            if ($this->Dates->save($date)) {
                $this->Flash->success(__('The date has been saved.'));

                return $this->redirect(['prefix' => false,'action' => 'index']);
            }
            $this->Flash->error(__('The date could not be saved. Please, try again.'));
        }
        $months = $this->Dates->Months->find('list', ['limit' => 200]);
        $years = $this->Dates->Years->find('list', ['limit' => 200]);
        $dynasties = $this->Dates->Dynasties->find('list', ['limit' => 200]);
        $rulers = $this->Dates->Rulers->find('list', ['limit' => 200]);
        $artifacts = $this->Dates->Artifacts->find('list', ['limit' => 200]);
        $this->set(compact('date', 'months', 'years', 'dynasties', 'rulers', 'artifacts'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Date id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $date = $this->Dates->get($id, [
            'contain' => []
        ]);
        if ($this->getRequest()->is(['patch', 'post', 'put'])) {
            $date = $this->Dates->patchEntity($date, $this->getRequest()->getData());
            if ($this->Dates->save($date)) {
                $this->Flash->success(__('The date has been saved.'));

                return $this->redirect(['prefix' => false,'action' => 'index']);
            }
            $this->Flash->error(__('The date could not be saved. Please, try again.'));
        }
        $months = $this->Dates->Months->find('list', ['limit' => 200]);
        $years = $this->Dates->Years->find('list', ['limit' => 200]);
        $dynasties = $this->Dates->Dynasties->find('list', ['limit' => 200]);
        $rulers = $this->Dates->Rulers->find('list', ['limit' => 200]);
        $artifacts = $this->Dates->Artifacts->find('list', ['limit' => 200]);
        $this->set(compact('date', 'months', 'years', 'dynasties', 'rulers', 'artifacts'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Date id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $this->getRequest()->allowMethod(['post', 'delete']);
        $date = $this->Dates->get($id);
        if ($this->Dates->delete($date)) {
            $this->Flash->success(__('The date has been deleted.'));
        } else {
            $this->Flash->error(__('The date could not be deleted. Please, try again.'));
        }

        return $this->redirect(['prefix' => false,'action' => 'index']);
    }
}
