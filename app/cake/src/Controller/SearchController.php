<?php

namespace App\Controller;

use App\Controller\AppController;
use App\Utility\CdliProcessor\TokenList;
use Cake\Event\Event;
use Cake\Http\Client;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;

class SearchController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadModel('Artifacts');
        $this->loadComponent('ElasticSearch');
        $this->loadComponent('GeneralFunctions');
        $this->loadComponent('GranularAccess');
        $this->loadComponent('Api', ['features' => ['Bibliography', 'Inscription', 'LinkedData', 'TableExport']]);
        $this->loadComponent('ArtifactImages');

        // Set access for public.
        $this->Auth->allow(['index', 'view', 'tokenList']);
    }

    /**
     * beforeFilter method
     *
     * @return \Cake\Http\Response|void
     */
    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);

        $this->session = $this->getRequest()->getSession();

        //Returns 1 if redirected from advanced search else null
        $this->isAdvancedSearch = (array_key_exists('advanced_search', $this->request->getQueryParams())) ? 1 : null;

        // Get search Settings for Session
        $this->searchSettings = $this->session->read('searchSettings');
        $this->searchSettings['PageSize'] = 25;
        // Searchable parameters on view
        $this->searchableFields = [
            "pdesignation", "authors", "editors", "year",
            "title", "ptype", "publisher", "series",
            "atype", "materials", "collection", "provenience",
            "archive", "period", "acomments", "translation",
            "transliteration", "icomments", "structure", "genres",
            "languages", "adesignation", "museum_no", "accession_no",
            "id", "seal_no", "composite_no", "bibtexkey", "designation_exactref",
            "written_in", "transliteration_permutation", "dates", "project",
            "credit", "aseal_no", "acomposite_no"
        ];

        // Used for displaying the search query
        $this->queryStringMapping = [
            'designation_exactref' => 'Publications exact reference',
            'pdesignation' => 'Publications designation',
            'adesignation' => 'Artifacts designation',
            'icomments' => 'Inscription comments',
            'acomments' => 'Artifacts comments',
            'atype' => 'Artifacts type',
            'ptype' => 'Publications type',
            'authors' => 'Authors',
            'editors' => 'Editors',
            'year' => 'Year',
            'title' => 'Title',
            'publisher' => 'Publisher',
            'series' => 'Series',
            'materials' => 'Materials',
            'collection' => 'Collections',
            'provenience' => 'Provenience',
            'archive' => 'Archive',
            'period' => 'Period',
            'translation' => 'Translation',
            'transliteration' => 'Transliteration',
            'structure' => 'Structure',
            'genres' => 'Genres',
            'languages' => 'Languages',
            'museum_no' => 'Museum number',
            'accession_no' => 'Accession number',
            'id' => 'Identification numbers',
            'seal_no' => 'Seal number',
            'composite_no' => 'Composite number',
            'bibtexkey' => 'Bibtex key',
            'inscription' => 'Inscriptions',
            'keyword' => 'Free Search',
            'publication' => 'Publications',
            'written_in' => 'Written in',
            'transliteration_permutation' => 'Transliteration Permutation',
            'dates' => 'Dates',
            'credit' => 'Credits',
            'project' => 'Projects'
        ];

        $this->inscriptionFields = [
            'transliteration',
            'translation',
            'structure',
            'icomments',
            'transliteration_permutation'
        ];

        $this->searchCategory = [
            'keyword',
            'publication',
            'collection',
            'provenience',
            'period',
            'inscription',
            'id',
        ];

        // Search Result View Settings
        $this->settings = [
            'LayoutType' => 1,
            'Page' => 1,
            'PageSize' => $this->searchSettings['PageSize'],
            'lastPage' => 0,
            'canViewPrivateArtifacts' => $this->GeneralFunctions->checkIfRolesExists([1, 4]) == 1 ? 1 : 0,
            'canViewPrivateInscriptions' => $this->GeneralFunctions->checkIfRolesExists([1, 5]) == 1 ? 1 : 0,
            'canViewPrivateImages' => $this->GeneralFunctions->checkIfRolesExists([1, 7]) == 1 ? 1 : 0,
            // 'sortBy' => 'relevance',
            'filter_dirty' => 0
        ];

        // To store whole array result with key as searchId (as timestamp) in session vaiable
        // Max searchId to be store = 4
        if (is_null($this->session->read('resultsStored'))) {
            $this->session->write('resultsStored', []);
            $this->resultsStored = [];
        } else {
            $this->resultsStored = $this->session->read('resultsStored');
        }
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        ini_set('memory_limit', -1);
        ini_set('max_execution_time', 300);

        $queryData = $this->getRequest()->getQueryParams();

        if (!empty($queryData)) {
            $searchId = time();

            return $this->redirect([
                'action' => 'view',
                $searchId,
                '?' => $queryData
            ]);
        } else {
            return $this->redirect($this->referer());
        }
    }

    /**
     * filter method
     *
     * @param
     * searchId : Unique Search ID.
     *
     * @return \Cake\Http\Response|void
     */
    public function filter($searchId)
    {
        // Filter submitted through form
        $filterData = $this->getRequest()->getData();

        $searchIdArray = explode('-', $searchId);

        if ($searchIdArray[0] === 'reset') {
            $searchId = $searchIdArray[1];
            $currentFilters = $this->resultsStored[$searchId]['filters'];

            $toBeReset = [];

            foreach ($currentFilters as $filter => $values) {
                $toBeReset[$filter] = array_keys(array_filter(
                    $values,
                    function ($value) {
                        return $value;
                    }
                ));
            }

            foreach ($toBeReset as $filter => $values) {
                foreach ($values as $value) {
                    $currentFilters[$filter][$value] = 0;
                }
            }
        } else {
            // Get filters stored in session
            $currentFilters = $this->resultsStored[$searchId]['filters'];

            $currentAppliedFilters = [];

            // Extract filters values which are set
            foreach ($currentFilters as $filter => $values) {
                // Check if filter values are selected or not
                if (!empty($values)) {
                    $selectedValues = array_keys(array_filter(
                        $values,
                        function ($value) {
                            return $value;
                        }
                    ));

                    // If empty check if the $filter exists in $filterData
                    if (!empty($selectedValues)) {
                        $currentAppliedFilters[$filter] = $selectedValues;
                    } else {
                        if (array_key_exists($filter, $filterData)) {
                            $currentAppliedFilters[$filter] = [];
                        }
                    }
                } else {
                    // If empty check if the $filter exists in $filterData
                    if (array_key_exists($filter, $filterData)) {
                        $currentAppliedFilters[$filter] = [];
                    }
                }
            }

            // Check if filters are updated
            $checkIfFilterChanged = 0;

            // Calculate difference between requested and store filters
            foreach ($currentAppliedFilters as $filter => $value) {
                $newValue = $filterData[$filter];
                $newValue = $newValue == '' ? [] : $newValue;

                $arrayDiffRemoved = array_diff($value, $newValue);
                $arrayDiffAdded = array_diff($newValue, $value);

                // If there is change in requested filter applied
                if (!empty($arrayDiffAdded) || !empty($arrayDiffRemoved)) {
                    $checkIfFilterChanged = 1;

                    // If unselected
                    foreach (array_diff($value, $newValue) as $changedValue) {
                        $currentFilters[$filter][$changedValue] = 0;
                    }

                    // If selected
                    foreach (array_diff($newValue, $value) as $changedValue) {
                        $currentFilters[$filter][$changedValue] = 1;
                    }
                }
            }

            // If there is no filter change then redirect to previous page
            if (!$checkIfFilterChanged) {
                $this->redirect($this->referer());
            }
        }

        // Set filter_dirty
        $this->session->write('resultsStored.'.$searchId.'.settings.filter_dirty', 1);

        // Set new filter
        $this->session->write('resultsStored.'.$searchId.'.filters', $currentFilters);

        $queryParams = $this->request->getQueryParams();

        return $this->redirect([
            'action' => 'view',
            $searchId,
            '?' => $queryParams
            ]);
    }

    /**
     * stripInscription method
     *
     * @param
     * inscriptionsQuery : inscriptions-text
     *
     * @return
     * Stripped inscriptions string with all non-sign data (-,[],?,etc.) removed.
     */
    public function stripInscription($inscriptionsQuery)
    {
        $strippedInscription = preg_replace('/[^(\w|\s|\d)]/', ' ', $inscriptionsQuery);
        $strippedInscription = preg_replace('/\s+/', ' ', $strippedInscription);
        return trim($strippedInscription);
    }

    /**
     * highlightInscriptions method
     *
     * @param
     * line and inscriptions query
     *
     * @return
     * Highlighted line of input inscriptions string
     */
    public function highlightInscriptions($line, $inscriptionsQuery)
    {
        //If translit permutation replace "OR" with "|" to be compatible with regex format.
        $regex = "/".preg_replace('/ OR /', '|', $inscriptionsQuery)."/i";

        $strippedInscriptionQuery = $this->stripInscription($inscriptionsQuery);
        //regexBrokenText is used for matching broken texts
        $regexBrokenText = "/".preg_replace('/ OR /', '|', $strippedInscriptionQuery)."/i";
        //Split translit line into words
        $words = preg_split('/\s+/', $line);
        $processedLine = ' ';

        //process each word in the line
        foreach ($words as $word) {
            if (empty($word)) {
                continue;
            }
            if (preg_match($regex, $word)) {
                //plain text matching
                $word = preg_replace($regex, '<span class = "highlightText">'.'$0'.'</span>', $word);
            } else {
                //remove all non-sign data (-,[],?,etc.) and compare with the input query
                $strippedWord = $this->stripInscription($word);
                //broken text matching
                if (preg_match($regexBrokenText, $strippedWord)) {
                    $word = '<span class = "highlightText">'.$word.'</span>';
                }
            }
            $processedLine .= $word.' ';
        }
        return trim($processedLine);
    }

    /**
     * processInscriptions method
     *
     * @param
     * line and inscriptions query
     *
     * @return
     * processed and formatted inscriptions string
     */
    public function processInscriptions($line, $inscriptionsQuery)
    {
        //Translitertaions
        $processedLine = "";
        if (preg_match("/^[0-9]+/", $line)) {
            //encode htmlentites as it can be in inscriptions as well
            $line = htmlentities($line);
            //Highlight matching inscriptions
            if (!empty($inscriptionsQuery)) {
                $line = $this->highlightInscriptions($line, $inscriptionsQuery);
            }
            $processedLine = $line."<br>";
        }
        //Translations
        elseif (preg_match("/^(#tr)/", $line)) {
            $line = substr($line, 4);
            $line = htmlentities($line);
            //Highlight matching inscriptions
            if (!empty($inscriptionsQuery)) {
                $line = $this->highlightInscriptions($line, $inscriptionsQuery);
            }
            $processedLine =  "<i> &emsp;".$line."</i> <br>";
        }
        //Comments
        elseif (preg_match("/^(\$ )/", $line) || preg_match("/^(# )/", $line)) {
            $line = substr($line, 2);
            $line = htmlentities($line);
            //Highlight matching inscriptions
            if (!empty($inscriptionsQuery)) {
                $line = $this->highlightInscriptions($line, $inscriptionsQuery);
            }
            $processedLine =  "<i> &emsp;".$line."</i><br>";
        }
        //structure
        elseif (preg_match("/^@/", $line)) {
            $line = substr($line, 1);
            $line = htmlentities($line);
            //Highlight matching inscriptions
            if (!empty($inscriptionsQuery)) {
                $line = $this->highlightInscriptions($line, $inscriptionsQuery);
            }
            $processedLine =  "<b>".$line."</b><br>";
        }
        return $processedLine;
    }

    /**
     * view method
     *
     * @param
     * searchId : Unique Search ID.
     *
     * @return \Cake\Http\Response|void
     */
    public function view($searchId)
    {
        $keyword = '';
        ini_set('memory_limit', -1);
        ini_set('max_execution_time', 300);

        //check for edit access
        $showedit = false;
        if ($this->GeneralFunctions->checkIfRolesExists([1])) {
            $showedit = true;
        }

        if (substr($searchId, 0, 6) === 'filter') {
            return $this->setaction('filter', substr($searchId, 7));
        } else {
            $queryData = ($this->isAdvancedSearch === 1) ? $this->getDataFromRequest($this->request->getQueryParams()) : $this->optimizedQuery($this->getRequest()->getQueryParams());
            $inscriptionsQuery = '';
            $queryString = '';
            //Convert query into readable query string. It Also extracts the inscriptions from search query.
            if ($this->isAdvancedSearch === 1) {
                foreach ($queryData as $searchField => $value) {
                    if ($searchField === 'transliteration_permutation') {
                        $signNames = $this->ElasticSearch->getSignNames($value);
                        $value = $value.' OR '.$signNames;
                    }
                    $queryString .= $value.' in '.$this->queryStringMapping[$searchField].' ' ;
                    if (in_array($searchField, $this->inscriptionFields)) {
                        $inscriptionsQuery .= $value.' ';
                    }
                    if (array_key_last($queryData) !== $searchField) {
                        $queryString .= 'AND ';
                    }
                }
            } else {
                foreach ($queryData as $searchField) {
                    foreach ($searchField as $searchCategory => $value) {
                        if ($searchCategory !== 'operator') {
                            $keyword=$value;
                            $queryString .= $value.' in '.$this->queryStringMapping[$searchCategory].' ' ;
                            if ($searchCategory === 'inscription') {
                                $inscriptionsQuery .= $value.' ';
                            }
                        } else {
                            $queryString.= $value. ' ';
                        }
                    }
                }
            }
            //Trim whitespace at right
            $queryString = rtrim($queryString);
            $inscriptionsQuery = rtrim($inscriptionsQuery);

            $resultSet = empty($this->resultsStored) ? -1 : (array_key_exists($searchId, $this->resultsStored) ? $this->resultsStored[$searchId]['resultSet'] : null);

            // To check if redirected from filter function
            $filterDirtyStatus = empty($this->resultsStored) ? 0 : $this->session->read('resultsStored.'.$searchId.'.settings.filter_dirty');

            if ($filterDirtyStatus) {
                $this->settings['Page'] = 1;
            }

            $requestedResultSet = (int)floor(($this->settings['Page'] - 1) * $this->settings['PageSize']/ 10000);

            // To check if requested page is within the resultSet.
            $outOfBoundPage = 0;

            if (!is_null($resultSet) && $requestedResultSet != $resultSet) {
                $outOfBoundPage = 1;
            }

            if (!array_key_exists($searchId, $this->resultsStored) || $outOfBoundPage || $filterDirtyStatus) {
                $this->getSearchResults($queryData, $searchId);
            }

            if ($requestedResultSet > 0) {
                $offset = ($this->settings['Page'] - 1) * $this->settings['PageSize'] - $requestedResultSet * 10000;
            } else {
                $offset = ($this->settings['Page'] == 1) ? 0 : ($this->settings['Page'] - 1) * $this->settings['PageSize'];
            }

            $result = array_slice($this->resultsStored[$searchId]['result'], $offset, $this->settings['PageSize'], true);

            $this->settings['lastPage'] = (int)ceil($this->resultsStored[$searchId]['totalHits']/$this->settings['PageSize']);

            $includedTransliterations = 0;
            $includedTranslations = 0;
            $imagesFilters = [];
            foreach ($result as $artifactID => $values) {
                $result[$artifactID] = array_merge(
                    $result[$artifactID],
                    $this->ElasticSearch->getPublicationWithArtifactId($artifactID)
                );

                $images = $this->ElasticSearch->filterImages($artifactID, $this->settings['canViewPrivateImages']);

                foreach ($images as $image) {
                    if ($image['asset_type'] === 'info') {
                        continue;
                    }
                    if (array_key_exists($image['asset_type'], $this->resultsStored[$searchId]['filters']['image_type'])) {
                        continue;
                    }
                    if (array_key_exists($image['asset_type'], $imagesFilters)) {
                        continue;
                    }
                    $imagesFilters = array_merge($imagesFilters, [$image['asset_type'] => 0]);
                }

                $result[$artifactID]['inscription'] = $this->ElasticSearch->getInscriptionWithArtifactId($artifactID, $this->settings['canViewPrivateInscriptions']);

                if (!empty($result[$artifactID]['inscription'])) {
                    $processedInscription = '';
                    foreach (explode("\n", $result[$artifactID]['inscription']['atf']) as $line) {
                        if ($line == "" || substr($line, 0, 2) == ">>") {
                            continue;
                        }
                        $processedLine = $this->processInscriptions($line, $value);
                        if (!empty($processedLine)) {
                            $processedInscription .= $processedLine;
                        }
                    }
                    $result[$artifactID]['inscription']['processedInscription'] = $processedInscription;
                }

                $hasTransliteration = empty($result[$artifactID]['inscription']['atf']) ? 0 : 1;
                $hasTranslation = empty($result[$artifactID]['inscription']['translation']) ? 0 : 1;

                if (!$includedTransliterations && !array_key_exists('transliteration', $this->resultsStored[$searchId]['filters']['transliteration']) && $hasTransliteration) {
                    $this->resultsStored[$searchId]['filters']['transliteration'] = array_merge($this->resultsStored[$searchId]['filters']['transliteration'], ['transliteration' => 0]);
                    $includedTransliterations = 1;
                }
                if (!$includedTranslations && !array_key_exists('translation', $this->resultsStored[$searchId]['filters']['translation']) && $hasTranslation) {
                    $this->resultsStored[$searchId]['filters']['translation'] = array_merge($this->resultsStored[$searchId]['filters']['translation'], ['translation' => 0]);
                    $includedTranslations = 1;
                }
            }

            if (!empty($imagesFilters)) {
                $this->resultsStored[$searchId]['filters']['image_type'] = array_merge($this->resultsStored[$searchId]['filters']['image_type'], $imagesFilters);
            }

            $commodityVizIds = array();
            foreach ($this->resultsStored[$searchId]['result'] as $artifactID => $values) {
                if (!empty($values['languages'])) {
                    if ($values['languages']['languages'] == 'Sumerian') {
                        array_push($commodityVizIds, $artifactID);
                    }
                }
            }

            if ($this->_isApiRequest()) {
                $this->_setApiVariables($searchId, $result, $this->settings);
                return;
            }

            $imagesLinks = [];
            foreach ($result as $artifactId => $row) {
                $pub_id = TableRegistry::get('EntitiesPublications')
                        ->find()
                        ->where([
                            'EntitiesPublications.entity_id' => $artifactId,
                            'EntitiesPublications.table_name' => "artifacts"
                        ])->first()->publication_id;
                $result[$artifactId]['artifact'] = array_merge($result[$artifactId]['artifact'], ['Pub_id' => $pub_id]);
                $primary_pub = TableRegistry::get('EntitiesPublications')
                    ->find()
                    ->contain([
                        'Publications' => [
                            'fields' => [
                                'Publications.year',
                                'Publications.designation',
                                'Publications.id',
                            ],
                            'Authors' => [
                                'fields' => [
                                    'Authors.author'
                                ]
                            ]
                        ]
                    ])
                   ->where([
                       'EntitiesPublications.entity_id' => $artifactId,
                       'EntitiesPublications.table_name' => "artifacts",
                       'EntitiesPublications.publication_type' => "primary"
                    ])->first();
                $result[$artifactId]['artifact'] = array_merge($result[$artifactId]['artifact'], ['pub' => $primary_pub]);

                $artifact = $this->Artifacts->get($artifactId);
                $this->GranularAccess->amendArtifactAssets($artifact);
                $imagesLinks[$artifactId] = $artifact->artifact_assets;
            }
            $parentChildMaterial = $this->ElasticSearch->parentChild('material');
            $parentChildGenre = $this->ElasticSearch->parentChild('genre');
            $parentChildLanguage = $this->ElasticSearch->parentChild('Language');
            $parentChildAtype = $this->ElasticSearch->parentChild('artifact_type');
            $parentChildArray = $parentChildLanguage + $parentChildGenre + $parentChildMaterial + $parentChildAtype;
            $filterData = $this->getFilterDataFromSession($searchId);
            $bucketsresult = ($this->isAdvancedSearch === 1) ? $this->ElasticSearch->searchAdvancedArtifacts($queryData, $this->settings, $filterData) : $this->ElasticSearch->simpleSearch($queryData, $this->settings, $filterData);
            $this->set([
                'searchId' => $searchId,
                'showedit' => $showedit,
                'result' => $result,
                'LayoutType' => $this->settings['LayoutType'],
                'PageSize' => $this->settings['PageSize'],
                'Page' => $this->settings['Page'],
                'lastPage' => $this->settings['lastPage'],
                'searchSettings' => $this->searchSettings,
                'filters' => $this->resultsStored[$searchId]['filters'],
                'searchableFields' => $this->searchableFields,
                'commodityVizIds' => $commodityVizIds,
                'isAdvancedSearch' => $this->isAdvancedSearch,
                'query' => $queryString,
                'inscriptionsQuery' => $inscriptionsQuery,
                'totalHits' => $this->resultsStored[$searchId]['totalHits'],
                'imagesLinks' => $imagesLinks,
                'bucketsresult' => $bucketsresult,
                'parentChildArray' => $parentChildArray
            ]);
        }
    }

    public function tokenList($id, $kind)
    {
        if (empty($this->resultsStored) || !array_key_exists($id, $this->resultsStored)) {
            throw new \Cake\Http\Exception\GoneException();
        }
        if ($kind !== 'signs' && $kind !== 'words') {
            throw new \Cake\Http\Exception\NotFoundException();
        }

        $type = $this->Api->prefers() === 'json' ? 'json' : 'txt';
        $artifactIds = array_keys($this->resultsStored[$id]['result']);

        $inscription_count = 0;
        $token_count = 0;
        $token_instance_count = 0;

        $list = [];
        foreach ($artifactIds as $artifactId) {
            $inscription_count += 1;

            $inscription = $this->Artifacts->Inscriptions->find()
                ->where(['Inscriptions.artifact_id' => $artifactId, 'Inscriptions.is_latest' => true])
                ->select(['atf']);
            if (!$this->settings['canViewPrivateInscriptions']) {
                $inscription = $inscription->matching('Artifacts', function ($query) {
                    return $query->where(['Artifacts.is_atf_public' => true]);
                });
            }
            $inscription = $inscription->first();

            if (empty($inscription->atf)) {
                continue;
            }

            $tokens = TokenList::processText($inscription->atf, $kind);

            foreach ($tokens as $token) {
                if (array_key_exists($token, $list)) {
                    $list[$token] += 1;
                } else {
                    $token_count += 1;
                    $list[$token] = 1;
                }
                $token_instance_count += 1;
            }
        }
        ksort($list);

        $data;
        if ($type === 'json') {
            $data = json_encode([
                'metadata' => [
                    'token_kind' => $kind,
                    'inscription_count' => $inscription_count,
                    'token_count' => $token_count,
                    'token_instance_count' => $token_instance_count
                ],
                'list' => $list
            ]);
        } else {
            $data = implode("\n", array_keys($list));
        }

        $this->Api->respondAs($type);
        return $this->getResponse()->withStringBody($data);
    }

    /**
     * utf2atf method
     *
     * converts utf8 version of transliteration to atf
    */
    public function utf2atf($value)
    {
        $value = preg_replace('/š|Š/', 'sz', $value);
        $value = preg_replace('/ś|Ś/', 's\'', $value);
        $value = preg_replace('/ṣ|Ṣ/', 's,', $value);
        $value = preg_replace('/ṭ|Ṭ/', 't,', $value);
        $value = preg_replace('/ḫ|Ḫ/', 'h', $value);
        $value = preg_replace('/ŋ|Ŋ/', 'g', $value);

        return strtolower($value);
    }

    /**
     * getDataFromRequest method
     *
     *  It returns array of required Key=>Value Pair for advanced search.
     *
     * @param
     * param : Type of Request i.e. data(URL params) or query(form POST) parameters.
     *
     * @return Array of Key=>Value pair.
     */
    public function getDataFromRequest($requestType)
    {
        $queryData = [];

        foreach ($requestType as $key => $value) {
            if ($value !== '') {
                if (array_key_exists($key, $this->settings)) {
                    $this->settings[$key] = $value;
                } elseif (in_array($key, $this->searchableFields)) {
                    if ($key === 'transliteration' || $key === 'transliteration_permutation') {
                        $value = $this->utf2atf($value);
                    }
                    $queryData[$key] = trim($value, '" || \'');
                }
            }
        }

        return $queryData;
    }

    /**
     * optimizedQuery method
     *
     * Handles AND/OR condition for same search Category.
     *
     * @param
     * param : URL Parameters.
     *
     * @return \Cake\Http\Response|void
     */
    public function optimizedQuery($param)
    {
        $paramModifiedArray = [];

        foreach ($param as $key => $value) {
            $checkType = substr($key, 0, -1);

            if (in_array($key, $this->searchCategory)) {
                if ($key === 'inscription') {
                    $value = $this->utf2atf($value);
                }
                array_push($paramModifiedArray, [$key => trim($value, '" || \'')]);
            } elseif (in_array($checkType, $this->searchCategory) || $checkType === 'operator') {
                array_push($paramModifiedArray, [$checkType => trim($value, '" || \'')]);
            } elseif (array_key_exists($key, $this->settings)) {
                $this->settings[$key] = $value;
            }
        }

        $sizeOfParamModifiedArray = sizeof($paramModifiedArray);

        $finalQueryArray = [];
        $trackingField = [];

        for ($loop = 0; $loop < $sizeOfParamModifiedArray; $loop++) {
            $key = array_keys($paramModifiedArray[$loop])[0];
            $value = array_values($paramModifiedArray[$loop])[0];

            if ($key === 'operator' && ($loop + 1) < $sizeOfParamModifiedArray) {
                $nextKey = array_keys($paramModifiedArray[$loop + 1])[0];
                $nextValue = array_values($paramModifiedArray[$loop+1])[0];

                if (key_exists($nextKey, $trackingField)) {
                    $previousPosition = $trackingField[$nextKey];
                    $getPrevValueFromFinalQueryArray = array_values($finalQueryArray[$previousPosition])[0];

                    $finalQueryArray[$previousPosition][$nextKey] =  $getPrevValueFromFinalQueryArray.' '.$value.' '.$nextValue;
                    $loop += 1;
                } else {
                    array_push($finalQueryArray, $paramModifiedArray[$loop]);
                }
            } else {
                array_push($finalQueryArray, $paramModifiedArray[$loop]);
                $trackingField[$key] =  sizeof($finalQueryArray) - 1;
            }
        }

        return $finalQueryArray;
    }

    /**
     * getFilterDataFromSession method
     *
     * Get Filters stored in Session for specific searchId.
     *
     * @param
     * searchId : Unique Search ID.
     *
     * @return Array of Filters
     */
    public function getFilterDataFromSession($searchId)
    {
        $currentFilters = $this->getRequest()->getSession()->read('resultsStored.'.$searchId.'.filters');

        $currentFilters = is_null($currentFilters) ? [] : $currentFilters;

        $setFilters = [];

        foreach ($currentFilters as $filter => $values) {
            if (!empty($values)) {
                $selectedValues = array_keys(array_filter(
                    $values,
                    function ($value) {
                        return $value;
                    }
                ));

                if (!empty($selectedValues)) {
                    $setFilters[$filter] = $selectedValues;
                }
            }
        }

        return $setFilters;
    }

    /**
     * getSearchResults method
     *
     * @param
     * queryData : Array of [Key => Value] where Key is searchCategory and value is value of searchCategory.
     * searchId : Unique Search ID.
     *
     * @return \Cake\Http\Response|void
     */
    public function getSearchResults($queryData, $searchId)
    {
        // If there are 4 search results already stored in session.
        if (sizeof($this->resultsStored) == 4) {
            $firstInsertedKey = min(array_keys($this->resultsStored));
            if ($searchId != $firstInsertedKey) {
                unset($this->resultsStored[$firstInsertedKey]);
            }
        }

        $filterData = $this->getFilterDataFromSession($searchId);

        // Retrieve results and updated settings from ElasticSearch Components
        $resultFromESComponent = ($this->isAdvancedSearch === 1) ? $this->ElasticSearch->searchAdvancedArtifacts($queryData, $this->settings, $filterData) : $this->ElasticSearch->simpleSearch($queryData, $this->settings, $filterData);

        if ($this->session->read('resultsStored.'.$searchId.'.settings.filter_dirty') || !empty($filterData)) {
            $resultFromESComponent['filters'] = $this->session->read('resultsStored.'.$searchId.'.filters');
        }

        $resultFromESComponent['settings'] = $this->settings;

        $this->resultsStored[$searchId] = $resultFromESComponent;

        // Store in Session variables
        $this->session->write('resultsStored', $this->resultsStored);
    }

    /**
     * @return bool
     */
    private function _isApiRequest()
    {
        return $this->Api->requestsApi();
    }

    /**
     * @param $searchId
     * @param array $results
     * @param array $settings
     */
    private function _setApiVariables($searchId, $results, $params)
    {
        $aspect = $this->getRequest()->getParam('aspect');
        $entities;

        if ($aspect == 'inscriptions') {
            $entities = [];
            $contain = ['Artifacts'];

            if ($this->Api->requestsFormat(['LinkedData'])) {
                array_push($contain, 'Artifacts.Dates', 'Artifacts.Languages', 'Artifacts.Composites');
            }

            foreach (array_keys($results) as $id) {
                $inscription = $this->Artifacts->Inscriptions
                    ->findByArtifactId($id)
                    ->contain($contain)
                    ->where(['is_latest' => 1])
                    ->first();
                if ($inscription) {
                    $entities[] = $inscription;
                }
            }
        } elseif ($aspect == 'publications') {
            $ids = array_unique(array_keys($results));
            $entities = $this->Artifacts->Publications
                ->find()
                ->contain(['EntryTypes', 'Authors', 'Editors', 'Journals'])
                ->matching('Artifacts', function ($q) use ($ids) {
                    return $q->whereInList('Artifacts.id', $ids);
                })
                ->all();
        } else {
            $entities = array_map(function ($id) {
                $artifact = $this->Artifacts->get($id, [
                    'contain' => [
                        'Proveniences',
                        'Origins',
                        'Periods',
                        'ArtifactTypes',
                        'Archives',
                        'Collections',
                        'Dates',
                        'ExternalResources',
                        'Genres',
                        'Languages',
                        'Materials',
                        'MaterialAspects',
                        'MaterialColors',
                        'Publications',
                        // TODO RetiredArtifacts
                    ]
                ]);

                $this->GranularAccess->amendArtifact($artifact);
                $artifact->artifacts_updates = $this->Artifacts->ArtifactsUpdates->find()
                    ->contain(['UpdateEvents' => ['Creators', 'Authors']])
                    ->where(['UpdateEvents.status' => 'approved', 'artifact_id' => $artifact->id]);

                return $artifact;
            }, array_keys($results));
        }

        $this->set([
            'entities' => $entities,
            '_admin' => $this->GranularAccess->isAdmin(),
            '_serialize' => 'entities'
        ]);

        $links = [
            $this->_buildUrl($searchId, '1', $params['PageSize'], 'first'),
            $this->_buildUrl($searchId, $params['Page'], $params['PageSize'], 'current'),
            $this->_buildUrl($searchId, $params['lastPage'], $params['PageSize'], 'last')
        ];

        if ($params['Page'] !== 1) {
            $links[] = $this->_buildUrl($searchId, $params['Page'] - 1, $params['PageSize'], 'prev');
        }

        if ($params['Page'] !== $params['lastPage']) {
            $links[] = $this->_buildUrl($searchId, $params['Page'] + 1, $params['PageSize'], 'next');
        }

        $this->response = $this->response->withHeader('Link', $links);
    }

    private function _buildUrl($searchId, $page, $perPage, $rel)
    {
        $url = Router::url([$searchId, '?' => array_merge(
            $this->request->getQueryParams(),
            [
                'Page' => $page,
                'PageSize' => $perPage
            ]
        )], true);

        return '<' . $url . '>; rel="' . $rel . '"';
    }
}
