<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Periods Controller
 *
 * @property \App\Model\Table\PeriodsTable $Periods
 *
 * @method \App\Model\Entity\Period[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PeriodsController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('Api', ['features' => ['LinkedData', 'TableExport']]);
        $this->loadComponent('GeneralFunctions');

        // Set access for public.
        $this->Auth->allow(['index', 'view']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $periods = $this->Periods->find('all')->order(['sequence' => 'ASC'])->all();
        $access_granted = $this->GeneralFunctions->checkIfRolesExists([1]);

        $this->set(compact('periods', 'access_granted'));
        $this->set('_serialize', 'periods');
    }

    /**
     * View method
     *
     * @param string|null $id Period id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $period = $this->Periods->get($id, [
            'contain' => ['Rulers', 'Years']
        ]);
        $access_granted = $this->GeneralFunctions->checkIfRolesExists([1]);

        $this->set(compact('access_granted'));
        $this->set('period', $period);
        $this->set('_serialize', 'period');
    }
}
