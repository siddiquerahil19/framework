<?php

declare(strict_types=1);

namespace App\Controller;

use Cake\Http\Exception\UnauthorizedException;

/**
 * ArtifactAssets Controller
 *
 * @property \App\Model\Table\ArtifactAssetsTable $ArtifactAssets
 * @method \App\Model\Entity\ArtifactAsset[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ArtifactAssetsController extends AppController
{
    /**
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('Api', ['features' => ['TableExport']]);
        $this->loadComponent('GranularAccess');

        $this->Auth->allow(['index', 'view', 'authorize']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $conditions = $this->GranularAccess->canViewPrivateImage() ? [] : [
            'ArtifactAssets.is_public' => true
        ];

        $this->paginate = [
            'contain' => ['Artifacts'],
            'order' => [
                'ArtifactAssets.artifact_id' => 'ASC',
                'ArtifactAssets.asset_type' => 'ASC'
            ],
            'conditions' => $conditions
        ];
        $assets = $this->paginate($this->ArtifactAssets);

        $this->set('artifactAssets', $assets);
        $this->set('_serialize', 'artifactAssets');
        $this->set('_displayField', 'path');
        $this->set('isAdmin', $this->GranularAccess->isAdmin());
    }

    /**
     * View method
     *
     * @param string|null $id Artifact Asset id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @throws \Cake\Http\Exception\UnauthorizedException When record not available to user.
     */
    public function view($id)
    {
        $asset = $this->ArtifactAssets->get($id, ['contain' => ['Artifacts']]);
        if (!$this->GranularAccess->canViewAsset($asset)) {
            throw new UnauthorizedException();
        }
        $this->ArtifactAssets->Artifacts->loadInto($asset->artifact, ['ArtifactAssets']);
        $this->set('asset', $asset);
        $this->set('isAdmin', $this->GranularAccess->isAdmin());
    }

    /**
     * Authorize image access
     *
     * @param string|null $id Artifact Asset id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function authorize()
    {
        $path = $this->getRequest()->getQuery('path');
        $asset = $this->ArtifactAssets->find('path', [$path])->contain(['Artifacts'])->first();

        if ($this->GranularAccess->canViewAsset($asset)) {
            return $this->getResponse()->withStatus(200);
        }

        if ($this->Auth->user()) {
            return $this->getResponse()->withStatus(403);
        } else {
            return $this->getResponse()->withStatus(401);
        }
    }
}
