<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * MaterialColors Controller
 *
 * @property \App\Model\Table\MaterialColorsTable $MaterialColors
 *
 * @method \App\Model\Entity\MaterialColor[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MaterialColorsController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('Api', ['features' => ['LinkedData', 'TableExport']]);
        $this->loadComponent('GeneralFunctions');

        // Set access for public.
        $this->Auth->allow(['index', 'view']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'maxLimit' => $this->MaterialColors->find()->count()
        ];
        $materialColors = $this->paginate($this->MaterialColors);
        $access_granted = $this->GeneralFunctions->checkIfRolesExists([1]);

        $this->set(compact('materialColors', 'access_granted'));
        $this->set('_serialize', 'materialColors');
    }

    /**
     * View method
     *
     * @param string|null $id Material Color id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $materialColor = $this->MaterialColors->get($id);
        $artifacts = $this->loadModel('ArtifactsMaterials');
        $access_granted = $this->GeneralFunctions->checkIfRolesExists([1]);
        $count = $artifacts->find('list', ['conditions' => ['material_color_id' => $id]])->count();

        $this->set(compact('access_granted', 'count'));
        $this->set('materialColor', $materialColor);
        $this->set('_serialize', 'materialColor');
    }
}
