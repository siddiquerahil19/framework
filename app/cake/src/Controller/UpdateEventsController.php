<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Datasource\ConnectionManager;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Exception\ForbiddenException;
use Cake\I18n\Time;

/**
 * UpdateEvents Controller
 *
 * @property \App\Model\Table\UpdateEventsTable $UpdateEvents
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\UpdateEvent[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UpdateEventsController extends AppController
{
    public function initialize(): void
    {
        parent::initialize();
        $this->loadModel('Artifacts');
        $this->loadModel('UpdateEvents');
        $this->loadModel('Users');
        $this->loadComponent('GranularAccess');

        $this->Auth->allow(['index', 'view', 'add', 'cancel', 'edit', 'delete', 'submit', 'decline']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => [
                'ExternalResources',
                'Reviewers',
                'Creators'
            ],
            'order' => [
                'UpdateEvents.created' => 'DESC'
            ]
        ];

        if (!$this->GranularAccess->canReviewEdits()) {
            $this->paginate['conditions'] = [
                'UpdateEvents.status' => 'approved'
            ];
        }

        $updateEvents = $this->paginate($this->UpdateEvents);

        $this->set(compact('updateEvents'));
    }

    /**
     * View method
     *
     * @param string|null $id
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $event = $this->UpdateEvents->get($id, [
            'contain' => [
                'Reviewers.Users',
                'Creators.Users',
                'Authors',
                'ExternalResources'
            ]
        ]);

        $isAdmin = $this->GranularAccess->isAdmin();
        $isReviewer = $this->GranularAccess->canReviewEdits();
        $isAuthor = !empty($event->creator->user) && $event->creator->user->id == $this->Auth->user('id');

        if (!$this->GranularAccess->canAccessEdits($event)) {
            $this->Flash->error($this->Auth->getConfig('authError'));
            return $this->redirect(['action' => 'index']);
        }

        // Add artifactsUpdates and inscriptions depending on access
        $this->GranularAccess->amendUpdateEvent($event);

        $this->set('updateEvent', $event);
        $this->set('isAdmin', $isAdmin);
        $this->set('isReviewer', $isReviewer);
        $this->set('isAuthor', $isAuthor);
        $this->set('isApprovable', $event->isApprovableByEditor() || $isAdmin);
    }

    /**
     * @param \App\Model\Entity\UpdateEvent[] $updates
     * @param string[] $errors
     * @return \Cake\Http\Response|null
     */
    public function add()
    {
        if (!$this->GranularAccess->canSubmitEdits()) {
            $this->Flash->error($this->Auth->getConfig('authError'));
            return $this->redirect(['action' => 'index']);
        }

        $event = $this->UpdateEvents->newEmptyEntity();
        $creator = $this->Users->get($this->Auth->user('id'), ['contain' => ['Authors']]);
        $event->creator = $creator->author;

        // Get updates from sessions
        $session = $this->getRequest()->getSession();
        $event->update_type = $this->request->getData('update_type') ?? $this->request->getQuery('update_type');
        if ($event->update_type == 'artifact') {
            $event->artifacts_updates = $session->read('ArtifactsUpdates.uploads');
        } elseif ($event->update_type == 'visual_annotation') {
            $event->artifact_asset_annotations = $session->read('ArtifactAssetAnnotations.uploads');
        } elseif (!empty($event->update_type)) {
            $event->inscriptions = $session->read('Inscriptions.uploads');
            foreach ($event->inscriptions as $inscription) {
                if ($inscription->has('artifact_id')) {
                    $inscription->artifact = $this->Artifacts->get($inscription->artifact_id, ['contain' => ['Inscriptions']]);
                }
            }
        }

        $isApprovable = $event->isApprovableByEditor() || $this->GranularAccess->isAdmin();

        if ($this->request->is('post')) {
            $action = $this->request->getData('action');

            if ($action != 'created' && $action != 'submitted') {
                throw new BadRequestException();
            }

            // Add metadata to event
            $event = $this->UpdateEvents->patchEntity($event, $this->request->getData());
            $event->created = Time::now();
            $event->status = $action;

            // Check for empty update
            if (empty($event->update_type)) {
                $this->Flash->error(__('No type of changes specified'));
            } elseif ($event->update_type === 'artifact' && !$event->has('artifacts_updates')) {
                $this->Flash->error(__('No changes of type {0} to upload', $event->update_type));
            } elseif ($event->update_type === 'visual_annotation' && !$event->has('artifact_asset_annotations')) {
                $this->Flash->error(__('No changes of type {0} to upload', $event->update_type));
            } elseif ($event->update_type === 'atf' && !$event->has('inscriptions')) {
                $this->Flash->error(__('No changes of type {0} to upload', $event->update_type));
            } elseif ($event->update_type === 'annotations' && !$event->has('inscriptions')) {
                $this->Flash->error(__('No changes of type {0} to upload', $event->update_type));
            } else {
                // Remove placeholder artifacts
                if (is_array($event->artifacts_updates)) {
                    foreach ($event->artifacts_updates as $update) {
                        if (!empty($update->artifact) && $update->artifact->isNew()) {
                            unset($update->artifact);
                        }
                    }
                }

                if ($this->UpdateEvents->save($event)) {
                    if ($event->update_type == 'artifact') {
                        $session->delete('ArtifactsUpdates.uploads');
                    } elseif ($event->update_type == 'visual_annotation') {
                        $session->delete('ArtifactAssetAnnotations.uploads');
                    } else {
                        $session->delete('Inscriptions.uploads');
                    }

                    $this->Flash->success(__('The update has been {0}.', $event->status));
                    return $this->redirect(['action' => 'view', $event->id]);
                }

                $this->Flash->error(__('The update could not be {0}. Please, try again.', $event->status));
            }
        }

        $external_resources = $this->UpdateEvents->ExternalResources->find('list', [
            'valueField' => ['abbrev'],
            'order' => ['abbrev' => 'ASC'],
            'limit' => 200
        ]);
        $this->set('event', $event);
        $this->set('external_resources', $external_resources);
        $this->set('canReviewEdits', $this->GranularAccess->canReviewEdits());
        $this->set('isApprovable', $isApprovable);
    }

    public function cancel()
    {
        $session = $this->getRequest()->getSession();
        $session->delete('ArtifactsUpdates.uploads');
        $session->delete('Inscriptions.uploads');
        $this->redirect(['action' => 'index']);
    }

    /**
     * @param string|null $id
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id)
    {
        if (!$this->GranularAccess->canSubmitEdits()) {
            $this->Flash->error($this->Auth->getConfig('authError'));
            return $this->redirect(['action' => 'view', $event->id]);
        }

        $event = $this->UpdateEvents->get($id, [
            'contain' => [
                'Reviewers',
                'Creators',
                'Authors',
                'ExternalResources'
            ]
        ]);
        $user = $this->Users->get($this->Auth->user('id'));

        if (!$event->isCreatedBy($user) && !$this->GranularAccess->isAdmin()) {
            $this->Flash->error($this->Auth->getConfig('authError'));
            return $this->redirect(['action' => 'view', $event->id]);
        }

        if ($this->request->is('post')) {
            $event = $this->UpdateEvents->patchEntity($event, $this->request->getData());

            if ($this->UpdateEvents->save($event)) {
                $this->Flash->success(__('The update has been updated.'));
                return $this->redirect(['action' => 'view', $event->id]);
            }

            $this->Flash->error(__('The update could not be updated.'));
        }

        $external_resources = $this->UpdateEvents->ExternalResources->find('list', [
            'valueField' => ['abbrev'],
            'order' => ['abbrev' => 'ASC'],
            'limit' => 200
        ]);
        $this->set('event', $event);
        $this->set('external_resources', $external_resources);
    }

    /**
     * @param string|null $id
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function submit($id)
    {
        if (!$this->GranularAccess->canSubmitEdits()) {
            $this->Flash->error($this->Auth->getConfig('authError'));
            return $this->redirect(['action' => 'view', $event->id]);
        }

        if ($this->request->is('post')) {
            $event = $this->UpdateEvents->get($id, [
                'contain' => [
                    'Reviewers',
                    'Creators',
                    'ExternalResources'
                ]
            ]);

            if ($event->status != 'created') {
                $this->Flash->error('Cannot submit already submitted updates.');
                return $this->redirect(['action' => 'view', $event->id]);
            }

            $user = $this->Users->get($this->Auth->user('id'));

            if ($event->isCreatedBy($user) || $this->GranularAccess->isAdmin()) {
                $event->status = 'submitted';

                if ($this->UpdateEvents->save($event)) {
                    $this->Flash->success(__('The update has been submitted.'));
                    return $this->redirect(['action' => 'view', $event->id]);
                }
            }

            $this->Flash->error(__('The update could not be submitted.'));
        }

        return $this->redirect(['action' => 'view', $id]);
    }

    /**
     * @param string|null $id
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function approve($id)
    {
        if (!$this->GranularAccess->canReviewEdits()) {
            $this->Flash->error($this->Auth->getConfig('authError'));
            return $this->redirect(['action' => 'view', $event->id]);
        }

        if ($this->request->is('post')) {
            $event = $this->UpdateEvents->get($id, [
                'contain' => [
                    'Reviewers',
                    'Creators',
                    'ExternalResources'
                ]
            ]);

            if ($event->status != 'submitted') {
                $this->Flash->error('Can only approve submitted updates.');
                return $this->redirect(['action' => 'view', $event->id]);
            }

            $this->GranularAccess->amendUpdateEvent($event);
            if (!$event->isApprovableByEditor() && !$this->GranularAccess->isAdmin()) {
                $this->Flash->error('Cannot approve update.');
                return $this->redirect(['action' => 'view', $event->id]);
            }

            $user = $this->Users->get($this->Auth->user('id'), ['contain' => ['Authors']]);
            $event->status = 'approved';
            $event->approved = Time::now();
            $event->approved_by = $user->author->id;

            $connection = ConnectionManager::get('default');
            $connection->begin();

            try {
                $success = $event->apply() && $this->UpdateEvents->save($event, ['associated' => false]);
            } catch (\Throwable $e) {
                $this->Flash->error(__('Technical info: {0}', $e->getMessage()));
                $success = false;
            }

            if ($success) {
                $connection->commit();
                $this->Flash->success(__('The update has been approved.'));
            } else {
                $connection->rollback();
                $this->Flash->error(__('The update could not be approved.'));
                $errors = $event->getError($event->update_type == 'artifact' ? 'artifacts_updates' : 'inscriptions');
                foreach ($errors as $error) {
                    if (!is_array($error)) {
                        $this->Flash->error($error);
                    }
                }
            }
        }

        return $this->redirect(['action' => 'view', $id]);
    }

    /**
     * @param string|null $id
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function decline($id)
    {
        if (!$this->GranularAccess->canReviewEdits()) {
            $this->Flash->error($this->Auth->getConfig('authError'));
            return $this->redirect(['action' => 'view', $event->id]);
        }

        if ($this->request->is('post')) {
            $event = $this->UpdateEvents->get($id, [
                'contain' => [
                    'Reviewers',
                    'Creators',
                    'ExternalResources'
                ]
            ]);

            if ($event->status != 'submitted') {
                $this->Flash->error('Can only decline submitted updates.');
                return $this->redirect(['action' => 'view', $event->id]);
            }

            $user = $this->Users->get($this->Auth->user('id'), ['contain' => ['Authors']]);
            $event->status = 'declined';
            $event->approved = Time::now();
            $event->approved_by = $user->author->id;

            if ($this->UpdateEvents->save($event, ['associated' => false])) {
                $this->Flash->success(__('The update has been declined.'));
            } else {
                $this->Flash->error(__('The update could not be declined.'));
            }
        }

        return $this->redirect(['action' => 'view', $id]);
    }

    /**
     * @param string|null $id
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException
     */
    public function delete($id = null)
    {
        if (!$this->GranularAccess->canSubmitEdits()) {
            $this->Flash->error($this->Auth->getConfig('authError'));
            return $this->redirect(['action' => 'view', $event->id]);
        }

        $this->getRequest()->allowMethod(['post', 'delete']);

        $event = $this->UpdateEvents->get($id, ['contain' => ['Creators']]);
        $user = $this->Users->get($this->Auth->user('id'));

        if ($event->status == 'approved') {
            $this->Flash->error('Cannot delete approved update.');
            return $this->redirect(['action' => 'view', $event->id]);
        }

        if ($event->isCreatedBy($user) || $this->GranularAccess->isAdmin()) {
            if ($this->UpdateEvents->delete($event)) {
                $this->Flash->success(__('The update has been deleted.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The update could not be deleted. Please, try again.'));
                return $this->redirect(['action' => 'view', $event->id]);
            }
        }

        $this->Flash->error($this->Auth->getConfig('authError'));
        return $this->redirect(['action' => 'view', $event->id]);
    }
}
