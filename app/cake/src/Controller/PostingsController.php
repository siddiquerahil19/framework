<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Postings Controller
 *
 * @property \App\Model\Table\PostingsTable $Postings
 *
 * @method \App\Model\Entity\Posting[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PostingsController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('GranularAccess');
        $this->Auth->allow(['news', 'highlights', 'view', 'index']);
    }

    /**
     * News method
     *
     * @return \Cake\Http\Response|void
     */
    public function news()
    {
        $newsarticles = $this->Postings->find('all', [
            'contain' => ['Artifacts'],
            'order' => ['publish_start' => 'DESC'],
            'conditions' => ['Postings.published' => 1, 'posting_type_id' => 1]
        ]);

        foreach ($newsarticles as $posting) {
            if (!is_null($posting->artifact)) {
                $this->GranularAccess->amendArtifactAssets($posting->artifact);
            }
        }

        $this->set('newsarticles', $newsarticles);
    }

    /**
     * Highlights method
     *
     * @return \Cake\Http\Response|void
     */
    public function highlights()
    {
        $highlights = $this->Postings->find('all', [
            'contain' => ['Artifacts'],
            'order' => ['publish_start' => 'DESC'],
            'conditions' => ['Postings.published' => 1, 'posting_type_id' => 3]
        ]);

        foreach ($highlights as $posting) {
            if (!is_null($posting->artifact)) {
                $this->GranularAccess->amendArtifactAssets($posting->artifact);
            }
        }

        $this->set('highlights', $highlights);
    }

    /**
     * View method
     *
     * @param string|null $id Posting id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $posting = $this->Postings->get($id, [
            'contain' => ['PostingTypes', 'Artifacts', 'Creators', 'Modifiers']
        ]);

        if (!is_null($posting->artifact)) {
            $this->GranularAccess->amendArtifactAssets($posting->artifact);
        }

        $this->set('canEdit', $this->GranularAccess->isAdmin());
        $this->set('posting', $posting);
    }
}
