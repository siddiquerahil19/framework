<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Home Controller
 *
 * @method \App\Model\Entity\Artifact[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class HomeController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('GranularAccess');
        $this->Auth->allow(['index', 'browse']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->loadModel('Postings');

        $newsarticles = $this->Postings->find('all', [
            'contain' => ['Artifacts'],
            'order' => ['publish_start' => 'DESC'],
            'conditions' => ['Postings.published' => 1, 'posting_type_id' => 1],
            'limit' => 6
        ]);
        $highlights = $this->Postings->find('all', [
            'contain' => ['Artifacts'],
            'order' => ['publish_start' => 'DESC'],
            'conditions' => ['Postings.published' => 1, 'posting_type_id' => 3],
            'limit' => 6
        ]);

        foreach ($newsarticles as $posting) {
            if (!is_null($posting->artifact)) {
                $this->GranularAccess->amendArtifactAssets($posting->artifact);
            }
        }
        foreach ($highlights as $posting) {
            if (!is_null($posting->artifact)) {
                $this->GranularAccess->amendArtifactAssets($posting->artifact);
            }
        }

        $this->set('newsarticles', $newsarticles);
        $this->set('highlights', $highlights);
    }

    /**
     * Browse method
     *
     * @return \Cake\Http\Response|void
     */
    public function browse()
    {
        $this->loadModel('Artifacts');
        $this->paginate = [
            'contain' => ['Proveniences', 'Periods', 'ArtifactTypes', 'Archives']
        ];
        $artifacts = $this->paginate($this->Artifacts);

        $this->set(compact('artifacts'));
        $this->set('_serialize', ['artifacts']);
    }
}
