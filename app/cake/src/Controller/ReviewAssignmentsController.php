<?php

declare(strict_types=1);

namespace App\Controller;

/**
 * ReviewAssignments Controller
 *
 * @property \App\Model\Table\OjsReviewAssignmentsTable $OjsReviewAssignments
 * @method \App\Model\Entity\OjsReviewAssignment[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ReviewAssignmentsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['OjsSubmissions', 'OjsReviewRounds'],
        ];
        $reviewAssignments = $this->paginate($this->OjsReviewAssignments);

        $this->set(compact('reviewAssignments'));
    }

    /**
     * View method
     *
     * @param string|null $id Review Assignment id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $reviewAssignment = $this->OjsReviewAssignments->get($id, [
            'contain' => ['OjsSubmissions', 'OjsReviewRounds','OjsUsers','OjsSubmissionComments'],
        ]);

        $this->set(compact('reviewAssignment'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $reviewAssignment = $this->OjsReviewAssignments->newEmptyEntity();
        if ($this->request->is('post')) {
            $reviewAssignment = $this->OjsReviewAssignments->patchEntity($reviewAssignment, $this->request->getData());
            if ($this->OjsReviewAssignments->save($reviewAssignment)) {
                $this->Flash->success(__('The review assignment has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The review assignment could not be saved. Please, try again.'));
        }
        $submissions = $this->OjsReviewAssignments->OjsSubmissions->find('list', ['limit' => 200]);
        $reviewRounds = $this->OjsReviewAssignments->OjsReviewRounds->find('list', ['limit' => 200]);
        $this->set(compact('reviewAssignment', 'submissions', 'reviewRounds'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Review Assignment id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $reviewAssignment = $this->OjsReviewAssignments->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $reviewAssignment = $this->OjsReviewAssignments->patchEntity($reviewAssignment, $this->request->getData());
            if ($this->OjsReviewAssignments->save($reviewAssignment)) {
                $this->Flash->success(__('The review assignment has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The review assignment could not be saved. Please, try again.'));
        }
        $submissions = $this->OjsReviewAssignments->OjsSubmissions->find('list', ['limit' => 200]);
        $reviewRounds = $this->OjsReviewAssignments->OjsReviewRounds->find('list', ['limit' => 200]);
        $this->set(compact('reviewAssignment', 'submissions', 'reviewRounds'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Review Assignment id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $reviewAssignment = $this->OjsReviewAssignments->get($id);
        if ($this->OjsReviewAssignments->delete($reviewAssignment)) {
            $this->Flash->success(__('The review assignment has been deleted.'));
        } else {
            $this->Flash->error(__('The review assignment could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
