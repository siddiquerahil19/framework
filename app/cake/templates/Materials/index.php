<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Material[]|\Cake\Collection\CollectionInterface $materials
 */
?>

<div class="d-flex justify-content-between align-items-end mb-2">
    <h1 class="display-3 header-txt text-left"><?= __('Materials') ?></h1>
    <?= $this->element('addButton'); ?>
</div>
<div class="text-left my-2">
    <?= $this->element('entityExport'); ?>
</div>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead align="left">
        <tr>
            <th scope="col"><?= $this->Paginator->sort('material') ?></th>
            <th scope="col"><?= $this->Paginator->sort('parent_id') ?></th>
            <?php if ($access_granted): ?>
                <th scope="col"><?= __('Action') ?></th>
            <?php endif ?>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($materials as $material): ?>
        <tr align="left">
            <td>
                <?= $this->Html->link(
                    $material->material,
                    ['controller' => 'Materials', 'action' => 'view', $material->id]
                ) ?>
            </td>
            <td>
                <?php if ($material->has('parent_material')): ?>
                    <?= $this->Html->link(
                        $material->parent_material->material,
                        ['controller' => 'Materials', 'action' => 'view', $material->parent_material->id]
                    ) ?>
                <?php endif; ?>
            </td>
            <?php if ($access_granted): ?>
                <td>
                    <?= $this->Html->link(
                        __('Edit'),
                        ['prefix' => 'Admin', 'action' => 'edit', $material->id],
                        ['escape' => false , 'class' => 'btn btn-warning btn-sm', 'title' => 'Edit']
                    ) ?>
                </td>
            <?php endif ?>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>

<?= $this->element('citeButton'); ?>
<?= $this->element('citeBottom'); ?>