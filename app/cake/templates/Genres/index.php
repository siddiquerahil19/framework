<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Genre[]|\Cake\Collection\CollectionInterface $genres
 */
?>
<div class="d-flex justify-content-between align-items-end mb-2">
    <h1 class="display-3 header-txt text-left"><?= __('Genres') ?></h1>
    <?= $this->element('addButton'); ?>
</div>
<div class="text-left my-2">
    <?= $this->element('entityExport'); ?>
</div>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead align="left">
        <tr>
            <th scope="col"><?= $this->Paginator->sort('genre') ?></th>
            <th scope="col"><?= $this->Paginator->sort('parent_id') ?></th>
            <?php if ($access_granted): ?>
                <th scope="col"><?= __('Action') ?></th>
            <?php endif ?>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($genres as $genre): ?>
            <tr align="left">
                <td>
                    <?= $this->Html->link(
                        $genre->genre,
                        ['controller' => 'Genres', 'action' => 'view', $genre->id]
                    ) ?>
                </td>
                <td>
                    <?php if ($genre->has('parent_genre')): ?>
                        <?= $this->Html->link(
                            $genre->parent_genre->genre,
                            ['controller' => 'Genres', 'action' => 'view', $genre->parent_genre->id]
                        ) ?>
                    <?php endif; ?>
                </td>
                <?php if ($access_granted): ?>
                    <td>
                        <?= $this->Html->link(
                            __('Edit'),
                            ['prefix' => 'Admin', 'action' => 'edit', $genre->id],
                            ['escape' => false , 'class' => 'btn btn-warning btn-sm', 'title' => 'Edit']
                        ) ?>
                    </td>
                <?php endif ?>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>

<?= $this->element('citeButton'); ?>
<?= $this->element('citeBottom'); ?>