<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsUpdate $artifactsUpdate
 */

$this->set('title', 'Catalogue revision');
?>
<div class="row justify-content-md-center">
    <div class="col-lg-12 boxed">
        <h2>
            <?php if (is_null($artifactsUpdate->artifact_id)): ?>
                <?= h($artifactsUpdate->getDesignation()) ?>
            <?php else: ?>
                <?= $this->Html->link(
                    h($artifactsUpdate->getDesignation()),
                    ['controller' => 'Artifacts', 'action' => 'view', $artifactsUpdate->artifact_id]
                ) ?>
            <?php endif; ?>
        </h2>

        <?php if (!empty($artifactsUpdate->update_event->event_comments)): ?>
            <p><?= h($artifactsUpdate->update_event->event_comments) ?></p>
        <?php endif; ?>
        <p><?= $this->element('updateEventHeader', ['update_event' => $artifactsUpdate->update_event]) ?></p>
        <?= $this->element('artifactUpdateDiff', ['new' => $artifactsUpdate, 'old' => $old]) ?>
    </div>
</div>
