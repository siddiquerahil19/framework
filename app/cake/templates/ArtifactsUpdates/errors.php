<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsUpdate $artifactsUpdate
 */

use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Cake\Utility\Inflector;

$this->set('title', 'Encountered errors in catalogue changes');

$table = TableRegistry::get('Artifacts');
?>
<div class="row justify-content-md-center">
    <div class="col-lg-12 boxed">
        <h2><?= __('Encountered errors') ?></h2>

        <ul>
            <?php foreach ($errors as $error): ?>
                <li><?= h($error) ?></li>
            <?php endforeach; ?>
        </ul>

        <?= $this->Form->postLink(
            __('Cancel'),
            ['controller' => 'ArtifactsUpdates', 'action' => 'add'],
            ['class' => 'btn cdli-btn-light']
        ) ?>
    </div>

    <div class="col-lg-12 boxed">
        <?php foreach ($artifactUpdates as $update): ?>
            <?php if ($update->hasErrors()): ?>
                <section>
                    <h3><?= h($update->getDesignation()) ?></h3>

                    <ul>
                        <?php foreach ($update->getErrors() as $field => $errors): ?>
                            <?php
                                $association = Inflector::pluralize(Inflector::camelize($field));
                                $displayName = Inflector::pluralize(Inflector::humanize($field));
                                $route = null;
                                if ($table->hasAssociation($association)) {
                                    $possibleRoute = [
                                        'controller' => $table->getAssociation($association)->getClassName(),
                                        'action' => 'index'
                                    ];
                                    if (Router::routeExists($possibleRoute)) {
                                        $route = $possibleRoute;
                                    }
                                }
                            ?>
                            <?php foreach ($errors as $error): ?>
                                <li>
                                    <p>
                                        <?= h($field) ?>: <?= h($error) ?>
                                        <?php if (!is_null($route)): ?>
                                            <br>
                                            <?= $this->Html->link(__('Browse {0}', $displayName), $route) ?>
                                        <?php endif; ?>
                                    </p>
                                </li>
                            <?php endforeach; ?>
                        <?php endforeach; ?>
                    </ul>
                </section>
            <?php endif; ?>
        <?php endforeach; ?>
        <?php echo $this->element('Paginator'); ?>
    </div>
</div>
