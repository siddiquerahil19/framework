<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsUpdate $artifactsUpdate
 */

$this->set('title', 'Confirm catalogue changes');

$changedColumns = [];
foreach ($artifactUpdates as $update) {
    foreach ($update->getChanged() as $key) {
        $changedColumns[$key] = true;
    }
}
?>
<div class="row justify-content-md-center">
    <div class="col-lg-12 boxed">
        <h2><?= __('Confirm changes') ?></h2>

        <hr>

        <?php if ($canSubmitEdits && !$noChanges): ?>
            <?= $this->Html->link(
                __('Confirm'),
                ['controller' => 'UpdateEvents', 'action' => 'add', '?' => ['update_type' => 'artifact']],
                ['class' => 'btn cdli-btn-blue']
                ) ?>
        <?php endif; ?>
        <?= $this->Form->postLink(
            __('Discard'),
            ['controller' => 'ArtifactsUpdates', 'action' => 'add'],
            ['class' => 'btn cdli-btn-light', 'confirm' => __('This will discard your changes. Are you sure?')]
        ) ?>
    </div>

    <div class="col-lg-12 boxed">
        <div style="overflow-x: auto;">
            <table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
                <thead>
                    <tr>
                        <th>artifact</th>
                        <?php foreach ($changedColumns as $key => $value): ?>
                            <th><?= h($key) ?></th>
                        <?php endforeach; ?>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($artifactUpdates as $update): ?>
                        <?php $old = is_null($update->artifact) ? [] : $update->artifact->getFlatData(); ?>
                        <tr>
                            <td><?= h($update->getDesignation()) ?></td>
                            <?php foreach ($changedColumns as $field => $value): ?>
                                <?php if ($field != 'artifact'): ?>
                                    <?php if ($update->has($field)): ?>
                                        <?php $value = $update->get($field); ?>
                                        <?php if ($value === '' && array_key_exists($field, $old)): ?>
                                            <td><del><?= h($old[$field]) ?></del></td>
                                        <?php elseif (!array_key_exists($field, $old) || $old[$field] === ''): ?>
                                            <td><ins><?= h($value) ?></ins></td>
                                        <?php elseif ($update->isArrayField($field)): ?>
                                            <td><?= $this->Diff->diff($old[$field], $value, '; ') ?></td>
                                        <?php else: ?>
                                            <td>
                                                <del><?= h($old[$field]) ?></del>
                                                <ins><?= h($value) ?></ins>
                                            </td>
                                        <?php endif; ?>
                                    <?php else: ?>
                                        <td></td>
                                    <?php endif; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </tr>
                    <?php endforeach; ?>
                    <?php if ($noChanges): ?>
                        <tr>
                            <td class="text-center"><?= h('No changes') ?></td>
                        </tr>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>

        <?php echo $this->element('Paginator'); ?>
    </div>
</div>
