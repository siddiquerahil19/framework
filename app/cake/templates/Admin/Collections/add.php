<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Collection $collection
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 text-left form-wrapper">
        <?= $this->Form->create($collection) ?>
            <legend class="form-heading mb-3"><?= __('Add Collection') ?></legend>
            <hr class="form-hr mb-4"/>
            <?php
                echo $this->Form->control('collection', ['class' => 'form-control w-100 mb-3', 'label' => 'Collection', 'type' => 'text', 'maxLength' => 255]);
                echo $this->Form->control('geo_coordinates', ['class' => 'form-control w-100 mb-3', 'label' => 'Geo-Coordiantes', 'type' => 'text', 'maxLength' => 50]);
                echo $this->Form->control('slug', ['class' => 'form-control w-100 mb-3', 'label' => 'Slug', 'type' => 'text', 'maxLength' => 50]);
                echo $this->Form->control('is_private', ['class' => 'mb-3 mr-1', 'label' => 'Is Private', 'type' => 'checkbox']);
                echo $this->Form->control('description', ['class' => 'form-control w-100 mb-3', 'label' => 'Description', 'type' => 'textarea', 'maxLength' => 4000]);
            ?>
                <div class="mb-3"></div>

          <?= $this->Form->button(__('Submit'),['class' => 'btn btn-primary cdli-btn-blue mr-2', 'id' => 'submit']) ?>
        <?= $this->Form->end() ?>

    </div>

</div>

<!-- Add CKEDITOR to Body Field -->
<?= $this->element('ckeditor', ['id' => 'description']) ?>

<!-- Show Alert box on reload -->
<script type="text/javascript">
    var submit = document.getElementById("submit");
    var count = false;
    submit.onclick = () => {
        count = true;
    }

    window.addEventListener('beforeunload', function (e) {
    if(count) {
        return false;
    }
    else {
    e.preventDefault(); 
    e.returnValue = '';
    }
    });
</script>