<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Abbreviation $abbreviation
 */
?>
<div class="row justify-content-md-center">
    <div class="col-lg-7 text-left form-wrapper">
        <?= $this->Form->create($abbreviation) ?>
            <legend class="form-heading mb-3"><?= __('Edit Abbreviation') ?></legend>
            <hr class="form-hr mb-4"/>
                <?php
                echo $this->Form->control('abbreviation', ['class' => 'form-control w-100 mb-3', 'required' => true]);
                echo $this->Form->control('fullform', ['class' => 'form-control w-100 mb-3']);
                echo $this->Form->control('bibtexkey', ['type' => 'text', 'class' => 'form-control w-100', 'error' => false, 'value' => $bibtexkey]); ?>
                <div class="mb-3">
                    <?php
                    if ($this->Form->isFieldError('bibtexkey')) {
                        echo $this->Form->error('bibtexkey', 'This Bibtexkey doesn\'t match with the saved entries');
                    }
                    ?>
                </div>
                <?php
                echo $this->Form->control('type', ['class' => 'form-control w-100 mb-4', 'type' => 'text']);
                ?>
        <div class="pt-2">
        <?= $this->Form->button(__('Save'), ['class' => 'btn cdli-btn-blue mr-2', 'id' => 'submit']) ?>
        <?= $this->Form->end() ?>
        <?= $this->element('deleteModal', [ 'entity_name' => $abbreviation->abbreviation, 'id' => $abbreviation->id]); ?>
        </div>
    </div>
</div>

<!-- Show Alert box on reload -->
<script type="text/javascript">
    var submit = document.getElementById("submit");
    var count = false;
    submit.onclick = () => {
        count = true;
    }

    document.onclick = function(event) {
    if(event.target.id == 'delete'){
        count = true;
    }
    }

    window.addEventListener('beforeunload', function (e) {
    if(count) {
        return false;
    }
    else {
    e.preventDefault(); 
    e.returnValue = '';
    }
    });
</script>
