<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ExternalResource $externalResource
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 text-left form-wrapper">
        <?= $this->Form->create($externalResource) ?>
            <legend class="form-heading mb-3"><?= __('Edit External Resource') ?></legend>
            <hr class="form-hr mb-4"/>
            <?php
                echo $this->Form->control('external_resource', ['class' => 'form-control w-100 mb-3', 'type' => 'text', 'required' => true]);
                echo $this->Form->control('base_url', ['class' => 'form-control w-100 mb-3', 'type' => 'text']);
                echo $this->Form->control('project_url', ['class' => 'form-control w-100 mb-3', 'type' => 'text']);
                echo $this->Form->control('abbrev', ['class' => 'form-control w-100 mb-3', 'type' => 'text']);
                // echo $this->Form->control('artifacts._ids', ['options' => $artifacts]);
            ?>

        <div>
        <?= $this->Form->button(__('Save'), ['class'=>'btn cdli-btn-blue']) ?>
        <?= $this->Form->end() ?>
        <?= $this->element('deleteModal', [ 'entity_name' => $externalResource->external_resource, 'id' => $externalResource->id]); ?>
        </div>

    </div>

</div>
