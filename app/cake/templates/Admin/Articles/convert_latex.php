<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Utility\Latex\Renderer\Html $renderer
 * @var \App\Utility\Latex\Extractor $extractor
 * @var \PhpLatex_Node $document
 */

$this->disableAutoLayout();
?>

<?= $renderer->render($document) ?>

<?php if (!empty($extractor->images)): ?>
    <hr>
    <p class="text-center">
        <b>Figures</b>
    </p>
    <hr>
    <?php foreach ($extractor->images as $image): ?>
        <?= $renderer->renderPart([$image['content']]) ?>
    <?php endforeach; ?>
<?php endif; ?>

<?php if (!empty($extractor->footnotes)): ?>
    <hr>
    <p class="text-center">
        <b>Footnotes</b>
    </p>
    <hr>
    <ul>
        <?php foreach ($extractor->footnotes as $footnote): ?>
            <li id="f<?= $footnote['index'] ?>">
                <sup>
                    <a href="#r<?= $footnote['index'] ?>">[<?= $footnote['index'] ?>]</a>
                </sup>
                <?= $renderer->renderPart($footnote['content']) ?>
            </li>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>

<hr>
<p class="text-center">
    <b>Version:</b> <?= date('Y-m-d') ?>
</p>
<hr>
