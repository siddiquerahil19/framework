<?php
/**
 * @var \App\View\AppView $this
 */
?>

<div class="row justify-content-md-center">
    <div class="col-lg-7 text-left form-wrapper">
        <h4>
            <?= empty($article->id) ? __('Add article') : __('Edit article') ?>
            <?php if (!empty($article->submission_id)): ?>
                <?= $this->Html->link(
                    __('OJS Submission'),
                    '#',
                    ['class' => 'btn cdli-btn-blue float-right']
                ) ?>
            <?php endif; ?>
        </h4>
        <hr class="form-hr mb-4"/>
        <?= $this->Form->create($article, ['type' => 'file']) ?>
            <div class="form-group">
                <?= $this->Form->control('title', ['class' => 'form-control col-12']) ?>
                <small class="form-text text-muted"><?= __('The title of the article.') ?></small>
            </div>
            <div class="form-row">
                <div class="col form-group">
                    <?= $this->Form->control('year', [
                        'class' => 'form-control article_title_input_sm',
                        'type' => 'text',
                        'disabled' => !$article->isNew()
                    ]) ?>
                    <small class="form-text text-muted"><?= __('The year of publication of the article.') ?></small>
                    <?php if ($article->isNew()): ?>
                        <small class="form-text text-danger"><?= __('Note: the year and article number cannot be changed.') ?></small>
                    <?php endif; ?>
                </div>
                <div class="col form-group">
                    <?= $this->Form->control('article_no', [
                        'class' => 'form-control article_title_input_sm',
                        'label' => __('Article Number'),
                        'disabled' => !$article->isNew()
                    ]) ?>
                    <small class="form-text text-muted"><?= __('The number of the article within the year.') ?></small>
                </div>
            </div>
            <div class="form-group">
                <?= $this->element('entityPicker', [
                    'model' => 'Authors',
                    'entities' => $article->authors,
                    'sequence' => true,
                    'joinFields' => ['email', 'institution']
                ]) ?>
            </div>
            <?php if ($articleContent['pdf']): ?>
                <div class="form-group">
                    <label for="pdf"><?= __('Upload PDF') ?></label>
                    <input id="pdf" name="pdf" type="file" accept="application/pdf" class="form-control w-100 mb-3">
                    <?php if (!empty($article->pdf_link)): ?>
                        <small class="form-text text-danger"><?= __('Uploading a new PDF will overwrite the existing one.') ?></small>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
            <?php if ($articleContent['latex']): ?>
                <div class="form-group">
                    <label for="content_html"><?= __('Article Content') ?></label>

                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item" role="presentation">
                            <a id="converter-latex-tab" class="nav-link active" data-toggle="tab" href="#converter-latex"
                               role="tab" aria-controls="converter-latex" aria-selected="false">
                                <i class="fa fa-file-text" aria-hidden="true"></i>
                                <?= __('LaTeX') ?>
                            </a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a id="converter-references-tab" class="nav-link" data-toggle="tab" href="#converter-references"
                               role="tab" aria-controls="converter-references" aria-selected="false">
                               <i class="fa fa-book" aria-hidden="true"></i>
                                <?= __('References') ?>
                            </a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a id="converter-images-tab" class="nav-link" data-toggle="tab" href="#converter-images"
                               role="tab" aria-controls="converter-images" aria-selected="false">
                               <i class="fa fa-picture-o" aria-hidden="true"></i>
                                <?= __('Images') ?>
                            </a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a id="converter-html-tab" class="nav-link" data-toggle="tab" href="#converter-html"
                               role="tab" aria-controls="converter-html" aria-selected="true">
                               <i class="fa fa-file-code-o" aria-hidden="true"></i>
                                <?= __('HTML') ?>
                            </a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a id="converter-logs-tab" class="nav-link" data-toggle="tab" href="#converter-logs"
                               role="tab" aria-controls="converter-logs" aria-selected="false">
                                <i class="fa fa-cogs" aria-hidden="true"></i>
                                <?= __('Logs') ?>
                            </a>
                        </li>
                    </ul>
                    <div id="converter-progress" class="progress bg-white" style="height: 5px;" aria-hidden="true">
                        <div class="progress-bar"></div>
                    </div>

                    <div class="tab-content" style="margin-top: 0;">
                        <div id="converter-latex" class="tab-pane show active" role="tabpanel" aria-labelledby="converter-latex-tab">
                            <?= $this->Form->control('content_latex', [
                                'required' => false,
                                'escape' => false,
                                'label' => false,
                                'autocomplete' => 'off',
                                'autocorrect' => 'off',
                                'autocapitalize' => 'off',
                                'spellcheck' => 'false',
                                'placeholder' => __('Paste LaTeX'),
                                'class' => 'form-control',
                                'rows' => 10,
                                'style' => 'width: 100%;'
                            ]) ?>

                            <div class="form-control">
                                <label for="latex"><?= __('Upload LaTeX') ?></label>
                                <input id="latex" name="latex" type="file" accept="text/x-tex" class="form-control">
                            </div>

                            <br>
                            <button id="converter-action-refresh" type="button" class="btn cdli-btn-blue">
                                <i class="fa fa-refresh" aria-hidden="true"></i>
                                <?= __('Generate HTML') ?>
                            </button>
                        </div>
                        <div id="converter-references" class="tab-pane" role="tabpanel" aria-labelledby="converter-references-tab">
                            <label for="content-references"><?= __('BibTeX') ?></label>
                            <input type="file" id="content-references" class="form-control form-control-file">
                            <small class="form-text text-muted" style="text-align: left; margin-bottom: 5%;">
                                Please upload a <code>.bib</code> file for the identified citations in your LaTeX file.
                                Processing the file may take some time.
                            </small>

                            <div class="table-responsive">
                                <table id="converter-references-table" cellpadding="0" cellspacing="0" class="table">
                                    <thead>
                                        <tr>
                                            <th><?= __('Identified citations') ?></th>
                                            <th><?= __('Formatted references') ?></th>
                                        </tr>
                                        <tr>
                                            <th>
                                                <small class="form-text text-muted"><?= __('This is the list of citations identified in your LaTeX file.') ?></small>
                                            </th>
                                            <th>
                                                <small class="form-text text-muted"><?= __('These are the respective references formatted in HTML.') ?></small>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td rowspan="2"><?= __(
                                                'Press "{0}" to identify references',
                                                [__('Generate HTML')]
                                            ) ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div id="converter-images" class="tab-pane" role="tabpanel" aria-labelledby="converter-images-tab">
                            <div class="table-responsive">
                                <table id="converter-images-table" cellpadding="0" cellspacing="0" class="table">
                                    <thead>
                                        <tr>
                                            <th><?= __('Image') ?></th>
                                            <th><?= __('File') ?></th>
                                            <th><?= __('Caption') ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td rowspan="3"><?= __(
                                                'Press "{0}" to identify images',
                                                [__('Generate HTML')]
                                            ) ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div id="converter-html" class="tab-pane" role="tabpanel" aria-labelledby="converter-html-tab">
                            <?= $this->Form->control('content_html', [
                                'required' => false,
                                'escape' => false,
                                'label' => false
                            ]) ?>
                        </div>
                        <div id="converter-logs" class="tab-pane" role="tabpanel" aria-labelledby="converter-logs-tab">
                        </div>
                    </div>
                </div>
            <?php elseif ($articleContent['html']): ?>
                <div class="form-group">
                    <?= $this->Form->control('content_html', [
                        'escape' => false,
                        'label' => __('Article Content'),
                        'cols' => 30,
                        'rows' => 10
                    ]) ?>
                </div>
            <?php endif; ?>
            <div class="form-group">
                <?= $this->Form->control('created', ['class' => 'form-control article_title_input_sm w-100 mb-3']) ?>
                <small class="form-text text-muted"><?= __('The initial publication date of the article.') ?></small>
            </div>
            <div class="form-group">
                <?= $this->Form->control('modified', ['class' => 'form-control article_title_input_sm w-100 mb-3']) ?>
                <small class="form-text text-muted"><?= __('The latest modification date of the article.') ?></small>
            </div>
            <div class="form-group">
                <?= $this->Form->control('article_status', [
                    'class' => 'form-control article_title_input_sm w-100 mb-3',
                    'options' => [0 => __('Drafted'), 3 => __('Published')]
                ]) ?>
                <small class="form-text text-muted"><?= __('The publication status of the article.') ?></small>
            </div>
            <?php if (!empty($article->id)): ?>
                <?= $this->Form->button(__('Save'), ['class' => 'btn cdli-btn-blue']) ?>
                <?= $this->Form->button(__('Delete'), [
                    'formnovalidate' => 'true',
                    'formaction' => $this->Url->build([
                        'controller' => 'Articles',
                        'action' => 'delete',
                        $article->article_type,
                        $article->id
                    ]),
                    'class' => 'btn btn-danger float-right',
                    'confirm' => __('Are you sure you want to delete this article?')
                ]) ?>
            <?php else: ?>
              <?= $this->Form->button(__('Submit'), ['class' => 'btn cdli-btn-blue']) ?>
            <?php endif; ?>
        <?= $this->Form->end() ?>
    </div>
</div>

<?php if ($articleContent['html']): ?>
    <?php $this->append('library'); ?>
        <script type="text/javascript" async src="/assets/js/mathjax/MathJax.js?config=TeX-AMS_HTML"></script>
        <script src="/assets/js/ckeditor/ckeditor.js"></script>
    <?php $this->end(); ?>

    <?php $this->append('script'); ?>
        <script>
            $(function () {
                CKEDITOR.editorConfig = function( config ) {
                    config.htmlEncodeOutput = true;
                };
                CKEDITOR.replace('content_html', {
                    filebrowserBrowseUrl: '/admin/ckeconnector/elfinder',
                    extraPlugins: 'mathjax,justify',
                    mathJaxLib: '/assets/js/mathjax/MathJax.js?config=TeX-AMS_HTML',
                    height: 530,
                    allowedContent: true,
                    removeFormatAttributes: '',
                    removeButtons: 'Underline'
                })
            })
        </script>
    <?php $this->end(); ?>
<?php endif; ?>

<?php if ($articleContent['latex']): ?>
    <?php $this->append('library'); ?>
        <script src="/assets/js/latex-converter.js"></script>
    <?php $this->end(); ?>

    <?php $this->append('script'); ?>
        <script>
            var latexConverter = {
                messages: {
                    confirmOverwrite: '<?= __('This will overwrite your changes to the HTML. Continue?') ?>'
                },
                urls: {
                    convertLatex: '<?= $this->Url->build(['action' => 'convertLatex']) ?>',
                    convertBibtex: '<?= $this->Url->build(['action' => 'convertBibtex']) ?>'
                }
            }

            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': $('[name="_csrfToken"]').val()
                }
            })
        </script>
    <?php $this->end(); ?>
<?php endif; ?>
