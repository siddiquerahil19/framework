<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\EntitiesPublication[]|\Cake\Collection\CollectionInterface $entitiesPublications
 */
?>
<h1 class="display-3 header-text text-left"><?= __('Entity-Publication Links') ?></h1>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead>
        <tr>
            <!-- <th scope="col"><?= $this->Paginator->sort('id') ?></th> -->
            <th scope="col"><?= $this->Paginator->sort('entity_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('entity_designation') ?></th>
            <th scope="col"><?= $this->Paginator->sort('table_name', "Entity Type") ?></th>
            <th scope="col"><?= $this->Paginator->sort('publication_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('publication_designation') ?></th>
            <th scope="col"><?= $this->Paginator->sort('exact_reference') ?></th>
            <th scope="col"><?= $this->Paginator->sort('publication_type') ?></th>
            <th scope="col"><?= $this->Paginator->sort('publication_comments') ?></th>
            <th scope="col"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($entitiesPublications as $entitiesPublication): ?>
        <tr>
            <!-- <td><?= $this->Number->format($entitiesPublication->id) ?></td> -->
            <td><?= $this->Html->link($entitiesPublication->entity_id, ['controller' => 'Artifacts', 'action' => 'view', $entitiesPublication->entity_id]) ?></td>
            <td>
                <?php if (isset($entitiesPublication->artifact->designation)) {
                    echo h($entitiesPublication->artifact->designation);
                } else {
                    echo '';
                }
                ?>
            </td>
            <td><?= h($entitiesPublication->table_name) ?></td>
            <td><?= $this->Html->link($entitiesPublication->publication_id, ['prefix' => false, 'controller' => 'Publications', 'action' => 'view', $entitiesPublication->publication_id]) ?></td>
            <td><?= h($entitiesPublication->publication->designation) ?></td>
            <td><?= h($entitiesPublication->exact_reference) ?></td>
            <td><?= h($entitiesPublication->publication_type) ?></td>
            <td><?= h($entitiesPublication->publication_comments) ?></td>
            <td>
            <?= $this->Html->link(__('Edit'), ['action' => 'edit',  $entitiesPublication->id],['escape' => false,'class'=>"btn btn-warning btn-sm"]) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>

