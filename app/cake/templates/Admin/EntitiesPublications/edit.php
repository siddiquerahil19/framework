<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\EntitiesPublication $entitiesPublication
 */
use Cake\Utility\Inflector;
?>
<h1 class="display-3 header-text text-left"><?= __('Edit Entity-Publication Link') ?></h1>



<?= $this->cell('PublicationView', [$entitiesPublication->publication->id])->render('display'); ?>

<!-- Here we exploit the $entity_type variable to get the correct CellView. -->
<?= $this->cell(Inflector::classify($entity_type) . 'View', [$entitiesPublication->entity_id])->render('display'); ?>

<div class="row justify-content-md-center ads">
    <div class="col-lg ads text-left form-wrapper mt-4">
        <legend class="form-heading mb-3"><?= __(
            'Edit link for entity #'.$entitiesPublication->entity_id.' of type "'.
                $entitiesPublication->table_name.'"'
        ) ?>
        </legend>
        <hr class="form-hr mb-4"/>
        <?= $this->Form->create(
            $entitiesPublication,
            ['url' => [$entitiesPublication->id, $flag, $entity_type, $parent_id]]
        ) ?>
            <?php echo $this->Form->control(
                'publication_id',
                ['type' => 'hidden',
                'value' => $entitiesPublication->publication->bibtexkey]
            ); ?>
            <?php echo $this->Form->control('entity_id', ['type' => 'hidden']); ?>
            <div class="layout-grid text-left">
                <div>
                    Publication Comments:
                    <?= $this->Form->control('publication_comments', ['label' => false, 'type' => 'textarea']); ?>
                </div>

                <div>
                    Exact Reference:
                    <?= $this->Form->control(
                        'exact_reference',
                        ['label' => false,
                        'type' => 'text',
                        'maxLength' => 20,
                        'required' => false]
                    ); ?>
                    Publication Type:
                    <?php $options = [
                        'primary' => 'primary',
                        'electronic' => 'electronic',
                        'citation' => 'citation',
                        'collation' => 'collation',
                        'history' => 'history',
                        'other' => 'other'
                        ];
                    echo $this->Form->control(
                        'publication_type',
                        ['label' => false,
                        'type' => 'select',
                        'options' => $options,
                        'default' => 'primary'],
                        ['class' => 'form-select']
                    );?>
                </div>
            </div>
            <?= $this->Form->button(__('Save'), ['class'=>'btn cdli-btn-blue']) ?>
        <?= $this->Form->end() ?>
        <?= $this->element('deleteModal', [ 'entity_name' => $entitiesPublication->entitiesPublication, 'id' => $entitiesPublication->id]); ?> 
  </div>
</div>
