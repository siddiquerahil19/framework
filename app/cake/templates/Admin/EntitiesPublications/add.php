<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\EntitiesPublication $entitiesPublication
 * @var \Cake\ORM\ResultSet $entitiesPublications
 * @var int $id
 * @var string $bibtexkey
 */
use Cake\Utility\Inflector;

if (isset($flag) && $flag != 'bulk') : ?>
    <h1 class="display-3 header-text text-left"><?= __('Add Entity-Publication Link') ?></h1>
<?php endif;?>
<?php if (!isset($flag) ||  $flag == '') : ?>
    <div class="row justify-content-md-center ads">
        <div class="col-lg ads text-left form-wrapper mt-4">
            <?= $this->Form->create($entitiesPublication) ?>
                <legend class="form-heading mb-3"><?= __('Link Entity and Publication') ?></legend>
                <hr class="form-hr mb-4"/>
                <div class="layout-grid text-left">
                    <div>
                        Entity ID:
                        <?= $this->Form->control('entity_id', ['label' => false, 'type' => 'text']); ?>
                        Publication Comments:
                        <?= $this->Form->control('publication_comments', ['label' => false, 'type' => 'textarea']); ?>
                        Entity type:
                        <?php $options = [
                            'artifacts' => 'artifact',
                            'proveniences' => 'provenience'
                        ];
                        echo $this->Form->control(
                            'table_name',
                            ['label' => false,
                                'type' => 'select',
                                'default' => 'artifact',
                                'options' => $options,
                                'class' => 'form-select']
                        );?>
                    </div>

                    <div>
                        Publication BibTex Key:
                        <?=  $this->Form->control(
                            'publication_id',
                            ['label' => false,
                            'type' => 'text',
                            'maxLength' => 200,
                            'id' => 'bibtexkeyAutocomplete',
                            'list' => 'bibtexkeyList',
                            'autocomplete' => 'off']
                        ); ?>
                        <datalist id="bibtexkeyList"></datalist>
                        Exact Reference:
                        <?= $this->Form->control(
                            'exact_reference',
                            ['label' => false,
                            'type' => 'text',
                            'maxLength' => 20,
                            'required' => false]
                        );?>
                        Publication Type:
                        <?php $options = [
                            'primary' => 'primary',
                            'electronic' => 'electronic',
                            'citation' => 'citation',
                            'collation' => 'collation',
                            'history' => 'history',
                            'other' => 'other'
                            ];
                        echo $this->Form->control(
                            'publication_type',
                            ['label' => false,
                            'type' => 'select',
                            'default' => 'primary',
                            'options' => $options,
                            'class' => 'form-select']
                        );?>
                    </div>
                </div>
                <?= $this->Form->button(__('Submit'), ['class'=>'btn cdli-btn-blue']) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>

<?php elseif ($flag == 'entity') : ?>
    <?= $this->cell('PublicationView', [$id])->render('display') ?>

    <div class="row justify-content-md-center ads">
        <div class="col-lg boxed">
            <?= $this->Form->create($entitiesPublication, ['url' => ['entity', $entity_type, $id]]) ?>
                <legend class="capital-heading"><?= __('Link ' . Inflector::camelize($entity_type) . ' to this publication') ?></legend>
                <?php echo $this->Form->control(
                    'publication_id',
                    ['type' => 'hidden',
                    'value' => $bibtexkey]
                ); ?>
                <div class="layout-grid text-left">
                    <div>
                        <?= __(Inflector::classify($entity_type) . ' ID') ?>
                        <?= $this->Form->control('entity_id', ['label' => false, 'type' => 'text']); ?>
                        Publication Comments:
                        <?= $this->Form->control('publication_comments', ['label' => false, 'type' => 'textarea']); ?>
                        <?= $this->Form->control(
                            'table_name',
                            ['type' => 'hidden', 'value' => $entity_type]
                        ); ?>
                    </div>

                    <div>
                        Exact Reference:
                        <?= $this->Form->control(
                            'exact_reference',
                            ['label' => false,
                            'type' => 'text',
                            'maxLength' => 20,
                            'required' => false]
                        ); ?>
                        Publication Type:
                        <?php $options = [
                            'primary' => 'primary',
                            'electronic' => 'electronic',
                            'citation' => 'citation',
                            'collation' => 'collation',
                            'history' => 'history',
                            'other' => 'other'
                            ];
                        echo $this->Form->control(
                            'publication_type',
                            ['label' => false,
                            'type' => 'select',
                            'default' => 'primary',
                            'options' => $options,
                            'class' => 'form-select']
                        );?>
                    </div>
                </div>
            <?= $this->Form->submit('Submit', ['class' => 'btn cdli-btn-blue']) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>

    <?php if ($entity_type == 'artifacts') : ?>
        <h3 class="display-4 pt-3 text-left"><?= __('Linked Artifacts') ?></h3>

        <div class="table-responsive">
            <table class="table-bootstrap my-3 mx-0">
                <thead>
                    <tr>
                        <th scope="col">Artifact Identifier</th>
                        <th scope="col">Artifact Designation</th>
                        <th scope="col">Exact Reference</th>
                        <th scope="col">Publication Type</th>
                        <th scope="col">Publication Comments</th>
                        <th scope="col"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($entitiesPublications as $entitiesPublication) : ?>
                    <tr>
                        <td><?= $entitiesPublication->has('artifact') ?
                            $this->Html->link(
                                'P'.substr(
                                    "00000{$entitiesPublication->artifact->id}",
                                    -6
                                ),
                                ['prefix' => false, 'controller' => 'Artifacts', 'action' => 'view', $entitiesPublication->artifact->id]
                            ) : '' ?></td>
                        <td><?= h($entitiesPublication->artifact->designation) ?></td>
                        <td><?= h($entitiesPublication->exact_reference) ?></td>
                        <td><?= h($entitiesPublication->publication_type) ?></td>
                        <td><?= h($entitiesPublication->publication_comments) ?></td>
                        <td>
                            <?= $this->Html->link(
                                $this->Html->tag('i', '', ['class' => 'fa fa-edit']),
                                ['action' => 'edit', $entitiesPublication->id, 'entity', 'artifacts', $id],
                                ['escape' => false, 'class' => 'btn btn-outline-success m-1',
                                'title' => 'Edit']
                            ) ?>
                            <?= $this->Form->postLink(
                                $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                                ['action' => 'delete', $entitiesPublication->id, 'entity', 'artifacts', $id],
                                ['confirm' => __('Are you sure you want to delete # {0}?', $entitiesPublication->id),
                                'escape' => false,
                                'class' => 'btn btn-outline-danger m-1',
                                'title' => 'Delete']
                            ) ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>

            <?php echo $this->element('Paginator'); ?>
        </div>

    <?php elseif ($entity_type == 'proveniences') : ?>
        <h3 class="display-4 pt-3 text-left"><?= __('Linked Proveniences') ?></h3>

        <div class="table-responsive">
            <table cellpadding="0" cellspacing="0" class="table-bootstrap mx-0">
                <thead>
                    <tr align="left">
                        <th scope="col">Provenience</th>
                        <th scope="col">Region</th>
                        <th scope="col">Geo Coordinates</th>
                        <th scope="col"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($entitiesPublications as $entitiesPublication): ?>
                        <?php $provenience = $entitiesPublication->provenience; ?>
                        <?php if (!empty($provenience)) : ?>
                            <tr align="left">
                                <td>
                                    <?= h($provenience->provenience) ?>
                                </td>
                                <td>
                                    <?php if ($provenience->has('region')): ?>
                                        <?= h($provenience->region->region) ?>
                                    <?php endif; ?>
                                </td>
                                <td><?= h($provenience->geo_coordinates) ?></td>
                                <td>
                                    <?= $this->Html->link(
                                        $this->Html->tag('i', '', ['class' => 'fa fa-edit']),
                                        ['action' => 'edit', $entitiesPublication->id, 'entity', 'proveniences', $id],
                                        ['escape' => false, 'class' => 'btn btn-outline-success m-1',
                                        'title' => 'Edit']
                                    ) ?>
                                    <?= $this->Form->postLink(
                                        $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                                        ['action' => 'delete', $entitiesPublication->id, 'entity', 'proveniences', $id],
                                        ['confirm' => __('Are you sure you want to delete # {0}?', $entitiesPublication->id),
                                        'escape' => false,
                                        'class' => 'btn btn-outline-danger m-1',
                                        'title' => 'Delete']
                                    ) ?>
                                </td>
                            </tr>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </tbody>
            </table>

            <?php echo $this->element('Paginator'); ?>
        </div>
    <?php endif; ?>

<?php elseif ($flag == 'publication') : ?>
    <!-- Here we exploit the $entity_type variable to get the correct CellView. -->
    <?= $this->cell(Inflector::classify($entity_type) . 'View', [$id])->render('display'); ?>

    <div class="row justify-content-md-center ads">
    <div class="col-lg boxed">
        <?= $this->Form->create($entitiesPublication, ['url' => [$flag, $entity_type, $id]]) ?>
            <legend class="capital-heading"><?= __(
                "Link Publication to entity #{$id} of type " . $entity_type
            )?>
            </legend>
            <?php echo $this->Form->control('entity_id', ['type' => 'hidden', 'value' => $id]); ?>
            <?php echo $this->Form->control('table_name', ['type' => 'hidden', 'value' => $entity_type]); ?>
            <div class="layout-grid text-left">
                <div>
                    Publication Comments:
                    <?= $this->Form->control('publication_comments', ['label' => false, 'type' => 'textarea']); ?>
                </div>

                <div>
                    Publication BibTex Key:
                    <?=  $this->Form->control(
                        'publication_id',
                        ['label' => false,
                        'type' => 'text',
                        'maxLength' => 200,
                        'id' => 'bibtexkeyAutocomplete',
                        'list' => 'bibtexkeyList',
                        'autocomplete' => 'off']
                    ); ?>
                    <datalist id="bibtexkeyList"></datalist>
                    Exact Reference:
                    <?= $this->Form->control(
                        'exact_reference',
                        ['label' => false,
                        'type' => 'text',
                        'maxLength' => 20,
                        'required' => false]
                    ); ?>
                    Publication Type:
                    <?php $options = [
                        'primary' => 'primary',
                        'electronic' => 'electronic',
                        'citation' => 'citation',
                        'collation' => 'collation',
                        'history' => 'history',
                        'other' => 'other'
                        ];
                    echo $this->Form->control(
                        'publication_type',
                        ['label' => false,
                        'type' => 'select',
                        'default' => 'primary',
                        'options' => $options,
                        'class' => 'form-select']
                    );?>
                </div>
            </div>
        <?= $this->Form->submit('Submit', ['class' => 'btn cdli-btn-blue']) ?>
        <?= $this->Form->end() ?>
    </div>
    </div>

    <h3 class="display-4 pt-3 text-left"><?= __('Linked Publications') ?></h3>
    <div class="table-responsive">
        <table class="table-bootstrap my-3 mx-0">
        <thead>
            <tr>
                <th scope="col">BibTex Key</th>
                <th scope="col">Publication Designation</th>
                <th scope="col">Publication Reference</th>
                <th scope="col">Exact Reference</th>
                <th scope="col">Publication Type</th>
                <th scope="col">Publication Comments</th>
                <th scope="col"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($entitiesPublications as $entitiesPublication) : ?>
            <tr>
                <td>
                <?= $this->Html->link(
                    $entitiesPublication->publication->bibtexkey,
                    ['prefix' => false, 'controller' => 'Publications', 'action' => 'view', $entitiesPublication->publication_id]
                )?>
              </td>
                <td><?= h($entitiesPublication->publication->designation) ?></td>
                <td>
                    <?= $this->Citation->formatReference($entitiesPublication->publication, 'bibliography', [
                            'template' => 'chicago-author-date',
                            'format' => 'html'
                        ]) ?>
                </td>
                <td><?= h($entitiesPublication->exact_reference) ?></td>
                <td><?= h($entitiesPublication->publication_type) ?></td>
                <td><?= h($entitiesPublication->publication_comments) ?></td>
                <td>
                    <?= $this->Html->link(
                        $this->Html->tag('i', '', ['class' => 'fa fa-edit']),
                        ['action' => 'edit', $entitiesPublication->id, $flag, $entity_type, $id],
                        ['escape' => false, 'class' => 'btn btn-outline-success m-1', 'title' => 'Edit']
                    ) ?>
                    <?= $this->Form->postLink(
                        $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                        ['action' => 'delete',
                        $entitiesPublication->id,
                        $flag,
                        $entity_type,
                        $id],
                        ['confirm' => __(
                            'Are you sure you want to delete # {0}?',
                            $entitiesPublication->id
                        ),
                        'escape' => false,
                        'class' => 'btn btn-outline-danger m-1',
                        'title' => 'Delete']
                    ) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
        </table>
    </div>
    <?php echo $this->element('Paginator'); ?>
<?php elseif ($flag == 'bulk') : ?>
    <?= $this->cell('BulkUpload::confirmation', [
                        'EntitiesPublications',
                        isset($error_list_validation) ? $error_list_validation:null,
                        isset($header) ? $header:null,
                        isset($entities) ? $entities:null]); ?>

    <?= $this->cell('BulkUpload', [
                        'EntitiesPublications',
                        isset($error_list) ? $error_list:null,
                        isset($header) ? $header:null,
                        isset($entities) ? $entities:null]); ?>

<?php endif; ?>
<script src="/assets/js/autocomplete.js"></script>
