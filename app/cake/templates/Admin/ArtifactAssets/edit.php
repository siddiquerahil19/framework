<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactAsset[]|\Cake\Collection\CollectionInterface $artifactAssets
 */

$this->assign('title', 'Edit access of visual asset');
?>

<div class="row justify-content-md-center">
    <div class="col-12 boxed">
        <h2><?= $asset->getDescription() ?></h2>
        <p><?= h($asset->artifact->designation) ?> (<?= h($asset->artifact->getCdliNumber()) ?>)</p>
        <?= $this->Form->create($asset) ?>
            <div class="form-group">
                <?= $this->Form->control('is_public', [
                    'class' => 'form-control',
                    'type' => 'toggle',
                    'label' => __('Is this visual asset public?')
                ]) ?>
            </div>

            <?= $this->Form->button(__('Submit'), [
                'class' => 'btn cdli-btn-blue',
                'name' => 'action',
                'value' => 'submitted'
            ]) ?>
        <?= $this->Form->end() ?>
    </div>

    <div class="col-12 boxed">
        <?= $this->cell('ArtifactAsset', [$asset->id]) ?>
    </div>
</div>
