<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Genre $genre
 */
?>

<div class="row justify-content-md-center">
    <div class="col-lg-7 text-left form-wrapper">
        <?= $this->Form->create($genre) ?>
            <legend class="form-heading mb-3"><?= __('Add Genre') ?></legend>
            <hr class="form-hr mb-4"/>
            <?php
                echo $this->Form->control('genre', ['class' => 'form-control w-100 mb-3', 'type' => 'text', 'required' => true]);
                echo $this->Form->control('parent_id', ['options' => $parentGenres, 'empty' => true ,'class' => 'form-control w-100 mb-3 custom-form-select']);
                echo $this->Form->control('genre_comments', ['class' => 'form-control w-100 mb-3', 'type' => 'text']);
            ?>

            <?= $this->Form->button(__('Submit'),['class'=>'btn cdli-btn-blue']) ?>
        <?= $this->Form->end() ?>
    </div>
</div>
