<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Provenience $provenience
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 text-left form-wrapper">
        <?= $this->Form->create($provenience) ?>
            <legend class="form-heading mb-3"><?= __('Edit Provenience') ?></legend>
            <?php
                echo $this->Form->control('provenience', ['class' => 'form-control w-100 mb-3', 'empty' => true, 'type' => 'text', 'maxLength' => 300]);
                echo $this->Form->control('region_id', ['class' => 'form-control w-100 mb-3', 'options' => $regions, 'empty' => true]);
                echo $this->Form->control('geo_coordinates', ['class' => 'form-control w-100 mb-3', 'empty' => true, 'type' => 'text', 'maxLength' => 50]);
                echo $this->Form->control('anc_name', ['class' => 'form-control w-100 mb-3', 'empty' => true, 'type' => 'text', 'maxLength' => 255]);
                echo $this->Form->control('transc_name', ['class' => 'form-control w-100 mb-3', 'empty' => true, 'type' => 'text', 'maxLength' => 255]);
                echo $this->Form->control('pleiades_id', ['label' => 'Pleiades Id', 'class' => 'form-control w-100 mb-3', 'empty' => true, 'type' => 'number', 'maxLength' => 11]);
                echo $this->Form->control('site_id', ['label' => 'Site Id', 'class' => 'form-control w-100 mb-3', 'empty' => true, 'type' => 'text', 'maxLength' => 3]);
                echo $this->Form->control('ara_name', ['class' => 'form-control w-100 mb-3', 'empty' => true, 'type' => 'text', 'maxLength' => 200]);
                echo $this->Form->control('arm_name', ['class' => 'form-control w-100 mb-3', 'empty' => true, 'type' => 'text', 'maxLength' => 200]);
                echo $this->Form->control('fas_name', ['class' => 'form-control w-100 mb-3', 'empty' => true, 'type' => 'text', 'maxLength' => 200]);
                echo $this->Form->control('geo_name', ['class' => 'form-control w-100 mb-3', 'empty' => true, 'type' => 'text', 'maxLength' => 200]);
                echo $this->Form->control('gre_name', ['class' => 'form-control w-100 mb-3', 'empty' => true, 'type' => 'text', 'maxLength' => 200]);
                echo $this->Form->control('heb_name', ['class' => 'form-control w-100 mb-3', 'empty' => true, 'type' => 'text', 'maxLength' => 200]);
                echo $this->Form->control('rus_name', ['class' => 'form-control w-100 mb-3', 'empty' => true, 'type' => 'text', 'maxLength' => 200]);
                echo $this->Form->control('osm_id', ['label' => 'Osm Id', 'class' => 'form-control w-100 mb-3', 'empty' => true, 'type' => 'text', 'maxLength' => 10]);
                echo $this->Form->control('osm_type', ['class' => 'form-select w-100 mb-3', 'options' => ["null" => 'select type', 'way' => 'way','node' => 'node'], 'empty' => false, 'type' => 'select']);
                echo $this->Form->control('geonames_id', ['label' => 'Geonames Id', 'class' => 'form-control w-100 mb-3', 'empty' => true, 'type' => 'number', 'maxLength' => 11]);
                echo $this->Form->control('lon_wgs1984', ['class' => 'form-control w-100 mb-3', 'empty' => true, 'type' => 'text', 'maxLength' => 200]);
                echo $this->Form->control('lat_wgs1984', ['class' => 'form-control w-100 mb-3', 'empty' => true, 'type' => 'text', 'maxLength' => 200]);
                echo $this->Form->control('accuracy', ['class' => 'form-control w-100 mb-3', 'empty' => false, 'type' => 'number', 'maxLength' => 11]);
            ?>

        <div>
        <?= $this->Form->button(__('Save'), ['class'=>'btn btn-primary cdli-btn-blue mr-2']) ?>
        <?= $this->Form->end() ?>
        <?= $this->element('deleteModal', [ 'entity_name' => $provenience->provenience, 'id' => $provenience->id]); ?>
        </div>

    </div>
</div>
