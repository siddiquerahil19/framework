<div class="row justify-content-md-center">
    <div class="col-lg-7 text-left form-wrapper">
        <?= $this->Form->create($featuredSealsEditData) ?>
            <legend class="form-heading mb-3"><?= __('Edit featured Seals') ?></legend>
            <hr class="form-hr mb-4"/>
                <?php
                echo $this->Form->control('title', ['class' => 'form-control w-100 mb-3','type' => 'text']);
                echo $this->Form->control('body', ['class' => 'form-control w-100 mb-3']);
                echo $this->Form->control('designation', ['class' => 'form-control w-100 mb-3', 'value'=>$featuredSealsEditData->artifact->designation, 'readonly' => true]);
                echo $this->Form->control('period', ['class' => 'form-control w-100 mb-3', 'value'=>$featuredSealsEditData->artifact->period->period, 'readonly' => true]);
                echo $this->Form->control('Artifact id', ['for'=> 'artifact_id','value'=>$featuredSealsEditData->artifact_id,'type' => 'text', 'class' => 'form-control w-100', 'required' => true]);
                echo $this->Form->control('Image type', ['for'=> 'image_id','value'=>'photo detail','type' => 'text', 'class' => 'form-control w-100', 'required' => true]);
                ?>
                <div>
                <?php $image = $this->ArtifactAssets->getThumbnail($featuredSealsEditData->artifact); ?>
                        <figure>
                            <?= $this->Html->image($image, [
                                'alt' => 'Artifact photograph',
                                'url' =>  $image,
                                'class'=> 'm-2 w-50 h-60'
                            ]) ?>
                            <figcaption>
                                <?= $this->Html->link('View full Image', $image) ?>
                                <span>
                                    <?= $this->Html->link(
                                        '',
                                        $image,
                                        ['class' => 'fa fa-share-square-o', 'aria-hidden'=> 'true']
                                    ) ?>
                                </span>
                            </figcaption>
                        </figure>
                </div>
        <div class="pt-2">
        <?= $this->Form->button(__('Save Changes'), ['class' => 'btn btn-primary cdli-btn-blue mr-2', 'id' => 'submit']) ?>
        <?= $this->Form->end() ?>
        </div>
    </div>
</div>

<!-- Show Alert box on reload -->
<script type="text/javascript">
    var submit = document.getElementById("submit");
    var count = false;
    submit.onclick = () => {
        count = true;
    }

    document.onclick = function(event) {
        if(event.target.id == 'delete'){
            count = true;
        }
    }

    window.addEventListener('beforeunload', function (e) {
        if(count) {
            return false;
        }
        else {
            e.preventDefault(); 
            e.returnValue = '';
        }
    });
</script>
