<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Artifact $artifact
 */
use Cake\ORM\TableRegistry;
?>

<div class="d-flex justify-content-between align-items-end mb-2">
    <h1 class="display-3 header-txt text-left"><?= __('Retired Artifacts') ?></h1>           
</div>

<table class="table-bootstrap my-3 mx-0">
    <thead>
        <tr>
            <th scope="col">Retired</th>
            <th scope="col">Redirected To</th>
            <th scope="col">Comments</th>        
            <th scope="col">Action</th>        
        </tr>
    </thead>
    <tbody>
        <?php foreach ($retired as $artifact): ?>        
                <tr>
                    <?php $retired_column = is_null($artifact->designation) ? 'P' . str_pad($artifact->id, 6, '0', STR_PAD_LEFT) : $artifact->designation .' ( ' . 'P' . str_pad($artifact->id, 6, '0', STR_PAD_LEFT) . ')'; ?>
                    <td><?= h($retired_column) ?></td>                      
                    <td>                  
                        <?php if ($artifact->has('redirect_artifact')): ?>                    
                        <?php $redirect_column =  is_null($artifact->redirect_artifact->designation) ? $artifact->redirect_artifact_id : $artifact->redirect_artifact->designation . ' (' . 'P' . str_pad($artifact->redirect_artifact_id, 6, '0', STR_PAD_LEFT) . ')'; ?>
                        <?= $this->Html->link(
                            $redirect_column,
                        ['prefix' => false, 'controller' => 'Artifacts', 'action' => 'view', $artifact->redirect_artifact_id]) ?> 
                        <?php endif; ?>                     
                    </td>
                        
                        <td><?= h($artifact->retired_comments) ?></td>
                    <td>
                        <?= $this->Html->link(
                            __('Edit'),
                            ['prefix' => false, 'action' => 'edit', $artifact->id],
                            ['escape' => false , 'class' => 'btn btn-warning btn-sm', 'title' => 'Edit']
                        ) ?>
                    </td>
                </tr>        
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>