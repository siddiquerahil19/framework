<?php
$this->assign('title', __('Seal chemistry add form'));
?>
<div class="row justify-content-md-center">

    <div class="col-7 text-left form-wrapper">
        <?= $this->Form->create($sealChemistryAddData,['type' => 'file']) ?>
            <legend class="form-heading mb-3"><?= __('Add chemical data for seal') ?></legend>
            <hr class="form-hr mb-4"/>
            <div class="form-group">
                <label for="chemicalSheet"><?= __('Upload Chemical Data') ?></label>
                <input name="chemicalSheet" type="file" class="form-control w-100 mb-3" required>
            </div>
            <?= $this->Form->control('Calibration file', [
                'class' => 'form-control w-100 mb-3 custom-form-select',
                'options' =>$fileNames,
                'empty' => true,
                'required' => true
            ]) ?>
            

          <?= $this->Form->button(__('Submit'),['class' => 'btn btn-primary cdli-btn-blue mr-2', 'id' => 'submit']) ?>
        <?= $this->Form->end() ?>

    </div>

</div>
<script type="text/javascript">
    var submit = document.getElementById("submit");
    var count = false;
    submit.onclick = () => {
        count = true;
    }

    window.addEventListener('beforeunload', function (e) {
    if(count) {
        return false;
    }
    else {
    e.preventDefault(); 
    e.returnValue = '';
    }
    });
</script>