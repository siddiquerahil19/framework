<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Region $region
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 text-left form-wrapper">
        <?= $this->Form->create($region) ?>
            <legend class="form-heading mb-3"><?= __('Edit Region') ?></legend>
            <hr class="form-hr mb-4"/>
            <?php
                echo $this->Form->control('region', ['class' => 'form-control w-100 mb-3', 'type' => 'text', 'required' => true]);
                echo $this->Form->control('geo_coordinates',  ['class' => 'form-control w-100 mb-3', 'type' => 'textarea'] );
                echo $this->Form->control('pleiades_id', ['class' => 'form-control w-100 mb-3', 'type' => 'number']);
            ?>

        <div>
        <?= $this->Form->button(__('Save'), ['class'=>'btn cdli-btn-blue']) ?>
        <?= $this->Form->end() ?>
        <?= $this->element('deleteModal', [ 'entity_name' => $region->region, 'id' => $region->id]); ?>
        </div>

    </div>


</div>
