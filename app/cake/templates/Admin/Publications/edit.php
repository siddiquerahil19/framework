<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Publication $publication
 */
?>
<h1 class="display-3 header-txt text-left"><?= __('Edit Publication Details') ?></h1>

<div class="row justify-content-md-center">
    <div class="col-lg ads text-left form-wrapper mt-4">
    <?= $this->Form->create($publication) ?>
            <legend class="form-heading mb-3"><?= __('Edit Publication') ?></legend>
            <hr class="form-hr mb-4"/>
            <div class="layout-grid text-left">
                <div>
                    Bibtex Key:
                    <?php echo $this->Form->control('bibtexkey', ['label' => false, 'type' => 'text']); ?>
                    Title:
                    <?php echo $this->Form->control('title', ['label' => false, 'type' => 'text', 'maxLength' => 255]);  ?>
                    Year:
                    <?php echo $this->Form->control('year', ['label' => false, 'type' => 'text', 'maxLength' => 20]); ?>
                    Entry Type:
                    <?php echo $this->Form->control('entry_type_id', ['label' => false, 'options' => $entryTypes, 'empty' => true]);  ?>
                    Address:
                    <?php echo $this->Form->control('address', ['label' => false, 'type' => 'text', 'maxLength' => 45]);  ?>
                    Annote:
                    <?php echo $this->Form->control('annote', ['label' => false, 'type' => 'text', 'maxLength' => 45]);  ?>
                    Book Title:
                    <?php echo $this->Form->control('book_title', ['label' => false, 'type' => 'text', 'maxLength' => 255]);  ?>
                    Chapter:
                    <?php echo $this->Form->control('chapter', ['label' => false, 'type' => 'text', 'maxLength' => 100]);  ?>
                    Cross Reference:
                    <?php echo $this->Form->control('crossref', ['label' => false, 'type' => 'text', 'maxLength' => 45]);  ?>
                    Edition:
                    <?php echo $this->Form->control('edition', ['label' => false, 'type' => 'text', 'maxLength' => 45]);  ?>
                    How Published:
                    <?php echo $this->Form->control('how_published', ['label' => false, 'type' => 'text', 'maxLength' => 255]);  ?>
                    Institution:
                    <?php echo $this->Form->control('institution', ['label' => false, 'type' => 'text', 'maxLength' => 45]);  ?>
                    Journal:
                    <?php echo $this->Form->control('journal_id', ['label' => false, 'options' => $journals, 'empty' => true]);  ?>
                    <?php
                        // Populating authors and editors values
                        $authors = [];
                        $editors = [];
                        if (isset($publication->authors)) {
                            foreach ($publication->authors as $author) {
                                array_push($authors, $author->author);
                            }
                        }
                        if (isset($publication->editors)) {
                            foreach ($publication->editors as $editor) {
                                array_push($editors, $editor->author);
                            }
                        }
                        $authors = implode(';', $authors);
                        $editors = implode(';', $editors);
                    ?>
                    Author List:
                    <div class="form-group">
                        <input id="authors_input" name="authors" type="text" class="form-control" value="<?= $authors ?>">
                        <div id="input-foot-authors-parent" class="input-foot-authors-parent"></div>
                    </div>
                </div>

                <div>
                    <div>
                        Month:
                        <?php echo $this->Form->control('month', ['label' => false, 'type' => 'text', 'maxLength' => 45]);  ?>
                        Note:
                        <?php echo $this->Form->control('note', ['label' => false, 'type' => 'text', 'maxLength' => 45]);  ?>
                        Number:
                        <?php echo $this->Form->control('number', ['label' => false, 'type' => 'text', 'maxLength' => 100]);  ?>
                        Organization:
                        <?php echo $this->Form->control('organization', ['label' => false, 'type' => 'text', 'maxLength' => 45]);  ?>
                        Pages:
                        <?php echo $this->Form->control('pages', ['label' => false, 'type' => 'text', 'maxLength' => 45]);  ?>
                        Publisher:
                        <?php echo $this->Form->control('publisher', ['label' => false, 'type' => 'text', 'maxLength' => 100]);  ?>
                        School:
                        <?php echo $this->Form->control('school', ['label' => false, 'type' => 'text', 'maxLength' => 80]);  ?>
                        Volume:
                        <?php echo $this->Form->control('volume', ['label' => false, 'type' => 'text', 'maxLength' => 50]);  ?>
                        Publication History:
                        <?php echo $this->Form->control('publication_history', ['label' => false, 'type' => 'text']);  ?>
                        Series:
                        <?php echo $this->Form->control('series', ['label' => false, 'type' => 'text', 'maxLength' => 100]);  ?>
                        OCLC:
                        <?php echo $this->Form->control('oclc', ['label' => false, 'type' => 'number']);  ?>
                        Designation:
                        <?php echo $this->Form->control('designation', ['label' => false, 'type' => 'text']); ?>
                        Editor List:
                        <div class="form-group">
                            <input id="editors_input" name="editors" type="text" class="form-control" value="<?= $editors ?>">
                            <div id="input-foot-editors-parent" class="input-foot-editors-parent"></div>
                        </div>
                    </div>
                </div>
            </div>
        <div>
        <?= $this->Form->button(__('Save'), ['class'=>'btn cdli-btn-blue']) ?>
        <?= $this->Form->end() ?>
        <?= $this->element('deleteModal', [ 'entity_name' => $publication->title, 'id' => $publication->id]); ?>
        </div>
    </div>
</div>

<script src="/assets/js/autocomplete.js"></script>
