<h1 class="display-3 header-txt text-left"><?= __('Select Publications For Merging') ?></h1>

<?= $this->Form->create(null, ['url' => ['action' => 'mergeSelect'], 'type' => 'get']) ?>
<div class="mx-0 boxed">
    <div class="col-lg ads row justify-content-md-center">
        <legend class="capital-heading"><?= __('Publication Search') ?></legend>
        <div class="layout-grid text-left">
            <div>
                BibTeX Key:
                <?= $this->Form->input('bibtexkey', ['label' => false , 'type' => 'text', 'value' => isset($data['bibtexkey']) ? $data['bibtexkey']:null]) ?>
                Title:
                <?= $this->Form->input('title', ['label' => false, 'type' => 'text', 'value' => isset($data['title']) ? $data['title']:null]) ?>
                Publisher:
                <?= $this->Form->input('publisher', ['label' => false, 'type' => 'text', 'value' => isset($data['publisher']) ? $data['publisher']:null]) ?>
                Author:
                <?= $this->Form->input('author', ['label' => false, 'type' => 'text', 'value' => isset($data['author']) ? $data['author']:null]) ?>
                Designation:
                <?= $this->Form->input('designation', ['label' => false, 'type' => 'text', 'value' => isset($data['designation']) ? $data['designation']:null]) ?>
            </div>
            <div>
                Entry Type:
                <?= $this->Form->control('entry_type_id', ['label' => '', 'options' => $entryTypes, 'empty' => true, 'value' => isset($data['entry_type_id']) ? $data['entry_type_id']:null, 'class' => 'form-select']);  ?>
                Journal:
                <?= $this->Form->control('journal_id', ['label' => '', 'options' => $journals, 'empty' => true, 'value' => isset($data['journal_id']) ? $data['journal_id']:null, 'class' => 'form-select']);  ?>
                Year:
                <div class="layout-grid text-left">
                    <div>
                    From:<?= $this->Form->control('from', ['label' => false, 'type' => 'number', 'value' => isset($data['from']) ? $data['from']:null]) ?>
                    </div>
                    <div>
                    To:<?= $this->Form->control('to', ['label' => false, 'type' => 'number', 'value' => isset($data['to']) ? $data['to']:null]) ?>
                    </div>
                </div>
            </div>
        </div>
        <table>
            <tr>
                <td>
                    <?= $this->Form->submit('Search', ['class' => 'btn cdli-btn-blue']) ?>
                </td>
                <td>
                    <?= $this->Html->link(
                        __('Reset'),
                        ['controller' => 'Publications', 'action' => 'mergeSelect'],
                        ['class' => 'btn']
                    ) ?>
                </td>
            </tr>
        </table>
    </div>
</div>

<?= $this->Form->end() ?>

<?php if ((!isset($publications)) or ($publications->count() == 0)) : ?>
<legend><?= __('No Search Results') ?></legend>
<?php else : ?>
    <?= $this->Form->create(null, ['url' => ['action' => 'merge']]) ?>
<div class="row justify-content-md-center">
    <table>
        <tr>
            <td>
                <?= $this->Form->submit('Merge', ['class' => 'btn cdli-btn-blue']) ?>
            </td>
            <td>
                <?= $this->Form->button('Reset', ['type' => 'reset', 'class' => 'btn']) ?>
            </td>
            <td>
                <button type="button" class="btn" onclick="checkAll()">Select All</button>
            </td>
        </tr>
    </table>
</div>
<div class="table-responsive">
    <table class="table-bootstrap my-3 mx-0">
        <thead>
            <tr>
                <th scope="col">BibTeX Key</th>
                <th scope="col">Authors</th>
                <th scope="col">Designation</th>
                <th scope="col">Reference</th>
                <th scope="col">Select</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($publications as $publication) : ?>
            <tr>
                <td>
                    <?= $this->Html->link(
                        h($publication->bibtexkey),
                        ['prefix' => false, 'action' => 'view', $publication->id]
                    ) ?>
                </td>
                <td align="left" nowrap="nowrap">
                    <?php foreach ($publication->authors as $author) : ?>
                        <?= $this->Html->link(
                            h($author->author),
                            ['prefix' => false, 'controller' => 'Authors', 'action' => 'view', $author->id]
                        ) ?><br>
                    <?php endforeach ?>
                </td>
                <td><?= h($publication->designation); ?></td>
                <td>
                    <?= $this->Citation->formatReference($publication, 'bibliography', [
                            'template' => 'chicago-author-date',
                            'format' => 'html'
                        ]) ?>
                </td>
                <td>
                    <?= $this->Form->checkbox('ids[]', ['value' => $publication->id,'hiddenField' => false]); ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<div class="row justify-content-md-center">
    <table>
        <tr>
            <td>
                <?= $this->Form->submit('Merge', ['class' => 'btn cdli-btn-blue']) ?>
            </td>
            <td>
                <?= $this->Form->button('Reset', ['type' => 'reset', 'class' => 'btn']) ?>
            </td>
            <td>
                <button type="button" class="btn" onclick="checkAll()">Select All</button>
            </td>
        </tr>
    </table>
</div>
    <?= $this->Form->end() ?>
<?php endif ?>
</div>

<script>
function checkAll() {
    var checkboxes = document.getElementsByTagName('input');
    for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i].type == 'checkbox') {
            checkboxes[i].checked = true;
        }
    }
}
</script>
