<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\UpdateEvent[]|\Cake\Collection\CollectionInterface $updateEvents
 */
?>

<h2><?= __('Review queue') ?></h2>

<div class="row justify-content-md-center">
    <div class="col-12 boxed">
        <table cellpadding="0" cellspacing="0" class="table-bootstrap my-3 mx-0">
            <thead>
                <tr>
                    <th scope="col"><?= __('Creator') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                    <th scope="col"><?= __('Project') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('update_type', __('Type of changes')) ?></th>
                    <th scope="col"><?= $this->Paginator->sort('status') ?></th>

                    <th scope="col"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($updateEvents as $updateEvent): ?>
                    <tr>
                        <td><?= $this->Html->link(
                            $updateEvent->creator->author,
                            ['prefix' => false, 'controller' => 'Authors', 'action' => 'view', $updateEvent->creator->id]
                        ) ?></td>
                        <td><?= h($updateEvent->created) ?></td>
                        <td>
                            <?php if ($updateEvent->has('external_resource')): ?>
                                <?= $this->Html->link(
                                    $updateEvent->external_resource->external_resource,
                                    ['prefix' => false, 'controller' => 'ExternalResources', 'action' => 'view', $updateEvent->external_resource->id]
                                ) ?>
                            <?php endif; ?>
                        </td>
                        <td><?= h($updateEvent->update_type) ?></td>
                        <td><?= h($updateEvent->status) ?></td>

                        <td>
                            <?= $this->Html->link(
                                __('Review'),
                                ['prefix' => false, 'action' => 'view', $updateEvent->id],
                                ['class' => 'btn cdli-btn-blue', 'target' => '_blank']
                            ) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<div class="row justify-content-md-center">
    <div class="col-lg-7 boxed">
        <?php echo $this->element('Paginator'); ?>
    </div>
</div>
