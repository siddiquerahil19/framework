<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SignReading $signReading
 */
?>
<div class="row justify-content-md-center">
    <div class="col-lg-7 text-left form-wrapper">
        <div class="signReadings form content">
            <?= $this->Form->create($signReading) ?>
                <legend class="form-heading mb-3"><?= __('Edit Sign Reading') ?></legend>
                <hr class="form-hr mb-4"/>
                <?php
                    echo $this->Form->control('sign_name',  ['class' => 'form-control w-100 mb-3', 'type' => 'text', 'required' => true]);
                    echo $this->Form->control('sign_reading',  ['class' => 'form-control w-100 mb-3', 'type' => 'text']);
                    echo $this->Form->control('preferred_reading',  ['class' => 'mb-4 mr-1']);
                    echo $this->Form->control('period_id', ['options' => $periods, 'empty' => true , 'class' => 'form-control w-100 mb-3 custom-form-select', 'id' => 'period_id', 'onchange' => 'checkValue(this);']);
                    echo $this->Form->control('provenience_id', ['options' => $proveniences, 'empty' => true, 'class' => 'form-control w-100 mb-3 custom-form-select', 'id' => 'proviences_id', 'onchange' => 'checkValue(this);']);
                    echo $this->Form->control('language_id', ['options' => $languages, 'empty' => true, 'class' => 'form-control w-100 mb-3 custom-form-select', 'id' => 'language_id', 'onchange' => 'checkValue(this);']);
                    echo $this->Form->control('meaning', ['class' => 'form-control w-100 mb-3', 'type' => 'text']);
                ?>
            <div>
            <?= $this->Form->button(__('Save'), ['class'=>'btn cdli-btn-blue']) ?>
            <?= $this->Form->end() ?>
            <?= $this->element('deleteModal', [ 'entity_name' => $signReading->sign_name, 'id' => $signReading->id]); ?>
            </div>
        </div>
    </div>
</div>
