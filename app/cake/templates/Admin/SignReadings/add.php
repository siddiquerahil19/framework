<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SignReading $signReading
 */
?>
<div class="row justify-content-md-center">
    <div class="col-lg-7 text-left form-wrapper">
            <?= $this->Form->create($signReading) ?>
            <fieldset>
                <legend class="form-heading mb-3"><?= __('Add Sign Reading') ?></legend>
                <hr class="form-hr mb-4"/>
                <?php
                    echo $this->Form->control('sign_name', ['class' => 'form-control w-100 mb-3', 'type' => 'text', 'required' => true]);
                    echo $this->Form->control('sign_reading', ['class' => 'form-control w-100 mb-3', 'type' => 'text']);
                    echo $this->Form->control('preferred_reading',  ['class' => 'mb-4 mr-1']);
                    echo $this->Form->control('period_id', ['options' => $periods, 'empty' => true , 'class' => 'form-control w-100 mb-3 custom-form-select', 'id' => 'period_id']);
                    echo $this->Form->control('provenience_id', ['options' => $proveniences, 'empty' => true, 'class' => 'form-control w-100 mb-3 custom-form-select', 'id' => 'proviences_id']);
                    echo $this->Form->control('language_id', ['options' => $languages, 'empty' => true, 'class' => 'form-control w-100 mb-3 custom-form-select', 'id' => 'language_id']);
                    echo $this->Form->control('meaning', ['class' => 'form-control w-100 mb-3', 'type' => 'text']);
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit'),['class' => 'btn btn-primary cdli-btn-blue mr-2', 'id' => 'submit']) ?>
            <?= $this->Form->end() ?>
    </div>
</div>

<!-- Show Alert box on reload -->
<script type="text/javascript">
    var submit = document.getElementById("submit");
    var count = false;
    submit.onclick = () => {
        count = true;
    }

    window.addEventListener('beforeunload', function (e) {
    if(count) {
        return false;
    }
    else {
    e.preventDefault(); 
    e.returnValue = '';
    }
    });
</script>
