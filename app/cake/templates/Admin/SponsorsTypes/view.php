<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SponsorType $sponsorType
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Sponsor Type'), ['action' => 'edit', $sponsorType->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Sponsor Type'), ['action' => 'delete', $sponsorType->id], ['confirm' => __('Are you sure you want to delete # {0}?', $sponsorType->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Sponsor Types'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sponsor Type'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Sponsor'), ['controller' => 'Sponsor', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sponsor'), ['controller' => 'Sponsor', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="sponsorTypes view large-9 medium-8 columns content">
    <h3><?= h($sponsorType->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Sponsor Type') ?></th>
            <td><?= h($sponsorType->sponsor_type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($sponsorType->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Sponsor') ?></h4>
        <?php if (!empty($sponsorType->sponsor)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Author Id') ?></th>
                <th scope="col"><?= __('Sponsor Type Id') ?></th>
                <th scope="col"><?= __('Cdli Title') ?></th>
                <th scope="col"><?= __('Contribution') ?></th>
                <th scope="col"><?= __('Sequence') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($sponsorType->sponsor as $sponsor): ?>
            <tr>
                <td><?= h($sponsor->id) ?></td>
                <td><?= h($sponsor->author_id) ?></td>
                <td><?= h($sponsor->sponsor_type_id) ?></td>
                <td><?= h($sponsor->cdli_title) ?></td>
                <td><?= h($sponsor->contribution) ?></td>
                <td><?= h($sponsor->sequence) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Sponsor', 'action' => 'view', $sponsor->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Sponsor', 'action' => 'edit', $sponsor->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Sponsor', 'action' => 'delete', $sponsor->id], ['confirm' => __('Are you sure you want to delete # {0}?', $sponsor->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
