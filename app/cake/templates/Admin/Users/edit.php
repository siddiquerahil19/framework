<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>

<div class="row justify-content-md-center">
    <div class="col-lg-7 text-left form-wrapper">
        <h2><?= __('Edit User') ?></h2>
        <hr class="form-hr mb-4"/>

        <?= $this->Flash->render() ?>

        <?= $this->Form->create($user) ?>
            <table class="table-bootstrap">
                <tbody>
                    <tr>
                        <th scope="row"><?= __('Username') ?></th>
                        <td><?= h($user->username) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Email') ?></th>
                        <td><?= h($user->email) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Created') ?></th>
                        <td><?= h($user->created_at) ?></td>
                    </tr>
                </tbody>
            </table>

            <hr class="form-hr mb-4"/>
            <div class="form-heading mb-3"><?= __('Roles') ?></div>

            <?php foreach ($roles as $role): ?>
                <div class="form-group">
                    <?= $this->Form->control('roles[][id]', [
                        'value' => $role->id,
                        'checked' => in_array($role->id, array_column($user->roles, 'id')),
                        'label' => ucfirst($role->name),
                        'type' => 'toggle',
                        'class' => 'form-control'
                    ]) ?>
                </div>
            <?php endforeach; ?>

            <hr class="form-hr mb-4"/>

            <div class="form-group">
                <?= $this->Form->control('active', [
                    'type' => 'toggle',
                    'class' => 'form-control'
                ]) ?>
            </div>

            <div class="form-group">
                <?= $this->Form->control('2fa_status', [
                    'label' => 'Two-factor authentication active',
                    'type' => 'toggle',
                    'class' => 'form-control'
                ]) ?>
            </div>

            <div class="form-group">
                <?= $this->Form->control('author_id', [
                    'type' => 'text',
                    'class' => 'form-control w-100'
                ]) ?>
            </div>

            <hr class="form-hr mb-4"/>

            <?= $this->Form->button(__('Save'), ['class' => 'btn cdli-btn-blue mr-2']) ?>
            <?php if ($user->active): ?>
                <?= $this->Form->button(__('Forgot Password'), ['class' => 'btn cdli-btn-blue mr-2', 'name' => 'fpbtn']) ?>
            <?php endif; ?>
        <?= $this->Form->end() ?>
    </div>
</div>
