<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="row justify-content-md-center">
    <div class="col-lg-7 boxed">
        <div class="capital-heading"><?= __('User Profile') ?></div>

        <table class="table-bootstrap">
            <tbody>
                <tr>
                    <th scope="row"><?= __('Username') ?></th>
                    <td><?= h($user->username) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Email') ?></th>
                    <td><?= h($user->email) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Created') ?></th>
                    <td><?= h($user->created_at) ?></td>
                </tr>
                <?php foreach ($roles as $role): ?>
                    <tr>
                        <th scope="row"><?= h(ucfirst($role->name)) ?></th>
                        <td><?= in_array($role->id, array_column($user->roles, 'id')) ? __('Yes') : __('No') ?></td>
                    </tr>
                <?php endforeach; ?>
                <tr>
                    <th scope="row"><?= __('Active') ?></th>
                    <td><?= $user->active ? __('Yes') : __('No'); ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('2FA Status') ?></th>
                    <td><?= $user->{'2fa_status'} ? __('Yes') : __('No'); ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Author') ?></th>
                    <td><?= $this->Html->link(
                        "{$user->author->author} ({$user->author_id})",
                        ['prefix' => false, 'controller' => 'Authors', 'action' => 'view', $user->author->id]
                    ) ?></td>
                </tr>
            </tbody>
        </table>

        <?= $this->Html->link(
            __('Edit'),
            ['action' => 'edit', $user->username],
            ['class' => 'btn cdli-btn-blue float-left']
        ) ?>

        <?= $this->Form->postButton(
            __('Forgot Password'),
            ['action' => 'forgotPassword', $user->username],
            ['class' => 'btn cdli-btn-blue float-right']
        ) ?>
    </div>
</div>
