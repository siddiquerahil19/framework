<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Region $region
 */
?>
<?php use Cake\Utility\Inflector; ?>

<div>
    <div class="text-left text-heading"><?= __(h(Inflector::humanize(Inflector::singularize(Inflector::tableize($this->request->getParam('controller')))))) ?>: <?= h($region->region) ?>
      <?php if ($access_granted): ?>
          <?= $this->Html->link(
              __('Edit'),
              ['prefix' => 'Admin', 'action' => 'edit', $region->id],
              ['escape' => false , 'class' => "btn cdli-btn-blue pull-right", 'title' => 'Edit']
          ) ?>
      <?php endif; ?>
    </div>
</div>

<div class="row justify-content-md-center">
    <div class="col-lg-10 boxed">
        <div id="map" style="height: 500px;z-index: 0;"></div>
    </div>
</div>

<?php if (!empty($region->proveniences)): ?>
    <div class="single-entity-wrapper mx-0 text-left mt-4">
        <div class="capital-heading"><?= __('Proveniences located within region') ?></div>
            <ul>
                <?php foreach ($proveniences_id  as $provenience => $id): ?>
                    <li><?= $this->Html->link(
                        $provenience,
                        ['controller' => 'Proveniences', 'action' => 'view', $id]
                    ) ?></li>
                <?php endforeach; ?>
            </ul>
    </div>
<?php endif; ?>

<?= $this->element('leaflet') ?>
<?php $this->append('script'); ?>
<script>
(function () {
    var region = <?= json_encode($region) ?>

    var map = L.map('map')
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map)

    var coordinates = JSON.parse('[' + region.geo_coordinates + ']')
    for (var j = 0; j < coordinates.length; j++) {
        coordinates[j].reverse()
    }

    var polygon = L.polygon(coordinates).addTo(map)
    var bounds = polygon.getBounds()
    map.setView(bounds.getCenter(), map.getBoundsZoom(bounds))

    for (var i = 0; i < region.proveniences.length; i++) {
        var provenience = region.proveniences[i]
        var coordinates = JSON.parse('[' + provenience.geo_coordinates + ']').reverse()
        var link = '<a href="/proveniences/' + provenience.id + '">' + provenience.provenience + '</a>'
        L.marker(coordinates).addTo(map).bindPopup(link)
    }
})()
</script>
<?php $this->end(); ?>

<?= $this->element('citeButton'); ?>
<?= $this->element('citeBottom'); ?>