<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Submission $submission
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Submission'), ['action' => 'edit', $submission->submission_id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Submission'), ['action' => 'delete', $submission->submission_id], ['confirm' => __('Are you sure you want to delete # {0}?', $submission->submission_id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Submissions'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Submission'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <?php foreach ($submission->review_assignments as $review_assignment):  ?>
            <div>
              <h2 class="review_hi">Review Round <?= $review_assignment->round ?></h2>
              <div >
                <p >
                  <span class="font-weight-bold">Author : </span>
                  <span>Somil Jain, 26 Aug 2021 </span>
                </p>
                <p class="review_fi">
                  <span class="fa fa-paperclip fi_icon"></span>
                  <a href="">Submission.pdf</a>
                </p>
              </div>
              <div>
                <p >
                  <span class="font-weight-bold">Reviewer : </span>
                  <span> <?= h($review_assignment->user_settings[4]->setting_value)." ".h($review_assignment->user_settings[3]->setting_value) ?>, <?= h($review_assignment->date_completed) ?> </span>
                </p>
                <div class="comment more">
                <?= $review_assignment->submission_comments[0]->comments?>
                </div>
              </div>
              <div>
                <p>
                  <span class="font-weight-bold">Author : </span>
                  <span>Somil Jain, 30 Aug 2021 </span>
                </p>
                <p class="review_fi">
                  <span class="fa fa-paperclip fi_icon" style="color:blue"></span>
                  <a href="">Revised_File.pdf</a>
                </p>
              </div>

              <div>
              <div class="accordion journal-accordion">
                <?= $this->Accordion->partOpen('Discussion', 'Discussion', 'h3') ?>
                <div>
                  <?php foreach ($submission->queries as $query):  ?>
                    <?php if($query->stage_id == '3'): ?>
                      <?php foreach ($query->notes as $note):  ?>
                        <div class="discuss">
                          <p>
                            <span class="font-weight-bold">Author : </span>
                            <span>Somil Jain, <?= $note->date_created ?> </span>
                          <p>
                          <p>
                            <?=  $note->contents ?>
                          </p>
                        </div>
                      <?php endforeach; ?>
                    <?php endif;  ?>
                  <?php endforeach; ?>
                </div>
                <?= $this->Accordion->partClose() ?>
             </div>
              </div>
            </div>
          </div>
          <?php endforeach; ?>

</div>
<script src="/assets/js/journals_dashboard.js"></script>
