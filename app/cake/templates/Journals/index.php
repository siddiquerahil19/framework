<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Journal[]|\Cake\Collection\CollectionInterface $journals
 */
?>

<div class="row justify-content-md-center">
    <div class="col-12">
        <div class="d-flex justify-content-between align-items-end mb-2">
            <h1 class="display-3 header-txt text-left"><?= __('Journals') ?></h1>           
            <?= $this->element('addButton'); ?>
        </div>
        <div class="text-left my-2">
            <?= $this->element('entityExport'); ?>
        </div>
        
        <div class="table-responsive">
            <table cellpadding="0" cellspacing="0" class="table-bootstrap">
                <thead>
                    <tr>
                        <th><?= $this->Paginator->sort('journal') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($journals as $journal): ?>
                        <tr>
                            <td><?= $this->Html->link(
                                h($journal->journal),
                                ['controller' => 'Journals', 'action' => 'view', $journal->id]
                            ) ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>

    <div class="col-lg-7 boxed">
        <?= $this->element('Paginator') ?>
    </div>
</div>

<?= $this->element('citeButton'); ?>
<?= $this->element('citeBottom'); ?>