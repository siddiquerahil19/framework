<?php
$this->assign('title', __('Seal Portal'));
?>
<h1 class="text-left display-3 header-txt">Seal Portal</h1>
<div class="text-left">
    <?php foreach ($posting as $post):?>
        <?= $post->body; ?>
    <?php endforeach; ?>
</div>
<div class="row mt-3">
    <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4 bot-sp">
        <div class="d-flex flex-column card-body text-left border">
            <h3 class="mb-4">
            <?=
                $this->Html->link(
                    'All CDLI Seals',
                    '/search?atype=seal&advanced_search=1'
                );
            ?>
            </h3>
            <p class="admin-text">View all seals in the CDLI, both seal impressions, composite seals and physical seals.</p>
        </div>
    </div>
    <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
        <div class="d-flex flex-column card-body text-left border">
            <h3 class="mb-4">
                <?= $this->Html->link(
                    'Composite Seals',
                    '/search?atype=seal&adesignation=composite&advanced_search=1'
                );?> 
            </h3>
            <p class="admin-text">Composites Seals are derived from seal impressions, and therefore the negatives of original cylinder seals now lost.</p>
        </div>
    </div>
    <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
        <div class="d-flex flex-column card-body text-left border">
            <h3 class="mb-4">
            <?= $this->Html->link(
                    'Physical Seals',
                    '/search?atype=seal&adesignation=physical&advanced_search=1'
                );?> 
            </h3>
            <p class="admin-text">View all Physical Seals, both stamp and cylinder shaped, in the CDLI.</p>
        </div>
    </div>
</div>
<div class="row mt-3">
    <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
        <div class="d-flex flex-column card-body text-left border">
            <h3 class="mb-4">
            <?= $this->Html->link(
                'Sealings',
                '/search?atype=sealing&advanced_search=1'
            );?> 
            </h3>
            <p class="admin-text">View all impressions of seals in the CDLI. Whenever possible seal impressions, or sealings, have been linked to a composite seal entry as well. </p>
        </div>
    </div>
</div>
<div>
    <h2 class="text-left display-4 section-title">Well Attested Seals</h2>
    <div class="row mt-5">
        <?php foreach ($featuredSeals as $seal):
            if($seal->artifact->is_public){
        ?>
            <?php $image = $this->ArtifactAssets->getThumbnail($seal->artifact); ?>
            <div class="card bs4-cdli-card col-sm-12 col-md-6 col-lg-4 mb-5">
                <img src="<?= $image ?>"
                     alt="Highlight <?= $seal->title . '.jpg' ?>"
                     class="card-img-top">
                <?php if($isadmin): 
                    echo $this->Html->link(
                            $this->Html->tag('i', '', ['class' => 'fa fa-edit']),
                            ['prefix' => 'Admin', 'controller' => 'Artifacts', 'action' => 'featuredSealsEdit', $seal->id],
                            ['escape' => false, 'class' => 'text-left btn btn-primary-outline m-1', 'title' => 'Edit']);
                endif; ?>
                <div class="card-body">
                    <p class="card-title">
                        <?= $this->Html->link(
                            strlen($seal->title) > 38 ? substr($seal->title, 0, 38) . '...' : $seal->title,
                            ['controller' => 'Artifacts', 'action' => 'view', $seal->artifact_id]
                        ) ?>
                    </p>
                    <p class="card-text">
                        <?=
                        $seal->body
                        ?>
                    </p>
                </div>
            </div>
        <?php 
            }
    endforeach;?>
    </div>
</div>
<div>
    <h2 class="text-left display-4 section-title">Seals and impressed artifacts by period</h2>
    <div class="table-responsive table-hover" id="sealTable">
        <table class="table-bootstrap my-3">
            <thead align="left">
                <tr>
                    <th scope="col"><?php
                    $this->Paginator->options([
                        'url' => [
                            '#' => 'sealTable'
                        ]
                    ]);
                    echo $this->Paginator->sort('Period')
                    ?></th>
                    <th scope="col">
                        <?php 
                        $this->Paginator->options([
                            'url' => [
                                '#' => 'sealTable'
                            ]
                        ]);
                        echo $this->Paginator->sort('Seals') 
                        ?>
                    </th>
                    <th scope="col">Impressed artifacts</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($sealPeriods as $sealPeriods): ?>
                    <?php if(!is_null($sealPeriods->period)){ ?>
                    <tr>
                        <td align="left"><?= $sealPeriods->period->period ?></td>
                        <td align="left">
                            <?php $link = '/search?atype=seal&period='.$sealPeriods->period->period.'&advanced_search=1'; ?>
                        <?= $this->Html->link(
                            $sealPeriods->Seals,
                            $link
                        );?> 
                        </td>
                        <td align="left"></td>
                    </tr>
                    <?php } ?>
                <?php endforeach; ?>
            </tbody>
        </table>
        <?php echo $this->element('Paginator'); ?>
    </div>
</div>

<?= $this->element('citeBottom') ?>
<?= $this->element('citeButton') ?>
