<?php
$this->assign('title', $score . ' ' . $designation);
?>

<h1 class="text-left"><?= $score." ".$designation ?></h1>
<?php
  $score1 = strpos($text,$score);
  $score3 = 0;
  $compositeNum = "";
  if(substr($text,$score1+strlen($score),1) == "."){ //if composite number is like Q12034.05
      $score2 = $score1+strlen($composite)+1;
      for($m = $score2;$m<=strlen($text);$m++){

        if($text[$m] == " "){
          $score3 = $m;
          break;
        }
      }
      $compositeNum = substr($text,$score1,$score3-$score1);
          
  }else{
    $compositeNum = $score;                        
  }
 
  $allwitness = explode('#atf', $text);
  for($i=0; $i < count($allwitness) ; $i++) {
      if(strpos($allwitness[$i],'>>') != null){
        $arr = explode('>>'.$compositeNum." ",$allwitness[$i]);
        for($i=0; $i<count($arr)-1; $i++) {
          
        //   echo "<br><br>start<br>";
          $key = 0;
          $finalNum = "";
          $str =$arr[$i+1];
          
          for($j = 0 ;$j<= strlen($str)-1 ; $j++ ){
            if($str[$j] == " "){
              $key = $j;
              break;
            }
          }
          

          $num = substr($str,0,$key);
          for($k = 0; $k < strlen($num) ; $k++){
            if($num[$k] != "0"){
              $finalNum = substr($num,$k);
              break;
            }else{
              continue;
            }
          }
          
          $str2 = $arr[$i];
          $key2 = strpos($str2,$finalNum.". ");
          if($key2 == ''){
              for($n = 0;$n < strlen($str2) ; $n++){
                  if(is_numeric($str2[$n]) && $str2[$n + 1] == "'" && $str2[$n + 2] == "."){
                    $key2 = $n;
                    break;
                  }
              }
          }

          $witnessText = substr($str2,$key2);
          $hasTranslation = strpos($witnessText, "#tr.");
          if($hasTranslation != ''){
            $translation = substr($witnessText,$hasTranslation+4);
            $witnessTextWithoutTranslation = substr($witnessText,0,$hasTranslation);
            ?>
              <div class="row mt-5 mb-3 text-left ">
                  <?= $compositeNum?>
                  <ul class="list-unstyled ml-5">
                    <li><?= $witnessTextWithoutTranslation ?></li>
                    <li><?= $translation ?></li>
                  </ul>
              </div>
            <?php
          }else{
            ?>
              <div class="row mt-5 mb-3 text-left">
                  <?= $compositeNum?>
                  <div class="ml-5">
                    <?= $witnessText ?>
                  </div>
              </div>
          <?php
          }
        
          //obs and rev text logic


          foreach($incArr as $a){
              $taggedTextEnd = strpos($a->atf , ">>".$compositeNum." ".$num);
              $taggedTextStart = 0;
              if($taggedTextEnd != '') {
                  $taggedatfIdEnd = strpos($a->atf," ");
                  $taggedatfId = substr($a->atf,1,$taggedatfIdEnd);
                  $atfIdLink = substr($a->atf,2,$taggedatfIdEnd - 2);  //for link to P number
                  $taggedsearchText = substr($a->atf,0,$taggedTextEnd );
                  
                  for($x =  strlen($taggedsearchText) -1; $x >=0 ; $x--  ) {
                    if(is_numeric($taggedsearchText[$x]) && $taggedsearchText[$x+1] == "." && $taggedsearchText[$x+2] == " ") {
                        $taggedTextStart = $x;
                        break;
                    } else if(is_numeric($taggedsearchText[$x]) && $taggedsearchText[$x+1] == "'" && $taggedsearchText[$x+2] == "."){
                        $taggedTextStart = $x;
                        break;
                    }
                  }
                  $taggedText = substr($taggedsearchText,$taggedTextStart,$taggedTextEnd-$taggedTextStart);
                  
                  $isrev = 0;
                  $isobv = 0;
                  for($x =  strlen($taggedsearchText) -1; $x >=0 ; $x--  ) {
                    if(strpos($taggedsearchText,"@reverse")) {
                      $hasTranslation = strpos($taggedText, "#tr.");
                      if($hasTranslation != ''){
                        $translation = substr($taggedText,$hasTranslation+4);
                        $taggedTextWithoutTranslation = substr($taggedText,0,$hasTranslation);
                      ?>
                        <div class="row mt-2 ml-3 text-left ">
                          <?= 
                          $this->Html->link($taggedatfId, ['controller' => 'artifacts', 'action' => 'view', $atfIdLink]);
                          ?>
                          <div class="ml-2">rev.</div>
                          <div class="ml-3">
                              <?= $taggedTextWithoutTranslation ?>
                          </div>
                        </div>
                    <?php
                      }else {
                        ?>
                        <div class="row mt-2 ml-3 text-left ">
                          <?= 
                          $this->Html->link($taggedatfId, ['controller' => 'artifacts', 'action' => 'view', $atfIdLink]);
                          ?>
                          <div class="ml-2">rev.</div>
                          <div class="ml-3">
                            <?=$taggedText ?>
                          </div>
                        </div>
                        <?php
                      }
                      ?>
                      <?php
                        break;
                    } else if(strpos($taggedsearchText,"@obverse")) {
                      $hasTranslation = strpos($taggedText, "#tr.");
                      if($hasTranslation != ''){
                        $taggedTextWithoutTranslation = substr($taggedText,0,$hasTranslation);
                      ?>
                        <div class="row mt-2 ml-3 text-left ">
                          <?= 
                          $this->Html->link($taggedatfId, ['controller' => 'artifacts', 'action' => 'view', $atfIdLink]);
                          ?>
                          <div class="ml-2">obv. </div>
                          <div class="ml-3">
                              <?= $taggedTextWithoutTranslation ?>
                          </div>
                        </div>
                    <?php
                      }else {
                        ?>
                        <div class="row mt-2 ml-3 text-left ">
                          <?= 
                          $this->Html->link($taggedatfId, ['controller' => 'artifacts', 'action' => 'view', $atfIdLink]);
                          ?>
                          <div class="ml-2">obv.</div>
                          <div class="ml-3">
                            <?=$taggedText ?>
                          </div>
                        </div>
                        <?php
                      }
                      ?>
                      <?php
                        break;
                    } else if(strpos($taggedsearchText,"@surface")) {
                      $hasTranslation = strpos($taggedText, "#tr.");
                      if($hasTranslation != ''){
                        $taggedTextWithoutTranslation = substr($taggedText,0,$hasTranslation);
                      ?>
                        <div class="row mt-2 ml-3 text-left ">
                          <?= 
                          $this->Html->link($taggedatfId, ['controller' => 'artifacts', 'action' => 'view', $atfIdLink]);
                          ?>
                           <div class="ml-2">surf.</div>
                            <div class="ml-3">
                              <?= $taggedTextWithoutTranslation ?>
                            </div>
                        </div>
                    <?php
                      }else {
                        ?>
                        <div class="row mt-2 ml-3 text-left ">
                          <?= 
                          $this->Html->link($taggedatfId, ['controller' => 'artifacts', 'action' => 'view', $atfIdLink]);
                          ?>
                          <div class="ml-2">surf.</div>
                          <div class="ml-3">
                            <?=$taggedText ?>
                          </div>
                        </div>
                        <?php
                      }
                      ?>
                      <?php
                        break;
                }
              }
                
            }
          }
        }
      }
    }




?>

<?= $this->element('citeBottom') ?>
<?= $this->element('citeButton') ?>
