<?php
/**
 * @var \App\View\AppView $this
 */
?>

<div class="row justify-content-md-center">
    <div class="col-12 text-left">
        <?= $this->Html->link(
            '<span class="fa fa-chevron-left"></span> ' . __('Back to artifact'),
            ['action' => 'view', $artifact->id],
            ['escapeTitle' => false]
        ) ?>
    </div>

    <div class="col-lg-8 text-left boxed">
        <h2><?= __('Visual assets of artifact') ?></h2>
        <p><?= h($artifact->designation) ?> (<?= h($artifact->getCdliNumber()) ?>)</p>

        <table class="table-bootstrap">
            <thead>
                <tr>
                    <th></th>
                    <th><?= __('Type of visual asset') ?></th>
                    <th><?= __('Artifact aspect') ?></th>
                    <?php if ($isAdmin): ?>
                        <th><?= __('Public') ?></th>
                        <th></th>
                    <?php endif; ?>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($artifact->artifact_assets as $asset): ?>
                    <?php
                        $thumbnail = $asset->getThumbnailPath();
                        $url = ['controller' => 'ArtifactAssets', 'action' => 'view', $asset->id];
                    ?>
                    <tr>
                        <td>
                            <?php if (is_null($thumbnail)): ?>
                                <?= $this->Html->link(__('View'), $url) ?>
                            <?php else: ?>
                                <?= $this->Html->image(DS . $thumbnail, ['url' => $url]) ?>
                            <?php endif; ?>
                        </td>
                        <td><?= h($asset->asset_type) ?></td>
                        <td><?= h($asset->artifact_aspect ?? '—') ?></td>
                        <?php if ($isAdmin): ?>
                            <td><?= h($asset->is_public) ? 'yes' : 'no' ?></td>
                            <td><?= $this->Html->link(__('Edit'), [
                                'prefix' => 'Admin',
                                'controller' => 'ArtifactAssets',
                                'action' => 'edit',
                                $asset->id
                            ]) ?></td>
                        <?php endif; ?>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
