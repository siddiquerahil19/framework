<?php
/**
* @var \App\View\AppView $this
* @var \App\Model\Entity\Artifact $artifact
*/


$roles = $this->getRequest()->getSession()->read('Auth.User.roles');
$name = $this->getRequest()->getSession()->read('Auth.User.username');
$ifRoleExists = !is_null($roles) ?  array_intersect($roles, [1, 2]) : [];
$ifRoleExists = !empty($ifRoleExists) ? 1 : 0;
$isPhysicalSeal = $artifact->has('seal_no') && str_contains($artifact->designation, 'physical');

$CDLI_NO = $artifact->getCdliNumber();
$this->assign('title', $artifact->designation . ' (' . $CDLI_NO . ')');
$this->assign('description', $artifact->getDescription());
$this->assign('image', '/dl/photo/' . $CDLI_NO . '.jpg');
?>

<?php if (!$hasgraph): ?>
    <?php $this->append('css'); ?>
        <link rel="stylesheet" href="\assets\css\conllu.js\jquery-ui-redmond.css">
        <link rel="stylesheet" href="\assets\css\conllu.js\style-vis.css">
    <?php $this->end(); ?>
<?php endif; ?>
<?php $this->append('library'); ?>
    <script type="text/javascript" src="\assets\js\conllu.js\lib\ext\head.load.min.js"></script>
<?php $this->end(); ?>

<main id="artifact">
    <div id="artifact-header" class="<?= $isPhysicalSeal ? 'physical-seal' : ''?>">
        <div id="artifact-title">
            <h1 class="display-3 header-txt"><?= h($artifact->designation) ?> (<?= h($CDLI_NO) ?>)</h1>
            <span class="my-4 artifact-desc"><?= $artifact->getDescription() ?></span>
        </div>

        <div id="artifact-actions">
            <?php // Export grid, edit buttons ?>
            <div class="d-flex justify-content-between w-100">
                <div>
                    <?php if (!is_null($artifact->seal_no)): ?>
                        <?= $this->Html->link(
                            'View Annotations',
                            '/#',
                            ['class' => 'btn btn-primary cdli-btn-blue mr-3 p-2 w-30']
                        ) ?>
                    <?php endif; ?>
                    <?= $this->element('artifactExport', ['artifact' => $artifact]) ?>
                </div>
                <?php if ($canSubmitEdits): ?>
                    <div class="dropdown">
                        <button class="btn cdli-btn-blue dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
                            <?= __('Edit') ?>
                        </button>
                        <div class="dropdown-menu">
                            <?= $this->Html->link(
                                'Edit Metadata',
                                ['controller' => 'Artifacts', 'action' => 'edit', $artifact->id],
                                ['class' => 'dropdown-item']
                            ) ?>
                            <?php if (!empty($artifact->artifact_assets)): ?>
                                <?= $this->Html->link(
                                    __('Edit Images'),
                                    ['controller' => 'Artifacts', 'action' => 'images', $artifact->id],
                                    ['class' => 'dropdown-item']
                                ) ?>
                            <?php endif; ?>
                            <?php if (!empty($artifact->inscription)): ?>
                                <?= $this->Html->link(
                                    'Edit Text or Annotation',
                                    ['controller' => 'Inscriptions', 'action' => 'edit', $artifact->inscription->id],
                                    ['class' => 'dropdown-item']
                                ) ?>
                            <?php else: ?>
                                <?= $this->Html->link(
                                    'Add Text',
                                    ['controller' => 'Inscriptions', 'action' => 'add', '?' => ['artifact' => $artifact->id]],
                                    ['class' => 'dropdown-item']
                                ) ?>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>

        <?php // Media ?>
        <div id="artifact-media">
            <div id="artifact-media-carousel" class="carousel" data-interval="false">
                <?php
                    $assets = $this->ArtifactAssets->orderAssets($artifact->artifact_assets);
                    if (!$isPhysicalSeal) {
                        $assets = array_filter($assets, function ($asset) {
                            return !is_null($asset->getThumbnailPath());
                        });
                    }
                ?>
                <div class="carousel-inner text-center">
                    <?php foreach ($assets as $index => $asset): ?>
                        <?php
                            $path = $isPhysicalSeal ? $asset->getPath() : $asset->getThumbnailPath();
                            $fullLink = $this->Url->build(['controller' => 'Artifacts', 'action' => 'reader', $artifact->id, $asset->id]);
                        ?>
                        <div class="carousel-item <?= $index === 0 ? ' active' : '' ?> <?= $asset->asset_type === 'lineart' ? 'bg-white' : '' ?>">
                            <a href="<?= $fullLink ?>" target="_blank">
                                <figure class="d-inline-flex flex-column justify-content-center align-items-center">
                                    <?= $this->Html->image(DS . $path, ['alt' => $asset->getDescription()]) ?>
                                </figure>
                                <div class="carousel-caption" class="text-white">
                                    <h5><?= h($asset->getDescription()) ?></h5>
                                </div>
                            </a>
                        </div>
                    <?php endforeach; ?>
                    <?php if (count($assets) === 0): ?>
                        <div class="carousel-item active">
                            <figure class="d-inline-flex flex-column justify-content-center align-items-center">
                                <?= $this->Html->image('/images/no-image.jpg', ['alt' => 'No image available']) ?>
                            </figure>
                        </div>
                    <?php endif; ?>
                </div>
                <?php if (count($assets) > 1): ?>
                    <ol class="carousel-indicators">
                        <?php foreach ($assets as $index => $asset): ?>
                            <li data-target="#artifact-media-carousel" data-slide-to="<?= $index ?>"
                                <?= $index === 0 ? ' class="active"' : '' ?>>
                            </li>
                        <?php endforeach; ?>
                    </ol>
                    <a class="carousel-control-prev" role="button" data-target="#artifact-media-carousel" data-slide="prev">
                        <span class="carousel-control-prev-icon fa fa-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" role="button" data-target="#artifact-media-carousel" data-slide="next">
                        <span class="carousel-control-next-icon fa fa-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                <?php endif; ?>
            </div>
            <?php if (count($artifact->artifact_assets) > 0): ?>
                <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
                        <span class="fa fa-bars" aria-hidden="true"></span>
                        <span class="sr-only">View list of visual assets</span>
                    </button>
                    <div class="dropdown-menu">
                        <?php foreach ($artifact->artifact_assets as $asset): ?>
                            <?= $this->Html->link(
                                h($asset->getDescription()),
                                ['controller' => 'Artifacts', 'action' => 'reader', $artifact->id, $asset->id],
                                ['class' => 'dropdown-item']
                            ) ?>
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>

        <?php // Summary ?>
        <div class="artifact-summary font-weight-light my-0 d-flex flex-wrap">
            <div>
                <p>Museum Collection(s)</p>
                <?php if (empty($artifact->collections)): ?>
                    -
                <?php else: ?>
                    <?php foreach ($artifact->collections as $index => $collection): ?>
                        <?= $this->Html->link(
                            $collection->collection,
                            ['controller' => 'Collections', 'action' => 'view',
                            $collection->id
                        ]) ?><!--
                        --><?= $index < count($artifact->collections) - 1 ? ',' : '' ?>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>

            <div>
                <p>Museum Number</p>
                <?php if (empty($artifact->museum_no)): ?>
                    -
                <?php else: ?>
                    <span><?= $artifact->museum_no ?></span>
                <?php endif; ?>
            </div>

            <div>
                <p>Period</p>
                <?php if (empty($artifact->period)): ?>
                    -
                <?php else: ?>
                    <?= $this->Html->link(
                        $artifact->period->period,
                        ['controller' => 'Periods', 'action' => 'view', $artifact->period->id]
                    ) ?>
                    <?= $artifact->is_period_uncertain == 1 ? '[uncertain]': '' ?>
                <?php endif; ?>
            </div>

            <div>
                <p>Provenience</p>
                <?php if (empty($artifact->provenience)): ?>
                    -
                <?php else: ?>
                    <?= $this->Html->link(
                        $artifact->provenience->provenience,
                        ['controller' => 'Proveniences', 'action' => 'view', $artifact->provenience->id]
                    ) ?>
                <?php endif; ?>
            </div>

            <?php if (is_null($artifact->seal_no)): ?>
                <div>
                    <p>Artifact Type</p>
                    <?php if (empty($artifact->artifact_type)): ?>
                        -
                    <?php else: ?>
                        <?= $this->Html->link(
                            ucfirst($artifact->artifact_type->artifact_type),
                            ['controller' => 'ArtifactTypes', 'action' => 'view', $artifact->artifact_type->id]
                        ) ?>
                    <?php endif; ?>
                </div>
            <?php endif; ?>

            <div>
                <p>Material(s)</p>
                <?php if (empty($artifact->materials)): ?>
                    -
                <?php else: ?>
                    <?php $this->append('localMaterials'); ?>
                        <?php foreach ($artifact->materials as $index => $material): ?>
                            <?php if (!empty($material->parent_id)): ?>
                                <?php $materialParent = array_search($material->parent_id, array_column($artifactAllMaterials, 'id')); ?>
                                <?= $this->Html->link(
                                    ucfirst($artifactAllMaterials[$materialParent]->material),
                                    ['controller' => 'Materials', 'action' => 'view', $material->parent_id]
                                ) ?>
                                &gt;
                            <?php endif; ?>
                            <?= $this->Html->link(
                                ucfirst($material->material),
                                ['controller' => 'Materials', 'action' => 'view', $material->id]
                            ) ?><!--
                            --><?= $index < count($artifact->materials) - 1 ? ',' : '' ?>
                        <?php endforeach; ?>
                    <?php $this->end(); ?>
                    <?= $this->fetch('localMaterials') ?>
                <?php endif; ?>
            </div>

            <?php if (is_null($artifact->seal_no)): ?>
                <div>
                    <p>Genre / Subgenre(s) </p>
                    <?php if (empty($artifact->genres)): ?>
                        -
                    <?php else: ?>
                        <?php $this->append('localGenres'); ?>
                            <?php foreach ($artifact->genres as $index => $genre): ?>
                                <?php if (!empty($genre->parent_id)): ?>
                                    <?php $genreParent = array_search($genre->parent_id, array_column($artifactAllGenres, 'id')); ?>
                                    <?= $this->Html->link(
                                        ucfirst($artifactAllGenres[$genreParent]->genre),
                                        ['controller' => 'Genres', 'action' => 'view', $genre->parent_id]
                                    ) ?>
                                    &gt;
                                <?php endif; ?>
                                <?= $this->Html->link(
                                    ucfirst($genre->genre),
                                    ['controller' => 'Genres', 'action' => 'view', $genre->id]
                                ) ?><!--
                                --><?php if ($genre->_joinData->is_genre_uncertain): ?>
                                    <?= __('[identification uncertain]') ?><!--
                                --><?php endif; ?><!--
                                --><?= $index < count($artifact->genres) - 1 ? ',' : '' ?>
                            <?php endforeach; ?>
                            <?php if (!empty(trim($genre->_joinData->comments))): ?>
                                (<?= trim($genre->_joinData->comments) ?>)
                            <?php endif; ?>
                        <?php $this->end(); ?>
                        <?= $this->fetch('localGenres') ?>
                    <?php endif; ?>
                </div>
            <?php endif; ?>

            <div>
                <p>Language(s)</p>
                <?php if (empty($artifact->languages)): ?>
                    -
                <?php else: ?>
                    <?php $this->append('localLanguages'); ?>
                        <?php foreach ($artifact->languages as $index => $language): ?>
                            <?= $this->Html->link(
                                ucfirst($language->language),
                                ['controller' => 'Languages', 'action' => 'view', $language->id]
                            )?><!--
                            --><?= $index < count($artifact->languages) - 1 ? ',' : '' ?>
                        <?php endforeach; ?>
                    <?php $this->end(); ?>
                    <?= $this->fetch('localLanguages') ?>
                <?php endif; ?>
            </div>

            <div>
                <p>Measurements</p>
                <?php if (!$artifact->has('height') && !$artifact->has('width') && !$artifact->has('thickness') && !$artifact->has('weight')): ?>
                    -
                <?php else: ?>
                    <?= $artifact->height ? h($artifact->height) : '' ?>
                    <?= $artifact->height && $artifact->width ? ' &times; ' : '' ?>
                    <?= $artifact->width ? h($artifact->width) : '' ?>
                    <?= ($artifact->height || $artifact->width) && $artifact->thickness ? ' &times; ' : '' ?>
                    <?= $artifact->thickness ? h($artifact->thickness) : '' ?>
                    <?= ($artifact->height || $artifact->width || $artifact->thickness) && $artifact->weight ? ', ' : ''?>
                    <?= $artifact->weight ? h($artifact->weight) . ' g' : '' ?>
                <?php endif; ?>
            </div>

            <div class="w-100">
                <p>
                    <span class="h6">
                        <?php if ($artifact->is_artifact_fake): ?>
                            <span class="p-2 badge badge-secondary">
                                <span class="fa fa-calendar-times-o" aria-hidden="true"></span>
                                <?= __('Modern forgery') ?>
                            </span>
                        <?php endif; ?>
                        <?php if ($artifact->has_fragments): ?>
                            <span class="p-2 badge badge-secondary">
                                <span class="fa fa-chain-broken" aria-hidden="true"></span>
                                <?= __('Has fragments') ?>
                            </span>
                        <?php endif; ?>
                        <?php if ($artifact->seal_no): ?>
                            <span class="p-2 badge badge-secondary"><?= __('Seal') ?></span>
                        <?php endif; ?>
                        <?php if ($artifact->composite_no): ?>
                            <span class="p-2 badge badge-secondary"><?= __('Composite') ?></span>
                        <?php endif; ?>
                        <?php if (!empty($artifact->seals)): ?>
                            <span class="p-2 badge badge-secondary"><?= __('Has seal impression(s)') ?></span>
                        <?php endif; ?>
                        <?php if (!empty($artifact->composites)): ?>
                            <span class="p-2 badge badge-secondary"><?= __('Witness') ?></span>
                        <?php endif; ?>
                        <?php if (!empty($artifact->inscription) && !$artifact->inscription->is_atf2conll_diff_resolved): ?>
                            <span class="p-2 badge badge-secondary"><?= __('Annotation unreviewed') ?></span>
                        <?php endif; ?>
                    </span>
                </p>
            </div>
        </div>
    </div>

    <div id="parent-div">
        <div class="d-flex justify-content-end mt-2">
            <button class="btn bg-white text-primary" onclick="expandAll()">Expand All</button>
            <button class="btn bg-white text-primary text-dark ml-2" onclick="collapseAll()" disabled>Collapse all</button>
        </div>

        <?php // Graph ?>
        <?php if ($hasgraph): 
            $chart->printScripts();
        ?>
            <h2 class="mt-2">Seal Chemistry</h2>
            <div class="pr-3 row justify-content-end">
                <div class="dropdown mr-1">
                    <button class="btn cdli-btn-blue dropdown-toggle" type="button"
                        id="dropdownMenuButton" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        <?= $elementCount.' elements' ?>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <?= $this->Html->link(
                            'All elements',
                            '?type='.$type.'&elementCount=All',
                            ['escape' => false,'class' => 'dropdown-item']
                        );?>
                        <?= $this->Html->link(
                            'Top 5 elements',
                            '?type='.$type.'&elementCount=Top 5',
                            ['escape'=> false,'class' => 'dropdown-item']
                        );?>
                        <?= $this->Html->link(
                            'Top 10 elements',
                            '?type='.$type.'&elementCount=Top 10',
                            ['escape'=> false,'class' => 'dropdown-item']
                        );?>
                    </div>
                </div>
                <div class="btn-group d-flex">
                        <?= $this->Html->link(
                            $this->Html->tag('i', '', array('class' => 'fa fa-table')),
                            '#chemInfo',
                            ['escape' => false,'onclick' => "expandChemicalInformation()",'class'=>'btn border border-secondary']
                        );?>
                    <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle" type="button"
                            id="dropdownMenuButton" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            <?php if($type=='column'): ?>
                                <i class="fa fa-bar-chart"></i>
                            <?php else: ?>
                                <i class="fa fa-line-chart"></i>
                            <?php endif; ?>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <?= $this->Html->link(
                                $this->Html->tag('i', '', array('class' => 'fa fa-bar-chart')).' Bar Chart',
                                '?type=column',
                                ['escape' => false,'class' => 'dropdown-item']
                            );?>
                            <?= $this->Html->link(
                                $this->Html->tag('i', '', array('class' => 'fa fa-line-chart')).' Line Chart',
                                "?type=line",
                                ['escape'=> false,'class' => 'dropdown-item']
                            );?>
                        </div>
                    </div>
                </div>
            </div>
            <div id="graphcontainer"></div>
            <script type="text/javascript">
               <?php echo $chart->render("chart"); ?>
            </script>
        <?php endif; ?>

        <?php // Accordion ?>
        <div class="artifact-detail accordion">
            <?php if (!empty($artifact->inscription)): ?>
                <?= $this->Accordion->partOpen('inscriptions', 'Text', 'h2') ?>
                    <div>
                        <div class="translate-indent">
                            <?= $artifact->inscription->getProcessedAtf() ?>
                        </div>
                        <br/>
                        <?= $this->Html->link(
                            'Consult previous versions and their differences',
                            ['controller' => 'Artifacts', $artifact->id, 'action' => 'history', 'atf']
                        ) ?>
                        <?php if (!empty($artifact->period)): ?>
                            <br/>
                            <?= $this->Html->link(__('Consult sign list of {0}', $artifact->period->period), [
                                'controller' => 'Resources',
                                'action' => 'tokenLists',
                                $artifact->period_id, 'signs',
                                '_ext' => 'txt'
                            ]) ?>
                            <br/>
                            <?= $this->Html->link(__('Consult word list of {0}', $artifact->period->period), [
                                'controller' => 'Resources',
                                'action' => 'tokenLists',
                                $artifact->period_id, 'words',
                                '_ext' => 'txt'
                            ]) ?>
                        <?php endif; ?>
                    </div>
                <?= $this->Accordion->partClose() ?>
            <?php endif; ?>

            <?php if (!empty($artifact->inscription->annotation)): ?>
                <?= $this->Accordion->partOpen('annotation', 'Textual annotations', 'h2') ?>
                    <?php if (!$artifact->inscription->is_atf2conll_diff_resolved): ?>
                        <div class="alert alert-warning">
                            This annotation was changed automatically to match a new
                            version of the transliteration, but has not yet been
                            reviewed since.
                            <?php if ($_admin): ?>
                                <?= $this->Html->link(
                                    __('Review this annotation.'),
                                    [
                                        'prefix' => 'Admin',
                                        'controller' => 'inscriptions',
                                        'action' => 'view',
                                        $artifact->inscription->id
                                    ]
                                ) ?>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>
                    <div>
                        <div class="conllu-parse" tabs="no">
                            <?= h(ltrim($conll_u)); ?>
                        </div>
                    </div>
                <?= $this->Accordion->partClose() ?>
            <?php endif; ?>

            <?= $this->Accordion->partOpen('collections', 'Collections', 'h2') ?>
                <ul>
                    <?php foreach ($artifact->collections as $collection): ?>
                        <li>
                            <?= $this->Html->link(
                                $collection->collection,
                                ['controller' => 'Collections', 'action' => 'view', $collection->id]
                            ) ?>
                        </li>
                    <?php endforeach; ?>
                </ul>
            <?= $this->Accordion->partClose() ?>

            <?= $this->Accordion->partOpen('physical_information', 'Physical Information', 'h2') ?>
                <ul>
                    <li>
                        <?= __('Artifact type') ?>:
                        <?php if (!empty($artifact->artifact_type->artifact_type)): ?>
                            <?= ucfirst($artifact->artifact_type->artifact_type) ?>
                        <?php endif; ?>
                        <?= $artifact->is_artifact_type_uncertain ? __('[identification uncertain]') : '' ?>
                    </li>
                    <li>
                        <?= __('Material') ?>:
                        <?= $this->fetch('localMaterials') ?>
                    </li>
                    <li>
                        <?= __('Measurements (mm)') ?>:
                        <?= $artifact->height ? h($artifact->height) . ' ' . __('high') : '' ?>
                        <?= $artifact->height && $artifact->width ? ' &times; ' : '' ?>
                        <?= $artifact->width ? h($artifact->width) . ' ' . __('wide') : '' ?>
                        <?= ($artifact->height || $artifact->width) && $artifact->thickness ? ' &times; ' : '' ?>
                        <?= $artifact->thickness ? h($artifact->thickness) . ' ' . __('thick') : '' ?>
                    </li>
                    <li>
                        <?= __('Weight') ?>:
                        <?= h($artifact->weight) ?>
                    </li>
                    <li>
                        <?= __('Artifact Preservation') ?>:
                        <?= h($artifact->artifact_preservation) ?>
                    </li>
                    <li>
                        <?= $artifact->has_fragments ? 'This artifact is composed of fragments.': '' ?>
                    </li>
                    <li>
                        <?= __('Condition Description') ?>:
                        <?= h($artifact->condition_description) ?>
                    </li>
                    <li>
                        <?= __('Join Information') ?>:
                        <?= h($artifact->join_information) ?>
                    </li>
                    <li>
                        <?= __('Seal no.') ?>:
                        <?= h($artifact->seal_no) ?>
                    </li>
                    <li>
                        <?= __('Seal information') ?>:
                        <?= $this->Text->autoParagraph(h($artifact->seal_information)) ?>
                    </li>
                    <li>
                        <?= $artifact->is_artifact_fake == 1 ? 'This artifact was made in modern times.': '' ?>
                    </li>
                    <li>
                        <?= __('Artifact comments') ?>:
                        <?= $this->Text->autoParagraph(h($artifact->artifact_comments)) ?>
                    </li>
                </ul>
            <?= $this->Accordion->partClose() ?>

            <?= $this->Accordion->partOpen('texts', 'Text Information', 'h2') ?>
                <ul>
                    <li>
                        <?= __('Genre(s)')  ?>:
                        <?= $this->fetch('localGenres') ?>
                    </li>
                    <li>
                        <?= $artifact->is_school_text == 1 ?  'This text is a school text.': '' ?>
                    </li>
                    <li>
                        <?=__('Language(s)') ?>:
                        <?= $this->fetch('localLanguages') ?>
                    </li>
                </ul>
            <?= $this->Accordion->partClose() ?>

            <?= $this->Accordion->partOpen('publications', 'Related publications', 'h2') ?>
                 <?php if ($ifRoleExists): ?>
                        <div class='mb-5'>
                            <?= $this->Html->link(
                                __('Link Publications'),
                                [
                                    'prefix' => 'Admin',
                                    'controller' => 'EntitiesPublications',
                                    'action' => 'add',
                                    'publication',
                                    'artifacts',
                                    $artifact->id
                                ],
                            ['class' => 'btn btn-action', 'style' => 'float: right;']
                            ) ?>
                        </div>
                <?php endif; ?>
                <hr/>
                <ul>
                    <?php foreach ($artifact->publications as $publication): ?>
                        <li>
                            <?php if ($ifRoleExists): ?>
                                <div class="float-right">
                                    <?= $this->Html->link(
                                        'Edit Publication',
                                        ['controller' => 'Publications', 'prefix' => 'Admin', 'action' => 'edit', $publication->id],
                                        ['class' => 'btn btn-primary cdli-btn-blue ml-3 p-2 w-30']
                                    ) ?>
                                </div>
                            <?php endif; ?>
                            <?= $this->element('publicationReference', ['publication' => $publication]) ?>
                            <hr/>
                         </li>
                    <?php endforeach; ?>
                </ul>

                <?php if (!empty($artifact->primary_publication_comments)) :?>
                    <h3 class="artifact-sub-heading"><?= __('Author comments') ?></h3>
                    <?= $this->Text->autoParagraph(h($artifact->primary_publication_comments)) ?>
                <?php endif;?>
            <?= $this->Accordion->partClose() ?>

            <?= $this->Accordion->partOpen('identifiers', 'Identifiers', 'h2') ?>
                <ul>
                    <li>
                        <?= __('Composite No.') ?>:
                        <?= h($artifact->composite_no) ?>
                    </li>
                    <li>
                        <?= __('Museum No.') ?>:
                        <?= h($artifact->museum_no) ?>
                    </li>
                    <li>
                        <?= __('Accession No.') ?>:
                        <?= h($artifact->accession_no) ?>
                    </li>
                </ul>
            <?= $this->Accordion->partClose() ?>

            <?= $this->Accordion->partOpen('provenience', 'Provenience', 'h2') ?>
                <ul>
                    <li>
                        <?= __('Provenience') ?>:
                        <?php if (!empty($artifact->provenience)): ?>
                            <?= $this->Html->link(
                                $artifact->provenience->provenience,
                                ['controller' => 'Proveniences', 'action' => 'view', $artifact->provenience->id]
                            ) ?>
                            <?= $artifact->is_provenience_uncertain ? '[uncertain]' : ''; ?>
                            <?php if ($artifact->provenience_comments): ?>
                                <?= __('Provenience Comments') ?>:
                                <?= $this->Text->autoParagraph(h($artifact->provenience_comments)) ?>
                            <?php endif; ?>
                        <?php endif; ?>
                    </li>
                    <li>
                        <?= __('Elevation') ?>:
                        <?= h($artifact->elevation) ?>
                    </li>
                    <li>
                        <?= __('Stratigraphic Level') ?>:
                        <?= h($artifact->stratigraphic_level) ?>
                    </li>
                    <li>
                        <?= __('Excavation No') ?>:
                        <?= h($artifact->excavation_no) ?>
                    </li>
                    <li>
                        <?= __('Findspot Square') ?>:
                        <?= h($artifact->findspot_square) ?>
                    </li>
                    <li>
                        <?= __('Findspot Comments') ?>:
                        <?= $this->Text->autoParagraph(h($artifact->findspot_comments)) ?>
                    </li>
                </ul>
            <?= $this->Accordion->partClose() ?>

            <?= $this->Accordion->partOpen('chronology', 'Chronology', 'h2') ?>
                <ul>
                    <li>
                        <?= __('Period') ?>:
                        <?php if (!empty($artifact->period)): ?>
                            <?= $this->Html->link(
                                $artifact->period->period,
                                ['controller' => 'Periods', 'action' => 'view', $artifact->period->id]
                            ) ?>
                            <?= $artifact->is_period_uncertain == 1 ? '[uncertain]': '' ?>
                        <?php endif; ?>
                    </li>
                    <li>
                        <?php if (!empty($artifact->period_comments)): ?>
                            <?= __('Period Comments') ?>:
                            <?= $this->Text->autoParagraph(h($artifact->period_comments)) ?>
                        <?php endif; ?>
                    </li>
                    <li>
                        <?= __('Dates Referenced') ?>:
                        <?= h($artifact->dates_referenced) ?>
                    </li>
                    <li>
                        <?= __('Alternative Years') ?>:
                        <?= h($artifact->alternative_years) ?>
                    </li>
                    <li>
                        <?= __('Date Comments') ?>:
                        <?= h($artifact->date_comments) ?>
                    </li>
                    <li>
                        <?= __('Accounting Period') ?>:
                        <?= $this->Number->format($artifact->accounting_period) ?>
                    </li>
                </ul>
            <?= $this->Accordion->partClose() ?>

            <?= $this->Accordion->partOpen('credits', 'Revisions and credits', 'h2') ?>
                <div class="table-response">
                    <table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
                        <thead>
                            <tr>
                                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                                <th scope="col"><?= __('Creator') ?></th>
                                <th scope="col"><?= __('Authors') ?></th>
                                <th scope="col"><?= __('Project') ?></th>
                                <th scope="col"><?= __('Reviewer') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('status') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($updateEvents as $updateEvent): ?>
                                <tr>
                                    <td><?= $this->Html->link(
                                        h($updateEvent->created),
                                        ['controller' => 'UpdateEvents', 'action' => 'view', $updateEvent->id]
                                    ) ?></td>
                                    <td><?= $this->Html->link(
                                        $updateEvent->creator->author,
                                        ['controller' => 'Authors', 'action' => 'view', $updateEvent->creator->id]
                                    ) ?></td>
                                    <td>
                                        <?php if ($updateEvent->has('authors') && !empty($updateEvent->authors)): ?>
                                            <?= implode('; ', array_map(function ($author) {
                                                return $this->Html->link(
                                                   $author->author,
                                                   ['controller' => 'Authors', 'action' => 'view', $author->id]
                                                );
                                            }, $updateEvent->authors)) ?>
                                        <?php else: ?>
                                            <?= $this->Html->link(
                                                $updateEvent->creator->author,
                                                ['controller' => 'Authors', 'action' => 'view', $updateEvent->creator->id]
                                            ) ?>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <?php if ($updateEvent->has('external_resource')): ?>
                                            <?= $this->Html->link(
                                                $updateEvent->external_resource->external_resource,
                                                ['prefix' => false, 'controller' => 'ExternalResources', 'action' => 'view', $updateEvent->external_resource->id]
                                            ) ?>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <?php if ($updateEvent->has('reviewer')): ?>
                                            <?= $this->Html->link(
                                                $updateEvent->reviewer->author,
                                                ['controller' => 'Authors', 'action' => 'view', $updateEvent->reviewer->id]
                                            ) ?>
                                        <?php endif; ?>
                                    </td>
                                    <td><?= h($updateEvent->status) ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            <?= $this->Accordion->partClose() ?>

            <?= $this->Accordion->partOpen('external_resources', 'External Resources', 'h2') ?>
                <p>Consult this artifact as presented on the website of collections and projects:</p>
                <ul>
                    <?php if (empty($artifact->external_resources)): ?>
                        There is no external resource for this artifact yet.
                    <?php endif; ?>
                    <?php foreach ($artifact->external_resources as $external_resource): ?>
                        <li>
                            <?= $this->Html->link(
                                $external_resource->external_resource,
                                $external_resource->base_url . $external_resource->_joinData->external_resource_key
                            ) ?>
                            <?= h($artifact->designation) ?> (<?= h($CDLI_NO) ?>)
                        </li>
                    <?php endforeach; ?>
                </ul>
            <?= $this->Accordion->partClose() ?>

            <?php if (!empty($artifact->composite_no)): ?>
                <?= $this->Accordion->partOpen('witnesses', 'Witnesses', 'h2') ?>
                    <p>
                        This artifact is a composite text.
                        <?= $this->Html->link(
                            __('View the score of {0}.', h($artifact->designation)),
                            ['action' => 'compositesScore', $artifact->composite_no])
                        ?>
                        See below for a list of its witness(es):
                    </p>

                    <table class="table-bootstrap">
                        <thead>
                            <tr>
                                <th scope="col"><?= h('Witness') ?></th>
                                <th scope="col"><?= h('Period') ?></th>
                                <th scope="col"><?= h('Provenience') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($artifact->witnesses as $witness): ?>
                                <tr>
                                    <td>
                                        <?= $this->Html->link(
                                            h($witness->designation) . ' (' . $witness->getCDLINumber() . ')',
                                            [$witness->id]
                                        ) ?>
                                    </td>
                                    <td>
                                        <?php if ($witness->has('period')): ?>
                                            <?= $this->Html->link(h($witness->period->period), [
                                                'controller' => 'Periods',
                                                'action' => 'view',
                                                $witness->period->id
                                            ]) ?>
                                        <?php else: ?>
                                            <?= '-' ?>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <?php if ($witness->has('provenience')): ?>
                                            <?= $this->Html->link(h($witness->provenience->provenience), [
                                                'controller' => 'Proveniences',
                                                'action' => 'view',
                                                $witness->provenience->id
                                            ]) ?>
                                        <?php else: ?>
                                            <?= '-' ?>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?= $this->Accordion->partClose() ?>
            <?php elseif (count($artifact->composites) > 0): ?>
                <?= $this->Accordion->partOpen('composites', 'Composites', 'h2') ?>
                    <p>This artifact is a witness to the following composite(s):</p>
                    <table class="table-bootstrap">
                        <thead>
                            <tr>
                                <th scope="col"><?= h('Composite') ?></th>
                                <th scope="col"><?= h('Score') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($artifact->composites as $composite): ?>
                                <tr>
                                    <td>
                                        <?= $this->Html->link(
                                            h($composite->designation) . ' (' . $composite->getCDLINumber() . ')',
                                            [$composite->id]
                                        ) ?>
                                    </td>
                                    <td>
                                        <?= $this->Html->link(
                                            'View the score',
                                            ['action' => 'compositesScore', $composite->composite_no]
                                        ) ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?= $this->Accordion->partClose() ?>
            <?php elseif (!empty($artifact->seal_no)): ?>
                <?= $this->Accordion->partOpen('seal_impressions', 'Seal impressions', 'h2') ?>
                    <p>
                        This artifact is a <?= strpos($artifact->designation, 'composite') ? 'composite' : 'physical' ?> seal.
                        See below of a list of its impression(s):
                    </p>
                    <table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
                        <thead>
                            <tr>
                                <th scope="col"><?= h('Impression') ?></th>
                                <th scope="col"><?= h('Provenience') ?></th>
                                <th scope="col"><?= h('Period') ?></th>
                                <th scope="col"><?= h('Date') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($artifact->impressions as $impression): ?>
                                <tr>
                                    <td>
                                        <?= $this->Html->link(
                                            h($impression->designation) . ' (' . $impression->getCDLINumber() . ')',
                                            [$impression->id]
                                        ) ?>
                                    </td>
                                    <td>
                                        <?php if ($impression->has('provenience')): ?>
                                            <?= $this->Html->link(h($impression->provenience->provenience), [
                                                'controller' => 'Proveniences',
                                                'action' => 'view',
                                                $impression->provenience->id
                                            ]) ?>
                                        <?php else: ?>
                                            <?= '-' ?>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <?php if ($impression->has('period')): ?>
                                            <?= $this->Html->link(h($impression->period->period), [
                                                'controller' => 'Periods',
                                                'action' => 'view',
                                                $impression->period->id
                                            ]) ?>
                                        <?php else: ?>
                                            <?= '-' ?>
                                        <?php endif; ?>
                                    </td>
                                    <td><?= $impression->dates_referenced ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?= $this->Accordion->partClose() ?>
            <?php elseif (count($artifact->seals) > 0): ?>
                <?= $this->Accordion->partOpen('seals', 'Seals', 'h2') ?>
                    <p>This artifact is impressed by the following seal(s):</p>
                    <table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
                        <thead>
                            <tr>
                                <th scope="col"><?= h('Seal') ?></th>
                                <th scope="col"><?= h('Provenience') ?></th>
                                <th scope="col"><?= h('Period') ?></th>
                                <th scope="col"><?= h('Date') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($artifact->seals as $seal): ?>
                                <tr>
                                    <td>
                                        <?= $this->Html->link(
                                            h($seal->designation) . ' (' . $seal->getCDLINumber() . ')',
                                            [$seal->id]
                                        ) ?>
                                    </td>
                                    <td>
                                        <?php if ($seal->has('provenience')): ?>
                                            <?= $this->Html->link(h($seal->provenience->provenience), [
                                                'controller' => 'Proveniences',
                                                'action' => 'view',
                                                $seal->provenience->id
                                            ]) ?>
                                        <?php else: ?>
                                            <?= '-' ?>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <?php if ($seal->has('period')): ?>
                                            <?= $this->Html->link(h($seal->period->period), [
                                                'controller' => 'Periods',
                                                'action' => 'view',
                                                $seal->period->id
                                            ]) ?>
                                        <?php else: ?>
                                            <?= '-' ?>
                                        <?php endif; ?>
                                    </td>
                                    <td><?= $seal->dates_referenced ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?= $this->Accordion->partClose() ?>
            <?php endif; ?>

            <?php if (!empty($artifact->artifacts_date_referenced)): ?>
                <?= $this->Accordion->partOpen('related_artifacts_date_referenced', 'Related Artifacts Date Referenced', 'h2') ?>
                    <div class="related">
                        <table cellpadding="0" cellspacing="0">
                            <thead>
                                <tr>
                                    <th scope="col"><?= __('Id') ?></th>
                                    <th scope="col"><?= __('Artifact Id') ?></th>
                                    <th scope="col"><?= __('Ruler Id') ?></th>
                                    <th scope="col"><?= __('Month Id') ?></th>
                                    <th scope="col"><?= __('Month No') ?></th>
                                    <th scope="col"><?= __('Year Id') ?></th>
                                    <th scope="col"><?= __('Day No') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($artifact->artifacts_date_referenced as $artifactsDateReferenced): ?>
                                    <tr>
                                        <td>
                                            <?php if (empty($artifactsDateReferenced->id)): ?>
                                                -
                                            <?php else: ?>
                                                <?= $this->Html->link(
                                                    $artifactsDateReferenced->id,
                                                    ['controller' => 'ArtifactsDateReferenced', 'action' => 'view', $artifactsDateReferenced->id]
                                                ) ?>
                                            <?php endif; ?>
                                        </td>
                                        <td>
                                            <?php if (empty($artifactsDateReferenced->artifact_id)): ?>
                                                -
                                            <?php else: ?>
                                                <?= h($artifactsDateReferenced->artifact_id) ?>
                                            <?php endif; ?>
                                        </td>
                                        <td>
                                            <?php if (empty($artifactsDateReferenced->ruler_id)): ?>
                                                -
                                            <?php else: ?>
                                                <?= h($artifactsDateReferenced->ruler_id) ?>
                                            <?php endif; ?>
                                        </td>
                                        <td>
                                            <?php if (empty($artifactsDateReferenced->month_id)): ?>
                                                -
                                            <?php else: ?>
                                                <?= h($artifactsDateReferenced->month_id) ?>
                                            <?php endif; ?>
                                        </td>
                                        <td>
                                            <?php if (empty($artifactsDateReferenced->month_no)): ?>
                                                -
                                            <?php else: ?>
                                                <?= h($artifactsDateReferenced->month_no) ?>
                                            <?php endif; ?>
                                        </td>
                                        <td>
                                            <?php if (empty($artifactsDateReferenced->year_id)): ?>
                                                -
                                            <?php else: ?>
                                                <?= h($artifactsDateReferenced->year_id) ?>
                                            <?php endif; ?>
                                        </td>
                                        <td>
                                            <?php if (empty($artifactsDateReferenced->day_no)): ?>
                                                -
                                            <?php else: ?>
                                                <?= h($artifactsDateReferenced->day_no) ?>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                <?= $this->Accordion->partClose() ?>
            <?php endif; ?>

            <?php if ($ifRoleExists && !empty($artifact->artifacts_shadow)): ?>
                <?= $this->Accordion->partOpen('related_artifacts_shadow', 'Artifact Shadow (Private)', 'h2') ?>
                    <ul>
                        <li>CDLI Comments: <?= h($artifact->artifacts_shadow[0]->cdli_comments) ?> /li>
                        <li>Collection Location: <?= h($artifact->artifacts_shadow[0]->collection_location) ?></li>
                        <li>Collection Comments: <?= h($artifact->artifacts_shadow[0]->collection_comments) ?></li>
                        <li>Acquisition History: <?= h($artifact->artifacts_shadow[0]->acquisition_history) ?></li>
                        <li>Artifact is: <?= $artifact->is_public ? 'Public' : 'Not Public' ?></li>
                        <li>Inscription is: <?php $artifact->is_atf_public ? 'Public' : 'Not Public' ?></li>
                        <li>Images are: <?php $artifact->are_images_public ? 'Public' : 'Not Public' ?></li>
                    </ul>
                <?= $this->Accordion->partClose() ?>
            <?php endif; ?>

            <?php if (!empty($artifact->retired_artifacts)): ?>
                <?= $this->Accordion->partOpen('related_retired_artifacts', 'Related Retired Artifacts', 'h2') ?>
                    <div class="related">
                        <table cellpadding="0" cellspacing="0">
                            <thead>
                                <tr>
                                    <th scope="col"><?= __('Id') ?></th>
                                    <th scope="col"><?= __('Artifact Id') ?></th>
                                    <th scope="col"><?= __('New Artifact Id') ?></th>
                                    <th scope="col"><?= __('Artifact Remarks') ?></th>
                                    <th scope="col"><?= __('Is Public') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($artifact->retired_artifacts as $retiredArtifacts): ?>
                                    <tr>
                                        <td>
                                            <?php if (empty($retiredArtifacts->id)): ?>
                                                -
                                            <?php else: ?>
                                                <?= $this->Html->link(
                                                    $retiredArtifacts->id,
                                                    ['controller' => 'RetiredArtifacts', 'action' => 'view', $retiredArtifacts->id]
                                                ) ?>
                                            <?php endif; ?>
                                        </td>
                                        <td>
                                            <?php if (empty($retiredArtifacts->artifact_id)): ?>
                                                -
                                            <?php endif; ?><?= h($retiredArtifacts->artifact_id) ?>
                                        </td>
                                        <td>
                                            <?php if (empty($retiredArtifacts->new_artifact_id)): ?>
                                                -
                                            <?php endif; ?><?= h($retiredArtifacts->new_artifact_id) ?>
                                        </td>
                                        <td>
                                            <?php if (empty($retiredArtifacts->artifact_remarks)): ?>
                                                -
                                            <?php endif; ?><?= h($retiredArtifacts->artifact_remarks) ?>
                                        </td>
                                        <td>
                                            <?php if (empty($retiredArtifacts->is_public)): ?>
                                                -
                                            <?php endif; ?><?= h($retiredArtifacts->is_public) ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                <?= $this->Accordion->partClose() ?>
            <?php endif; ?>

            <?php if ($hasgraph): ?>
                <?= $this->Accordion->partOpen('chemInfo', 'Chemical Information', 'h2') ?>
                    <div id="chemInfo">
                        <div class="table-responsive table-hover">
                            <table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
                                <thead align="left">
                                    <tr>
                                        <th scope="col">Sample reading</th>
                                        <th scope="col">Date</th>
                                        <th scope="col">Time</th>
                                        <th scope="col">Duration</th>
                                        <?php foreach ($headers as $col): ?>
                                            <th scope='col'><?= $col ?></th>
                                            <th scope='col'>+/-</th>
                                        <?php endforeach; ?>
                                        <?php if ($_admin): ?>
                                            <th scope="col">Action</th>
                                        <?php endif; ?>

                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($tableData as $index=>$row): ?>
                                    <?php $finalJson = json_decode(json_decode(json_encode($row->chemistry, JSON_HEX_TAG)), true); ?>
                                        <tr>
                                            <td align="left"><?= h($row->reading)." ".h($row->area) ?></td>
                                            <td align="left"><?= h($row->date) ?></td>
                                            <td align="left"><?= h($row->time) ?></td>
                                            <td align="left"><?= h($row->duration) ?></td>
                                            <?php foreach ($headers as $key => $col): ?>
                                                <?php if (isset($finalJson[$col])): ?>
                                                    <td align="left"><?= h($finalJson[$col]['value']) ?></td>
                                                    <td align="left"><?= h($finalJson[$col]['+/-']) ?></td>
                                                <?php else: ?>
                                                    <td align="left"></td>
                                                    <td align="left"></td>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                            <?php if($_admin):?>
                                                <td align="left">
                                                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#sealChemistryDeleteModal">
                                                        Delete
                                                    </button>
                                                </td>
                                            <?php endif;?>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                            <div class="text-center">
                                <?php echo $this->element('Paginator'); ?>
                            </div>
                        </div>
                        <div class="modal fade" id="sealChemistryDeleteModal" tabindex="-1" role="dialog" aria-labelledby="sealChemistryDeleteModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Are you sure?</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                                    <?= $this->Html->link(
                                        'Yes',
                                        ['controller' => 'Artifacts', 'action' => 'deleteChemistry', $row->id, $row->artifact_id],
                                        ['class' => 'btn btn-danger']
                                    ) ?>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?= $this->Accordion->partClose() ?>
            <?php endif; ?>

            <?= $this->Accordion->partOpen('Notes', 'Notes', 'h2') ?>
                <div>
                    <?= __('General Notes') ?>:

                    <?php if (empty($artifact->general_comments)): ?>
                        -
                    <?php else: ?>
                        <?= $this->Text->autoParagraph(h($artifact->general_comments)) ?>
                    <?php endif; ?>
                </div>

                <div>
                    <?= __('CDLI Notes') ?>:
                    <?php if (empty($artifact->cdli_comments)): ?>
                        -
                    <?php else: ?>
                        <?= $this->Text->autoParagraph(h($artifact->cdli_comments)) ?>
                    <?php endif; ?>
                </div>
            <?= $this->Accordion->partClose() ?>
        </div>

        <?= $this->element('citeBottom', ['data' => $artifact])?>
    </div>
</main>

 <?php $this->append('script'); ?>
    <?= $this->Html->script('focus-visible.js') ?>
    <?= $this->Html->script('collapseall.js') ?>
    <?= $this->element('smoothscroll') ?>

    <script type="text/javascript" src="/assets/js/conllu.js/lib/ext/jquery.min.js"></script>
    <script type="text/javascript" src="/assets/js/conllu.js/lib/ext/jquery.svg.min.js"></script>
    <script type="text/javascript" src="/assets/js/conllu.js/lib/ext/jquery.svgdom.min.js"></script>
    <script type="text/javascript" src="/assets/js/conllu.js/lib/ext/jquery-ui.min.js"></script>
    <script type="text/javascript" src="/assets/js/conllu.js/lib/ext/waypoints.min.js"></script>

    <script type="text/javascript" src="/assets/js/conllu.js/lib/brat/configuration.js"></script>
    <script type="text/javascript" src="/assets/js/conllu.js/lib/brat/util.js"></script>
    <script type="text/javascript" src="/assets/js/conllu.js/lib/brat/annotation_log.js"></script>

    <script type="text/javascript" src="/assets/js/conllu.js/lib/ext/webfont.js"></script>
    <script type="text/javascript" src="/assets/js/conllu.js/lib/brat/dispatcher.js"></script>
    <script type="text/javascript" src="/assets/js/conllu.js/lib/brat/url_monitor.js"></script>
    <script type="text/javascript" src="/assets/js/conllu.js/lib/brat/visualizer.js"></script>

    <script type="text/javascript" src="/assets/js/conllu.js/annodoc.js"></script>
    <script type="text/javascript" src="/assets/js/conllu.js/lib/local/config.js"></script>
    <script type="text/javascript" src="/assets/js/conllu.js/conllu.js"></script>
    <script type="text/javascript">
        var root = '';
        head.js();

        /* not used here */
        var documentCollections = {};
        var webFontURLs = [
            root + '/assets/css/conllu.js/fonts/PT_Sans-Caption-Web-Regular.ttf',
            root + '/assets/css/conllu.js/fonts/Liberation_Sans-Regular.ttf'
        ];

        $(function() {
            $("#annotation-toggle").change(function() {
                var ischecked = $(this).is(":checked");
                if (ischecked) {
                    Annodoc.activate(Config.bratCollData, documentCollections);
                }
            });
        });
    </script>
<?php $this->end(); ?>

<?= $this->element('citeButton', ['data' => $artifact])?>
