<?php
/**
 * @var \App\View\AppView $this
 */

$new = iterator_to_array($inscriptions);
$old = array_slice($new, 1);

if ($this->Paginator->hasNext()) {
    $new = array_slice($new, 0, -1);
} else {
    $old[] = (object) ['annotation' => ''];
}
?>

<div class="row justify-content-md-center">
    <div class="col-lg-12 text-left">
        <?= $this->Html->link(
            '<span class="fa fa-chevron-left"></span> ' . __('Back to artifact'),
            ['action' => 'view', $artifact->id],
            ['escapeTitle' => false]
        ) ?>
    </div>

    <?php foreach (array_map(null, $old, $new) as $diff): ?>
        <?php $inscription = $diff[1]; ?>
        <div class="col-lg-12 boxed">
            <h2><?= $this->element('updateEventHeader', ['update_event' => $inscription->update_event]) ?></h2>

            <?php if (!empty($inscription->update_event->event_comments)): ?>
                <p><?= h($inscription->update_event->event_comments) ?></p>
            <?php endif; ?>

            <pre><?= $this->Diff->diff($diff[0]->annotation, $diff[1]->annotation, "\n") ?></pre>
        </div>
    <?php endforeach; ?>

    <div class="col-lg-12 boxed">
        <?php echo $this->element('Paginator'); ?>
    </div>
</div>
