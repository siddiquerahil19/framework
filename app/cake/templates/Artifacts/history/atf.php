<?php
/**
 * @var \App\View\AppView $this
 */

$new = iterator_to_array($inscriptions);
$old = array_slice($new, 1);

if ($this->Paginator->hasNext()) {
    $new = array_slice($new, 0, -1);
} else {
    $old[] = (object) ['atf' => ''];
}
?>

<div class="row justify-content-md-center">
    <div class="col-lg-12 text-left">
        <h1>Transliteration history</h1>
        <h2><?= $this->Html->link(
            $artifact['designation'] . ' (' . $artifact->getCdliNumber() . ')',
            ['action' => 'view', $artifact->id]
        ) ?></h2>
    </div>

    <?php foreach (array_map(null, $old, $new) as $diff): ?>
        <?php $inscription = $diff[1]; ?>
        <div class="col-lg-12 boxed">
            <?= $this->element('updateEventHeader', ['update_event' => $inscription->update_event]) ?>
            <?php if (!empty($inscription->update_event->event_comments)): ?>
                <p><?= h($inscription->update_event->event_comments) ?></p>
            <?php endif; ?>

            <pre><?= $this->Diff->diffAtf($diff[0]->atf, $diff[1]->atf) ?></pre>
        </div>
    <?php endforeach; ?>

    <div class="col-lg-12 boxed">
        <?php echo $this->element('Paginator'); ?>
    </div>
</div>
