<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Artifact $artifact
 * @var \App\Model\Entity\ArtifactAsset $asset
 */
?>

<?php // Bypass the default container ?>
    <div class="col-12 boxed mx-0">
        <h2><?= $this->Html->link(
            $artifact->designation . ' (' . $artifact->getCdliNumber() . ')',
            ['action' => 'view', $artifact->id],
            ['escapeTitle' => false]
        ) ?></h2>
        <p class="artifact-desc"><?= $artifact->getDescription() ?></p>
    </div>
</div>

<div class="d-md-flex">
    <div class="text-center boxed m-0" style="flex-grow: 1;">
        <?= $this->cell('ArtifactAsset', ['asset' => $asset->id]) ?>
    </div>

    <?php if ($artifact->has('inscription')): ?>
        <div class="text-left boxed m-0 collapse show" id="transliteration">
            <button class="btn cdli-btn-blue float-right" type="button" data-toggle="collapse" data-target="#transliteration" aria-expanded="false" aria-controls="transliteration">
                <?= __('Collapse') ?>
            </button>
            <p class="my-2">Transliteration</p>
            <div class="border p-3" style="height: 100vh; overflow-y: auto;">
                <?= $artifact->inscription->getProcessedAtf() ?>
            </div>
        </div>

        <button class="btn cdli-btn-blue align-self-start" style="margin: 40px;" type="button" data-toggle="collapse" data-target="#transliteration" aria-expanded="false" aria-controls="transliteration">Open</button>
    <?php endif; ?>
</div>

<style media="screen">
    .collapse.show + button,
    .collapsing + button {
        display: none;
    }
    .collapsing {
        transition: none !important;
    }
    .a9s-annotation .a9s-inner {
        stroke-width: 4;
        stroke: red;
    }
</style>

<div class="container-fluid text-center contentWrapper">
