<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Artifact $artifact
 */

$additional = [
    "Abbreviations" => "Abbreviations",
    "Agade mails" => "AgadeMails",
    "Archives" => "Archives",
    "Artifacts" => "Artifacts",
    "Artifact types" => "ArtifactTypes",
    "Authors" => "Authors",
    "CDLI tablet" => "CdliTablet",
    "Dynasties" => "Dynasties",
    "External resources" => "ExternalResources",
    "Journals" => "Journals",
    "Material aspects" => "MaterialAspects",
    "Material colors" => "MaterialColors",
    "Materials" => "Materials",
    "Publications" => "Publications",
    "Regions" => "Regions",
    "Rulers" => "Rulers",
    "Sign readings" => "SignReadings"
];
?>
<main class="container">
	<h1 class="display-3 header-text text-left">Browse</h1>
	<p class="text-left page-summary-text mt-4"> To browse a subset of artifacts that match your interest, please choose a featured category to explore.</p>
	<div>
		<h2 class="text-left display-4 section-title">Featured</h2>
		<div class="row mt-5">
      <div class="bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-6">
				<div class="card-body text-left border">
					<p class="card-title">
						<?= $this->Html->link(
							__('Heatmap'),
							['controller' => 'Heatmap', 'action' => 'index']
						) ?>
					</p>
					<p class="card-text">Use this Heatmap to visualise the location and numbers of tablets found in different sites. The map can be tweaked using a search and a filter feature.</p>
				</div>
			</div>

			<div class="bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-6">
				<div class="card-body text-left border">
					<p class="card-title">
						<?= $this->Html->link(
							__('Proveniences'),
							['controller' => 'Proveniences', 'action' => 'index']
						) ?>
					</p>
					<p class="card-text">Cuneiform tablets have been found across the wider Middle East, the vast majority are from Iraq, but substantial amounts of material come from neighboring countries.</p>
				</div>
			</div>

			<div class="bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-6">
				<div class="card-body text-left border">
					<p class="card-title">
						<?= $this->Html->link(
							__('Periods'),
							['controller' => 'Periods', 'action' => 'index']
						) ?>
					</p>
					<p class="card-text">Written for more than 3,000 years cuneiform writing covers a wider range of time periods.</p>
				</div>
			</div>

			<div class="bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-6">
				<div class="card-body text-left border">
					<p class="card-title">
						<?= $this->Html->link(
							__('Genres'),
							['controller' => 'Genres', 'action' => 'index']
						) ?>
					</p>
					<p class="card-text">Used to write everything from accounting documents, to epic poetry and scientific texts, the preserved cuneiform texts over many genres.</p>
				</div>
			</div>

			<div class="bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-6">
				<div class="card-body text-left border">
					<p class="card-title">
						<?= $this->Html->link(
							__('Languages'),
							['controller' => 'Languages', 'action' => 'index']
						) ?>
					</p>
					<p class="card-text">Cuneiform was used to write over 10 different languages and many other dialects.</p>
				</div>
			</div>

			<div class="bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-6">
				<div class="card-body text-left border">
					<p class="card-title">
						<?= $this->Html->link(
							__('Collections'),
							['controller' => 'Collections', 'action' => 'index']
						) ?>
					</p>
					<p class="card-text">Cuneiform tablets can be found in modern collections around the world.</p>
				</div>
			</div>

		</div>

		<h2 class="text-left display-4 section-title">Dispersed categories</h2>
		<?php $this->Grid->Alphabetical($additional)?>
	</div>
	<?php echo $this->element('smoothscroll'); ?>

	<?php $data = ['title' => 'Browse'] + $this->Citation->getDefaultData(); ?>
	<?= $this->element('citeBottom', ['data' => $data]) ?>
	<?= $this->element('citeButton', ['data' => $data]) ?>
</main>
