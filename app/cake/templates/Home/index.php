<?php
$this->assign('description', 'By making the form and content of cuneiform texts available online, the CDLI is opening pathways to the rich historical tradition of the ancient Middle East. In close collaboration with researchers, museums and an engaged public, the project seeks to unharness the extraordinary content of these earliest witnesses to our shared world heritage.');
$this->assign('image', '/images/cdlilogo.svg');

    $searchCategory = [
        // $category =. $placeholder
        'keyword' => 'Free Search',
        'publication' => 'Publications',
        'collection' => 'Collections',
        'provenience' => 'Proveniences',
        'period' => 'Periods',
        'inscription' => 'Inscriptions',
        'id' => 'Identification numbers',
    ];
echo $this->Html->script('searchResult.js', ['defer' => true]);
?>

<main class="container">
    <div>
        <h1 class="display-3 text-left header-txt">The CDLI Collection</h1>
        <p class="text-justify page-summary-text mt-4">By making the form and content of cuneiform texts available online, the CDLI is opening pathways to the rich historical tradition of the ancient Middle East. In close collaboration with researchers, museums and an engaged public, the project seeks to unharness the extraordinary content of these earliest witnesses to our shared world heritage.</p>
    </div>

    <?= $this->Form->create(null, [
        'type'=>'GET',
        'url' => [
            'controller' => 'Search',
            'action' => 'index'
        ]
    ]) ?>

        <div class="mt-5">

            <!-- search layout when no script is enabled -->
            <noscript>
                <style>
                    .default-search-block,
                    .default-add-search-btn{
                        display:none;
                    }
                </style>

                <div id="dynamic_field">
                    <div>
                        <div class="rectangle-2 container p-3">
                            <div class="search-page-grid" id="1">
                                <?= $this->Form->label('input1', null, ['hidden']); ?>
                                <?= $this->Form->text('', [
                                        'name' => 'keyword',
                                        'id' => 'input1',
                                        'placeholder' => 'Search for publications, provenience, collection no.',
                                        'aria-label' => 'Search',
                                        'required'
                                    ]
                                ); ?>

                                <?= $this->Form->label('1', null, ['hidden']); ?>
                                <?=$this->Form->select(
                                    '',
                                    [
                                        // $category =. $placeholder
                                        'keyword' => 'Free Search',
                                        'publication' => 'Publications',
                                        'collection' => 'Collections',
                                        'provenience' => 'Proveniences',
                                        'period' => 'Periods',
                                        'inscription' => 'Inscriptions',
                                        'id' => 'Identification numbers',
                                    ],
                                    [
                                        'default' => 'keyword',
                                        'class' => 'form-group mb-0',
                                        'id' => '1'
                                    ]
                                );?>
                            </div>
                        </div>
                    </div>

                    <?php for ($i = 1; $i <= 2; $i++):?>
                        <div id="row<?=$i?>">
                            <div class="container rectangle-23 p-3">
                                <div class="align-items-baseline">
                                    <?=$this->Form->select(
                                        'operator'.$i, [
                                            'AND' => 'AND',
                                            'OR' => 'OR'
                                        ], [
                                            'value' => 'AND',
                                            'class' => 'mb-3 mt-0 cdli-btn-light btn-and mr-3 float-left',
                                            'id' => $i
                                        ]
                                    );?>

                                    <div class="search-page-grid w-100-sm">
                                        <?= $this->Form->label('input'.($i + 1), null, ['hidden']); ?>
                                        <?= $this->Form->text('', [
                                                'name' => 'keyword'.$i,
                                                'id' => 'input'.($i + 1),
                                                'placeholder' => 'Search for publications, provenience, collection no.',
                                                'aria-label' => 'Search'
                                            ]
                                        ); ?>

                                        <?= $this->Form->label(($i + 1), null, ['hidden']); ?>
                                        <?=$this->Form->select(
                                            '', [
                                                // $category =. $placeholder
                                                'keyword'.$i => 'Free Search',
                                                'publication'.$i => 'Publications',
                                                'collection'.$i => 'Collections',
                                                'provenience'.$i => 'Proveniences',
                                                'period'.$i => 'Periods',
                                                'inscription'.$i => 'Inscriptions',
                                                'id'.$i => 'Identification numbers',
                                            ], [
                                                'default' => 'keyword'.$i,
                                                'class' => 'form-group mb-0',
                                                'id' => ($i + 1)
                                            ]
                                        );?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endfor; ?>
                </div>
            </noscript>

            <div id="dynamic_field">
                <div>
                    <div class="rectangle-2 container p-3 default-search-block">
                        <div class="search-page-grid w-100-sm" id="1">
                            <?= $this->Form->label('input1', null, ['hidden']); ?>
                            <?= $this->Form->text('', [
                                    'name' => 'keyword',
                                    'id' => 'input1',
                                    'placeholder' => 'Search for publications, provenience, collection no.',
                                    'aria-label' => 'Search',
                                    'required'
                                ]
                            ); ?>

                            <?= $this->Form->label('1', null, ['hidden']); ?>
                            <?=$this->Form->select(
                                '', [
                                    // $category =. $placeholder
                                    'keyword' => 'Free Search',
                                    'publication' => 'Publications',
                                    'collection' => 'Collections',
                                    'provenience' => 'Proveniences',
                                    'period' => 'Periods',
                                    'inscription' => 'Inscriptions',
                                    'id' => 'Identification numbers',
                                ], [
                                    'default' => 'keyword',
                                    'class' => 'form-group mb-0',
                                    'id' => '1'
                                ]
                            );?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container d-flex cdli-btn-group px-0 search-add-field-group">
            <?= $this->Form->button('Search', [
                    'escapeTitle' => false,
                    'type' => 'submit',
                    'class' => 'btn cdli-btn-blue'
                ]
            ); ?>
            <?= $this->Form->button(
                '<span class="fa fa-plus-circle plus-icon" aria-hidden="true"></span> Add search field', [
                    'escapeTitle' => false,
                    'type' => 'button',
                    'class' => 'btn cdli-btn-light default-add-search-btn',
                    'name' => 'add',
                    'id' => 'add'
                ]
            ); ?>
            <?= $this->Html->link(
                'Search Settings',
                ['controller' => 'SearchSettings', 'action' => 'index'],
                ['class' => 'd-none d-lg-block search-links mr-5']
            ); ?>
            <?= $this->Html->link(
                'Advanced Search',
                ['controller' => 'Advancedsearch', 'action' => 'index'],
                ['class' => 'd-none d-lg-block search-links mr-5']
            ); ?>
        </div>
    <?= $this->Form->end(); ?>

    <hr class="line mt-5"/>

    <h2 class="col-6 text-left display-4 section-title">Highlights</h2>
    <div class="row mt-5">
        <?php foreach ($highlights as $highlight): ?>
            <div class="card bs4-cdli-card col-sm-12 col-md-6 col-lg-4 mb-5">
                <img src="<?= $this->ArtifactAssets->getThumbnail($highlight->artifact) ?>"
                     alt="Highlight <?= $highlight->title . ' ' . $highlight->image_type ?>"
                     class="card-img-top">
                <div class="card-body">
                    <p class="card-title">
                        <?= $this->Html->link(
                            strlen($highlight->title) > 38 ? substr($highlight->title, 0, 38) . '...' : $highlight->title,
                            ['controller' => 'Postings', 'action' => 'view', $highlight->id]
                        ) ?>
                    </p>
                    <p class="card-text">
                        <?=
                        strlen(strip_tags($highlight->body)) > 100 ? substr(strip_tags($highlight->body), 0, 100) . '...' : strip_tags($highlight->body)
                        // use above to trim extra card-length
                        ?>
                    </p>
                </div>
            </div>
        <?php endforeach;?>
    </div>
    <div class="text-left">
        <?= $this->Html->link(
            'View all Highlights',
            ['controller' => 'Postings', 'action' => 'highlights']
        ) ?>
    </div>

    <h2 class="text-left display-4 section-title">News</h2>
    <div class="row mt-5">
        <?php foreach ($newsarticles as $news): ?>
            <div class="card bs4-cdli-card col-sm-12 col-md-6 col-lg-4 mb-5">
                <img src="<?= $this->ArtifactAssets->getThumbnail($news->artifact) ?>"
                     alt="News <?= $news->title . ' ' . $news->image_type ?>"
                     class="card-img-top">
                <div class="card-body">
                    <p class="card-title">
                        <?= $this->Html->link(
                            strlen($news->title) > 38 ? substr($news->title, 0, 38) . '...' : $news->title,
                            ['controller' => 'Postings', 'action' => 'view', $news->id]
                        ) ?>
                    </p>
                    <?php if (!is_null($news->publish_start)): ?>
                        <p class="date-style"><?= $news->publish_start->i18nFormat('yyyy-MM-dd') ?></p>
                    <?php endif; ?>
                    <p class="card-text">
                        <?=
                        strlen(strip_tags($news->body)) > 100 ? substr(strip_tags($news->body), 0, 100) . '...' : strip_tags($news->body)
                        // use above to trim extra card-length
                        ?>
                    </p>
                </div>
            </div>
        <?php endforeach;?>
    </div>
    <div class="text-left">
        <?= $this->Html->link(
            'View all News',
            ['controller' => 'Postings', 'action' => 'news']
        ) ?>
    </div>
</main>
<?php echo $this->element('smoothscroll'); ?>

<?= $this->element('citeBottom'); ?>
<?= $this->element('citeButton'); ?>
