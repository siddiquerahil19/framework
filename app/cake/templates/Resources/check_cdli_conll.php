<?php
$this->assign('title', 'Check validity of CDLI-CoNLL');
?>

<div class="row justify-content-md-center">
    <div class="col-lg-10 boxed">
        <h2><?= __('Check validity of CDLI-CoNLL') ?></h2>
        <p>Use the <a href="https://github.com/cdli-gh/morphology-pre-annotation-tool">
        Morphology Pre-annotation Tool (MPAT)</a> to check validity of a CDLI-CoNLL
        file.</p>

        <?= $this->Form->create(null, ['type' => 'file']) ?>
            <div class="form-group">
                <?= $this->Form->control('conll_text', [
                    'class' => 'form-control',
                    'label' => __('Paste text'),
                    'style' => 'width: 100%;',
                    'type' => 'textarea'
                ]) ?>
            </div>
            <div class="form-group">
                <?= $this->Form->control('conll_file', [
                    'class' => 'form-control',
                    'label' => __('Upload file'),
                    'type' => 'file'
                ]) ?>
            </div>

            <div class="form-group">
                <?= $this->Form->submit(__('Submit'), ['class' => 'form-control btn cdli-btn-blue']) ?>
            </div>
        <?= $this->Form->end() ?>
    </div>

    <div class="col-lg-10 boxed">
        <h2><?= __('Output') ?></h2>
        <?php if (isset($output)): ?>
            <?php foreach ($output as [$level, $message]): ?>
                <div class="alert alert-<?= $level ?>">
                    <?= h($message) ?>
                </div>
            <?php endforeach; ?>
        <?php else: ?>
            <p class="text-muted">
                <?= __('(none)') ?>
            </p>
        <?php endif; ?>
    </div>

    <?= $this->element('citeBottom') ?>
    <?= $this->element('citeButton') ?>
</div>
