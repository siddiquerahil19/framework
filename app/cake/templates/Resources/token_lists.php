<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Period $period
 */

use Cake\I18n\FrozenTime;
?>

<div class="row justify-content-md-center">
    <div class="col-lg-10 boxed">
        <div class="capital-heading"><?= __('Generated token lists') ?></div>

        <p>Sign and word lists for all periods, generated from CDLI data.</p>

        <table class="table-bootstrap">
            <thead>
                <tr>
                    <th><?= __('Period') ?></th>
                    <th><?= __('Word list') ?></th>
                    <th><?= __('Sign list') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($periods as $period): ?>
                    <tr>
                        <td><?= $this->Html->link($period->period, [
                            'controller' => 'Periods',
                            'action' => 'view',
                            $period->id
                        ]) ?></td>
                        <?php foreach (['words', 'signs'] as $kind): ?>
                            <td>
                                <?php if (isset($tokenLists[$period->id][$kind])): ?>
                                    <?= __('Generated {0}', FrozenTime::createFromTimestamp($tokenLists[$period->id][$kind])) ?>
                                    <br>
                                    <?= $this->Html->link(__('Download JSON'), [
                                        $period->id,
                                        $kind,
                                        '_ext' => 'json'
                                    ]) ?>
                                    &bull;
                                    <?= $this->Html->link(__('Plain Text'), [
                                        $period->id,
                                        $kind,
                                        '_ext' => 'txt'
                                    ]) ?>
                                <?php else: ?>
                                    &mdash;
                                <?php endif; ?>
                            </td>
                        <?php endforeach; ?>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
