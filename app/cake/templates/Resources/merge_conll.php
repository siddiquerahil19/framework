<?php
?>

<div class="row justify-content-md-center">
    <div class="col-lg-10 boxed">
        <h2><?= __('Merge two CDLI-CoNLL files') ?></h2>
        <p>Use <a href="https://github.com/acoli-repo/conll-merge">conll-merge</a>
        to merge two CoNLL files. The output consists of the annotations of the
        second file applied to the tokenization of the first file.</p>

        <?= $this->Form->create(null, ['type' => 'file']) ?>
            <div class="form-group">
                <?= $this->Form->control('a', [
                    'class' => 'form-control',
                    'label' => __('Base/new file'),
                    'style' => 'width: 100%;',
                    'type' => 'textarea'
                ]) ?>
            </div>
            <div class="form-group">
                <?= $this->Form->control('b', [
                    'class' => 'form-control',
                    'label' => __('File to merge/previous version'),
                    'style' => 'width: 100%;',
                    'type' => 'textarea'
                ]) ?>
            </div>

            <div class="form-group">
                <?= $this->Form->submit(__('Submit'), ['class' => 'form-control btn cdli-btn-blue']) ?>
            </div>
        <?= $this->Form->end() ?>
    </div>

    <div class="col-lg-10 boxed">
        <h2><?= __('Output') ?></h2>
        <?php if (isset($output)): ?>
            <pre><?= h($output) ?></pre>
        <?php else: ?>
            <p class="text-muted">
                <?= __('(none)') ?>
            </p>
        <?php endif; ?>
    </div>
</div>
