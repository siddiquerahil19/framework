<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Dynasty[]|\Cake\Collection\CollectionInterface $dynasties
 */

use Cake\Utility\Inflector;

$title = 'Documentation for ' . Inflector::pluralize(Inflector::humanize($audience));
$this->assign('title', $title);
?>
<div class="container-fluid text-center contentWrapper">
    <main id="docs" class="container text-left">
        <h1><?= $title ?></h1>
        <span class="page-summary-text mt-4">
            This page indexes documentation specificaly addressed to <?php echo Inflector::pluralize(Inflector::humanize($audience)); ?>. <?php echo
            $this->Html->link(
                'Click here to go to the full documentation index.',
                ['controller' => 'Docs', 'action' => 'index']
            );?>
        </span>
        <?php echo '<ul class="list-unstyled mt-5">';
        foreach ($doc_heads as $doc_head) {
            if (str_contains(implode(',',$doc_head['audiences']), ucfirst(Inflector::underscore(Inflector::humanize($audience))))) {
                echo '<li>';
                echo $this->Html->link(
                    $doc_head['title'],
                    ['controller' => 'Docs', 'action' => 'view', $doc_head['link_part']]
                );
                if (strlen($doc_head['category'])) {
                    echo ' ('.$doc_head['category'].')';
                }
            echo '</li>';
            }
        }?>
        </ul>

        <?= $this->element('citeBottom'); ?>
        <?= $this->element('citeButton'); ?>
    </main>
</div>
