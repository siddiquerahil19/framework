<?php

use League\CommonMark\Environment\Environment;
use League\CommonMark\Extension\CommonMark\CommonMarkCoreExtension;
use League\CommonMark\Extension\HeadingPermalink\HeadingPermalinkExtension;
use League\CommonMark\Extension\TableOfContents\TableOfContentsExtension;
use League\CommonMark\Extension\Autolink\AutolinkExtension;
use League\CommonMark\Extension\ExternalLink\ExternalLinkExtension;
use League\CommonMark\MarkdownConverter;
use League\CommonMark\Extension\GithubFlavoredMarkdownExtension;

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Doc $doc
 */

$config = [
    'table_of_contents' => [
        'html_class' => 'table-of-contents',
        'position' => 'top',
        'style' => 'bullet',
        'min_heading_level' => 1,
        'max_heading_level' => 6,
        'normalize' => 'relative',
        'placeholder' => null,
    ],
];
$environment = new Environment($config);
$environment->addExtension(new CommonMarkCoreExtension());
$environment->addExtension(new HeadingPermalinkExtension());
$environment->addExtension(new TableOfContentsExtension());
$environment->addExtension(new AutolinkExtension());
$environment->addExtension(new ExternalLinkExtension());
$environment->addExtension(new GithubFlavoredMarkdownExtension());
$converter = new MarkdownConverter($environment);

$citation = [
    'type' => 'webpage',
    'title' => $head['section'] . ': ' . $head['title'],
    'container-title' => 'Cuneiform Digital Library Initiative',
    'author' => array_map(function ($author) {
        [$family, $given] = explode(', ', $author);
        if (isset($given)) {
            return compact('given', 'family');
        } else {
            return ['literal' => $author];
        }
    }, $head['authors']),
    'issued' => [['date-parts' => explode('-', date('Y-m-d'))]],
    'accessed' => [['date-parts' => explode('-', date('Y-m-d'))]],
    'URL' => 'https://cdli.mpiwg-berlin.mpg.de' . $this->getRequest()->getRequestTarget()
];

$this->assign('title', $head['section'] . ': ' . $head['title']);
?>
<link rel="stylesheet" href="\assets\css\conllu.js\jquery-ui-redmond.css">
<link rel="stylesheet" href="\assets\css\conllu.js\style-vis.css">
<script type="text/javascript" src="\assets\js\conllu.js\lib\ext\head.load.min.js"></script>

<div class="row justify-content-center">
    <div class="col-lg-9 col-12 text-left">
        <h1 class="display-4"><?= h($head['title']) ?></h1>
        <span class="page-summary-text mt-4">
            This is a documentation page.
            <?= $this->Html->link(
                'Click here to go to the full documentation index.',
                ['controller' => 'Docs', 'action' => 'index']
            ) ?>
        </span>

        <div class="font-weight-bold mt-4">
            Table of contents
        </div>

        <div id="docs-body">
            <?= $converter->convert($doc_body) ?>
        </div>

        <?= $this->element('citeBottom', ['data' => $citation]) ?>
        <?= $this->element('citeButton', ['data' => $citation]) ?>
    </div>
</div>

<script src="/assets/js/focus-visible.js"></script>
<script src="/assets/js/collapseall.js"></script>
<script src="/assets/js/smoothScroll.js"></script>
<script type="text/javascript" src="/assets/js/conllu.js/lib/ext/jquery.min.js"></script>
<script type="text/javascript" src="/assets/js/conllu.js/lib/ext/jquery.svg.min.js"></script>
<script type="text/javascript" src="/assets/js/conllu.js/lib/ext/jquery.svgdom.min.js"></script>
<script type="text/javascript" src="/assets/js/conllu.js/lib/ext/jquery-ui.min.js"></script>
<script type="text/javascript" src="/assets/js/conllu.js/lib/ext/waypoints.min.js"></script>

<script type="text/javascript" src="/assets/js/conllu.js/lib/brat/configuration.js"></script>
<script type="text/javascript" src="/assets/js/conllu.js/lib/brat/util.js"></script>
<script type="text/javascript" src="/assets/js/conllu.js/lib/brat/annotation_log.js"></script>

<script type="text/javascript" src="/assets/js/conllu.js/lib/ext/webfont.js"></script>
<script type="text/javascript" src="/assets/js/conllu.js/lib/brat/dispatcher.js"></script>
<script type="text/javascript" src="/assets/js/conllu.js/lib/brat/url_monitor.js"></script>
<script type="text/javascript" src="/assets/js/conllu.js/lib/brat/visualizer.js"></script>

<script type="text/javascript" src="/assets/js/conllu.js/annodoc.js"></script>
<script type="text/javascript" src="/assets/js/conllu.js/lib/local/config.js"></script>
<script type="text/javascript" src="/assets/js/conllu.js/conllu.js"></script>
<script type="text/javascript">
    var root = ''; // filled in by jekyll
    head.js();
    var documentCollections = {};
    var webFontURLs = [
        root + '/assets/css/conllu.js/fonts/PT_Sans-Caption-Web-Regular.ttf',
        root + '/assets/css/conllu.js/fonts/Liberation_Sans-Regular.ttf'
    ];
    $(function() {
        Annodoc.activate(Config.bratCollData, documentCollections);
    });
</script>
