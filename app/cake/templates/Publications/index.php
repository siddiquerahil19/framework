<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Publication[]|\Cake\Collection\CollectionInterface $publications
 */
?>

<div class="d-flex justify-content-between align-items-end mb-2">
    <h1 class="display-3 header-txt text-left"><?= __('Publications') ?></h1>
    <?= $this->element('addButton'); ?>
</div>

<p class="text-justify page-summary-text pt-3 pb-3">From this page you can access the bibliography relating to cuneiform artifacts and to some entities such as proveniences and periods. Use the search function provided to filter out the publications of your choice and browse below using the pager.</p>
<div class="text-left my-2">
    <?= $this->element('entityExport'); ?>
</div>
<?= $this->Form->create(null, ['url' => ['action' => 'index'], 'type' => 'get']) ?>
<div class="mx-0 boxed">
    <div class="col-lg ads row justify-content-md-center">
        <legend class="capital-heading"><?= __('Bibliography Search') ?></legend>
        <div class="layout-grid text-left">
            <div class="grid-1">
                BibTeX Key
                <?= $this->Form->input('bibtexkey', ['label' => false , 'type' => 'text', 'value' => isset($data['bibtexkey']) ? $data['bibtexkey']:null]) ?>
                Title
                <?= $this->Form->input('title', ['label' => false, 'type' => 'text', 'value' => isset($data['title']) ? $data['title']:null]) ?>
                Publisher
                <?= $this->Form->input('publisher', ['label' => false, 'type' => 'text', 'value' => isset($data['publisher']) ? $data['publisher']:null]) ?>
                Author
                <?= $this->Form->input('author', ['label' => false, 'type' => 'text', 'value' => isset($data['author']) ? $data['author']:null]) ?>
                Designation
                <?= $this->Form->input('designation', ['label' => false, 'type' => 'text', 'value' => isset($data['designation']) ? $data['designation']:null]) ?>
            </div>
            <div class="grid-2">
                Artifact
                <?= $this->Form->input('artifact', ['label' => false, 'type' => 'text', 'value' => isset($data['artifact']) ? $data['artifact']:null]) ?>
                Entry Type
                <?= $this->Form->control('entry_type_id', ['label' => false, 'options' => $entryTypes, 'empty' => true, 'value' => isset($data['entry_type_id']) ? $data['entry_type_id']:null, 'class' => 'form-select']);  ?>
                Journal
                <?= $this->Form->control('journal_id', ['label' => false, 'options' => $journals, 'empty' => true, 'value' => isset($data['journal_id']) ? $data['journal_id']:null, 'class' => 'form-select']);  ?>
                Year
                <div class="layout-grid text-left">
                    <div class="grid-1">
                    From<?= $this->Form->control('from', ['label' => false, 'type' => 'number', 'value' => isset($data['from']) ? $data['from']:null]) ?>
                    </div>
                    <div class="grid-2">
                    To<?= $this->Form->control('to', ['label' => false, 'type' => 'number', 'value' => isset($data['to']) ? $data['to']:null]) ?>
                    </div>
                </div>
            </div>
        </div>
        <table>
            <tr>
                <td>
                    <?= $this->Form->submit('Search', ['class' => 'btn cdli-btn-blue']) ?>
                </td>
                <td>
                    <?= $this->Html->link(
                        __('Reset'),
                        ['controller' => 'Publications', 'action' => 'index'],
                        ['class' => 'btn']
                    ) ?>
                </td>
            </tr>
        </table>
    </div>
</div>
<?= $this->Form->end() ?>

<?php if ((!isset($publications)) or (count($publications) == 0)): ?>
    <legend><?= __('No Search Results') ?></legend>
<?php else: ?>
    <div class="table-responsive">
        <table class="table-bootstrap mx-0">
            <thead>
                <tr>
                    <th scope="col">BibTeX key</th>
                    <th scope="col">Authors</th>
                    <th scope="col">Designation</th>
                    <th scope="col">Reference</th>
                    <?php if ($access_granted): ?>
                        <th scope="col"><?= __('Action') ?></th>
                    <?php endif ?>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($publications as $publication): ?>
                <tr>
                    <td>
                        <?= $this->Html->link(
                                        h($publication->bibtexkey),
                                        ['prefix' => false, 'action' => 'view', $publication->id]) ?>
                    </td>
                    <td align="left" nowrap="nowrap">
                        <?php foreach ($publication->authors as $author): ?>
                        <?= $this->Html->link(
                                        h($author->author),
                                        ['prefix' => false, 'controller' => 'Authors', 'action' => 'view', $author->id]) ?><br>
                        <?php endforeach ?>
                    </td>
                    <td><?= h($publication->designation) ?></td>
                    <td>
                        <?= $this->Citation->formatReference($publication, 'bibliography', [
                            'template' => 'chicago-author-date',
                            'format' => 'html'
                        ]) ?>

                    </td>
                    <td><?php if ($access_granted): ?>
                        <?= $this->Html->link(__('Edit'),['prefix' => 'Admin', 'action' => 'edit', $publication->id],
                                ['escape' => false , 'class' => "btn btn-outline-primary", 'title' => 'Edit']) ?>
                    <?php endif ?>
                  </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <?php echo $this->element('Paginator'); ?>
<?php endif ?>

<?= $this->element('citeButton'); ?>
<?= $this->element('citeBottom'); ?>
