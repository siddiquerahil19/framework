<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Publication $publication
 * @var \App\Model\Provenience[] $proveniences
 * @var boolean $is_admin
 */
?>
<h1 class="display-3 header-txt text-left"><?= __('View Publication Details') ?></h1>

<div class="text-left my-2">
    <div class="export-grid col-4">
        <?= $this->Dropdown->open('Export Publication' . ' <i class="fa fa-chevron-down" aria-hidden="true"></i>') ?>
            <div class="row" style="min-width: 15rem;">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <?= $this->Html->link(
                        __('Download the Publication in text format to your computer' . ' <i class="fa fa-download" aria-hidden="true"></i>'),
                        ['controller' => 'Publications', 'action' => 'view', 'id' => $publication->id, 'format' => 'txt'],
                        ['download'=> 'Publications', 'escape' => false],
                        ['class' => 'ml-3']
                    ) ?>
                    <h3 style="color: black;">Flat Data</h3>
                    <?= $this->Html->link(
                        __('As {0}', 'CSV'),
                        ['controller' => 'Publications', 'action' => 'view', 'id' => $publication->id, 'format' => 'csv'],
                        ['class' => 'ml-3']
                    ) ?>
                    <?= $this->Html->link(
                        __('As {0}', 'TSV'),
                        ['controller' => 'Publications', 'action' => 'view', 'id' => $publication->id, 'format' => 'tsv'],
                        ['class' => 'ml-3']
                    ) ?>
                    <h3 style="color: black;">Linked Data</h3>
                    <?= $this->Html->link(
                        __('As {0}', 'TTL'),
                        ['controller' => 'Publications', 'action' => 'view', 'id' => $publication->id, 'format' => 'ttl'],
                        ['class' => 'ml-3']
                    ) ?>
                    <?= $this->Html->link(
                        __('As {0}', 'RDF/JSON'),
                        ['controller' => 'Publications', 'action' => 'view', 'id' => $publication->id, 'format' => 'rdfjson'],
                        ['class' => 'ml-3']
                    ) ?>
                    <h3 style="color: black;">Expanded Data</h3>
                    <?= $this->Html->link(
                        __('As {0}', 'JSON'),
                        ['controller' => 'Publications', 'action' => 'view', 'id' => $publication->id, 'format' => 'json'],
                        ['class' => 'ml-3']
                    ) ?>
                    <h3 style="color: black;">Bibliography</h3>
                    <?= $this->Html->link(
                        __('As {0}', 'BibTex'),
                        ['controller' => 'Publications', 'action' => 'view', 'id' => $publication->id, 'format' => 'bib'],
                        ['class' => 'ml-3']
                    ) ?>
                    <?= $this->Html->link(
                        __('As {0}', 'RIS'),
                        ['controller' => 'Publications', 'action' => 'view', 'id' => $publication->id, 'format' => 'ris'],
                        ['class' => 'ml-3']
                    ) ?>
                </div>
            </div>
        <?= $this->Dropdown->close() ?>
    </div>
</div>

<div class="row justify-content-md-center">
    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Publication') ?>
        <?php if ($is_admin) : ?>
            <?= $this->Html->link(
    __('Edit'),
    ['prefix' => 'Admin',
                'action' => 'edit',
                $publication->id],
    ['escape' => false,
                'class' => "btn btn-outline-primary",
                'title' => 'Edit',
                'style' => "float: right;"]
    );?>
        <?php endif ?>
        </div>
        <table class="table-bootstrap mx-0">
            <tbody>
                <tr>
                    <th scope="row"><?= __('Designation') ?></th>
                    <td>
                        <?= h($publication->designation) ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Reference') ?></th>
                    <td>
                        <?= $this->Citation->formatReference($publication, 'bibliography', [
                            'template' => 'chicago-author-date',
                            'format' => 'html'
                        ]) ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Authors') ?></th>
                    <td>
                        <?php foreach ($publication->authors as $author) : ?>
                            <?= $this->Html->link(
                            h($author->author),
                            ['controller' => 'Authors',
                                'action' => 'view',
                                $author->id]
                        ) ?><br>
                        <?php endforeach; ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Editors') ?></th>
                    <td>
                        <?php foreach ($publication->editors as $editor) : ?>
                            <?= $this->Html->link(
                                h($editor->author),
                                ['controller' => 'Authors',
                                'action' => 'view',
                                $editor->id]
                            ) ?><br>
                        <?php endforeach; ?>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>


<?php if (!empty($publication->artifacts)) : ?>
    <div class="boxed mx-0">
        <div class="capital-heading"><?= __('Related Artifacts') ?>
            <?php if ($is_admin) : ?>
                <?= $this->Html->link(
                    __('Link Artifacts'),
                    [
                        'prefix' => 'Admin',
                        'controller' => 'EntitiesPublications',
                        'action' => 'add',
                        'entity',
                        'artifacts',
                        $publication->id
                    ],
                    ['class' => 'btn btn-action', 'style' => 'float: right;']
                ) ?>
            <?php endif ?>
        </div>
        <div class="table-responsive">
            <table cellpadding="0" cellspacing="0" class="table-bootstrap mx-0">
                <thead>
                    <th scope="col"><?= __('Artifact Identifier') ?></th>
                    <th scope="col"><?= __('Artifact Designation') ?></th>
                    <th scope="col"><?= __('Musuem Collections') ?></th>
                    <th scope="col"><?= __('Period') ?></th>
                    <th scope="col"><?= __('Provenience') ?></th>
                    <th scope="col"><?= __('Artifact Type') ?></th>
                    <th scope="col"><?= __('Publication Type') ?></th>
                    <th scope="col"><?= __('Exact Reference') ?></th>
                    <th scope="col"><?= __('Publication Comments') ?></th>
                </thead>
                <tbody>
                    <?php foreach ($publication->artifacts as $artifact) : ?>
                    <tr>
                        <td><?= $this->Html->link(h('P'.substr("00000{$artifact->id}", -6)), ['controller' => 'Artifacts', 'action' => 'view', $artifact->id]) ?>
                        </td>
                        <td><?= h($artifact->designation) ?></td>
                        <td>
                            <?php foreach ($artifact->collections as $collection) : ?>
                                <?= $this->Html->link($collection->collection, ['controller' => 'Collections', 'action' => 'view', $collection->id]) ?>
                            <?php endforeach; ?>
                        </td>
                        <td>
                            <?php if (!empty($artifact->period)) : ?>
                                <?= $this->Html->link($artifact->period->period, ['controller' => 'Periods', 'action' => 'view', $artifact->period->id]) ?>
                            <?php endif; ?>
                        </td>
                        <td>
                            <?php if (!empty($artifact->provenience)) : ?>
                                <?= $this->Html->link($artifact->provenience->provenience, ['controller' => 'Proveniences', 'action' => 'view', $artifact->provenience->id]) ?>
                            <?php endif; ?>
                        </td>
                        <td>
                            <?php if (!empty($artifact->artifact_type)) : ?>
                                <?= $this->Html->link($artifact->artifact_type->artifact_type, ['controller' => 'ArtifactTypes', 'action' => 'view', $artifact->artifact_type->id]) ?>
                            <?php endif; ?>
                        </td>
                        <td><?= h($artifact->_joinData->publication_type) ?></td>
                        <td><?= h($artifact->_joinData->exact_reference) ?></td>
                        <td><?= h($artifact->publication_comments) ?></td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
</div>
<?php endif; ?>


<?php if (!empty($publication->proveniences)) : ?>
    <div class="boxed mx-0">
        <div class="capital-heading"><?= __('Related Proveniences') ?>
            <?php if ($is_admin) : ?>
                <?= $this->Html->link(
                    __('Link Proveniences'),
                    [
                        'prefix' => 'Admin',
                        'controller' => 'EntitiesPublications',
                        'action' => 'add',
                        'entity',
                        'proveniences',
                        $publication->id
                    ],
                    ['class' => 'btn btn-action', 'style' => 'float: right;']
                ) ?>
            <?php endif ?>
        </div>
        <div class="table-responsive">
            <table cellpadding="0" cellspacing="0" class="table-bootstrap mx-0">
                <thead>
                    <tr align="left">
                        <th scope="col">Provenience</th>
                        <th scope="col">Region</th>
                        <th scope="col">Geo Coordinates</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($publication->proveniences as $provenience): ?>
                        <tr align="left">
                            <td>
                                <?= $this->Html->link(
                                    $provenience->provenience,
                                    ['controller' => 'Proveniences', 'action' => 'view', $provenience->id]
                                ) ?>
                            </td>
                            <td>
                                <?php if ($provenience->has('region')): ?>
                                    <?= $this->Html->link(
                                        $provenience->region->region,
                                        ['controller' => 'Regions', 'action' => 'view', $provenience->region->id]
                                    ) ?>
                                <?php endif; ?>
                            </td>
                            <td><?= h($provenience->geo_coordinates) ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
<?php endif; ?>

<?= $this->element('citeBottom', ['data' => $publication]); ?>
<?= $this->element('citeButton', ['data' => $publication]); ?>
