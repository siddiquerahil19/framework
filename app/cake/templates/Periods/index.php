<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Period[]|\Cake\Collection\CollectionInterface $periods
 */
?>
<div class="d-flex justify-content-between align-items-end mb-2">
    <h1 class="display-3 header-txt text-left"><?= __('Periods') ?></h1>
    <?= $this->element('addButton'); ?>
</div>
<div class="text-left my-2">
    <?= $this->element('entityExport'); ?>
</div>

<p class="text-left page-summary-text mt-4 pl">Cuneiform writing has a long history and was in use as Proto-Cuneiform which appeared at the turn of the fourth millennium, up to the Christian Era. Scroll down to see the most recent periods of the timeline and click on the geo-temporal period of your choice to know more.</p>

<div class="timelinemid timelinecontainer2"></div>

<div class="timelinebody timeline">
    <?php $a=1 ?>
    <?php foreach ($periods as $period): ?>
        <?php if($a%2==0) : ?>
            <div class="timelinecontainer timelineleft">
        <?php else: ?>
            <div class="timelinecontainer timelineright">
        <?php endif; ?>
            <div class="timelinecontentBlock">
                <p>
                    <?= $this->Html->link(
                        $period->period,
                        ['controller' => 'Periods', 'action' => 'view', $period->id]
                    ) ?>
                </p>
            </div>
        </div>
        <?php $a=$a+1 ?>
    <?php endforeach; ?>
</div>

<div class="timelinemid2 timelinecontainer3"></div>

<?= $this->element('citeButton'); ?>
<?= $this->element('citeBottom'); ?>