<div class="boxed mx-0">

    <div class="capital-heading">
        <?= __('Provenience Information:') ?>
        <br/>
        <?= h($provenience->provenience) ?>
    </div>

    <table class="table-bootstrap">
        <tbody>
            <tr>
                <th scope="row"><?= __('Provenience') ?></th>
                <td><?= h($provenience->provenience) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Region') ?></th>
                <td><?= $provenience->has('region') ? $this->Html->link($provenience->region->region, ['controller' => 'Regions', 'action' => 'view', $provenience->region->id]) : '' ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Geo Coordinates') ?></th>
                <td><?= h($provenience->geo_coordinates) ?></td>
            </tr>
            <!--  <tr>
                <th scope="row"><?= __('Id') ?></th>
                <td><?= $this->Number->format($provenience->id) ?></td>
            </tr> -->
        </tbody>
    </table>

</div>
