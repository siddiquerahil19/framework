<?php
/**
 * @var \App\View\AppView $this
 */
?>

<div class="canvas">
   <div id="help">
      <div class="help-header">
         Control Guide
         <div class="help-icon">
              <img id="help-close" src="/images/3dviewericons/closeicon.svg" alt="Help close icon">
         </div>
      </div>
      <div class="help-body animate">
         <div class="row">
            <div class="col">
                <?= $this->Html->image('/images/3dviewericons/rotate.svg', ['alt' => 'Rotate icon']) ?>
               <span class="hads">Rotate</span><br><span class="conts"> Drag model to rotate in 360 degrees. <br> ( R )</span>
            </div>
            <div class="col">
                <?= $this->Html->image('/images/3dviewericons/zoomin.svg', ['alt' => 'Zoom IN icon']) ?>
               <span class="hads">Zoom In</span><br><span class="conts"> Makes model larger on every click. <br> ( + )</span>
            </div>
            <div class="col">
                <?= $this->Html->image('/images/3dviewericons/undo.svg', ['alt' => 'Undo icon']) ?>
               <span class="hads">Undo</span><br><span class="conts"> Undo last interaction. <br> ( Z )</span>
            </div>
           <div class="col">
               <?= $this->Html->image('/images/3dviewericons/light.svg', ['alt' => 'Light icon']) ?>
               <span class="hads">Light</span><br><span class="conts"> Drag mouse to change light direction. <br> ( L )</span>
            </div>
         </div>
         <div class="row">
            <div class="col">
                <?= $this->Html->image('/images/3dviewericons/move.svg', ['alt' => 'Move icon']) ?>
               <span class="hads">Move</span><br><span class="conts"> Drag model to change position.<br> ( M )</span>
            </div>
            <div class="col">
                <?= $this->Html->image('/images/3dviewericons/zoomout.svg', ['alt' => 'Zoom out icon']) ?>
                <span class="hads">Zoom Out</span><br><span class="conts"> Makes model smaller on every click. <br> ( - )</span>
            </div>
            <div class="col">
                <?= $this->Html->image('/images/3dviewericons/reset.svg', ['alt' => 'Reset icon']) ?>
                <span class="hads">Reset</span><br><span class="conts"> Return model to original view setting.</span>
            </div>
            <div class="col">
                <?= $this->Html->image('/images/3dviewericons/fullscreen.svg', ['alt' => 'Fullscreen icon']) ?>
               <span class="hads">Full Screen</span><br><span class="conts"> Makes viewer cover full screen. <br> (F, Esc to exit)</span>
            </div>
         </div>
      </div>
   </div>

   <div class="threeDcontainer" id="canvas" style="border: none; filter: blur(0);"></div>
   <div id="cdlilink" class="d-none"><?= basename($asset->getPath(), '.ply') ?></div>

   <div id="progress">
       <div id="progressGif"><img src="/images/3dviewericons/progress.gif"></div>
       <div id="message">Loading 3D model data ...</div>
   </div>

   <p>
       3D Viewer Designed by <a href="http://virtualcuneiform.org/">The Virtual Cuneiform Tablet Reconstruction Project</a>.
   </p>
</div>

<?= $this->Html->css('3dviewer'); ?>
<?= $this->Html->script('3dviewer/three.min'); ?>
<?= $this->Html->script('3dviewer/PLYLoader'); ?>
<?= $this->Html->script('3dviewer/Detector'); ?>
<?= $this->Html->script('3dviewer/Arcball'); ?>
<?= $this->Html->script('3dviewer/InteractiveViewer'); ?>
