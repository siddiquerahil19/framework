<div id="rti-viewer">
    <?= $this->Html->css('/apps/rti-viewer/css/webrtiviewer.css'); ?>
    <?= $this->Html->css('/apps/rti-viewer/css/ui-lightness/jquery-ui-1.10.3.custom.css'); ?>
    <?= $this->Html->script('/apps/rti-viewer/js/jquery-1.11.3.min.js'); ?>
    <?= $this->Html->script('/apps/rti-viewer/js/jquery-ui.min.js'); ?>
    <?= $this->Html->script('/apps/rti-viewer/js/pep.min.js'); ?>
    <?= $this->Html->script('/apps/rti-viewer/spidergl/spidergl.js'); ?>
    <?= $this->Html->script('/apps/rti-viewer/spidergl/multires.js'); ?>
    <script type="text/javascript">
        createRtiViewer('rti-viewer', <?= json_encode(dirname(DS . $asset->getPath())) ?>, 900, 600);
    </script>
</div>
