<p><?= $this->Html->link(
    __('Open image') . ' <span class="fa fa-external-link"></span>',
    DS . $asset->getPath(),
    ['target' => '_blank', 'escapeTitle' => false]
) ?></p>

<div id="viewer" class="bg-dark text-center" style="height: 100vh;">
    <noscript>
        <img src="<?= DS . $asset->getPath() ?>" style="max-height: 100%; max-width: 100%;">
    </noscript>
</div>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@recogito/annotorious-openseadragon@2.7.2/dist/annotorious.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/openseadragon/3.0.0/openseadragon.min.js" integrity="sha512-Dq5iZeGNxm7Ql/Ix10sugr98niMRyuObKlIzKN1SzUysEXBxti479CTsCiTV00gFlDeDO3zhBsyCOO+v6QVwJw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdn.jsdelivr.net/npm/@recogito/annotorious-openseadragon@2.7.2/dist/openseadragon-annotorious.min.js"></script>
<script type="text/javascript">
$(function() {
    var viewer = OpenSeadragon({
        id: 'viewer',

        // Location of UI images
        prefixUrl: 'https://cdn.jsdelivr.net/npm/openseadragon@3.1/build/openseadragon/images/',

        // Show navigator in top right
        showNavigator: true,
        navigatorSizeRatio: 0.2,

        // Show more controls
        showFlipControl: true,
        showRotationControl: true,

        // Load image
        maxZoomLevel: 1,
        preserveImageSizeOnResize: true,
        tileSources: {
            type: 'image',
            url: <?= json_encode(DS . $asset->getPath()) ?>
        }
    })

    // Zoom to top center of image
    function setZoom () {
        var zoom = viewer.viewport.imageToViewportZoom(1)
        if (zoom < 1) {
            viewer.viewport.zoomTo(zoom, null, true)
            viewer.viewport.defaultZoomLevel = zoom
            viewer.viewport.minZoomLevel = zoom
        } else {
            var bounds = viewer.viewport._contentBounds
            var aspect = viewer.viewport.getAspectRatio()
            viewer.viewport.fitBoundsWithConstraints(new OpenSeadragon.Rect(
                0,
                0,
                bounds.width,
                Math.min(bounds.width / aspect, bounds.height)
            ), true)
        }
    }
    viewer.addHandler('tile-loaded', setZoom)
    viewer.addHandler('resize', setZoom)

    // Load annotations in W3C WebAnnotation format
    var annotations = <?= json_encode(array_map(function ($annotation) {
        return $annotation->getW3cJson();
    }, $asset->annotations)) ?>;
    var CreditsWidget = function (args) {
        var annotation = annotations.find(annotation => annotation.id === args.annotation.id)
        var container = document.createElement('div')
        container.setAttribute('class', 'text-left p-1')
        container.setAttribute('style', 'font-size: 0.7em;')
        container.append('Authors: ')
        for (var i = 0; i < annotation.creator.length; i++) {
            if (i !== 0) {
                container.append(', ')
            }
            var a = document.createElement('a')
            a.setAttribute('href', '/' + annotation.creator[i].id)
            a.textContent = annotation.creator[i].name
            container.append(a)
        }
        container.append(document.createElement('br'))
        container.append('Created: ', (new Date(annotation.created)).toLocaleString(), ', ')
        container.append('modified: ', (new Date(annotation.modified)).toLocaleString())
        if (annotation.license) {
            container.append('License: ' + annotation.license)
        }
        return container
    }
    var anno = OpenSeadragon.Annotorious(viewer, {
        readOnly: true,
        widgets: ['COMMENT', 'TAG', CreditsWidget]
    })
    anno.setAnnotations(annotations)
})
</script>
