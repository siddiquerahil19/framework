<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Inscription $inscription
 */

$this->assign('title', "Text of {$inscription->artifact->designation}, revision {$inscription->id}");
?>


<div class="row justify-content-md-center">
    <div class="col-lg-12 text-left">
        <?php if ($inscription->update_event->update_type === 'atf'): ?>
            <h1>Inscription</h1>
        <?php else: ?>
            <h1>Linguistic Annotation</h1>
        <?php endif; ?>
        <h2><?= $this->Html->link(
            $inscription->artifact->designation . ' (' . $inscription->artifact->getCdliNumber() . ')',
            ['action' => 'view', $inscription->artifact->id]
        ) ?></h2>
        <?= $this->element('updateEventHeader', ['update_event' => $inscription->update_event]);?>
        <pre><?= $this->Diff->diffInscription(
            $oldInscription,
            $inscription,
            $inscription->update_event->update_type
        ) ?></pre>

        <?php if ($inscription->update_event->update_type === 'atf' && $inscription->has('annotation')): ?>
            <h3><?= __('Automatic changes to CoNLL annotation') ?></h3>

            <pre><?= $this->Diff->diffInscription($oldInscription, $inscription, 'annotation') ?></pre>
        <?php endif; ?>
    </div>
</div>

<?= $this->element('citeButton'); ?>
<?= $this->element('citeBottom'); ?>
