<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsUpdate $artifactsUpdate
 */

$this->set('Confirm text changes');
?>

<div class="row justify-content-md-center">
    <div class="col-lg-7 boxed">
        <h2><?= __('Confirm changes') ?></h2>

        <hr>

        <p>
            Make sure the following changes are correct, and that the designation
            and the CDLI number in the left column match those found in the ATF.
            The lines highlighted in green indicate additions, while the lines in
            red indicate deletions. If a line changes, the old version will show
            up in red followed by the new version in green.
        </p>

        <?php if ($canSubmitEdits && !$noChanges): ?>
            <?= $this->Html->link(
                __('Confirm'),
                ['controller' => 'UpdateEvents', 'action' => 'add', '?' => ['update_type' => $updateType]],
                ['class' => 'btn cdli-btn-blue']
            ) ?>
        <?php endif; ?>
        <?= $this->Form->postLink(
            __('Discard'),
            ['controller' => 'Inscriptions', 'action' => 'add'],
            ['class' => 'btn cdli-btn-light', 'confirm' => __('This will discard your changes. Are you sure?')]
        ) ?>
    </div>

    <div class="col-lg-12 boxed">
        <div style="overflow-x: auto;">
            <table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
                <thead>
                    <tr>
                        <th><?= __('Artifact') ?></th>
                        <th><?= __('Text revision') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($inscriptions as $inscription): ?>
                        <tr>
                            <td><?= h($inscription->artifact->designation) ?> (<?= h($inscription->artifact->getCdliNumber()) ?>)</td>
                            <td>
                                <?php if ($inscription->hasTokenWarnings()): ?>
                                    <?php foreach ($inscription->getTokenWarnings() as $warning): ?>
                                        <div class="alert alert-<?= h($warning[0]) ?>">
                                            <?= h($warning[1]) ?>
                                        </div>
                                    <?php endforeach; ?>
                                <?php elseif ($inscription->hasAnnotationWarnings()): ?>
                                    <?php foreach ($inscription->getAnnotationWarnings() as $warning): ?>
                                        <div class="alert alert-<?= h($warning[0]) ?>">
                                            <?= h($warning[1]) ?>
                                        </div>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                                <pre><?=
                                    $this->Diff->diffInscription($inscription->artifact->inscription, $inscription, $updateType)
                                ?></pre>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <?php if ($noChanges): ?>
                        <tr>
                            <td colspan="2" class="text-center"><?= __('No changes') ?></td>
                        </tr>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>

        <?php echo $this->element('Paginator'); ?>
    </div>
</div>
