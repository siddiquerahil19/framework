<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsUpdate $artifactsUpdate
 */
?>
<div class="row justify-content-md-center">
    <div class="col-lg-12 boxed">
        <h2><?= __('Encountered errors') ?></h2>

        <ul>
            <?php foreach ($errors as $error): ?>
                <li><?= h($error) ?></li>
            <?php endforeach; ?>
        </ul>

        <?= $this->Form->postLink(
            __('Cancel'),
            ['controller' => 'Inscriptions', 'action' => 'add'],
            ['class' => 'btn cdli-btn-light']
        ) ?>
    </div>

    <div class="col-lg-12 boxed">
        <?php foreach ($inscriptions as $inscription): ?>
            <section>
                <h3>P<?= h(str_pad($inscription->artifact_id, 6, '0', STR_PAD_LEFT)) ?></h3>

                <ul>
                    <?php foreach ($inscription->getErrors() as $field => $errors): ?>
                        <?php foreach ($errors as $error): ?>
                            <li><?= h($field) ?>:
                                <?php if ($field === 'atf'): ?>
                                    <?php $parts = explode("\n\n", $error, 2); ?>
                                    <?= h($parts[0]) ?>
                                    <?php if (array_key_exists(1, $parts)): ?>
                                        <details class="d-inline-block align-text-top">
                                            <summary><?= __('Click for technical info') ?></summary>
                                            <pre><?= h($parts[1]) ?></pre>
                                        </details>
                                    <?php endif; ?>
                                <?php else: ?>
                                    <?= h($error) ?>
                                <?php endif; ?>
                            </li>
                        <?php endforeach; ?>
                    <?php endforeach; ?>
                </ul>
            </section>
        <?php endforeach; ?>
        <?php echo $this->element('Paginator'); ?>
    </div>
</div>
