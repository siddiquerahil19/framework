<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Inscription[]|\Cake\Collection\CollectionInterface $inscriptions
 */
?>
<div class="d-flex justify-content-between align-items-end mb-2">
    <h1 class="display-3 header-txt text-left"><?= __('Text revisions') ?></h1>
    <?= $this->element('addButton'); ?>
</div>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead align="left">
        <tr>
            <th scope="col"><?= $this->Paginator->sort('id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('artifact_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('is_atf2conll_diff_resolved') ?></th>
            <th scope="col"><?= $this->Paginator->sort('accepted_by') ?></th>
            <th scope="col"><?= $this->Paginator->sort('accepted') ?></th>
            <th scope="col"><?= $this->Paginator->sort('update_events_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('is_latest') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($inscriptions as $inscription): ?>
        <tr>
            <td><?= $this->Html->link($this->Number->format($inscription->id), ['action' => 'view', $inscription->id]) ?></td>
            <td><?= $inscription->has('artifact') ? $this->Html->link($inscription->artifact->id, ['controller' => 'Artifacts', 'action' => 'view', $inscription->artifact->id]) : '' ?></td>
            <td><?= h($inscription->is_atf2conll_diff_resolved) ?></td>
            <td><?= $this->Number->format($inscription->accepted_by) ?></td>
            <td><?= h($inscription->accepted) ?></td>
            <td><?= $this->Number->format($inscription->update_events_id) ?></td>
            <td><?= h($inscription->is_latest) ?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php echo $this->element('Paginator'); ?>

<?= $this->element('citeButton'); ?>
<?= $this->element('citeBottom'); ?>
