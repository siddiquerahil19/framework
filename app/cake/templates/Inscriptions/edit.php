<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Inscription $inscription
 */

$this->assign('title', 'Edit text of ' . $artifact->getCdliNumber());
$this->Form->setTemplates([
    'inputContainer' => '<div class="input form-group {{type}}{{required}}">{{content}}</div>'
]);

$image = $this->ArtifactAssets->getImage($artifact);
?>

<div class="row justify-content-md-center">
    <?php if (!is_null($image)): ?>
        <div class="col-lg-6">
            <div class="boxed">
                <h2><?= __('Artifact image') ?></h2>
                <figure>
                    <?= $this->Html->image($image, [
                        'alt' => __('Artifact photograph'),
                        'url' => $image,
                        'class' => 'w-100'
                    ]) ?>
                </figure>
            </div>
        </div>
    <?php endif; ?>

    <div class="col-lg-6">
        <div class="boxed">
            <div class="cycle-tab">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="cycle-tab-item" style="flex: initial;">
                        <a class="nav-link<?= $tab === 'atf' ? ' active' : '' ?>"
                            id="atf-tab" data-toggle="tab" href="#atf"
                            role="tab" aria-controls="atf" aria-selected="true">
                            <?= __('Transliterations') ?>
                        </a>
                    </li>
                    <li class="cycle-tab-item" style="flex: initial;">
                        <a class="nav-link<?= $tab === 'conll' ? ' active' : '' ?>"
                            id="conll-tab" data-toggle="tab" href="#conll"
                            role="tab" aria-controls="conll" aria-selected="false">
                            <?= __('Annotations') ?>
                        </a>
                    </li>
                </ul>
            </div>

            <div class="tab-content">
                <div class="tab-pane<?= $tab === 'atf' ? ' show active' : '' ?>"
                    id="atf" role="tabpanel" aria-labelledby="atf-tab">
                    <?= $this->Form->create($inscription, [
                        'type' => 'file',
                        'url' => ['controller' => 'Inscriptions', 'action' => 'add'],
                        'class' => 'pt-2'
                    ]) ?>
                        <p>
                            This form is for submitting changes to transliterations
                            of texts, in the C-ATF format. You can use the
                            <?= $this->Html->link('Uqnu editor', '/uqnu' , ['class' => 'link']) ?>
                            to check and edit your ATF before submitting it.
                        </p>

                        <?= $this->Form->control('atf', [
                            'name' => 'atf_paste',
                            'label' => __('ATF'),
                            'class' => 'form-control',
                            'style' => 'width: 100%;',
                            'type' => 'textarea',
                            'rows' => '20'
                        ]) ?>

                        <?= $this->Form->submit('Change atf', ['class' => 'btn cdli-btn-blue']) ?>
                    <?= $this->Form->end(); ?>
                </div>
                <div class="tab-pane<?= $tab === 'conll' ? ' show active' : '' ?>"
                    id="conll" role="tabpanel" aria-labelledby="conll-tab">
                    <?= $this->Form->create($inscription, [
                        'type' => 'file',
                        'url' => ['controller' => 'Inscriptions', 'action' => 'add'],
                        'class' => 'pt-2'
                    ]) ?>
                        <p>
                            This form is for submitting changes to annotations
                            of texts, in the CDLI-CoNLL format. You can use the
                            <?= $this->Html->link('Morphology Pre-Annotation Tool', [
                                'controller' => 'Resources',
                                'action' => 'checkCdliConll'
                            ]) ?>
                            to check and edit your CDLI-CoNLL before submitting it.
                        </p>

                        <?php if (!$inscription->is_atf2conll_diff_resolved): ?>
                            <div class="alert alert-warning">
                                This annotation was changed automatically to match a new
                                version of the transliteration, but has not yet been
                                reviewed since.
                                <?php if ($isAdmin): ?>
                                    <?= $this->Html->link(
                                        __('Review this annotation.'),
                                        [
                                            'prefix' => 'Admin',
                                            'controller' => 'inscriptions',
                                            'action' => 'view',
                                            $inscription->id
                                        ]
                                    ) ?>
                                <?php endif; ?>
                            </div>
                        <?php endif; ?>

                        <?= $this->Form->control('annotation', [
                            'name' => 'conll_paste',
                            'class' => 'form-control',
                            'label' => __('CDLI-CoNLL'),
                            'style' => 'width: 100%;',
                            'type' => 'textarea',
                            'rows' => '20'
                        ]) ?>
                        <?= $this->Form->submit('Change annotation', ['class' => 'btn cdli-btn-blue']) ?>
                    <?= $this->Form->end(); ?>
                </div>
            </div>
        </div>
    </div>

    <?php if (!is_null($artifact)): ?>
        <div class="col-lg-12">
            <?= $this->cell('ArtifactView', [$artifact->id])->render('display') ?>
        </div>
    <?php endif; ?>
</div>
