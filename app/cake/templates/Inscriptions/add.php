<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Inscription $inscription
 */

$this->assign('title', 'Add text or annotation');
$this->Form->setTemplates([
    'inputContainer' => '<div class="input form-group {{type}}{{required}}">{{content}}</div>'
]);
?>
<div class="row justify-content-md-center">
    <div class="col-lg-7 boxed">
        <h2><?= __('Add or edit transliteration(s), transcription(s), and translation(s)') ?></h2>

        <p>
            <?php if (!$canSubmitEdits): ?>
                You need to register an account to submit addition or edition
                suggestions to a CDLI editor for review. Please email cdli-support
                (at) orinst.ox.ac.uk to activate your crowdsourcing privilege.
            <?php endif; ?>
        </p>

        <div class="cycle-tab">
            <ul class="nav nav-tabs" role="tablist">
                <li class="cycle-tab-item" style="flex: initial;">
                    <a class="nav-link active" id="atf-tab" data-toggle="tab" href="#atf"
                       role="tab" aria-controls="atf" aria-selected="true">
                        <?= __('Transliterations') ?>
                    </a>
                </li>
                <li class="cycle-tab-item" style="flex: initial;">
                    <a class="nav-link" id="conll-tab" data-toggle="tab" href="#conll"
                       role="tab" aria-controls="conll" aria-selected="false">
                        <?= __('Annotations') ?>
                    </a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane show active" id="atf" role="tabpanel" aria-labelledby="atf-tab">
                    <?= $this->Form->create(null, [
                        'type' => 'file',
                        'url' => ['controller' => 'Inscriptions', 'action' => 'add'],
                        'class' => 'pt-2'
                    ]) ?>
                        <p>
                            This form is for submitting additions and changes to
                            transliterations of texts, in the C-ATF format.
                        </p>

                        <p>
                            Uploaded files and pasted text can contain multiple ATF entries.
                            The associated artifact will be derived from the first line of
                            each ATF entry. You cannot submit ATF for artifacts that are not
                            in the catalogue.
                        </p>

                        <p>
                            You can use the
                            <?= $this->Html->link('Uqnu editor', '/uqnu' , ['class' => 'link']) ?>
                            to check and edit your ATF before submitting it.
                        </p>

                        <?= $this->Form->control('atf_paste', [
                            'class' => 'form-control',
                            'label' => __('Paste text'),
                            'style' => 'width: 100%;',
                            'type' => 'textarea'
                        ]) ?>

                        <?= $this->Form->control('atf_file', [
                            'class' => 'form-control',
                            'label' => __('Upload file'),
                            'type' => 'file'
                        ]) ?>

                        <?= $this->Form->submit('Proceed', ['class' => 'btn cdli-btn-blue']) ?>
                    <?= $this->Form->end(); ?>
                </div>
                <div class="tab-pane" id="conll" role="tabpanel" aria-labelledby="conll-tab">
                    <?= $this->Form->create(null, [
                        'type' => 'file',
                        'url' => ['controller' => 'Inscriptions', 'action' => 'add'],
                        'class' => 'pt-2'
                    ]) ?>
                        <p>
                            This form is for submitting additions and changes to
                            annotations of texts, in the CDLI-CoNLL format.
                        </p>

                        <p>
                            Uploaded files and pasted text can contain multiple CDLI-CoNLL entries.
                            The associated artifact will be derived from the first line of
                            each CDLI-CoNLL entry. You cannot submit CDLI-CoNLL for artifacts
                            that are not in the catalogue. You also cannot submit CDLI-CoNLL
                            for artifacts without transliterations.
                        </p>

                        <p>
                            You can use the
                            <?= $this->Html->link('Morphology Pre-Annotation Tool', [
                                'controller' => 'Resources',
                                'action' => 'checkCdliConll'
                            ]) ?>
                            to check and edit your CDLI-CoNLL before submitting it.
                        </p>

                        <?= $this->Form->control('conll_paste', [
                            'class' => 'form-control',
                            'label' => __('Paste text'),
                            'style' => 'width: 100%;',
                            'type' => 'textarea'
                        ]) ?>

                        <?= $this->Form->control('conll_file', [
                            'class' => 'form-control',
                            'label' => __('Upload file'),
                            'type' => 'file'
                        ]) ?>
                        <?= $this->Form->submit('Proceed', ['class' => 'btn cdli-btn-blue']) ?>
                    <?= $this->Form->end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
