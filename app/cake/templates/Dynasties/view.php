<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Dynasty $dynasty
 */
?>
<?php use Cake\Utility\Inflector; ?>

<div>
    <div class="text-heading text-left"><?= __(h(Inflector::humanize(Inflector::singularize(Inflector::tableize($this->request->getParam('controller')))))) ?>: <?= h($dynasty->dynasty) ?>
      <?php if ($access_granted): ?>
          <?= $this->Html->link(
              __('Edit'),
              ['prefix' => 'Admin', 'action' => 'edit', $dynasty->id],
              ['escape' => false , 'class' => "btn cdli-btn-blue pull-right", 'title' => 'Edit']
          ) ?>
      <?php endif; ?>
    </div>
</div>
<div class="text-left mt-4" style="font-size: large;">
    <ul>
        <li><?= __('Polity') ?>: <?= h($dynasty->polity) ?></li>
        <li><?= __('Dynasty') ?>: <?= h($dynasty->dynasty) ?></li>
        <li><?= __('Provenience') ?>: <?= $dynasty->has('provenience') ? $this->Html->link($dynasty->provenience->id, ['controller' => 'Proveniences', 'action' => 'view', $dynasty->provenience->id]) : '' ?></li>
    </ul>
</div>

<?php  if (!empty($dynasty->dates)): ?>
    <div class="single-entity-wrapper mx-0">
        <div class="capital-heading"><?= __('Related Dates') ?></div>
        <table cellpadding="0" cellspacing="0" class="table-bootstrap">
            <thead>
                <th scope="col"><?= __('Day No') ?></th>
                <th scope="col"><?= __('Day Remarks') ?></th>
                <th scope="col"><?= __('Month Id') ?></th>
                <th scope="col"><?= __('Is Uncertain') ?></th>
                <th scope="col"><?= __('Month No') ?></th>
                <th scope="col"><?= __('Year Id') ?></th>
                <th scope="col"><?= __('Dynasty Id') ?></th>
                <th scope="col"><?= __('Ruler Id') ?></th>
                <th scope="col"><?= __('Absolute Year') ?></th>
            </thead>
            <tbody>
                <?php foreach ($dynasty->dates as $dates): ?>
                <tr>
                    <td><?= h($dates->day_no) ?></td>
                    <td><?= h($dates->day_remarks) ?></td>
                    <td><?= h($dates->month_id) ?></td>
                    <td><?= h($dates->is_uncertain) ?></td>
                    <td><?= h($dates->month_no) ?></td>
                    <td><?= h($dates->year_id) ?></td>
                    <td><?= h($dates->dynasty_id) ?></td>
                    <td><?= h($dates->ruler_id) ?></td>
                    <td><?= h($dates->absolute_year) ?></td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?php endif; ?>

<?php if (!empty($dynasty->rulers)): ?>
    <div class="boxed mx-0">
        <div class="capital-heading"><?= __('Related Rulers') ?></div>
        <table cellpadding="0" cellspacing="0" class="table-bootstrap">
            <thead>
                <th scope="col"><?= __('Sequence') ?></th>
                <th scope="col"><?= __('Ruler') ?></th>
                <th scope="col"><?= __('Period Id') ?></th>
                <th scope="col"><?= __('Dynasty Id') ?></th>
            </thead>
            <tbody>
                <?php foreach ($dynasty->rulers as $rulers): ?>
                <tr>
                    <td><?= h($rulers->sequence) ?></td>
                    <td><?= h($rulers->ruler) ?></td>
                    <td><?= h($rulers->period_id) ?></td>
                    <td><?= h($rulers->dynasty_id) ?></td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?php endif; ?>

<?= $this->element('citeButton'); ?>
<?= $this->element('citeBottom'); ?>
