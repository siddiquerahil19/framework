<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Dynasty[]|\Cake\Collection\CollectionInterface $dynasties
 */
?>

<div class="d-flex justify-content-between align-items-end mb-2">
    <h1 class="display-3 header-txt text-left"><?= __('Dynasties') ?></h1>
    <?= $this->element('addButton'); ?>
</div>
<div class="text-left my-2">
    <?= $this->element('entityExport'); ?>
</div>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead align="left">
        <tr>
            <th scope="col"><?= $this->Paginator->sort('polity') ?></th>
            <th scope="col"><?= $this->Paginator->sort('dynasty') ?></th>
            <th scope="col"><?= $this->Paginator->sort('sequence') ?></th>
            <th scope="col"><?= $this->Paginator->sort('provenience_id') ?></th>
            <?php if ($access_granted): ?>
                <th scope="col"><?= __('Action') ?></th>
            <?php endif ?>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($dynasties as $dynasty): ?>
            <tr align="left">
                <td><?= h($dynasty->polity) ?></td>
                <td>
                    <?= $this->Html->link(
                        $dynasty->dynasty,
                        ['controller' => 'Dynasties', 'action' => 'view', $dynasty->id]
                    ) ?>
                </td>
                <td><?= $this->Number->format($dynasty->sequence) ?></td>
                <td>
                    <?php if ($dynasty->has('provenience')): ?>
                        <?= $this->Html->link(
                            $dynasty->provenience->provenience,
                            ['controller' => 'Proveniences', 'action' => 'view', $dynasty->provenience->id]
                        ) ?>
                    <?php endif; ?>
                </td>
                <td>
                <?php if ($access_granted): ?>
                    <?= $this->Html->link(
                        __('Edit'),
                        ['prefix' => 'Admin', 'action' => 'edit', $dynasty->id],
                        ['escape' => false , 'class' => 'btn btn-warning btn-sm', 'title' => 'Edit']
                    ) ?>
                <?php endif ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>

<?= $this->element('citeButton'); ?>
<?= $this->element('citeBottom'); ?>