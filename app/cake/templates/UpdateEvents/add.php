<?php
/**
 * @var \App\View\AppView $this
 */
$this->Form->setTemplates([
    'inputContainer' => '<div class="input form-group {{type}}{{required}}">{{content}}</div>'
]);
$this->set('title', __('Add information to changes'));
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($event) ?>
            <legend class="capital-heading"><?= __('Add information to changes') ?></legend>
            <p>This form can be used to provide credit for the submitted changes.</p>

            <?php
                $updateTypeOptions = [
                    ['text' => 'Transliteration', 'value' => 'atf'],
                    ['text' => 'Translation', 'value' => 'translation'],
                    ['text' => 'Annotation', 'value' => 'annotation'],
                    ['text' => 'Metadata', 'value' => 'artifact']
                ];
                $selectedOption = array_search($event->update_type, array_column($updateTypeOptions, 'value'));
                $updateTypeOptions[$selectedOption]['selected'] = true;
            ?>
            <div class="form-group">
                <?= $this->Form->control('update_type', [
                    'class' => 'form-control',
                    'options' => $updateTypeOptions,
                    'val' => $event->update_type,
                    // Not checked on server side, only for user convenience
                    'disabled' => true
                ]) ?>
            </div>
            <div class="form-group">
                <?= $this->Form->control('event_comments', [
                    'class' => 'form-control'
                ]) ?>
                <small class="form-text text-muted">Add a comment to summarise the changes.</small>
            </div>
            <div class="form-group">
                <?= $this->Form->control('external_resource_id', [
                    'class' => 'form-control',
                    'options' => $external_resources,
                    'empty' => true
                ]) ?>
                <small class="form-text text-muted">Credit a contributing project.</small>
            </div>
            <div class="form-group">
                <?= $this->element('entityPicker', [
                    'model' => 'Authors',
                    'entities' => $event->authors
                ]) ?>
                <small class="form-text text-muted">Credit any additional authors.</small>
            </div>

            <hr>

            <div class="submit">
                <?= $this->Form->button(__('Submit'), [
                    'class' => 'btn cdli-btn-blue',
                    'name' => 'action',
                    'value' => 'submitted'
                ]) ?>
                <?= $this->Form->button(__('Save as draft'), [
                    'class' => 'btn cdli-btn-light',
                    'name' => 'action',
                    'value' => 'created'
                ]) ?>
                <?= $this->Html->link(__('Cancel'), ['action' => 'cancel'], [
                    'class' => 'btn cdli-btn-light float-right'
                ]) ?>
            </div>
        <?= $this->Form->end() ?>
    </div>

    <div class="col-lg-12 boxed">
        <?php if ($event->update_type == 'artifact' && !empty($event->artifacts_updates)) {
            echo $this->element('artifactUpdateDiffStat', ['updateEvent' => $event]);
        } elseif ($event->update_type == 'visual_annotation' && !empty($event->artifact_asset_annotations)) {
            echo $this->element('annotationDiffStat', ['updateEvent' => $event]);
        } elseif (!empty($event->inscriptions)) {
            echo $this->element('inscriptionDiffStat', ['updateEvent' => $event]);
        } ?>
    </div>
</div>
