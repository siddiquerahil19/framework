<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Journal[]|\Cake\Collection\CollectionInterface $journals
 */

$title = [
    'cdlb' => 'Cuneiform Digital Library Bulletin',
    'cdlj' => 'Cuneiform Digital Library Journal',
    'cdln' => 'Cuneiform Digital Library Notes',
    'cdlp' => 'Cuneiform Digital Library Preprints'
][$type];

$this->assign('title', $title);
?>

<div>
    <div class="row">
        <div class="col-9 text-left">
            <h3 class="display-4 pt-3 text-left pl-0">
                <?= h($title) ?>
            </h3>
        </div>

        <div class="col-12 text-left">
            <?php if ($type == 'cdlj') :?>
                <p class="text-justify page-summary-text mt-4">
                    The Cuneiform Digital Library Journal is an electronic journal
                    constituted in conjunction with the organization and work of
                    the Cuneiform Digital Library Initiative to afford contributors
                    to that effort the opportunity to make known to an international
                    community the results of their research into topics related
                    to those of the CDLI.
                </p>
                <p class="text-justify page-summary-text mt-4">
                    The CDLJ is a refereed e-journal for Assyriology. We are
                    interested in publishing a broad and international range
                    of cuneiform research articles that will appeal to academic
                    researchers as well as interested members of the public.
                </p>
                <p class="text-justify page-summary-text mt-4">
                    Contributions dealing with the major themes of the Cuneiform
                    Digital Library Initiative, that is, with text analyses of
                    4th and 3rd millennium documents (incorporating text, photographs,
                    data, drawings, interpretations), early language, writing,
                    paleography, administrative history, mathematics, metrology,
                    and the technology of modern cuneiform editing are welcome.
                    Articles in the Cuneiform Digital Library Journal are chosen
                    for their quality academic content and for their use of the
                    electronic medium.
                </p>
                <p class="text-justify page-summary-text mt-4">
                    For more information on this journal, visit the
                    <?= $this->Html->link(h('CDLJ and CDLB information page.'),
                    ['controller' => 'Postings', 'action' => 'view', '197']) ?>
                </p>
            <?php elseif ($type == 'cdlb') :?>
                <p class="text-justify page-summary-text mt-4">
                    The Cuneiform Digital Library Bulletin is an electronic
                    journal constituted in conjunction with the organization
                    and work of the Cuneiform Digital Library Initiative to
                    afford contributors to that effort the opportunity to make
                    known to an international community the results of their
                    research into topics related to those of the CDLI.
                </p>
                <p class="text-justify page-summary-text mt-4">
                    The CDLB is a refereed e-journal for Assyriology and is conceived
                    as a sister publication of the Cuneiform Digital Library Journal.
                    While the latter journal seeks substantive contributions dealing
                    with the major themes of the Cuneiform Digital Library Initiative,
                    that is, with text analyses of 4th and 3rd millennium documents
                    (incorporating text, photographs, data, drawings, interpretations),
                    early language, writing, paleography, administrative history,
                    mathematics, metrology, and the technology of modern cuneiform
                    editing are welcome, articles in the Bulletin should be short
                    notes of at most five pages that deal with specific topics,
                    collations, etc., and do not attempt to offer synthetic
                    treatments of complex subjects.
                <p class="text-justify page-summary-text mt-4">
                    For more information on this journal, visit the
                    <?= $this->Html->link(h('CDLJ and CDLB information page.'),
                    ['controller' => 'Postings', 'action' => 'view', '197']) ?>
                </p>
            <?php elseif ($type == 'cdlp') :?>
                <p class="text-justify page-summary-text mt-4">
                    CDLI is pleased to present here the results of research
                    in progress submitted, for inclusion in a preprint series
                    hosted by the project, by experts in fields associated with
                    Assyriology and Ancient Near Eastern Archaeology.
                </p>
                <p class="text-justify page-summary-text mt-4">
                    For more information on this journal, visit the
                    <?= $this->Html->link(h('CDLP information page.'),
                    ['controller' => 'Postings', 'action' => 'view', '198']) ?>
                </p>
            <?php else :?>
                <p class="text-justify page-summary-text mt-4">
                    Cuneiform Digital Library Notes is a non-profit, electronic
                    bulletin board of published notes for cuneiform studies.
                    We have set ourselves the task of publishing in this online
                    journal short notices of a high academic standard which also
                    try to utilize the potential of electronic publication.
                    For more information on this journal, visit the
                    <?= $this->Html->link(h('CDLN information page.'),
                    ['controller' => 'Postings', 'action' => 'view', '199']) ?>
                </p>
            <?php endif ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <table class="table-bootstrap my-3">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Author(s)</th>
                    <th>Title</th>
                    <th>Date</th>
                    <?php if ($access_granted): ?>
                        <th><?= __('Action') ?></th>
                    <?php endif ?>
                </tr>
            </thead>
            <tbody align="left">
                <?php foreach ($articles as $article): ?>
                    <tr>
                        <td><?= h($article->getArticleNumber()) ?></td>

                        <td class="w-25">
                            <?php foreach ($article->authors as $author): ?>
                                <?= $this->Html->link(
                                    h($author->author),
                                    ['controller' => 'Authors', 'action' => 'view', $author->id]
                                ) . ($author == end($article['authors']) ? '' : '; ') ?>
                            <?php endforeach; ?>
                        </td>

                        <td class="w-50">
                            <?= $this->Html->link(strip_tags($article->title, '<sup><sub><i>'), [
                                'controller' => 'Articles',
                                'action' => 'view',
                                'type' => $article->article_type,
                                'id' => $article->getArticleNumberPath()
                            ], ['escapeTitle' => false]) ?>
                            <?php if ($article->article_status != 3): ?>
                                <span class="text-uppercase p-2 badge badge-secondary">
                                    unpublished draft
                                </span>
                            <?php endif;?>
                        </td>

                        <td><?= date('Y-m-d', strtotime($article['created'])) ?></td>

                        <?php if ($access_granted): ?>
                            <td>
                                <?= $this->Html->image(
                                    '/images/edit.svg',
                                    [
                                        'alt' => __('Edit article'),
                                        'url' => [
                                            'prefix' => 'Admin',
                                            'controller' => 'Articles',
                                            'action' => 'edit',
                                            'type' => $article->article_type,
                                            'article' => $article->id
                                        ]
                                    ]
                                ) ?>
                            </td>
                        <?php endif ?>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<?= $this->element('citeBottom'); ?>
<?= $this->element('citeButton'); ?>
