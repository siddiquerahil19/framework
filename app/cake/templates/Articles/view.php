<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Article $article
 */

use Cake\ORM\TableRegistry;
use Cake\Routing\Router;

$journals = [
    'cdlj' => ['title' => 'Cuneiform Digital Library Journal', 'ISSN' => '1540-8779'],
    'cdlb' => ['title' => 'Cuneiform Digital Library Bulletin', 'ISSN' => '1540-8760'],
    'cdlp' => ['title' => 'Cuneiform Digital Library Preprints', 'ISSN' => null],
    'cdln' => ['title' => 'Cuneiform Digital Library Notes', 'ISSN' => '1546-6566']
];
$journal = $journals[$article->article_type];

$this->assign('title', $article->title);
?>

<?php $this->append('meta'); ?>
    <meta name="citation_title" content="<?= $article->title ?>">
    <?php foreach ($article->authors as $author): ?>
        <meta name="citation_author" content="<?= $author->first ?> <?= $author->last ?>">
        <?php if (!empty($author->_joinData['institution'])): ?>
            <meta name="citation_author_institution" content="<?= $author->_joinData->institution ?>">
        <?php elseif (!empty($author->institution)): ?>
            <meta name="citation_author_institution" content="<?= $author->institution ?>">
        <?php endif; ?>
        <?php if (!empty($author->orcid_id)): ?>
            <meta name="citation_author_orcid" content="<?= $author->orcid_id ?>">
        <?php endif; ?>
    <?php endforeach; ?>
    <meta name="citation_publication_date" content="<?= date('Y/m/d', strtotime($article['created'])) ?>">
    <meta name="citation_journal_title" content="<?= $journal['title'] ?>">
    <?php if (!is_null($journal['ISSN'])): ?>
        <meta name="citation_issn" content="<?= $journal['ISSN'] ?>">
    <?php endif; ?>
    <?php if ($article->article_type === 'cdlp'): ?>
        <meta name="citation_issue" content="<?= $article->article_no ?>">
    <?php else: ?>
        <meta name="citation_volume" content="<?= $article->year ?>">
        <meta name="citation_issue" content="<?= $article->article_no ?>">
    <?php endif; ?>
    <meta name="citation_publisher" content="Cuneiform Digital Library Initiative">
    <meta name="citation_pdf_url" content="<?= Router::url([$article->article_type, $article->getArticleNumberPath(), '_ext' => 'pdf'], true) ?>">
<?php $this->end(); ?>

<div class="row justify-content-md-center text-left">
    <div class="col-lg-8" id="article_single_view">
        <!-- Title -->
        <h2>
            <?php if ($access_granted): ?>
                <?= $this->Html->link(
                    __('Edit Article'),
                    [
                        'prefix' => 'Admin',
                        'controller' => 'Articles',
                        'action' => 'edit',
                        'type' => $article->article_type,
                        'article' => $article->id
                    ],
                    ['class' => 'd-inline p-2 btn cdli-btn-blue mx-1 float-right']
                ) ?>
            <?php endif; ?>
            <?= strip_tags($article->title, '<sup><sub><i>') ?>
        </h2>
        <!-- Article info -->
        <div id="article_info" class="row">
            <div class="col-8 text-left">
                <?php if ($article->article_status != 3): ?>
                    <p class="mb-1">
                        <span class="text-uppercase p-2 badge badge-secondary">
                            unpublished draft
                        </span>
                    </p>
                <?php endif;?>
                <p class="mb-0">
                    <?= strtoupper($article->article_type) ?> <?= $article->year ?>:<?= $article->article_no ?>
                </p>
                <p class="mb-0">
                    <?php if ($article->article_type == 'cdln') : ?>
                        <?= $this->Html->link($journal['title'], ['controller' => 'articles', 'type' => 'cdln']) ?>
                    <?php elseif ($article->article_type == 'cdlb') :  ?>
                        <?= $this->Html->link($journal['title'], ['controller' => 'articles', 'type' => 'cdlb']) ?>
                    <?php elseif ($article->article_type == 'cdlj') :  ?>
                        <?= $this->Html->link($journal['title'], ['controller' => 'articles', 'type' => 'cdlj']) ?>
                    <?php else : ?>
                        <?= $this->Html->link($journal['title'], ['controller' => 'articles', 'type' => 'cdlp']) ?>
                    <?php endif; ?>
                    <?php if ($journal['title']  && $journal['ISSN'] != null): ?>
                        (ISSN: <?= $journal['ISSN'] ?>)
                    <?php endif; ?>
                </p>
                <p class="mb-0">Published on <?= date('Y-m-d', strtotime($article['created'])); ?></p>
                <p class="mb-0">
                    <span class="font-italic">
                        <?php if (date('Y-m-d', strtotime($article['created'])) > '2021-08-31'): ?>
                            &copy; The Authors
                        <?php else: ?>
                            &copy; Cuneiform Digital Library Initiative
                        <?php endif; ?>
                    </span>
                </p>
                <p class="mb-0">
                    <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">
                        <img alt="Creative Commons License"
                             style="border-width: 0;"
                             src="https://i.creativecommons.org/l/by/4.0/80x15.png" />
                    </a>
                    This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a> except when noted otherwise and in the case of artifact images which follow the CDLI <a href="/terms-of-use">terms of use</a>.
                </p>
            </div>

            <!-- Author info -->
            <div class="col-4 text-right">
                <?php foreach ($article->authors as $author): ?>
                    <div class="mb-3">
                        <p class="mb-0">
                            <?php $name = explode(',', $author->author); ?>
                            <?php if (!empty($name[1]) && !empty($name[0])): ?>
                                <?= $this->Html->link(
                                    $name[1] . ' ' . $name[0],
                                    ['controller' => 'Authors', 'action' => 'view', $author->id]
                                ) ?>
                            <?php else: ?>
                                <?= $this->Html->link(
                                    $author['author'],
                                    ['controller' => 'Authors', 'action' => 'view', $author->id]
                                ) ?>
                            <?php endif; ?>
                            <?php if (!empty($author->orcid_id)): ?>
                                <?= $this->Orcid->format($author->orcid_id, 'inline') ?>
                            <?php endif; ?>
                        </p>
                        <?php if (!empty($author->_joinData['email'])): ?>
                            <p class="mb-0 font-italic font-weight-light"><?= $author->_joinData->email ?></p>
                        <?php elseif (!empty($author->email)): ?>
                            <p class="mb-0 font-italic font-weight-light"><?= $author->email ?></p>
                        <?php endif; ?>
                        <?php if (!empty($author->_joinData['institution'])): ?>
                            <p class="mb-0 font-italic font-weight-light"><?= $author->_joinData->institution ?></p>
                        <?php elseif (!empty($author->institution)): ?>
                            <p class="mb-0 font-italic font-weight-light"><?= $author->institution ?></p>
                        <?php endif; ?>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>

        <!-- Content -->
        <!-- mb-3 d-flex align-items-baseline -->
        <ul class="nav nav-tabs" role="tablist" id="article_tabs">
            <li class="nav-item" role="presentation">
                <button class="nav-link active" id="content-tab" data-toggle="tab" data-target="#content"
                        type="button" role="tab" aria-controls="content" aria-selected="true">Article</a>
            </li>
            <?php if (!empty($article->review_rounds)): ?>
                <li class="nav-item" role="presentation">
                    <button class="nav-link" id="reviews-tab" data-toggle="tab" data-target="#reviews"
                            type="button" role="tab" aria-controls="reviews" aria-selected="false">Reviews</a>
                </li>
            <?php endif; ?>
            <?php if ($article->is_pdf_uploaded): ?>
                <li class="download_button">
                    <?= $this->Html->link(
                        '<span class="fa fa-download"></span> ' . __('Download PDF'),
                        [$article->article_type, $article->getArticleNumberPath(), '_ext' => 'pdf'],
                        [
                            'escapeTitle' => false,
                            'class' => 'btn btn-sm cdli-btn-light',
                            'role' => 'button',
                            'target' => '_blank'
                        ]
                    ) ?>
                </li>
            <?php endif; ?>
        </ul>
        <div class="tab-content">
            <div class="tab-pane show active" id="content" role="tabpanel" aria-labelledby="content-tab">
                <?= $article->content_html ?>
                <?= $this->element('citeBottom', ['data' => $article]); ?>
            </div>
            <div class="tab-pane" id="reviews" role="tabpanel" aria-labelledby="reviews-tab">
                <?php
                    $sequence = 0;
                    $file_revisions = glob("./webroot/pubs/ojs/1/articles/$article->submission_id/submission/review/revision/*.pdf");
                    $file_submissions = glob("./webroot/pubs/ojs/1/articles/$article->submission_id/submission/review/*.pdf");
                    $sequence_revision = 0;
                    $sequence_submission = 0;
                    $sequence_file = 0;
                ?>
                <?php foreach ($article->review_rounds as $review_round): ?>
                    <div>
                        <h2 class="review_hi">Review Round <?= $review_round->round ?></h2>
                        <div>
                            <p>
                                <?php
                                    $uploader_id = $ojs_file[$sequence_file]['submission_file']['uploader_user_id'] - 1;
                                    $date = explode('T', $ojs_file[$sequence_file]['submission_file']['date_uploaded']) ;
                                ?>
                                <span class="font-weight-bold">Author:</span>
                                <span>
                                    <?= h($user_ojs_first[$uploader_id]['setting_value']) ?>
                                    <?= h($user_ojs_last[$uploader_id]['setting_value']) ?>,
                                    <?= h($date[0]) ?>
                                </span>
                                <?php $sequence_file = $sequence_file + 1; ?>
                            </p>
                            <p class="review_fi">
                                <span class="fa fa-paperclip fi_icon"></span>
                                <?php if (!empty($file_submissions)): ?>
                                    <a href=""><?= basename($file_submissions[$sequence_submission]) ?></a>
                                    <?php $sequence_submission = $sequence_submission+1; ?>
                                <?php else: ?>
                                    <a href="">Submission.pdf</a>
                                <?php endif; ?>
                            </p>
                        </div>
                        <div>
                            <?php foreach ($review_round->review_assignments as $review_assignment): ?>
                                <?php $reviewer_id = $review_assignment->reviewer_id - 1; ?>
                                <p>
                                    <span class="font-weight-bold">Reviewer:</span>
                                    <span>
                                        <?= h($user_ojs_first[$reviewer_id]['setting_value']) ?>
                                        <?= h($user_ojs_last[$reviewer_id]['setting_value']) ?>,
                                        <?= h($review_assignment->date_completed) ?>
                                    </span>
                                </p>
                                <div class="comment more">
                                    <?= $submission_comment[$sequence]['comments']; ?>
                                    <?php $sequence = $sequence + 1; ?>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <div>
                            <p>
                                <?php
                                    $uploader_revision_id = $ojs_file[$sequence_file]['submission_file']['uploader_user_id'] - 1;
                                    $date = explode('T', $ojs_file[$sequence_file]['submission_file']['date_uploaded'])
                                ?>
                                <span class="font-weight-bold">Author:</span>
                                <span>
                                    <?= h($user_ojs_first[$uploader_revision_id]['setting_value']) ?>
                                    <?= h($user_ojs_last[$uploader_revision_id]['setting_value']) ?>,
                                    <?= h($date[0]) ?>
                                </span>
                                <?php $sequence_file = $sequence_file + 1; ?>
                            </p>
                            <p class="review_fi">
                                <span class="fa fa-paperclip fi_icon" style="color:blue"></span>
                                <?php if (!empty($file_revisions)): ?>
                                    <a href=""><?= basename($file_revisions[$sequence_revision]) ?></a>
                                    <?php $sequence_revision = $sequence_revision + 1; ?>
                                <?php else: ?>
                                    <a href="">Revision.pdf</a>
                                <?php endif; ?>
                            </p>
                        </div>
                    </div>
                <?php endforeach; ?>
                <div>
                    <div class="accordion journal-accordion">
                        <?= $this->Accordion->partOpen('Discussion', 'Discussion', 'h2') ?>
                            <div>
                                <?php foreach ($article->queries as $query):  ?>
                                    <?php if ($query->stage_id == '3'): ?>
                                        <?php foreach ($query->notes as $note):  ?>
                                            <?php if ($note->user_id != 1): ?>
                                                <div class="discuss">
                                                    <p>
                                                        <?php $note_author_id = $note->user_id - 1; ?>
                                                        <?php if (in_array($note->user_id, $reviewer)): ?>
                                                            <span class="font-weight-bold">Reviewer:</span>
                                                        <?php else: ?>
                                                            <span class="font-weight-bold">Author:</span>
                                                        <?php endif; ?>
                                                        <span>
                                                            <?= h($user_ojs_first[$note_author_id]['setting_value']) ?>
                                                            <?= h($user_ojs_last[$note_author_id]['setting_value']) ?>,
                                                            <?= $note->date_created ?>
                                                        </span>
                                                    </p>
                                                    <p>
                                                        <?= $note->contents ?>
                                                    </p>
                                                </div>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </div>
                        <?= $this->Accordion->partClose() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->element('citeButton', ['data' => $article]); ?>

<?php $this->append('script'); ?>
<script src="/assets/js/journals_dashboard.js"></script>
<?php $this->end(); ?>
