<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Ruler[]|\Cake\Collection\CollectionInterface $rulers
 */
?>

<div class="d-flex justify-content-between align-items-end mb-2">
    <h1 class="display-3 header-txt text-left"><?= __('Rulers') ?></h1>    
    <?= $this->element('addButton'); ?>   
</div>
<div class="text-left my-2">
    <?= $this->element('entityExport'); ?>
</div>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead align="left">
        <tr>
            <th scope="col"><?= $this->Paginator->sort('sequence') ?></th>
            <th scope="col"><?= $this->Paginator->sort('ruler') ?></th>
            <th scope="col"><?= $this->Paginator->sort('period_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('dynasty_id') ?></th>
            <?php if ($access_granted): ?>
                <th scope="col"><?= __('Action') ?></th>
            <?php endif ?>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($rulers as $ruler): ?>
            <tr align="left">
                <td><?= $this->Number->format($ruler->sequence) ?></td>
                <td>
                    <?= $this->Html->link(
                        $ruler->ruler,
                        ['controller' => 'Rulers', 'action' => 'view', $ruler->id]
                    ) ?>
                </td>
                <td>
                    <?php if ($ruler->has('period')): ?>
                        <?= $this->Html->link(
                            $ruler->period->period,
                            ['controller' => 'Periods', 'action' => 'view', $ruler->period->id]
                        ) ?>
                    <?php endif; ?>
                </td>
                <td>
                    <?php if ($ruler->has('dynasty')): ?>
                        <?= $this->Html->link(
                            $ruler->dynasty->dynasty,
                            ['controller' => 'Dynasties', 'action' => 'view', $ruler->dynasty->id]
                        ) ?>
                    <?php endif; ?>
                </td>
                <td>
                <?php if ($access_granted): ?>
                    <?= $this->Html->link(
                        __('Edit'),
                        ['prefix' => 'Admin', 'action' => 'edit', $ruler->id],
                        ['escape' => false , 'class' => 'btn btn-warning btn-sm', 'title' => 'Edit']
                    ) ?>
                <?php endif ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>

<?= $this->element('citeButton'); ?>
<?= $this->element('citeBottom'); ?>