<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Sponsor $sponsor
 */
 $files = glob(WWW_ROOT . 'files-up/images/sponsor-img/' . $sponsor->id . '.*');
 $image;
 if (count($files) > 0) {
     $image = '/files-up/images/sponsor-img/' . basename($files[0]);
 } else {
     $image = '/files-up/images/sponsor-img/sponsor.png';
 }
?>
<?php use Cake\Utility\Inflector; ?>

<div>
    <div class="text-heading text-left"><?= __(h(Inflector::humanize(Inflector::singularize(Inflector::tableize($this->request->getParam('controller')))))) ?>: <?= h($sponsor->sponsor) ?>
      <?php if ($access_granted): ?>
          <?= $this->Html->link(
              __('Edit'),
              ['prefix' => 'Admin', 'action' => 'edit', $sponsor->id],
              ['escape' => false , 'class' => "btn cdli-btn-blue pull-right", 'title' => 'Edit']
          ) ?>
      <?php endif; ?>
    </div>
    <div class="text-left">
        <em><?= h($sponsor->sponsor_type->sponsor_type) ?><br></em>
        <div class="mt-4"><?= html_entity_decode($sponsor->contribution); ?></div>
        <a href="<?= $sponsor->url; ?>">Visit <?= h($sponsor->sponsor); ?> 's home page.</a>
    </div>
</div>

<?= $this->element('citeBottom'); ?>
<?= $this->element('citeButton'); ?>
