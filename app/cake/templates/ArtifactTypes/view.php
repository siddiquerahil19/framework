<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactType $artifactType
 */

use Cake\Utility\Inflector;

$page_title = h(Inflector::humanize($artifactType->artifact_type) . " - " .Inflector::humanize(Inflector::singularize(Inflector::tableize($this->request->getParam('controller')))));
$this->assign('title', $page_title); ?>
<div class="w-100">
    <div class="w-100">
        <h1 class="text-heading text-left"><?=$page_title?></h1>
    </div>
    <?php if ($access_granted): ?>
        <div class="w-100 text-right">
            <?= $this->Html->link(
                __('Edit'),
                ['prefix' => 'Admin', 'action' => 'edit', $artifactType->id],
                ['escape' => false , 'class' => "btn cdli-btn-blue", 'title' => 'Edit']
            ) ?>
      </div>
    <?php endif; ?>
    <?php if ($artifactType->has('parent_artifact_type')): ?>
        <div class="single-entity-wrapper text-left mx-0">
            <h2 class="capital-heading"><?= __('Parent Artifact Type') ?></h2>
                <ul><li>
                    <?= $this->Html->link($artifactType->parent_artifact_type->artifact_type, ['controller' => 'ArtifactTypes', 'action' => 'view', $artifactType->parent_artifact_type->id]) ?>
                </ul></li>
        </div>
    <?php endif; ?>
    <?php if (!empty($artifactType->child_artifact_types)): ?>
        <div class="single-entity-wrapper text-left mx-0">
            <h2 class="capital-heading"><?= __('Children Artifact Types') ?></h2>
            <ul>
                <?php foreach ($artifactType->child_artifact_types as $childArtifactTypes): ?>
                    <li>
                        <?= $this->Html->link(
                            h($childArtifactTypes->artifact_type),
                            ['controller' => 'ArtifactTypes', 'action' => 'view', $childArtifactTypes->id]
                        ) ?>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>
    <?php if ($count != 0): ?>
        <div class="single-entity-wrapper text-left mx-0">
            <h2 class="capital-heading"><?= __('Related Artifacts') ?></h2>
            <p>
                <?php echo 'There are '.$count.' artifacts related to '.h($artifactType->artifact_type) ?>
                <br>
                <?= $this->Html->link(
                    __('Click here to view the artifacts'),
                    ['controller' => 'Search',
                    '?' => ['atype' => h($artifactType->artifact_type), 'advanced_search'=> 1]]
                ) ?>
            </p>
        </div>
    <?php endif; ?>
</div>
<?= $this->element('citeButton'); ?>
<?= $this->element('citeBottom'); ?>
