<?php
/**
 * @var \App\View\AppView $this
 */

$title = $asset->getDescription();
$artifactLink = $this->Html->link(
    "{$asset->artifact->designation} ({$asset->artifact->getCdliNumber()})",
    ['controller' => 'Artifacts', 'action' => 'view', $asset->artifact->id]
);
$title = "$title of $artifactLink";
$this->assign('title', strip_tags($title));
?>

<div class="row">
    <div class="col-12 boxed">
        <?php if ($isAdmin): ?>
            <?= $this->Html->link(
                __('Edit'),
                ['prefix' => 'Admin', 'controller' => 'ArtifactAssets', 'action' => 'edit', $asset->id],
                ['class' => 'btn cdli-btn-blue float-right']
            ) ?>
        <?php endif; ?>

        <h2 class="mb-3">
            <?= $title ?>
            <?php if (!$asset->is_public): ?>
                <span class="p-2 badge badge-secondary"><?= __('Private') ?></span>
            <?php endif; ?>
        </h2>

        <?= $this->cell('ArtifactAsset', [$asset->id]) ?>
    </div>
</div>

<div class="row">
    <div class="col-6">
        <div class="boxed">
            <h3><?= __('Visual assets of this artifact') ?></h3>

            <table class="table-bootstrap">
                <thead>
                    <tr>
                        <th><?= __('Type of visual asset') ?></th>
                        <th><?= __('Artifact aspect') ?></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($asset->artifact->artifact_assets as $relatedAsset): ?>
                        <tr <?= $relatedAsset->id === $asset->id ? 'class="font-weight-bold"' : '' ?>>
                            <td><?= h($relatedAsset->asset_type) ?></td>
                            <td><?= h($relatedAsset->artifact_aspect ?? '—') ?></td>
                            <td>
                                <?php if ($relatedAsset->id !== $asset->id): ?>
                                    <?= $this->Html->link(__('View'), [$relatedAsset->id]) ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>

    <div class="col-6">
        <div class="boxed">
            <h3><?= __('File information') ?></h3>

            <table class="table-bootstrap">
                <tbody>
                    <tr>
                        <th><?= __('File format') ?></th>
                        <td><?= h($asset->file_format) ?></td>
                    </tr>
                    <tr>
                        <th><?= __('File size') ?></th>
                        <td><?= $this->Number->format($asset->file_size / (1024 * 1024)) ?> MiB</td>
                    </tr>
                    <?php if ($asset->file_format === 'jpg'): ?>
                        <tr>
                            <th><?= __('Image size') ?></th>
                            <td><?= h($asset->image_width) ?>x<?= h($asset->image_height) ?></td>
                        </tr>
                        <tr>
                            <th><?= __('Image color') ?></th>
                            <td>
                                <div class="d-inline-block align-bottom" style="width: 1.5em; height: 1.5em; background-color: <?= h($asset->image_color_average) ?>"></div>
                                <?= h($asset->image_color_average) ?>
                                (<?= h($asset->image_color_depth) ?> bit)
                            </td>
                        </tr>
                    <?php endif; ?>
                </tbody>
            </table>

            <?php if ($asset->file_format === 'jpg'): ?>
                <?php $assetData = exif_read_data(WWW_ROOT . $asset->getPath(), null, true); ?>
                <?php foreach (['IFD0', 'EXIF'] as $section): ?>
                    <h4><?= $section ?></h4>
                    <table class="table-bootstrap" style="font-family: monospace;">
                        <tbody>
                            <?php foreach ($assetData[$section] as $key => $value): ?>
                                <tr>
                                    <th><?= h($key) ?></th>
                                    <td><?= h($value) ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
</div>
