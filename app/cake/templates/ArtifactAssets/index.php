<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactAsset[]|\Cake\Collection\CollectionInterface $artifactAssets
 */
$this->assign('title', 'Visual assets of artifact');
?>
<div class="row justify-content-md-center">
    <div class="col-12 boxed">
        <h3><?= __('Visual assets of artifact') ?></h3>
        <?= $this->element('entityExport') ?>

        <table class="table-bootstrap mt-3">
            <thead>
                <tr>
                    <th scope="col"><?= __('Image') ?></th>
                    <th scope="col"><?= __('Type of visual asset') ?></th>
                    <th scope="col"><?= __('Image size') ?></th>
                    <th scope="col"><?= __('Image color') ?></th>
                    <th scope="col"><?= __('File format') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('file_size', __('File size')) ?></th>
                    <th scope="col"><?= $this->Paginator->sort('created', __('Created')) ?></th>
                    <th scope="col"><?= __('Modified') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('artifact_id', __('Artifact')) ?></th>
                    <?php if ($isAdmin): ?>
                        <th scope="col"><?= __('Actions') ?></th>
                    <?php endif; ?>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($artifactAssets as $asset): ?>
                    <tr>
                        <td>
                            <?= $this->Html->link(__('View'), ['controller' => 'ArtifactAssets', 'action' => 'view', $asset->id]) ?>
                        </td>
                        <td>
                            <?= h($asset->asset_type) ?>
                            <?php if (!$asset->is_public): ?>
                                <span class="p-2 badge badge-secondary"><?= __('Private') ?></span>
                            <?php endif; ?>
                        </td>
                        <td>
                            <?php if ($asset->has('image_width')): ?>
                                <?=
                                    $this->Number->format($asset->image_width)
                                ?>x<?=
                                    $this->Number->format($asset->image_height)
                                ?>
                            <?php endif; ?>
                        </td>
                        <td>
                            <?php if ($asset->has('image_color_average')): ?>
                                <div class="d-inline-block align-bottom" style="width: 1.5em; height: 1.5em; background-color: <?= h($asset->image_color_average) ?>;" title="<?= h($asset->image_color_average) ?>"></div>
                                (<?= $asset->image_color_depth ?> bit)
                            <?php endif; ?>
                        </td>
                        <td><?= h($asset->file_format) ?></td>
                        <td><?= $this->Number->format($asset->file_size / (1024 * 1024)) ?> MiB</td>
                        <td><?= $asset->created ?></td>
                        <td><?= $asset->modified ?></td>
                        <td>
                            <?php if ($asset->has('artifact')): ?>
                                <?= $this->Html->link($asset->artifact->getCdliNumber(), [
                                    'prefix' => false,
                                    'controller' => 'Artifacts',
                                    'action' => 'view',
                                    $asset->artifact->id
                                ]) ?>
                            <?php endif; ?>
                            <?php if ($asset->has('artifact_aspect')): ?>
                                (<?= $asset->artifact_aspect ?>)
                            <?php endif; ?>
                        </td>
                        <?php if ($isAdmin): ?>
                            <td>
                                <?= $this->Html->link(__('Edit'), ['prefix' => 'Admin', 'controller' => 'ArtifactAssets', 'action' => 'edit', $asset->id]) ?>
                            </td>
                        <?php endif; ?>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<div class="row justify-content-md-center">
    <div class="col-lg-7 boxed">
        <?php echo $this->element('Paginator'); ?>
    </div>
</div>
