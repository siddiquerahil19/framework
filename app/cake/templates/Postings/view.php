<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Posting $posting
 */

$this->assign('title', $posting->title);

$image = $this->ArtifactAssets->getImage($posting->artifact);
if (!is_null($image)) {
    $this->assign('image', $image);
}
?>
<div class="row justify-content-md-center posting">
    <?php if ($posting->posting_type_id === 2): ?>
        <div class="col-10 p-4 text-left">
            <?php if ($canEdit) : ?>
                <?= $this->Html->link(
                    __('Edit'),
                    ['prefix' => 'Admin', 'controller' => 'Postings', 'action' => 'edit', $posting->id],
                    ['escape' => false, 'class' => 'btn cdli-btn-blue float-right']
                ) ?>
            <?php endif; ?>

            <h2><?= h($posting->title) ?></h2>

            <div class="mt-4 text-justify"><?= html_entity_decode($posting->body) ?></div><br/>

            <?= $this->element('citeBottom'); ?>
        </div>
    <?php else: ?>
        <?php if (!is_null($image)): ?>
            <div class="col-lg-5 p-5 left-panel">
                <img src="<?= $image ?>" alt="<?= $posting->title ?>" class="img-fluid">
            </div>
        <?php endif; ?>

        <div class="col-lg-7 p-5 right-panel text-left">
            <?php if ($canEdit) : ?>
                <?= $this->Html->link(
                    __('Edit'),
                    ['prefix' => 'Admin', 'controller' => 'Postings', 'action' => 'edit', $posting->id],
                    ['escape' => false, 'class' => 'btn cdli-btn-blue float-right']
                ) ?>
            <?php endif ?>

            <h2><?= h($posting->title) ?></h2>

            <div class="text-justify py-3">
                <?= html_entity_decode($posting->body) ?>
            </div>

            <?php if ($posting->posting_type_id === 1): ?>
                <p class="text-muted">
                    <span><?= date_format($posting->publish_start, 'F d, Y') ?></span>
                    <br>
                    <span>
                        <?= __('Posted by {0}', $this->Html->link(
                            $posting->creator->author,
                            ['controller' => 'authors', 'action' => 'view', $posting->created_by]
                        )) ?>
                    </span>
                </p>
            <?php elseif (!is_null($posting->artifact_id)): ?>
                <?= $this->Html->link(
                    'See the artifact',
                    ['controller' => 'Artifacts', 'action' => 'view', $posting->artifact_id],
                    ['class' => 'btn cdli-btn-blue']
                ) ?>
            <?php endif; ?>
        </div>
        <?= $this->element('citeBottom'); ?>
    <?php endif; ?>
</div>

<?= $this->element('citeButton'); ?>
