<main>
    <h2 class="text-left display-4 section-title">News</h2>
    <div class="row mt-5">
        <?php foreach ($newsarticles as $news): ?>
            <div class="card bs4-cdli-card col-sm-12 col-md-6 col-lg-4 mb-5">
                <img src="<?= $this->ArtifactAssets->getThumbnail($news->artifact) ?>"
                     alt="News <?= $news->title . ' ' . $news->image_type ?>"
                     class="card-img-top">
                <div class="card-body">
                    <p class="card-title">
                        <?= $this->Html->link(
                            strlen($news->title) > 38 ? substr($news->title, 0, 38) . '...' : $news->title,
                            ['controller' => 'Postings', 'action' => 'view', $news->id]
                        ) ?>
                    </p>
                    <?php if (!is_null($news->publish_start)): ?>
                        <p class="date-style"><?= $news->publish_start->i18nFormat('yyyy-MM-dd') ?></p>
                    <?php endif; ?>
                    <p class="card-text">
                        <?=
                        strlen(strip_tags($news->body)) > 100 ? substr(strip_tags($news->body), 0, 100) . '...' : strip_tags($news->body)
                        // use above to trim extra card-length
                        ?>
                    </p>
                </div>
            </div>
        <?php endforeach;?>
    </div>
</main>
<?php echo $this->element('smoothscroll'); ?>
