<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\MaterialAspect[]|\Cake\Collection\CollectionInterface $materialAspects
 */
?>

<div class="d-flex justify-content-between align-items-end mb-2">
    <h1 class="display-3 header-txt text-left"><?= __('Material Aspects') ?></h1>  
    <?= $this->element('addButton'); ?>    
</div>
<div class="text-left my-2">
    <?= $this->element('entityExport'); ?>
</div>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead align="left">
        <tr>
            <th scope="col"><?= $this->Paginator->sort('material_aspect') ?></th>
            <?php if ($access_granted): ?>
                <th scope="col"><?= __('Action') ?></th>
            <?php endif ?>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($materialAspects as $materialAspect): ?>
            <tr align="left">
                <td>
                    <?= $this->Html->link(
                        $materialAspect->material_aspect,
                        ['controller' => 'MaterialAspects', 'action' => 'view', $materialAspect->id]
                    ) ?>
                </td>
                <?php if ($access_granted): ?>
                    <td>
                        <?= $this->Html->link(
                            __('Edit'),
                            ['prefix' => 'Admin', 'action' => 'edit', $materialAspect->id],
                            ['escape' => false , 'class' => 'btn btn-warning btn-sm', 'title' => 'Edit']
                        ) ?>
                    </td>
                <?php endif ?>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>

<?= $this->element('citeButton'); ?>
<?= $this->element('citeBottom'); ?>