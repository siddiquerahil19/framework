<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsExternalResource[]|\Cake\Collection\CollectionInterface $artifactsExternalResources
 */
?>

<div class="d-flex justify-content-between align-items-end mb-2">
    <h1 class="display-3 header-txt text-left"><?= __('Artifacts External Resources') ?></h1>
</div>
<div class="text-left my-2">
    <?= $this->element('entityExport'); ?>
</div>
<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead>
        <tr>
            <th scope="col"><?= $this->Paginator->sort('artifact_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('external_resource_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('external_resource_key') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($artifactsExternalResources as $artifactsExternalResource): ?>
        <tr>
            <td><?= $artifactsExternalResource->has('artifact') ? $this->Html->link($artifactsExternalResource->artifact->designation, ['controller' => 'Artifacts', 'action' => 'view', $artifactsExternalResource->artifact->id]) : '' ?></td>
            <td><?= $artifactsExternalResource->has('external_resource') ? $this->Html->link($artifactsExternalResource->external_resource->external_resource, ['controller' => 'ExternalResources', 'action' => 'view', $artifactsExternalResource->external_resource->id]) : '' ?></td>
            <td><?= h($artifactsExternalResource->external_resource_key) ?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>

<?= $this->element('citeButton'); ?>
<?= $this->element('citeBottom'); ?>
