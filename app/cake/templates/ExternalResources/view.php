<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ExternalResource $externalResource
 */
?>

<div class="row justify-content-md-center">
    <div class="col-lg-7 boxed">
        <div class="capital-heading"><?= h($externalResource->external_resource) ?></div>

        <table class="table-bootstrap">
            <tbody>
                <tr>
                    <th scope="row"><?= __('External Resource') ?></th>
                    <td><?= h($externalResource->external_resource) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Base Url') ?></th>
                    <td><?= h($externalResource->base_url) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Project Url') ?></th>
                    <td><?= h($externalResource->project_url) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Abbrev') ?></th>
                    <td><?= h($externalResource->abbrev) ?></td>
                </tr>
            </tbody>
        </table>

        <div class=" row float-right">
            <?php if ($access_granted): ?>
                <?= $this->Html->link(
                    __('Edit'),
                    ['prefix' => 'Admin', 'action' => 'edit', $externalResource->id],
                    ['escape' => false , 'class' => 'btn btn-warning', 'title' => 'Edit']
                ) ?>
            <?php endif; ?>
        </div>
    </div>
</div>

<div class="boxed mx-0">
    <?php if ($count != 0): ?>
        <div class="capital-heading"><?= __('Related Artifacts') ?></div>
        <p>
            <?= $this->Html->link(
                __('Number of related artifacts - {0}', $count),
                ['controller' => 'Search', 'action' => 'index']
            ) ?>
        </p>
    <?php endif; ?>
</div>

<?= $this->element('citeButton'); ?>
<?= $this->element('citeBottom'); ?>