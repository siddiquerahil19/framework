<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ExternalResource[]|\Cake\Collection\CollectionInterface $externalResources
 */
?>
<div class="d-flex justify-content-between align-items-end mb-2">
    <h1 class="display-3 header-txt text-left"><?= __('External Resources') ?></h1>    
    <?= $this->element('addButton'); ?>   
</div>
<div class="text-left my-2">
    <?= $this->element('entityExport'); ?>
</div>
<br>
<?php $this->Grid->AlphabeticalHtmlView($abbrev_arr)?>

<?= $this->element('citeButton'); ?>
<?= $this->element('citeBottom'); ?>