<?php
    $sortBy = [
        'relevance' => 'Relevance'
    ];
    $publicationTypes = [
        "primary" => "Primary",
        "history" => "History",
        "citation" => "Citation",
        "collation" => "Collation",
        "other" => "Other"
    ];
?>
<div class="ads">
    <?= $this->Form->create(null, [
        'type'=>'POST',
    ]) ?>
        <div class="">
            <div class="d-flex align-items-center justify-content-between mb-3">
                <h1 class="display-3 text-left">Advanced Search</h1>
                <?= $this->Form->button('Search', [
                    'type' => 'submit',
                    'class' => 'btn cdli-btn-blue mt-1 mb-3 pl-10 pr-10 w-30 pull-right'
                ]); ?>
            </div>
            <div class="d-flex text-left mb-2">
                <?= $this->Html->link('Search settings', ['controller' => 'SearchSettings', 'action' => 'index'], ['class' => 'mr-5'])?>
                <?= $this->Html->link('Search Help', 'https://cdli-gh.github.io/guides/cdli_search.html')?>
            </div>
        </div>
    <!--    <div class="text-left my-5">
            <label>Sort By:</label>
            <?php /*
                echo $this->Form->select(
                        'sortBy',
                        $sortBy,
                        [
                        'class' => 'ads-sort-select'
                    ]
                    )
            */?>
        </div> -->
        <div class="layout-grid text-left">
            <div  class="grid-1">
                <div class="darken">
                    <div class="d-flex align-items-baseline justify-content-between">
                        <h2>Publication Data</h2>
                        <!-- <span class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" title="Tooltip on top"></span> -->
                    </div>
                    <?= $this->Form->control('pdesignation', [
                        'label' => 'Publication Designation',
                        'value' => isset($_GET['pdesignation']) ? $_GET['pdesignation'] : ''
                    ]); ?>
                    <?= $this->Form->control('authors', [
                        'label' => 'Author(s)',
                        'value' => isset($_GET['authors']) ? $_GET['authors'] : ''
                    ]); ?>
                    <?= $this->Form->control('editors', [
                        'label' => 'Editor(s)',
                        'value' => isset($_GET['editors']) ? $_GET['editors'] : ''
                    ]); ?>
                    <?= $this->Form->control('year', [
                        'label' => 'Publication Year',
                        'value' => isset($_GET['year']) ? $_GET['year'] : ''
                    ]); ?>
                    <?= $this->Form->control('title', [
                        'label' => 'Title',
                        'value' => isset($_GET['title']) ? $_GET['title'] : ''
                    ]); ?>
                    <?= $this->Form->control(
                        'ptype',
                        [
                            'options' => $publicationTypes,
                            'label' => 'Publication Type',
                            'empty' => 'Select a publication type',
                            'value' => isset($_GET['ptype']) ? $_GET['ptype'] : ''
                        ]
                    );?>
                    <?= $this->Form->control('publisher', [
                        'label' => 'Publisher',
                        'value' => isset($_GET['publisher']) ? $_GET['publisher'] : ''
                    ]); ?>
                    <?= $this->Form->control('series', [
                        'label' => 'Series',
                        'value' => isset($_GET['series']) ? $_GET['series'] : ''
                    ]); ?>
                    <?= $this->Form->control('designation_exactref', [
                        'label' => 'Exact Reference',
                        'value' => isset($_GET['designation_exactref']) ? $_GET['designation_exactref'] : ''
                    ]); ?>
                    <?= $this->Form->control('bibtexkey', [
                        'label' => 'Bibtex Key',
                        'value' => isset($_GET['bibtexkey']) ? $_GET['bibtexkey'] : ''
                    ]); ?>
                </div>
                <div class="darken">
                    <div class="d-flex align-items-baseline justify-content-between">
                        <h2>Artifact Data</h2>
                        <!-- <span class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" title="Tooltip on top"></span> -->
                    </div>
                    <?= $this->Form->control('atype', [
                        'label' => 'Artifact Type',
                        'value' => isset($_GET['atype']) ? $_GET['atype'] : ''
                    ]); ?>
                    <?= $this->Form->control('materials', [
                        'label' => 'Material',
                        'value' => isset($_GET['materials']) ? $_GET['materials'] : ''
                    ]); ?>
                    <?= $this->Form->control('collection', [
                        'label' => 'Collection',
                        'value' => isset($_GET['collection']) ? $_GET['collection'] : ''
                    ]); ?>
                    <?= $this->Form->control('provenience', [
                        'label' => 'Provenience',
                        'value' => isset($_GET['provenience']) ? $_GET['provenience'] : ''
                    ]); ?>
                    <?= $this->Form->control('written_in', [
                        'label' => 'Written in',
                        'value' => isset($_GET['written_in']) ? $_GET['written_in'] : ''
                    ]); ?>
                    <?= $this->Form->control('archive', [
                        'label' => 'Archive',
                        'value' => isset($_GET['archive']) ? $_GET['archive'] : ''
                    ]); ?>
                    <?= $this->Form->control('period', [
                        'label' => 'Period',
                        'value' => isset($_GET['period']) ? $_GET['period'] : ''
                    ]); ?>
                    <?= $this->Form->control('acomments', [
                        'label' => 'Artifact Comment',
                        'value' => isset($_GET['acomments']) ? $_GET['acomments'] : ''
                    ]); ?>
                    <?= $this->Form->control('dates', [
                        'label' => 'Dates',
                        'value' => isset($_GET['dates']) ? $_GET['dates'] : ''
                    ]); ?>
                </div>
            </div>
            <div class="grid-2">
                <div class="darken">
                    <div class="d-flex align-items-baseline justify-content-between">
                        <h2>Inscriptional data</h2>

                        <!-- <span class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" title="Tooltip on top"></span> -->
                    </div>
                    <?= $this->Form->control('translation', [
                        'label' => 'Translation',
                        'value' => isset($_GET['translation']) ? $_GET['translation'] : ''
                    ]); ?>
                    <?= $this->Form->control('transliteration', [
                        'label' => 'Transliteration',
                        'value' => isset($_GET['transliteration']) ? $_GET['transliteration'] : ''
                    ]); ?>
                    <?= $this->Form->control('transliteration_permutation', [
                        'label' => 'Transliteration Permutation (All sign readings per sign)',
                        'value' => isset($_GET['transliteration_permutation']) ? $_GET['transliteration_permutation'] : ''
                    ]); ?>
                    <?= $this->Form->control('icomments', [
                        'label' => 'Comment',
                        'value' => isset($_GET['icomments']) ? $_GET['icomments'] : ''
                    ]); ?>
                    <?= $this->Form->control('structure', [
                        'label' => 'ATF structure (eg. "edge")',
                        'value' => isset($_GET['structure']) ? $_GET['structure'] : ''
                    ]); ?>
                    <?= $this->Form->control('genres', [
                        'label' => 'Genre',
                        'value' => isset($_GET['genres']) ? $_GET['genres'] : ''
                    ]); ?>
                    <?= $this->Form->control('languages', [
                        'label' => 'Language',
                        'value' => isset($_GET['languages']) ? $_GET['languages'] : ''
                    ]); ?>
                </div>
                <div class="darken">
                    <div class="d-flex align-items-baseline justify-content-between">
                        <h2>Artifact Identification</h2>
                        <!-- <span class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" title="Tooltip on top"></span> -->
                    </div>
                    <?= $this->Form->control('adesignation', [
                        'label' => 'Designation',
                        'value' => isset($_GET['adesignation']) ? $_GET['adesignation'] : ''
                    ]); ?>
                    <?= $this->Form->control('museum_no', [
                        'label' => 'Museum Number (Collection no.)',
                        'value' => isset($_GET['museum_no']) ? $_GET['museum_no'] : ''
                    ]); ?>
                    <?= $this->Form->control('accession_no', [
                        'label' => 'Accession Number',
                        'value' => isset($_GET['accession_no']) ? $_GET['accession_no'] : ''
                    ]); ?>
                    <?= $this->Form->control('id', [
                        'label' => 'Artifact Number (P no.)',
                        'value' => isset($_GET['id']) ? $_GET['id'] : ''
                    ]); ?>
                    <?= $this->Form->control('seal_no', [
                        'label' => 'Seal Number  (S. no.)',
                        'value' => isset($_GET['seal_no']) ? $_GET['seal_no'] : ''
                    ]); ?>
                    <?= $this->Form->control('composite_no', [
                        'label' => 'Composite Number  (Q no.)',
                        'value' => isset($_GET['composite_no']) ? $_GET['composite_no'] : ''
                    ]); ?>
                </div>
                <div class="darken">
                    <div class="d-flex align-items-baseline justify-content-between">
                        <h2>Credits</h2>
                        <!-- <span class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" title="Tooltip on top"></span> -->
                    </div>
                    <?= $this->Form->control('credit', [
                        'label' => 'Author/Contributor',
                        'value' => isset($_GET['credit']) ? $_GET['credit'] : ''
                    ]); ?>
                    <?= $this->Form->control('project', [
                        'label' => 'Project',
                        'value' => isset($_GET['project']) ? $_GET['project'] : ''
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="d-flex mb-3 auto">
        <?= $this->Form->button('Search', [
            'type' => 'submit',
            'class' => 'btn cdli-btn-blue mt-1 mb-3 pl-10 pr-10 w-30 float-right'
        ]); ?>
    </div>
</div>
    <?= $this->Form->end()?>
<?php echo $this->element('smoothscroll'); ?>
