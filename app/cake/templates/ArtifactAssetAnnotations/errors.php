<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsUpdate $artifactsUpdate
 */
?>
<div class="row justify-content-md-center">
    <div class="col-lg-12 boxed">
        <h2><?= __('Encountered errors') ?></h2>

        <ul>
            <?php foreach ($errors as $error): ?>
                <li><?= h($error) ?></li>
            <?php endforeach; ?>
        </ul>

        <?= $this->Form->postLink(
            __('Cancel'),
            ['controller' => 'ArtifactAssetAnnotations', 'action' => 'add'],
            ['class' => 'btn cdli-btn-light']
        ) ?>
    </div>

    <div class="col-lg-12 boxed">
        <?php foreach ($annotations as $annotation): ?>
            <section>
                <?php if ($annotation->has('artifact_asset')): ?>
                    <h3><?= $annotation->artifact_asset->artifact->getCdliNumber() ?></h3>
                <?php else: ?>
                    <h3>(unknown)</h3>
                <?php endif; ?>

                <ul>
                    <?php foreach ($annotation->getErrors() as $field => $errors): ?>
                        <?php foreach ($errors as $error): ?>
                            <li>
                                <?= h($field) ?>:
                                <?php if (is_array($error)): ?>
                                    <?php foreach ($error as $subFields => $subErrors): ?>
                                        <?= h($subFields) ?>:
                                        <?php foreach ($subErrors as $subError): ?>
                                            <?= h($subError) ?>
                                        <?php endforeach; ?>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <?= h($error) ?>
                                <?php endif; ?>
                            </li>
                        <?php endforeach; ?>
                    <?php endforeach; ?>
                </ul>
            </section>
        <?php endforeach; ?>
        <?php echo $this->element('Paginator'); ?>
    </div>
</div>
