<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Inscription $inscription
 */

$this->assign('title', 'Add Image annotation');

?>
<div class="row justify-content-md-center">
    <div class="col-lg-7 boxed">
        <h2><?= __('Add Image Annotations') ?></h2>
        <p>
            <?php if (!$canSubmitEdits): ?>
                You need to register an account to submit addition or edition
                suggestions to a CDLI editor for review. Please email cdli-support
                (at) orinst.ox.ac.uk to activate your crowdsourcing privilege.
            <?php endif; ?>
        </p>

        <div class="cycle-tab">
            <div class="tab-content">
                <div class="tab-pane show active" id="anno" role="tabpanel" aria-labelledby="anno-tab">
                    <?= $this->Form->create(null, [
                        'type' => 'file',
                        'url' => ['controller' => 'ArtifactAssetAnnotations', 'action' => 'add'],
                        'class' => 'pt-2'
                    ]) ?>
                        <p>
                            This form is for submitting image annotations in W3C.JSON format.
                        </p>

                        <?= $this->Form->control('anno_file', [
                            'class' => 'form-control',
                            'label' => __('Upload file'),
                            'type' => 'file'
                        ]) ?>

                        <?= $this->Form->submit('Proceed', ['class' => 'btn cdli-btn-blue']) ?>
                    <?= $this->Form->end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
