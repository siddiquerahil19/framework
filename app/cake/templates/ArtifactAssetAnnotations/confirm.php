<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsUpdate $artifactsUpdate
 */

$this->set('Confirm Annotations');
?>

<div class="row justify-content-md-center">
    <div class="col-lg-7 boxed">
        <h2><?= __('Confirm changes') ?></h2>

        <hr>

        <p>
            Make sure the following changes are correct.
        </p>

        <?php if ($canSubmitEdits): ?>
            <?= $this->Html->link(
                __('Confirm'),
                ['controller' => 'UpdateEvents', 'action' => 'add', '?' => ['update_type' => 'visual_annotation']],
                ['class' => 'btn cdli-btn-blue']
            ) ?>
        <?php endif; ?>
        <?= $this->Form->postLink(
            __('Discard'),
            ['controller' => 'ArtifactAssetAnnotations', 'action' => 'add'],
            ['class' => 'btn cdli-btn-light', 'confirm' => __('This will discard your changes. Are you sure?')]
        ) ?>
    </div>
    <div class="col-lg-12 boxed">
        <div style="overflow-x: auto;">
            <table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
                <thead>
                    <tr>
                        <td>Artifact</td>
                        <td>Annotation image</td>
                        <td>Annotation body purpose</td>
                        <td>Annotation body value</td>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($annotations as $annotation): ?>
                        <tr>
                            <td rowspan="<?= count($annotation->bodies) ?>"><?= $this->Html->link(
                                $annotation->artifact_asset->artifact->getCdliNumber(),
                                ['controller' => 'Artifacts', 'action' => 'view', $annotation->artifact_asset->artifact->id]
                            ) ?></td>
                            <td rowspan="<?= count($annotation->bodies) ?>">
                                <figure>
                                    <?= $this->Html->image(DS . $annotation->artifact_asset->getPath(), [
                                        'alt' => 'Annotated image',
                                        'url' =>  $annotation->artifact_asset->getPath(),
                                        'class' => 'm-2',
                                        'style' => 'max-width: 100px; max-height: 100px;'
                                    ]) ?>
                                    <figcaption><?= h($annotation->artifact_asset->getDescription()) ?></figcaption>
                                </figure>
                            </td>
                            <td><?= $annotation->bodies[0]->purpose ?? '' ?></td>
                            <td><?= $annotation->bodies[0]->value ?? '' ?></td>
                        </tr>
                        <?php foreach (array_slice($annotation->bodies, 1) as $body): ?>
                            <tr>
                                <td><?= $body->purpose ?? '' ?></td>
                                <td><?= $body->value ?? '' ?></td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
