<?php
use Cake\Core\Configure;
use Cake\Error\Debugger;

$this->layout = 'default';

?>

<div class="text-center">
    <img src="/images/broken-tablet.svg" alt="<?= __('Drawing of a clay tablet broken into 6 pieces') ?>" />
    <h2 class="mt-4">Oops! Something went wrong loading this page</h2>
    <p>Please try again or contact a site administrator.</p>
    <a href="/">Go to the home page</a>

    <?php if (Configure::read('debug')): ?>
        <?php
            $this->layout = 'dev_error';

            $this->assign('title', $message);
            $this->assign('templateName', 'error500.ctp');
        ?>
        <?php $this->start('file'); ?>
        <?php if (!empty($error->queryString)): ?>
            <p class="notice">
                <strong>SQL Query: </strong>
                <?= h($error->queryString) ?>
            </p>
        <?php endif; ?>
        <?php if (!empty($error->params)): ?>
            <strong>SQL Query Params: </strong>
            <?php Debugger::dump($error->params) ?>
        <?php endif; ?>
        <?php if ($error instanceof Error) : ?>
            <strong>Error in: </strong>
            <?= sprintf('%s, line %s', str_replace(ROOT, 'ROOT', $error->getFile()), $error->getLine()) ?>
        <?php endif; ?>
        <?= $this->element('auto_table_warning') ?>
        <?php if (extension_loaded('xdebug')): ?>
            <?php xdebug_print_function_stack(); ?>
        <?php endif; ?>
        <?php $this->end(); ?>
    <?php endif; ?>
</div>
