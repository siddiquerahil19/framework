<?php

use \Cake\Utility\Inflector;
use \Cake\Routing\Router;

$this->assign('title', 'Search results');

$sortOptions = ['' => ''];
foreach ($sortFields as $field => $settings) {
    if ($settings[$layout]) {
        $sortOptions[$field] = $settings['label'];
    }
}
?>

<div class="row justify-content-center">
    <div class="col-lg-9 col-12">
        <h1 class="display-3 text-left">
            <?= __('Search the CDLI collection')?>
        </h1>
    </div>

    <div class="col-lg-9 col-12 boxed">
        <?php if ($query['type'] === 'simple'): ?>
            <?= $this->element('simpleSearch', ['query' => $query['data']]) ?>
        <?php else: ?>
            <?= $this->Form->create(['data' => $query['data'], 'schema' => []], ['type' => 'GET', 'url' => ['action' => 'view', 'advanced']]) ?>
                <table class="table">
                    <thead>
                        <tr>
                            <th colspan="2"><?= __('Search parameters') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($query['data'] as $field => $value): ?>
                            <tr>
                                <th>
                                    <label for="advanced-field-<?= h($field) ?>" class="mb-0 py-2">
                                        <?= Inflector::humanize($field) ?>
                                    </label>
                                </th>
                                <td>
                                    <?= $this->Form->control($field, [
                                        'id' => 'advanced-field-' . $field,
                                        'val' => $value,
                                        'label' => false,
                                        'type' => str_starts_with($field, 'atf_transliteration_') ? 'toggle' : 'text',
                                        'hiddenField' => false,
                                        'class' => 'form-control w-100',
                                        'readonly' => true
                                    ]) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>

                <div class="float-right">
                    <?= $this->Html->link(
                        __('Simple search'),
                        ['controller' => 'NewSearch', 'action' => 'index'],
                        ['class' => 'btn mr-3']
                    ) ?>

                    <?= $this->Html->link(
                        __('Search settings'),
                        ['controller' => 'NewSearch', 'action' => 'view', 'settings'],
                        ['class' => 'btn mr-3']
                    ) ?>

                    <?= $this->Form->button(__('Modify advanced search'), ['class' => 'btn mr-3']) ?>
                </div>
            <?= $this->Form->end() ?>
        <?php endif; ?>
    </div>
</div>

<div class="text-muted">
    Showing <?= count($results) ?> entries
    of <?= h($metrics['count']['value']) ?><?= $metrics['count']['relation'] === 'gte' ? '+' : '' ?> results
    found in <?= $this->Number->format($metrics['duration'] / 1000, ['after' => ' s']) ?>
</div>

<hr class="line" />

<div class="row text-left py-1">
    <div class="col-md-6 col-12">
        <button type="button" class="btn cdli-btn-blue my-1" data-toggle="modal" data-target="#search-modal-share">
            <?= __('Share') ?>
            <span class="fa fa-share-alt ml-1" aria-hidden="true"></span>
        </button>
        <button type="button" class="btn cdli-btn-blue my-1" data-toggle="modal" data-target="#search-modal-export">
            <?= __('Export') ?>
            <span class="fa fa-download ml-1" aria-hidden="true"></span>
        </button>
        <div class="dropdown d-inline-block mr-3 my-1">
            <button class="btn cdli-btn-blue dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
                <?= __('More') ?>
            </button>
            <div class="dropdown-menu">
                <?php
                    $languageCounts = $filterBuckets['language']['buckets'];
                    $languageCounts = array_combine(array_column($languageCounts, 'key'), array_column($languageCounts, 'doc_count'));
                ?>
                <?php if (array_key_exists('Sumerian', $languageCounts) && $languageCounts['Sumerian'] > 0): ?>
                    <?= $this->Html->link(
                        __('Commodity visualization') . ' <span class="fa fa-external-link" aria-hidden="true"></span>',
                        $this->Url->build(['action' => 'view', 'commodity-viz']) . '?' . $this->getRequest()->getUri()->getQuery(),
                        ['class' => 'dropdown-item', 'escapeTitle' => false, 'target' => '_blank']
                    ) ?>
                <?php endif; ?>

                <?= $this->Html->link(
                    __('Uqnu ATF editor') . ' <span class="fa fa-external-link" aria-hidden="true"></span>',
                    '/uqnu',
                    ['class' => 'dropdown-item', 'escapeTitle' => false, 'target' => '_blank']
                ) ?>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-12 text-md-right">
        <?php if ($layout === 'full'): ?>
            <?= $this->Form->create(null, ['type' => 'GET', 'class' => 'd-inline-block mr-2', 'id' => 'search-sort-form']) ?>
                <?= $this->Form->hideQueryParameters($this->getRequest()->getUri()->getQuery()) ?>
                <label for="order[0]" class="sr-only"><?= __('Sort by') ?></label>
                <?= $this->Form->control('order[0]', [
                    'label' => false,
                    'type' => 'select',
                    'options' => $sortOptions,
                    'value' => isset($query['order'][0]) ? $query['order'][0] : null,
                    'class' => 'form-control',
                    'templates' => ['inputContainer' => '<div class="input {{type}}{{required}} d-inline-block align-middle">{{content}}</div>']
                ]) ?>
                <button type="submit" class="btn cdli-btn-blue d-inline-block"><?= __('Sort') ?></button>
            <?= $this->Form->end() ?>
        <?php endif; ?>

        <div class="d-inline-block">
            <?= $this->Html->link(
                '<span class="fa fa-th-large" aria-hidden="true"></span>',
                ['?' => ['layout' => 'full'] + $this->getRequest()->getQueryParams()],
                [
                    'class' => 'btn border-dark my-1 mr-2 ' . ($layout === 'full' ? 'bg-dark text-white' : 'bg-white text-dark'),
                    'style' => 'font-size: 1.5em;',
                    'aria-label' => 'Toggle view to card layout',
                    'escapeTitle' => false,
                    'title' => 'Card layout'
                ]
            ) ?>
            <?= $this->Html->link(
                '<span class="fa fa-bars" aria-hidden="true"></span>',
                ['?' => ['layout' => 'compact'] + $this->getRequest()->getQueryParams()],
                [
                    'class' => 'btn border-dark my-1 ' . ($layout === 'compact' ? 'bg-dark text-white' : 'bg-white text-dark'),
                    'style' => 'font-size: 1.5em;',
                    'aria-label' => 'Toggle view to table layout',
                    'escapeTitle' => false,
                    'title' => 'Table layout'
                ]
            ) ?>
        </div>
    </div>
</div>

<div class="row text-left">
    <!-- Filters -->
    <?php $filterStatus = $searchSettings[$layout . ':sidebar'] ? 'd-lg-block d-none' : 'd-none'; ?>
    <div id="search-filters" class="col-lg-3 col-12 pt-3 <?= $filterStatus ?>">
        <h3>
            <?= __('Filters') ?>

            <button id="search-filter-collapse" class="btn bg-white float-right mx-1">
                <span class="h3 m-0">
                    <span class="fa fa-chevron-left" aria-hidden="true"></span>
                </span>
                <span class="sr-only"><?= __('Close filters') ?></span>
            </button>
        </h3>

        <?= $this->Form->create(null, ['type' => 'GET']) ?>
            <?= $this->Form->hideQueryParameters($this->getRequest()->getUri()->getQuery(), '/^f\[/') ?>
            <?= $this->Form->submit(__('Apply'), ['class' => 'btn cdli-btn-blue mt-3']) ?>

            <?= $this->Form->button(__('Expand all'), [
                'id' => 'search-filter-expand-all',
                'class' => 'btn mt-3 d-none'
                ]) ?>
            <?= $this->Form->button(__('Collapse all'), [
                'id' => 'search-filter-collapse-all',
                'class' => 'btn mt-3 d-none'
            ]) ?>

            <div class="accordion">
                <?php foreach (array_unique(array_column($filters, 'section')) as $label): ?>
                    <h4 class="mt-3"><?= h(ucfirst($label)) ?></h4>

                    <?php foreach (array_keys(array_column($filters, 'section'), $label) as $field): ?>
                        <?php
                            $field = array_keys($filters)[$field];
                            $inUse = array_key_exists($field, $query['filters']);
                            if (array_key_exists("filter:$field", $searchSettings) && !$searchSettings["filter:$field"]) {
                                continue;
                            }
                        ?>
                        <?= $this->Accordion->partOpen($field . '-filter', $filters[$field]['label'], 'p', $inUse) ?>
                            <?php foreach ($filterBuckets[$field]['buckets'] as $index => $bucket): ?>
                                <?= $this->Form->control('f[' . $field . '][]', [
                                    'id' => $field . '-' . $index,
                                    'label' => '<span class="text-muted float-right">' .
                                        $this->Number->format($bucket['doc_count']) .
                                        '</span>&nbsp;' . h($bucket['key'] ?? $index),
                                    'templates' => ['nestingLabel' => '{{hidden}}<label class="w-100 mb-0"{{attrs}}>{{input}}{{text}}</label>'],
                                    'type' => 'checkbox',
                                    'hiddenField' => false,
                                    'checked' => array_key_exists($field, $query['filters']) &&
                                        in_array($bucket['key'] ?? $index, $query['filters'][$field], true),
                                    'value' => h($bucket['key'] ?? $index),
                                    'escape' => false
                                ]) ?>
                            <?php endforeach; ?>
                        <?= $this->Accordion->partClose() ?>
                    <?php endforeach; ?>
                <?php endforeach; ?>
            </div>

            <?= $this->Form->submit(__('Apply'), ['class' => 'btn cdli-btn-blue mt-3']) ?>
        <?= $this->Form->end() ?>
    </div>

    <!-- Results -->
    <?php $filterStatus = $searchSettings[$layout . ':sidebar'] ? 'col-lg-9 col-12' : 'col-12'; ?>
    <div id="search-results" class="<?= $filterStatus ?>">
        <div class="clearfix">
            <?php $filterButtonStatus = $searchSettings[$layout . ':sidebar'] ? 'd-lg-none d-inline' : 'd-inline'; ?>
            <?= $this->Html->link(
                __('Filters') . ' <span class="fa fa-chevron-right ml-1 align-middle" aria-hidden="true"></span>',
                '#',
                [
                    'id' => 'search-filter-open',
                    'class' => 'float-md-left my-3 btn cdli-btn-blue ' . $filterButtonStatus,
                    'escapeTitle' => false
                ]
            ) ?>

            <div class="my-3 float-md-right text-center">
                <div class="d-inline-block">
                    <?= $this->element('paginatorSimple') ?>
                </div>
            </div>
        </div>

        <?php if ($layout === 'full'): ?>
            <!-- Card layout -->
            <?php foreach (array_column($results, '_source') as $result): ?>
                <div class="search-card row mb-5 mx-0">
                    <!-- Media -->
                    <?php if (!is_null($result['asset']) && count($result['asset'])): ?>
                        <?php
                        $assets = [];
                        foreach ($result['asset'] as $asset) {
                            if (!$access['asset'] && !$asset['is_public']) {
                                continue;
                            }
                            $assets[] = $this->ArtifactAssets->newEntityFromData([
                                'asset_type' => $asset['type'],
                                'file_format' => $asset['file_format'],
                                'artifact_aspect' => $asset['artifact_aspect'],
                                'artifact_id' => $result['id']
                            ]);
                        }
                        $mainImage = $this->ArtifactAssets->orderAssets($assets)[0];
                        $thumbnail = $this->ArtifactAssets->getThumbnail([$mainImage]);
                        $showingLineart = $mainImage->asset_type === 'lineart' && !is_null($mainImage->getThumbnailPath());
                        ?>
                        <div class="search-card-media col-md-5 col-12 <?= $showingLineart ? 'bg-white border border-dark' : '' ?>">
                            <div class="search-media">
                                <a href="/<?= $mainImage->getPath() ?>">
                                    <figure>
                                        <?= $this->Html->image($thumbnail, ['alt' => 'Artifact photograph']) ?>
                                        <figcaption>
                                            View full image
                                            <span class="fa fa-share-square-o" aria-hidden="true"></span>
                                        </figcaption>
                                    </figure>
                                </a>

                                <?php foreach ($assets as $asset): ?>
                                    <?php if ($asset === $mainImage) { continue; } ?>
                                    <p class="text-center">
                                        <?= $this->Html->link('View ' . $asset->getDescription(), [
                                            'controller' => 'ArtifactAssets',
                                            'action' => 'view',
                                            $asset->id
                                        ]) ?>
                                    </p>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    <?php else: ?>
                        <div class="search-card-media col-md-5 col-12">
                            <div class="search-media">
                                <figure>
                                    <?= $this->Html->image($this->ArtifactAssets->getThumbnail([]), ['alt' => 'No image available']) ?>
                                </figure>
                            </div>
                        </div>
                    <?php endif; ?>

                    <!-- Content -->
                    <div class="search-card-content col-md-7 col-12 px-4 py-5 text-left border">
                        <!-- Header -->
                        <h2 class="d-flex">
                            <?= $this->Html->link(
                                $result['designation'] . ' (P' . str_pad($result['id'], 6, '0', STR_PAD_LEFT) . ')',
                                ['controller' => 'Artifacts', 'action' => 'view', $result['id']],
                                ['class' => 'display-5', 'target' => '_blank']
                            ) ?>
                            <?php if ($canSubmitEdits): ?>
                                <div class="dropdown d-inline-block ml-3">
                                    <button class="btn btn-light dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
                                        <?= __('Edit') ?>
                                    </button>
                                    <div class="dropdown-menu">
                                        <?= $this->Html->link(
                                            'Edit Metadata',
                                            ['controller' => 'Artifacts', 'action' => 'edit', $result['id']],
                                            ['class' => 'dropdown-item']
                                        ) ?>
                                        <?php if (!empty($result['atf'])): ?>
                                            <?= $this->Html->link(
                                                'Edit Text or Annotation',
                                                ['controller' => 'Inscriptions', 'action' => 'edit', $result['inscription_id']],
                                                ['class' => 'dropdown-item']
                                            ) ?>
                                        <?php else: ?>
                                            <?= $this->Html->link(
                                                'Add Text',
                                                ['controller' => 'Inscriptions', 'action' => 'add', '?' => ['artifact' => $result['id']]],
                                                ['class' => 'dropdown-item']
                                            ) ?>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </h2>

                        <!-- Other info -->
                        <p class="my-3 text-uppercase">
                            <?php if ($result['seal_no']): ?>
                                <span class="p-2 badge badge-secondary"><?= __('Seal') ?></span>
                            <?php endif; ?>
                            <?php if ($result['composite_no']): ?>
                                <span class="p-2 badge badge-secondary"><?= __('Composite') ?></span>
                            <?php endif; ?>
                            <?php if ($result['impression_seal_no']): ?>
                                <span class="p-2 badge badge-secondary"><?= __('Has seal impression(s)') ?></span>
                            <?php endif; ?>
                            <?php if ($result['witness_composite_no']): ?>
                                <span class="p-2 badge badge-secondary"><?= __('Witness') ?></span>
                            <?php endif; ?>
                            <?php if ($result['annotation'] && !$result['is_atf2conll_diff_resolved']): ?>
                                <span class="p-2 badge badge-secondary"><?= __('Annotation unreviewed') ?></span>
                            <?php endif; ?>
                        </p>

                        <?php if ($result['seal_no']): ?>
                            <p class="my-0"><b>Seal No.:</b> <?= $result['seal_no'] ?></p>
                        <?php endif; ?>
                        <?php if ($result['composite_no']): ?>
                            <p class="my-0"><b>Composite No.:</b> <?= $result['composite_no'] ?></p>
                        <?php endif; ?>

                        <?php if ($result['impression_seal_no']): ?>
                            <p class="my-0"><b>Impressed with seal(s):</b> <?= implode(', ', $result['impression_seal_no']) ?></p>
                        <?php endif; ?>
                        <?php if ($result['witness_composite_no']): ?>
                            <p class="my-0"><b>Witness to composite(s):</b> <?= implode(', ', $result['witness_composite_no']) ?></p>
                        <?php endif; ?>

                        <?php if ($searchSettings['full:field:publication']): ?>
                            <p class="my-0">
                                <b>Primary Publication:</b>
                                <?php $primaryIndex = array_search('primary', array_column($result['publication'], 'type')); ?>
                                <?php if ($primaryIndex !== false): ?>
                                    <?= implode('; ', $result['publication'][$primaryIndex]['authors'] ?? []) ?>
                                    <?=
                                        ' (' . $result['publication'][$primaryIndex]['year'] . ') ' .
                                        ($result['publication'][$primaryIndex]['exact_reference'] ??
                                         $result['publication'][$primaryIndex]['designation'])
                                    ?>
                                <?php endif; ?>
                            </p>
                        <?php endif; ?>

                        <?php if ($searchSettings['full:field:collection']): ?>
                            <p class="my-0"><b>Collection:</b> <?= $result['collection'] ? implode('; ', $result['collection']) : '&mdash;' ?> </p>
                        <?php endif; ?>

                        <?php if ($searchSettings['full:field:museum_no']): ?>
                            <p class="my-0"><b>Museum no.:</b> <?= $result['museum_no'] ?? '&mdash;' ?></p>
                        <?php endif; ?>

                        <?php if ($searchSettings['full:field:provenience']): ?>
                            <p class="my-0"><b>Provenience:</b> <?= $result['provenience'] ?? '&mdash;' ?></p>
                        <?php endif; ?>

                        <?php if ($searchSettings['full:field:period']): ?>
                            <p class="my-0"><b>Period:</b> <?= $result['period'] ?? '&mdash;' ?></p>
                        <?php endif; ?>

                        <?php if ($searchSettings['full:field:artifact_type']): ?>
                            <p class="my-0"><b>Object Type:</b> <?= $result['artifact_type'] ?? '&mdash;' ?></p>
                        <?php endif; ?>

                        <?php if ($searchSettings['full:field:material']): ?>
                            <p class="my-0"><b>Material:</b> <?= $result['material'] ? implode('; ', $result['material']) : '&mdash;' ?></p>
                        <?php endif; ?>

                        <?php if ($searchSettings['full:field:dates_referenced'] && !empty($result['dates_referenced'])): ?>
                            <p class="my-0"><b>Date:</b> <?= $result['dates_referenced'] ?? '&mdash;' ?></p>
                        <?php endif; ?>

                        <?php if ($searchSettings['full:field:atf'] && !empty($result['atf']) && ($result['is_atf_public'] || $access['atf'])): ?>
                            <p class="mt-3">
                                <b>Transliteration:</b>
                                <br>
                                <?= $this->Atf->format($result['atf'], $query['highlight']) ?>
                            </p>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php else: ?>
            <!-- Table layout -->

            <?= $this->Form->create(null, ['type' => 'GET', 'class' => 'boxed m-0 mb-3 px-3 py-2', 'id' => 'search-sort-form']) ?>
                <?= $this->Form->hideQueryParameters($this->getRequest()->getUri()->getQuery()) ?>
                <?php
                    $sortTemplates = [
                        'inputContainer' => '<div class="input form-group mx-3 my-1 d-inline-block {{type}}{{required}}">{{content}}</div>'
                    ]
                ?>
                <b><?= __('Sort options') ?>:</b>
                <?= $this->Form->control('order[0]', [
                    'label' => '<span class="sr-only">' . __('Sort field') . '</span> 1.',
                    'escape' => false,
                    'type' => 'select',
                    'options' => $sortOptions,
                    'templates' => $sortTemplates,
                    'value' => isset($query['order'][0]) ? $query['order'][0] : null,
                    'class' => 'form-control d-inline-block mx-3'
                ]) ?>
                <?= $this->Form->control('order[1]', [
                    'label' => '<span class="sr-only">' . __('Sort field') . '</span> 2.',
                    'escape' => false,
                    'type' => 'select',
                    'options' => $sortOptions,
                    'templates' => $sortTemplates,
                    'value' => isset($query['order'][1]) ? $query['order'][1] : null,
                    'class' => 'form-control d-inline-block mx-3'
                ]) ?>
                <?= $this->Form->control('order[2]', [
                    'label' => '<span class="sr-only">' . __('Sort field') . '</span> 3.',
                    'escape' => false,
                    'type' => 'select',
                    'options' => $sortOptions,
                    'templates' => $sortTemplates,
                    'value' => isset($query['order'][2]) ? $query['order'][2] : null,
                    'class' => 'form-control d-inline-block mx-3'
                ]) ?>
                <button type="submit" class="btn cdli-btn-blue d-inline-block"><?= __('Sort') ?></button>
            <?= $this->Form->end() ?>

            <div id="search-results-compact">
                <div class="table-responsive mb-5 mx-0">
                    <table class="table text-nowrap">
                        <thead>
                            <tr>
                                <th class="position-sticky bg-white"><?= __('Artifact') ?></th>

                                <?php if ($searchSettings['compact:field:publication']): ?>
                                    <th><?= __('Primary Publication') ?></th>
                                <?php endif; ?>

                                <?php if ($searchSettings['compact:field:collection']): ?>
                                    <th><?= __('Collection(s)') ?></th>
                                <?php endif; ?>

                                <?php if ($searchSettings['compact:field:museum_no']): ?>
                                    <th><?= __('Museum Number') ?></th>
                                <?php endif; ?>

                                <?php if ($searchSettings['compact:field:genre']): ?>
                                    <th><?= __('Genre(s)') ?></th>
                                <?php endif; ?>

                                <?php if ($searchSettings['compact:field:period']): ?>
                                    <th><?= __('Period') ?></th>
                                <?php endif; ?>

                                <?php if ($searchSettings['compact:field:provenience']): ?>
                                    <th><?= __('Provenience') ?></th>
                                <?php endif; ?>

                                <?php if ($searchSettings['compact:field:dates_referenced']): ?>
                                    <th><?= __('Dates referenced') ?></th>
                                <?php endif; ?>

                                <?php if ($searchSettings['compact:field:artifact_type']): ?>
                                    <th><?= __('Artifact type') ?></th>
                                <?php endif; ?>

                                <?php if ($searchSettings['compact:field:material']): ?>
                                    <th><?= __('Material(s)') ?></th>
                                <?php endif; ?>

                                <?php if ($searchSettings['compact:field:language']): ?>
                                    <th><?= __('Language(s)') ?></th>
                                <?php endif; ?>

                                <?php if ($searchSettings['compact:field:accession_no']): ?>
                                    <th><?= __('Accession Number') ?></th>
                                <?php endif; ?>

                                <?php if ($searchSettings['compact:field:written_in']): ?>
                                    <th><?= __('Written in') ?></th>
                                <?php endif; ?>

                                <?php if ($searchSettings['compact:field:excavation_no']): ?>
                                    <th><?= __('Excavation Number') ?></th>
                                <?php endif; ?>

                                <?php if ($searchSettings['compact:field:archive']): ?>
                                    <th><?= __('Archive(s)') ?></th>
                                <?php endif; ?>

                                <?php if (count($query['highlight'])): ?>
                                    <th><?= __('Transliteration') ?></th>
                                <?php endif; ?>
                            </tr>
                        </thead>
                        <tbody class="border-right">
                            <?php foreach (array_column($results, '_source') as $result): ?>
                                <tr>
                                    <th class="position-sticky border-right bg-white">
                                        <?= $this->Html->link(
                                        'P' . str_pad($result['id'], 6, '0', STR_PAD_LEFT),
                                        ['controller' => 'Artifacts', 'action' => 'view', $result['id']],
                                        ['target' => '_blank']
                                        ) ?>
                                    </th>

                                    <?php if ($searchSettings['compact:field:publication']): ?>
                                        <?php $primaryIndex = array_search('primary', array_column($result['publication'], 'type')); ?>
                                        <?php if ($primaryIndex !== false): ?>
                                            <td>
                                                <?= $result['publication'][$primaryIndex]['exact_reference'] ??
                                                $result['publication'][$primaryIndex]['designation'] ?>
                                            </td>
                                        <?php else: ?>
                                            <td></td>
                                        <?php endif; ?>
                                    <?php endif; ?>

                                    <?php if ($searchSettings['compact:field:collection']): ?>
                                        <td><?= $result['collection'] ? h(implode('; ', $result['collection'])) : '' ?></td>
                                    <?php endif; ?>

                                    <?php if ($searchSettings['compact:field:museum_no']): ?>
                                        <td><?= h($result['museum_no']) ?></td>
                                    <?php endif; ?>

                                    <?php if ($searchSettings['compact:field:genre']): ?>
                                        <td><?= $result['genre'] ? h(implode('; ', $result['genre'])) : '' ?></td>
                                    <?php endif; ?>

                                    <?php if ($searchSettings['compact:field:period']): ?>
                                        <td><?= h($result['period']) ?></td>
                                    <?php endif; ?>

                                    <?php if ($searchSettings['compact:field:provenience']): ?>
                                        <td><?= h($result['provenience']) ?></td>
                                    <?php endif; ?>

                                    <?php if ($searchSettings['compact:field:dates_referenced']): ?>
                                        <td><?= h($result['dates_referenced']) ?></td>
                                    <?php endif; ?>

                                    <?php if ($searchSettings['compact:field:artifact_type']): ?>
                                        <td><?= h($result['artifact_type']) ?></td>
                                    <?php endif; ?>

                                    <?php if ($searchSettings['compact:field:material']): ?>
                                        <td><?= $result['material'] ? h(implode('; ', $result['material'])) : '' ?></td>
                                    <?php endif; ?>

                                    <?php if ($searchSettings['compact:field:language']): ?>
                                        <td><?= $result['language'] ? h(implode('; ', $result['language'])) : '' ?></td>
                                    <?php endif; ?>

                                    <?php if ($searchSettings['compact:field:accession_no']): ?>
                                        <td><?= h($result['accession_no']) ?></td>
                                    <?php endif; ?>

                                    <?php if ($searchSettings['compact:field:written_in']): ?>
                                        <td><?= h($result['written_in']) ?></td>
                                    <?php endif; ?>

                                    <?php if ($searchSettings['compact:field:excavation_no']): ?>
                                        <td><?= h($result['excavation_no']) ?></td>
                                    <?php endif; ?>

                                    <?php if ($searchSettings['compact:field:archive']): ?>
                                        <td><?= $result['archive'] ? h(implode('; ', $result['archive'])) : '' ?></td>
                                    <?php endif; ?>

                                    <?php if (count($query['highlight'])): ?>
                                        <td><?= $this->Atf->formatSnippet($result['atf'], $query['highlight']) ?></td>
                                    <?php endif; ?>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        <?php endif; ?>

        <?= $this->element('Paginator') ?>
    </div>
</div>

<div class="row">
    <div class="text-muted text-left">
        <?= __('Results per page') ?>:
        <?php foreach ([10, 25, 100, 500, 1000] as $limit): ?>
            <?= $this->Html->link($limit,
                $this->Paginator->generateUrlParams(['limit' => $limit]),
                ['class' => 'btn bg-white text-black view-btn mx-1' . ($this->Paginator->param('perPage') === $limit ? ' border-dark' : '')]
            ) ?>
        <?php endforeach; ?>
    </div>
</div>

<?php echo $this->element('smoothscroll'); ?>

<?php $this->append('modal'); ?>
<div class="modal" id="search-modal-share" tabindex="-1" role="dialog" aria-labelledby="search-modal-share-title" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="search-modal-share-title"><?= __('Share this search') ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <p><?= __('Copy this web address containing your search and your filter parameters, and share it so others can see the artifacts resulting from this search.') ?></p>

                <?= $this->Form->control('search-modal-share-link', [
                    'class' => 'form-control w-100',
                    'label' => ['class' => 'sr-only'],
                    'value' => Router::url(null, true)
                ]) ?>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn cdli-btn-blue" id="search-modal-share-copy">
                    <?= __('Copy to clipboard') ?>
                </button>
            </div>
        </div>
      </div>
</div>

<div class="modal" id="search-modal-export" tabindex="-1" role="dialog" aria-labelledby="search-modal-export-title" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="search-modal-export-title"><?= __('Download search results') ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <p><?= __('Download the results of the current search results page. To change the number of results to download, adjust the number of results per page first. If you need to download more results, please use our <a href="https://github.com/cdli-gh/data">Github data downloads</a>.') ?></p>

                <?php
                    function exportLink($view, $name, $format, $aspect = null)
                    {
                        $params = $view->getRequest()->getQueryParams();
                        $params['format'] = $format;
                        if (!is_null($aspect)) {
                            $params['aspect'] = $aspect;
                        }
                        return $view->Html->link(__('As {0}', $name), ['?' => $params]);
                    }
                ?>

                <div class="row">
                    <div class="col-lg-4 col-md-6 col-12">
                        <h5>Metadata / catalogue</h5>
                        <h6>Flat catalogue</h6>
                        <p><?= exportLink($this, 'CSV', 'csv') ?></p>
                        <p><?= exportLink($this, 'TSV', 'tsv') ?>

                        <h6>Expanded catalogue</h6>
                        <p><?= exportLink($this, 'JSON', 'json') ?>

                        <h6>Linked catalogue</h6>
                        <p><?= exportLink($this, 'TTL', 'ttl') ?></p>
                        <p><?= exportLink($this, 'JSON-LD', 'jsonld') ?></p>
                        <p><?= exportLink($this, 'RDF/JSON', 'rdfjson') ?></p>
                        <p><?= exportLink($this, 'RDF/XML', 'rdf') ?>
                    </div>
                    <div class="col-lg-4 col-md-6 col-12">
                        <h5>Text / annotations</h5>
                        <h6>Text data</h6>
                        <p><?= exportLink($this, 'ATF', 'atf', 'inscriptions') ?></p>
                        <p><?= exportLink($this, 'JTF', 'jtf', 'inscriptions') ?>

                        <h6>Annotation data</h6>
                        <p><?= exportLink($this, 'CDLI-CoNLL', 'cdli-conll', 'inscriptions') ?></p>
                        <p><?= exportLink($this, 'CoNLL-U', 'conll-u', 'inscriptions') ?>

                        <h6>Linked annotations</h6>
                        <p><?= exportLink($this, 'TTL', 'ttl', 'inscriptions') ?></p>
                        <p><?= exportLink($this, 'JSON-LD', 'jsonld', 'inscriptions') ?></p>
                        <p><?= exportLink($this, 'RDF/JSON', 'rdfjson', 'inscriptions') ?></p>
                        <p><?= exportLink($this, 'RDF/XML', 'rdf', 'inscriptions') ?>
                    </div>
                    <div class="col-lg-4 col-md-6 col-12">
                        <h6>Word lists</h6>
                        <p><?= $this->Html->link(__('As {0}', 'text'), [
                            'action' => 'tokenList',
                            'kind' => 'words',
                            '?' => $this->getRequest()->getQueryParams(),
                            '_ext' => 'txt'
                        ]); ?></p>
                        <p><?= $this->Html->link(__('As {0}', 'JSON'), [
                            'action' => 'tokenList',
                            'kind' => 'words',
                            '?' => $this->getRequest()->getQueryParams() + ['kind' => 'words'],
                            '_ext' => 'txt'
                        ]); ?></p>

                        <h6>Sign lists</h6>
                        <p><?= $this->Html->link(__('As {0}', 'text'), [
                            'action' => 'tokenList',
                            'kind' => 'signs',
                            '?' => $this->getRequest()->getQueryParams(),
                            '_ext' => 'json'
                        ]); ?></p>
                        <p><?= $this->Html->link(__('As {0}', 'JSON'), [
                            'action' => 'tokenList',
                            'kind' => 'signs',
                            '?' => $this->getRequest()->getQueryParams(),
                            '_ext' => 'json'
                        ]); ?></p>

                        <h5>Related publications</h5>
                        <p><?= exportLink($this, 'CSV', 'csv', 'publications') ?></p>
                        <p><?= exportLink($this, 'BibTeX', 'bib', 'publications') ?></p>
                    </div>
                </div>
            </div>
        </div>
      </div>
</div>
<?php $this->end(); ?>

<?php $this->append('script'); ?>
<script type="text/javascript">
    $(function () {
        $('#search-filter-collapse, #search-filter-open').click(function (e) {
            $('#search-filters').toggleClass('d-lg-block d-lg-none d-none')
            $('#search-filter-open').toggleClass('d-lg-none d-inline')
            $('#search-results').toggleClass('col-lg-9')
            $('#search-results-compact .table-responsive').scroll()
            e.preventDefault()
        })

        $('#search-filter-expand-all, #search-filter-collapse-all').removeClass('d-none')
        $('#search-filter-expand-all').click(function (e) {
            e.preventDefault()
            $('#search-filters .accordion-textbox').prop('checked', true)
        })
        $('#search-filter-collapse-all').click(function (e) {
            e.preventDefault()
            $('#search-filters .accordion-textbox').prop('checked', false)
        })

        $('#search-modal-share-copy').click(function (e) {
            e.preventDefault()
            $('#search-modal-share-link').select()
            document.execCommand('copy')
        })

        $('#search-results-compact .table-responsive').scroll(function () {
            if ($(this).scrollLeft()) {
                $('#search-results-compact').addClass('shadow-left')
            } else {
                $('#search-results-compact').removeClass('shadow-left')
            }

            if ($(this).scrollLeft() < this.scrollWidth - this.offsetWidth) {
                $('#search-results-compact').addClass('shadow-right')
            } else {
                $('#search-results-compact').removeClass('shadow-right')
            }
        })

        $('#search-results-compact .table-responsive').scroll()
    })
</script>
<?php $this->end(); ?>
