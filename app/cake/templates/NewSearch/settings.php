<?php
$schema = [];
foreach ($settings as $key => $value) {
    $schema[$key] = ['type' => gettype($value)];
}

$pageSizes = ['10' => 10, '25' => 25, '100' => 100, '500' => 500, '1000' => 1000];

$fullSort = ['' => ''];
$compactSort = ['' => ''];
foreach ($sortFields as $sortField => $sortFieldSettings) {
    if ($sortFieldSettings['full']) {
        $fullSort[$sortField] = $sortFieldSettings['label'];
    }
    if ($sortFieldSettings['compact']) {
        $compactSort[$sortField] = $sortFieldSettings['label'];
    }
}

$this->assign('title', __('Search Settings'));
?>

<div class="text-left">
    <?= $this->Form->create(['data' => $settings, 'schema' => $schema], ['type' => 'post']) ?>
        <div>
            <div class="d-inline-block float-right py-4">
                <?= $this->Form->submit(__('Save'), ['class' => 'btn cdli-btn-blue']) ?>
            </div>

            <h2 class="display-3 text-left"><?= __('Search Settings') ?></h2>
        </div>

        <div class="row">
            <div class="col-12 col-md-6">
                <div class="boxed mx-0">
                    <h3>Full view options</h3>

                    <?= $this->Form->control('full:page_size', [
                        'class' => 'form-control mb-3',
                        'label' => 'Number of search results per page',
                        'type' => 'select',
                        'options' => $pageSizes
                    ]) ?>

                    <?= $this->Form->control('full:sort', [
                        'label' => __('Sort by'),
                        'type' => 'select',
                        'options' => $fullSort,
                        'class' => 'form-control mb-3'
                    ]) ?>

                    <?= $this->Form->control('full:sidebar', ['label' => 'Show filters by default (desktop)', 'type' => 'toggle']) ?>

                    <h4>Add/remove fields from search results</h4>
                    <?= $this->Form->control('full:field:publication', ['label' => 'Primary publication', 'type' => 'toggle']) ?>
                    <?= $this->Form->control('full:field:collection', ['label' => 'Museum Collection(s)', 'type' => 'toggle']) ?>
                    <?= $this->Form->control('full:field:museum_no', ['label' => 'Museum Number', 'type' => 'toggle']) ?>
                    <?= $this->Form->control('full:field:provenience', ['label' => 'Provenience', 'type' => 'toggle']) ?>
                    <?= $this->Form->control('full:field:period', ['label' => 'Period', 'type' => 'toggle']) ?>
                    <?= $this->Form->control('full:field:artifact_type', ['label' => 'Artifact type', 'type' => 'toggle']) ?>
                    <?= $this->Form->control('full:field:material', ['label' => 'Material(s)', 'type' => 'toggle']) ?>
                    <?= $this->Form->control('full:field:dates_referenced', ['label' => 'Referenced date(s)', 'type' => 'toggle']) ?>
                    <?= $this->Form->control('full:field:atf', ['label' => 'Transliteration', 'type' => 'toggle']) ?>
                </div>

                <div class="boxed mx-0">
                    <h3>Filter options</h3>

                    <?= $this->Form->control('filter:period', ['label' => 'Period', 'type' => 'toggle']) ?>
                    <?= $this->Form->control('filter:provenience', ['label' => 'Provenience', 'type' => 'toggle']) ?>
                    <?= $this->Form->control('filter:artifact_type', ['label' => 'Artifact type', 'type' => 'toggle']) ?>
                    <?= $this->Form->control('filter:material', ['label' => 'Material', 'type' => 'toggle']) ?>
                    <?= $this->Form->control('filter:collection', ['label' => 'Museum Collection', 'type' => 'toggle']) ?>
                    <?= $this->Form->control('filter:genre', ['label' => 'Genre', 'type' => 'toggle']) ?>
                    <?= $this->Form->control('filter:language', ['label' => 'Language', 'type' => 'toggle']) ?>
                    <?= $this->Form->control('filter:dates_referenced', ['label' => 'Dates', 'type' => 'toggle']) ?>
                    <?= $this->Form->control('filter:publication_authors', ['label' => 'Author', 'type' => 'toggle']) ?>
                    <?= $this->Form->control('filter:publication_year', ['label' => 'Publication date', 'type' => 'toggle']) ?>
                    <?= $this->Form->control('filter:publication_journal', ['label' => 'Journal', 'type' => 'toggle']) ?>
                    <?= $this->Form->control('filter:publication_editors', ['label' => 'Editors', 'type' => 'toggle']) ?>
                    <?= $this->Form->control('filter:publication_series', ['label' => 'Series', 'type' => 'toggle']) ?>
                    <?= $this->Form->control('filter:publication_publisher', ['label' => 'Publisher', 'type' => 'toggle']) ?>
                    <?= $this->Form->control('filter:asset_type', ['label' => 'Image', 'type' => 'toggle']) ?>
                    <?= $this->Form->control('filter:atf_transliteration', ['label' => 'Transliteration', 'type' => 'toggle']) ?>
                    <?= $this->Form->control('filter:annotation', ['label' => 'Annotation', 'type' => 'toggle']) ?>
                    <?= $this->Form->control('filter:atf_translation', ['label' => 'Translation', 'type' => 'toggle']) ?>
                </div>
            </div>

            <div class="col-12 col-md-6">
                <div class="boxed mx-0">
                    <h3>Search Result options</h3>

                    <?= $this->Form->control('layout_type', [
                        'class' => 'form-control',
                        'label' => 'Default layout type',
                        'type' => 'select',
                        'options' => ['full' => 'full', 'compact' => 'compact']
                    ]) ?>
                </div>

                <div class="boxed mx-0">
                    <h3>Compact view options</h3>

                    <?= $this->Form->control('compact:page_size', [
                        'class' => 'form-control mb-3',
                        'label' => 'Number of search results per page',
                        'type' => 'select',
                        'options' => $pageSizes
                    ]) ?>

                    <?= $this->Form->control('compact:sort_1', [
                        'label' => __('Sort by') . ' (1)',
                        'type' => 'select',
                        'options' => $compactSort,
                        'templates' => [
                            'inputContainer' => '<div class="input mr-2 d-inline-block {{type}}{{required}}">{{content}}</div>'
                        ],
                        'class' => 'form-control mb-3'
                    ]) ?>

                    <?= $this->Form->control('compact:sort_2', [
                        'label' => __('Sort by') . ' (2)',
                        'type' => 'select',
                        'options' => $compactSort,
                        'templates' => [
                            'inputContainer' => '<div class="input mr-2 d-inline-block {{type}}{{required}}">{{content}}</div>'
                        ],
                        'class' => 'form-control mb-3'
                    ]) ?>

                    <?= $this->Form->control('compact:sort_3', [
                        'label' => __('Sort by') . ' (3)',
                        'type' => 'select',
                        'options' => $compactSort,
                        'templates' => [
                            'inputContainer' => '<div class="input mr-2 d-inline-block {{type}}{{required}}">{{content}}</div>'
                        ],
                        'class' => 'form-control mb-3'
                    ]) ?>

                    <?= $this->Form->control('compact:sidebar', ['label' => 'Show filters by default (desktop)', 'type' => 'toggle']) ?>

                    <h4>Add/remove fields from search results</h4>
                    <?= $this->Form->control('compact:field:publication', ['label' => 'Primary publication', 'type' => 'toggle']) ?>
                    <?= $this->Form->control('compact:field:collection', ['label' => 'Museum Collection(s)', 'type' => 'toggle']) ?>
                    <?= $this->Form->control('compact:field:period', ['label' => 'Period', 'type' => 'toggle']) ?>
                    <?= $this->Form->control('compact:field:provenience', ['label' => 'Provenience', 'type' => 'toggle']) ?>
                    <?= $this->Form->control('compact:field:artifact_type', ['label' => 'Artifact type', 'type' => 'toggle']) ?>
                    <?= $this->Form->control('compact:field:material', ['label' => 'Material(s)', 'type' => 'toggle']) ?>
                    <?= $this->Form->control('compact:field:accession_no', ['label' => 'Accession number', 'type' => 'toggle']) ?>
                    <?= $this->Form->control('compact:field:museum_no', ['label' => 'Museum Number', 'type' => 'toggle']) ?>
                    <?= $this->Form->control('compact:field:written_in', ['label' => 'Written in', 'type' => 'toggle']) ?>
                    <?= $this->Form->control('compact:field:excavation_no', ['label' => 'Excavation number', 'type' => 'toggle']) ?>
                    <?= $this->Form->control('compact:field:archive', ['label' => 'Archive(s)', 'type' => 'toggle']) ?>
                    <?= $this->Form->control('compact:field:dates_referenced', ['label' => 'Referenced date(s)', 'type' => 'toggle']) ?>
                    <?= $this->Form->control('compact:field:genre', ['label' => 'Genre(s)/sub-genre(s)', 'type' => 'toggle']) ?>
                    <?= $this->Form->control('compact:field:language', ['label' => 'Language(s)', 'type' => 'toggle']) ?>
                </div>
            </div>
        </div>

        <div class="row justify-content-center mt-5">
            <?= $this->Form->submit(__('Save'), ['class' => 'wide-btn btn cdli-btn-blue mx-1']) ?>
            <?= $this->Form->submit(__('Reset'), ['class' => 'wide-btn btn cdli-btn mx-1', 'name' => 'reset']) ?>
        </div>
    <?= $this->Form->end()?>
</div>
