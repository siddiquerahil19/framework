<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Collection[]|\Cake\Collection\CollectionInterface $collections
 */
?>

<div class="d-flex justify-content-between align-items-end mb-2">
    <h2 class="display-3 header-txt text-left"><?= __('Collections') ?></h2>
    <?= $this->element('addButton'); ?>
</div>

<div class="row mt-3">
    <?php foreach ($collections_card as $cards): ?>
        <div class="bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
            <div class="d-flex flex-column card-body text-left border">
            <h5 class="mb-4"><?= $this->Html->link($cards->collection, ['action' => 'view', $cards->id]) ?></h5>
            <?php if (strlen(strip_tags($cards->description)) > 90): ?>
                <?= substr(strip_tags($cards->description), 0, 90) . '...' ?>
            <?php else: ?>
                <?= strip_tags($cards->description) ?>
            <?php endif; ?>
            </div>
        </div>
    <?php endforeach; ?>
</div>
<?php foreach (range('A', 'Z') as $char): ?>
    <?= $this->Html->link($char, ['action' => 'view', '?' => ['letter' => $char]], ['class' => 'btn btn-action']) ?>
<?php endforeach ?>
<div class="text-left my-2">
    <?= $this->element('entityExport'); ?>
</div>
<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead class="text-left">
        <tr>
            <th scope="col"><?= $this->Paginator->sort('collection') ?></th>
            <?php if ($access_granted): ?>
                <th scope="col"><?= __('Action') ?></th>
            <?php endif ?>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($collections as $collection): ?>
            <tr class="text-left">
                <td>
                    <?= $this->Html->link(
                        $collection->collection,
                        ['controller' => 'Collections', 'action' => 'view', $collection->id]
                    ) ?>
                    <?php if ($collection->is_private == 1): ?>
                        (private)
                    <?php endif;?>
                </td>

                <?php if ($access_granted): ?>
                    <td>
                        <?= $this->Html->link(
                            __('Edit'),
                            ['prefix' => 'Admin', 'action' => 'edit', $collection->id],
                            ['escape' => false , 'class' => 'btn btn-warning btn-sm', 'title' => 'Edit']
                        ) ?>
                    </td>
                <?php endif ?>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>

<?= $this->element('citeButton'); ?>
<?= $this->element('citeBottom'); ?>