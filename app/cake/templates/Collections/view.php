<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Collection $collection
 */
?>
<?php use Cake\Utility\Inflector; ?>

<div>
    <div class="text-heading text-left"><?= __(h(Inflector::humanize(Inflector::singularize(Inflector::tableize($this->request->getParam('controller')))))) ?>: <?= h($collection->collection) ?>
        <?php if ($collection->is_private): ?>
              <?= __('(PRIVATE)')?>
        <?php endif; ?>
        <?php if ($access_granted): ?>
          <?= $this->Html->link(
              __('Edit'),
              ['prefix' => 'Admin', 'action' => 'edit', $collection->id],
              ['escape' => false , 'class' => "btn cdli-btn-blue pull-right", 'title' => 'Edit']
          ) ?>
        <?php endif; ?>
    </div>
</div>

<div class="pt-3 text-left">
    <?= $collection->description ?>
</div>

<?php if ($count!=0): ?>
    <div class="single-entity-wrapper mx-0 text-left">
        <div class="capital-heading"><?= __('Related Artifacts') ?></div>
        <p>
          <?php if ($count==1):?>
            <?php echo 'There is only '.$count.' artifact of this collection in the CDLI Dataset' ?><br>
          <?php elseif ($count>1):?>
            <?php echo 'There are '.$count.' artifacts of this collection in the CDLI Dataset ' ?><br>
          <?php endif; ?>
          <?= $this->Html->link(
            __('Click here to view the artifacts'),
            [
              'controller' => 'Search',
              '?' => [ 'keyword' => h($collection->collection)]]
            ) ?>
        </p>
    </div>
<?php endif; ?>

<?= $this->element('citeButton'); ?>
<?= $this->element('citeBottom'); ?>