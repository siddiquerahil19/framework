<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Provenience[]|\Cake\Collection\CollectionInterface $proveniences
 */
?>

<div class="d-flex justify-content-between align-items-end mb-2">
    <h1 class="display-3 header-txt text-left"><?= __('Proveniences') ?></h1>  
    <?= $this->element('addButton'); ?>
</div>
<div class="text-left my-2">
    <?= $this->element('entityExport'); ?>
</div>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead>
        <tr align="left">
            <th scope="col"><?= $this->Paginator->sort('provenience') ?></th>
            <th scope="col"><?= $this->Paginator->sort('region_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('geo_coordinates') ?></th>
            <?php if ($access_granted): ?>
                <th scope="col"><?= __('Action') ?></th>
            <?php endif ?>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($proveniences as $provenience): ?>
            <tr align="left">
                <td>
                    <?= $this->Html->link(
                        $provenience->provenience,
                        ['controller' => 'Proveniences', 'action' => 'view', $provenience->id]
                    ) ?>
                </td>
                <td>
                    <?php if ($provenience->has('region')): ?>
                        <?= $this->Html->link(
                            $provenience->region->region,
                            ['controller' => 'Regions', 'action' => 'view', $provenience->region->id]
                        ) ?>
                    <?php endif; ?>
                </td>
                <td><?= h($provenience->geo_coordinates) ?></td>
                <?php if ($access_granted): ?>
                    <td>
                        <?= $this->Html->link(
                            __('Edit'),
                            ['prefix' => 'Admin', 'action' => 'edit', $provenience->id],
                            ['escape' => false , 'class' => 'btn btn-warning btn-sm', 'title' => 'Edit']
                        ) ?>
                    </td>
                <?php endif ?>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>

<?= $this->element('citeButton'); ?>
<?= $this->element('citeBottom'); ?>