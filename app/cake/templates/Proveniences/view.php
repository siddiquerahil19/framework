<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Provenience $provenience
 */
?>
<?php use Cake\Utility\Inflector; ?>

<div>
    <div class="text-heading text-left"><?= __(h(Inflector::humanize(Inflector::singularize(Inflector::tableize($this->request->getParam('controller')))))) ?>: <?= h($provenience->provenience) ?>
      <?php if ($access_granted): ?>
          <?= $this->Html->link(
              __('Edit'),
              ['prefix' => 'Admin', 'action' => 'edit', $provenience->id],
              ['escape' => false , 'class' => "btn cdli-btn-blue pull-right", 'title' => 'Edit']
          ) ?>
      <?php endif; ?>
    </div>
</div>

<div class="text-left mt-4" style="font-size: large;">
    <ul>
        <li><?= __('Region') ?>: <?= $provenience->has('region') ? $this->Html->link($provenience->region->region, ['controller' => 'Regions', 'action' => 'view', $provenience->region->id]) : '' ?></li>
    </ul>
</div>

<div class="row justify-content-md-center">
    <div class="col-lg-10 boxed">
        <div id="map" style="height: 500px;z-index: 0;"></div>
    </div>
</div>

<?php if (!empty($provenience->archives)): ?>
    <div class="single-entity-wrapper text-left mx-0">
        <div class="capital-heading"><?= __('Related Archives') ?></div>
            <ul>
                <?php foreach ($provenience->archives as $archives): ?>
                    <li><?= $this->Html->link(h($archives->archive),['controller' => 'Archives', 'action' => 'view', $archives->id]) ?></li>
                <?php endforeach; ?>
            </ul>
    </div>
<?php endif; ?>

<?php if (!empty($provenience->artifacts)): ?>
    <div class="single-entity-wrapper mx-0">
        <div class="capital-heading"><?= __('Related Artifacts') ?></div>
        <table cellpadding="0" cellspacing="0" class="table-bootstrap">
            <thead>
                <th scope="col"><?= __('Ark No') ?></th>
                <th scope="col"><?= __('Credit Id') ?></th>
                <th scope="col"><?= __('Primary Publication Comments') ?></th>
                <th scope="col"><?= __('Cdli Collation') ?></th>
                <th scope="col"><?= __('Cdli Comments') ?></th>
                <th scope="col"><?= __('Composite No') ?></th>
                <th scope="col"><?= __('Condition Description') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Date Comments') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col"><?= __('Dates Referenced') ?></th>
                <th scope="col"><?= __('Designation') ?></th>
                <th scope="col"><?= __('Electronic Publication') ?></th>
                <th scope="col"><?= __('Elevation') ?></th>
                <th scope="col"><?= __('Excavation No') ?></th>
                <th scope="col"><?= __('Findspot Comments') ?></th>
                <th scope="col"><?= __('Findspot Square') ?></th>
                <th scope="col"><?= __('Height') ?></th>
                <th scope="col"><?= __('Join Information') ?></th>
                <th scope="col"><?= __('Museum No') ?></th>
                <th scope="col"><?= __('Artifact Preservation') ?></th>
                <th scope="col"><?= __('Is Public') ?></th>
                <th scope="col"><?= __('Is Atf Public') ?></th>
                <th scope="col"><?= __('Are Images Public') ?></th>
                <th scope="col"><?= __('Seal No') ?></th>
                <th scope="col"><?= __('Seal Information') ?></th>
                <th scope="col"><?= __('Stratigraphic Level') ?></th>
                <th scope="col"><?= __('Surface Preservation') ?></th>
                <th scope="col"><?= __('General Comments') ?></th>
                <th scope="col"><?= __('Thickness') ?></th>
                <th scope="col"><?= __('Width') ?></th>
                <th scope="col"><?= __('Provenience Id') ?></th>
                <th scope="col"><?= __('Period Id') ?></th>
                <th scope="col"><?= __('Is Provenience Uncertain') ?></th>
                <th scope="col"><?= __('Is Period Uncertain') ?></th>
                <th scope="col"><?= __('Artifact Type Id') ?></th>
                <th scope="col"><?= __('Accession No') ?></th>
                <th scope="col"><?= __('Accounting Period') ?></th>
                <th scope="col"><?= __('Alternative Years') ?></th>
                <th scope="col"><?= __('Dumb2') ?></th>
                <th scope="col"><?= __('Custom Designation') ?></th>
                <th scope="col"><?= __('Period Comments') ?></th>
                <th scope="col"><?= __('Provenience Comments') ?></th>
                <th scope="col"><?= __('Is School Text') ?></th>
                <th scope="col"><?= __('Written In') ?></th>
                <th scope="col"><?= __('Is Object Type Uncertain') ?></th>
                <th scope="col"><?= __('Archive Id') ?></th>
                <th scope="col"><?= __('Created By') ?></th>
                <th scope="col"><?= __('Db Source') ?></th>
                <th scope="col"><?= __('Weight') ?></th>
                <th scope="col"><?= __('Translation Source') ?></th>
                <th scope="col"><?= __('Atf Up') ?></th>
                <th scope="col"><?= __('Atf Source') ?></th>
                <!-- <th scope="col"><?= __('Actions') ?></th> -->
            </thead>
            <tbody>
                <?php foreach ($provenience->artifacts as $artifacts): ?>
                <tr>
                    <td><?= h($artifacts->ark_no) ?></td>
                    <td><?= h($artifacts->credit_id) ?></td>
                    <td><?= h($artifacts->primary_publication_comments) ?></td>
                    <td><?= h($artifacts->cdli_collation) ?></td>
                    <td><?= h($artifacts->cdli_comments) ?></td>
                    <td><?= h($artifacts->composite_no) ?></td>
                    <td><?= h($artifacts->condition_description) ?></td>
                    <td><?= h($artifacts->created) ?></td>
                    <td><?= h($artifacts->date_comments) ?></td>
                    <td><?= h($artifacts->modified) ?></td>
                    <td><?= h($artifacts->dates_referenced) ?></td>
                    <td><?= h($artifacts->designation) ?></td>
                    <td><?= h($artifacts->electronic_publication) ?></td>
                    <td><?= h($artifacts->elevation) ?></td>
                    <td><?= h($artifacts->excavation_no) ?></td>
                    <td><?= h($artifacts->findspot_comments) ?></td>
                    <td><?= h($artifacts->findspot_square) ?></td>
                    <td><?= h($artifacts->height) ?></td>
                    <td><?= h($artifacts->join_information) ?></td>
                    <td><?= h($artifacts->museum_no) ?></td>
                    <td><?= h($artifacts->artifact_preservation) ?></td>
                    <td><?= h($artifacts->is_public) ?></td>
                    <td><?= h($artifacts->is_atf_public) ?></td>
                    <td><?= h($artifacts->are_images_public) ?></td>
                    <td><?= h($artifacts->seal_no) ?></td>
                    <td><?= h($artifacts->seal_information) ?></td>
                    <td><?= h($artifacts->stratigraphic_level) ?></td>
                    <td><?= h($artifacts->surface_preservation) ?></td>
                    <td><?= h($artifacts->general_comments) ?></td>
                    <td><?= h($artifacts->thickness) ?></td>
                    <td><?= h($artifacts->width) ?></td>
                    <td><?= h($artifacts->provenience_id) ?></td>
                    <td><?= h($artifacts->period_id) ?></td>
                    <td><?= h($artifacts->is_provenience_uncertain) ?></td>
                    <td><?= h($artifacts->is_period_uncertain) ?></td>
                    <td><?= h($artifacts->artifact_type_id) ?></td>
                    <td><?= h($artifacts->accession_no) ?></td>
                    <td><?= h($artifacts->accounting_period) ?></td>
                    <td><?= h($artifacts->alternative_years) ?></td>
                    <td><?= h($artifacts->dumb2) ?></td>
                    <td><?= h($artifacts->custom_designation) ?></td>
                    <td><?= h($artifacts->period_comments) ?></td>
                    <td><?= h($artifacts->provenience_comments) ?></td>
                    <td><?= h($artifacts->is_school_text) ?></td>
                    <td><?= h($artifacts->written_in) ?></td>
                    <td><?= h($artifacts->is_object_type_uncertain) ?></td>
                    <td><?= h($artifacts->archive_id) ?></td>
                    <td><?= h($artifacts->created_by) ?></td>
                    <td><?= h($artifacts->db_source) ?></td>
                    <td><?= h($artifacts->weight) ?></td>
                    <td><?= h($artifacts->translation_source) ?></td>
                    <td><?= h($artifacts->atf_up) ?></td>
                    <td><?= h($artifacts->atf_source) ?></td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?php endif; ?>

<?php if (!empty($provenience->dynasties)): ?>
    <div class="single-entity-wrapper text-left mx-0">
        <div class="capital-heading"><?= __('Related Dynasties') ?></div>
            <ul>
                <?php foreach ($provenience->dynasties as $dynasties): ?>
                    <li><?= $this->Html->link(h($dynasties->dynasty),['controller' => 'Dynasties', 'action' => 'view', $dynasties->id]) ?></li>
                <?php endforeach; ?>
            </ul>
    </div>
<?php endif; ?>

<?php if (!empty($provenience->publications)): ?>
    <div class="single-entity-wrapper text-left mx-0">
        <div class="capital-heading"><?= __('Related Publications') ?>
            <?php if ($is_admin) : ?>
                <?= $this->Html->link(
                    __('Link Publications'),
                    [
                        'prefix' => 'Admin',
                        'controller' => 'EntitiesPublications',
                        'action' => 'add',
                        'publication',
                        'proveniences',
                        $provenience->id
                    ],
                    ['class' => 'btn btn-action', 'style' => 'float: right;']
                ) ?>
            <?php endif; ?>
        </div>
        <div class="table-responsive">
            <table class="table-bootstrap mx-0">
                <thead>
                    <tr>
                        <th scope="col">BibTeX key</th>
                        <th scope="col">Authors</th>
                        <th scope="col">Designation</th>
                        <th scope="col">Reference</th>
                        <?php if ($access_granted): ?>
                            <th scope="col"><?= __('Action') ?></th>
                        <?php endif ?>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($provenience->publications as $publication): ?>
                    <tr>
                        <td>
                            <?= $this->Html->link(
                                            h($publication->bibtexkey),
                                            ['prefix' => false, 'action' => 'view', $publication->id]) ?>
                        </td>
                        <td align="left" nowrap="nowrap">
                            <?php foreach ($publication->authors as $author): ?>
                            <?= $this->Html->link(
                                            h($author->author),
                                            ['prefix' => false, 'controller' => 'Authors', 'action' => 'view', $author->id]) ?><br>
                            <?php endforeach ?>
                        </td>
                        <td><?= h($publication->designation) ?></td>
                        <td>
                            <?= $this->Citation->formatReference($publication, 'bibliography', [
                                'template' => 'chicago-author-date',
                                'format' => 'html'
                            ]) ?>

                        </td>
                        <td><?php if ($access_granted): ?>
                            <?= $this->Html->link(__('Edit'),['prefix' => 'Admin', 'action' => 'edit', $publication->id],
                                    ['escape' => false , 'class' => "btn btn-outline-primary", 'title' => 'Edit']) ?>
                        <?php endif ?>
                    </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
<?php endif; ?>

<?php if ($count != 0): ?>
    <div class="single-entity-wrapper text-left mx-0">
        <div class="capital-heading"><?= __('Related Artifacts') ?></div>
        <p>
          <?php echo 'There are '.$count.' artifacts related to '.h(Inflector::humanize(Inflector::singularize(Inflector::tableize($this->request->getParam('controller')))))?><br>
            <?= $this->Html->link(
                __('Click here to view the artifacts'),
                [
                  'controller' => 'Search',
                  '?' => [ 'keyword' => h($provenience->provenience) ]]
            ) ?>
        </p>
    </div>
<?php endif; ?>

<?= $this->element('citeButton'); ?>
<?= $this->element('citeBottom'); ?>

<?php
   $geo = explode(",",$provenience->geo_coordinates);
   $lon = $geo[0];
   $lat = $geo[1];
   $name = $provenience->provenience ;
   $id = $provenience->id;
?>

<?= $this->element('leaflet') ?>
<?php $this->append('script'); ?>
<script>
(function () {
    var lon = "<?php echo"$lon"?>";
    var lat = "<?php echo"$lat"?>";
    var name = "<?php echo"$name"?>";
    var id = "<?php echo"$id"?>";
    var region = <?= json_encode($provenience->region) ?>

    var map = L.map('map')
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map)

    var coordinates = JSON.parse('[' + region.geo_coordinates + ']')
    for (var j = 0; j < coordinates.length; j++) {
        coordinates[j].reverse()
    }

    var polygon = L.polygon(coordinates).addTo(map)
    var bounds = polygon.getBounds()
    map.setView(bounds.getCenter(), map.getBoundsZoom(bounds))

    var coordinates = JSON.parse('[' + lon + ', ' + lat + ']').reverse()
    var link = '<a href="/proveniences/' + id + '">' + name + '</a>'
    L.marker(coordinates).addTo(map).bindPopup(link)
})()
</script>
<?php $this->end(); ?>
