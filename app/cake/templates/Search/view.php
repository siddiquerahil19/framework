<?php

use Cake\Routing\Router;
use Cake\Utility\Hash;

// Returns modified Advance Search Link
function modifyAdvancedSearchLike($searchableFields)
{
    $newGET = [];
    foreach ($_GET as $param => $value) {
        if (in_array($param, $searchableFields)) {
            $newGET[$param] = $value;
        }
    }

    return $newGET;
}

// When page per size changes
function modifyPageNumberIfPerSizeChanges($pageSize)
{
    if (array_key_exists('Page', $_GET)) {
        unset($_GET['Page']);
    }
    $newGET = array_merge($_GET, ['PageSize' => $pageSize]);
    return $newGET;
}
/**
* updateFilters method
*
* @param listofFilters, results of buckets query, mapped array of parents and it's children
*
* @return Updated filters to print parent and number of hits
*/

function updateFilters($listOfFilters, $bucketsresult, $parentChildArray)
{
    $parentsArray = array_keys($parentChildArray);
    foreach ($parentsArray as $parent) {
        foreach ($parentChildArray as $grandParent =>$childArray) {
            if (in_array($parent, $childArray) && $parent != $grandParent) {
                $hasGrandParents[$parent] = $grandParent;
            }
        }
    }
    foreach ($listOfFilters as $filtername => $value) {
        foreach ($parentChildArray as $parent => $childarray) {
            foreach ($childarray as $child) {
                if ($filtername == $child) {
                    if (array_key_exists($parent, $hasGrandParents)) {
                        $grandparent = $hasGrandParents[$parent];
                        $listOfFilters[$filtername] = $grandparent.' -> '.$parent.' -> '.$listOfFilters[$filtername];
                    } else {
                        $listOfFilters[$filtername] = $parent.' -> '.$listOfFilters[$filtername];
                    }
                }
            }
        }
    }
    foreach ($listOfFilters as $filtername =>$value) {
        $listOfFilters[$filtername] = $listOfFilters[$filtername].' ('.$bucketsresult[$filtername].')';
    }
    return $listOfFilters;
}

// Search Category
$searchCategory = [
// $category =. $placeholder
    'keyword' => 'Free Search',
    'publication' => 'Publications',
    'collection' => 'Collections',
    'provenience' => 'Proveniences',
    'period' => 'Periods',
    'inscription' => 'Inscriptions',
    'id' => 'Identification numbers',
];

// Fetch Searched parameters
$searchBar = [];
if ($isAdvancedSearch != 1) {
    foreach ($_GET as $key => $value) {
        $checkType = substr($key, 0, -1);
        if (array_key_exists($key, $searchCategory)) {
            array_push($searchBar, [$key, trim($value, '" || \'')]);
        } elseif (array_key_exists($checkType, $searchCategory) || $checkType === 'operator') {
            array_push($searchBar, [$checkType, trim($value, '" || \'')]);
        }
    }
}

// Sort By
$filterOptions = [
//    'r' => 'Relevance',
//    'l' => 'Lorem',
//    'ipsum' => 'dolor'
];

// Full Filter Status
$fullFilterStatus = $searchSettings['full:sidebar'] && $LayoutType == 1;
// Compact Filter Status
$compactFilterStatus = $searchSettings['compact:sidebar'] && $LayoutType == 2;

$mappingSearchSettings = [
    'object' => [
        'collection' => 'Museum Collections',
        'period' => 'Period',
        'provenience' => 'Provenience',
        'atype' => 'Artifact type',
        'materials' => 'Material',
        'image_type' => 'Image Type',
        'dates' => 'Dates'
    ],
    'textual' => [
        'genres' => 'Genre',
        'languages' => 'Language'
    ],
    'publication' => [
        'authors' => 'Authors',
        'year' => 'Date of publication'
    ],
    'inscription' => [
        'transliteration' => 'Transliteration',
        'translation' => 'Translation'
    ]
];

// Default Filters populate
$filterArray = [
    'object' => [],
    'textual' => [],
    'publication' => [],
    'inscription' => []
];

// Filters applied
$filteredArray = [
    'object' => [],
    'textual' => [],
    'publication' => [],
    'inscription' => []
];

// Applied Filters
$appliedFilters = [];
foreach ($mappingSearchSettings as $type => $subType) {
    foreach ($subType as $key => $value) {
        $prefix = $fullFilterStatus ? 'full:' : ($compactFilterStatus ? 'compact:' : '');
        if (($fullFilterStatus || $compactFilterStatus || !($fullFilterStatus ||            $compactFilterStatus)) &&  is_array($searchSettings[$prefix.$type]) && in_array($key, $searchSettings[$prefix.$type])) {
            $filterArray[$type] = array_merge_recursive($filterArray[$type], [$key => $filters[$key]]);
            $filterApplied = array_keys(array_filter($filterArray[$type][$key], function ($value) {
                return $value;
            }));
            $filteredArray[$type] = array_merge_recursive($filteredArray[$type], [$key => $filterApplied ]);
            $appliedFilters = array_merge($appliedFilters, $filterApplied);
        }
    }
}

foreach ($filterArray as $majorfiltercategories => $filtercategories) {
    foreach ($filtercategories as $filtercategory => $specificfilters) {
        foreach ($specificfilters as $filtername => $value2) {
            if (array_key_exists($filtername, $bucketsresult) == false) {
                unset($filterArray[$majorfiltercategories][$filtercategory][$filtername]);
            }
        }
    }
}

echo $this->Html->css('filter');
echo $this->Html->script('searchResult.js', ['defer' => true]);

// By Default Javascript Status set. If <noscript> block is executed, it will be unset.
$JAVASCRIPT_STATUS = 1;

// Temporary fix
$param = 1;
?>
<?= $this->Html->script('drawer.js', ['defer' => true]) ?>
<div class="translucent-film d-none" id="faded-bg"></div>
<div class="">
    <h1 class="container px-0 search-heading header-txt display-3 text-left">
        <?= __('Search the CDLI collection')?>
    </h1>
    <?= $this->Form->create(null, [
            'type'=>'GET',
            'url' => [
                'controller' => 'Search',
                'action' => 'index'
            ]
        ]) ?>

        <div>
            <div id="dynamic_field">
                <div>
                    <noscript>
                        <?php $JAVASCRIPT_STATUS = 0; ?>
                        <style>
                            .default-search-block,
                            .default-add-search-btn {
                                display:none;
                            }
                        </style>
                        <div id="dynamic_field">
                            <div>
                                <div class="rectangle-2 container p-3">
                                    <div class="search-page-grid" id="1">
                                        <?= $this->Form->label('input1', null, ['hidden']); ?>
                                        <?= $this->Form->text(
            '',
            [
                                                'name' => empty($searchBar) ? 'publication' : $searchBar[0][0],
                                                'id' => 'input1',
                                                'value' => empty($searchBar) ? '' : $searchBar[0][1],
                                                'placeholder' => 'Search for publications, provenience, collection no.',
                                                'aria-label' => 'Search',
                                                'required']
                                        ); ?>
                                        <?= $this->Form->label('1', null, ['hidden']); ?>
                                        <?=$this->Form->select(
                                            '',
                                            [
                                                // $category =. $placeholder
                                                'keyword' => 'Free Search',
                                                'publication' => 'Publications',
                                                'collection' => 'Collections',
                                                'provenience' => 'Proveniences',
                                                'period' => 'Periods',
                                                'inscription' => 'Inscriptions',
                                                'id' => 'Identification numbers',
                                            ],
                                            [
                                                'value' => empty($searchBar) ? 'publication' : $searchBar[0][0],
                                                'class' => 'form-group mb-0',
                                                'id' => '1'
                                            ]
                                        );?>
                                    </div>
                                </div>
                            </div>
                            <?php $extraSearchBox = (sizeof($searchBar) - 1)/2?>
                            <?php for ($i = $extraSearchBox; $i <= 2; $i++):?>
                                <div id="row<?=$i?>">
                                    <div class="container rectangle-23 p-3">
                                        <div class="align-items-baseline">
                                            <?=$this->Form->select(
                                            'operator'.$i,
                                            [
                                                    'AND' => 'AND',
                                                    'OR' => 'OR'
                                                ],
                                            [
                                                    'value' => sizeof($searchBar) < ($i*2) ? 'AND' : $searchBar[$i*2 - 1][1],
                                                    'class' => 'mb-3 mt-0 cdli-btn-light btn-and mr-3 float-left',
                                                    'id' => $i
                                                ]
                                            );?>
                                            <div class="search-page-grid w-100-sm">
                                                <?= $this->Form->label('input'.($i + 1), null, ['hidden']); ?>
                                                <?= $this->Form->text(
                                                '',
                                                [
                                                        'name' => 'publication'.$i,
                                                        'id' => 'input'.($i + 1),
                                                        'value' => sizeof($searchBar) < ($i*2) ? '' : $searchBar[$i*2][1],
                                                        'placeholder' => 'Search for publications, provenience, collection no.',
                                                        'aria-label' => 'Search'
                                                    ]
                                                ); ?>
                                                <?= $this->Form->label(($i + 1), null, ['hidden']); ?>
                                                <?=$this->Form->select(
                                                    '',
                                                    [
                                                        // $category =. $placeholder
                                                        'keyword'.$i => 'Free Search',
                                                        'publication'.$i => 'Publications',
                                                        'collection'.$i => 'Collections',
                                                        'provenience'.$i => 'Proveniences',
                                                        'period'.$i => 'Periods',
                                                        'inscription'.$i => 'Inscriptions',
                                                        'id'.$i => 'Identification numbers',
                                                    ],
                                                    [
                                                        'value' => sizeof($searchBar) < ($i*2) ? 'publication'.$i : $searchBar[$i*2][0].$i,
                                                        'class' => 'form-group mb-0',
                                                        'id' => ($i + 1)
                                                    ]
                                                );?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endfor; ?>
                    </noscript>
                    <div class="rectangle-2 container p-3 default-search-block">
                        <div class="search-page-grid w-100-sm" id="1">
                            <?= $this->Form->label('input1', null, ['hidden']); ?>
                            <?= $this->Form->text(
                                                    '',
                                                    [
                                    'name' => empty($searchBar) ? "publication" : $searchBar[0][0],
                                    'id' => 'input1',
                                    'value' => empty($searchBar) ? '' : $searchBar[0][1],
                                    'placeholder' => 'Search for publications, provenience, collection no.',
                                    'aria-label' => 'Search',
                                    'required'
                                ]
                            ); ?>
                            <?= $this->Form->label(1, null, ['hidden']); ?>
                            <?=$this->Form->select(
                                '',
                                [
                                    // $category =. $placeholder
                                    'keyword' => 'Free Search',
                                    'publication' => 'Publications',
                                    'collection' => 'Collections',
                                    'provenience' => 'Proveniences',
                                    'period' => 'Periods',
                                    'inscription' => 'Inscriptions',
                                    'id' => 'Identification numbers',
                                ],
                                [
                                    'value' => empty($searchBar) ? 'publication' : $searchBar[0][0],
                                    'class' => 'form-group mb-0',
                                    'id' => '1'
                                ]
                            );?>
                        </div>
                    </div>
                </div>
                <?php if ((sizeof($searchBar) > 1) && $JAVASCRIPT_STATUS) {
                                ?>
                    <?php $extraSearchBox = (sizeof($searchBar)-1)/2;
                                $extraSearchBox = $extraSearchBox <= 2 ? $extraSearchBox : 2; ?>
                    <?php for ($i = 1; $i <= $extraSearchBox; $i++):?>
                        <div id="row<?=$i?>">
                            <div class="container rectangle-23 p-3">
                                <div class="align-items-baseline">
                                    <?=$this->Form->select(
                        'operator'.$i,
                        [
                                            'AND' => 'AND',
                                            'OR' => 'OR'
                                        ],
                        [
                                            'value' => $searchBar[$i*2 - 1][1],
                                            'class' => 'mb-3 mt-0 cdli-btn-light btn-and mr-3 float-left',
                                            'id' => $i
                                        ]
                                    ); ?>
                                    <?= $this->Form->button(
                                        '<b>&times;</b>',
                                        [
                                            'class' => 'btn cdli-btn-light ml-3 float-right btn_remove',
                                            'id' => $i,
                                            'escapeTitle' => false,
                                            'name' => 'remove'
                                        ]
                                    ); ?>

                                    <div class="search-page-grid w-100-sm">
                                        <?= $this->Form->label('input'.($i + 1), null, ['hidden']); ?>
                                        <?= $this->Form->text(
                                        '',
                                        [
                                                'name' => 'publication'.$i,
                                                'id' => 'input'.($i + 1),
                                                'value' => $searchBar[$i*2][1],
                                                'placeholder' => 'Search for publications, provenience, collection no.',
                                                'aria-label' => 'Search'
                                            ]
                                        ); ?>
                                        <?= $this->Form->label(($i + 1), null, ['hidden']); ?>
                                        <?=$this->Form->select(
                                            '',
                                            [
                                                // $category =. $placeholder
                                                'keyword'.$i => 'Free Search',
                                                'publication'.$i => 'Publications',
                                                'collection'.$i => 'Collections',
                                                'provenience'.$i => 'Proveniences',
                                                'period'.$i => 'Periods',
                                                'inscription'.$i => 'Inscriptions',
                                                'id'.$i => 'Identification numbers',
                                            ],
                                            [
                                                'value' => $searchBar[$i*2][0].$i,
                                                'class' => 'form-group mb-0',
                                                'id' => ($i + 1)
                                            ]
                                        ); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endfor; ?>
                <?php
                            } ?>
            </div>
        </div>

        <div class="container d-flex cdli-btn-group px-0 search-add-field-group">
            <?= $this->Form->button('Search', [
                'type' => 'submit',
                'class' => 'btn cdli-btn-blue'
            ]); ?>
            <button type="button" name="add" id="add" class="btn cdli-btn-light default-add-search-btn">
                <span class="fa fa-plus-circle plus-icon"></span> Add search field
            </button>
            <?= $this->Html->link(
                __('Search Settings'),
                ['controller' => 'SearchSettings', 'action' => 'index'],
                ['class' => 'd-none d-lg-block search-links mr-5']
            ) ?>
            <?php if ($isAdvancedSearch === 1):?>
                <?= $this->Html->link(
                'Modify Advanced Search',
                [
                        'controller' => 'Advancedsearch',
                        'action' => 'index',
                        '?' => modifyAdvancedSearchLike($searchableFields)
                    ],
                [
                        'class' => 'd-none d-lg-block search-links mr-5'
                    ]
                )?>
            <?php else:?>
                <?= $this->Html->link(
                    __('Advanced Search'),
                    ['controller' => 'Advancedsearch', 'action' => 'index'],
                    ['class' => 'd-none d-lg-block search-links mr-5']
                ) ?>
            <?php endif;?>
        </div>
    <?= $this->Form->end()?>

    <hr class="line" />

    <?php if (!empty($result)):?>
        <!-- Filter trigger button and dropdown and pagination controls -->
            <div class="d-none d-md-flex align-items-center justify-content-between mt-5">
                <div>
                    <button
                        class="show-card-btn
                        <?php if ($compactFilterStatus || $fullFilterStatus) {
                    echo "d-none";
                } ?>
                        btn btn-outline-dark rounded-0 px-4"
                        data-toggle="modal"
                        data-target="#filter-modal">
                        Filter categories
                    </button>
                </div>

                  <div class="d-flex align-items-center">
                    <div class="d-none d-md-flex align-items-center ">
                        <?php if ($LayoutType == 1): ?>
                            <?= $this->Form->button(
                            '<span
                                    class="fa fa-th-large fa-2x pt-2 pb-1 px-1"
                                    aria-hidden="true">
                                </span>',
                            [
                                    'class' => 'btn bg-dark text-white border-dark ml-4 view-btn',
                                    'aria-label' =>'Toggle view to card layout',
                                    'escapeTitle' => false,
                                    'title' => 'Card layout'
                                ]
                            ); ?>
                            <?= $this->Form->create(null, [
                                'url' => [
                                    $searchId,
                                    '?' => array_merge($_GET, ['LayoutType' => 2])
                                ]
                                                          ]); ?>
                                <?= $this->Form->button(
                                                              '<span
                                    class="fa fa-bars fa-2x pt-2 pb-1 px-1"
                                    aria-hidden="true">
                                    </span>',
                                                              [
                                        'class' => 'btn bg-white border-dark view-btn',
                                        'aria-label' =>'Toggle view to table layout',
                                        'escapeTitle' => false,
                                        'title' => 'Table layout'
                                    ]
                                );?>
                            <?= $this->Form->end()?>
                        <?php elseif ($LayoutType == 2): ?>
                            <?= $this->Form->create(null, [
                                'url' => [
                                    $searchId,
                                    '?' => array_merge($_GET, ['LayoutType' => 1])
                                ]
                                                          ]); ?>
                                <?= $this->Form->button(
                                                              '<span
                                    class="fa fa-th-large fa-2x pt-2 pb-1 px-1"
                                    aria-hidden="true">
                                    </span>',
                                                              [
                                        'class' => 'btn bg-white border-dark ml-4 view-btn',
                                        'aria-label' =>'Toggle view to card layout',
                                        'escapeTitle' => false,
                                        'title' => 'Card layout'
                                    ]
                                ); ?>
                            <?= $this->Form->end()?>
                            <?= $this->Form->button(
                                    '<span
                                class="fa fa-bars fa-2x pt-2 pb-1 px-1"
                                aria-hidden="true">
                                </span>',
                                    [
                                    'class' => 'btn bg-dark text-white border-dark view-btn',
                                    'aria-label' =>'Toggle view to table layout',
                                    'escapeTitle' => false,
                                    'title' => 'Table layout'
                                ]
                            );?>
                        <?php endif;?>
                    </div>
                </div>
            </div>
            <!-- Display query on successful search -->
            <div class="query-success">Search results for <span class = "queryString"> "<?php echo $query ?>"</span>
              <a href="#share-modal"><i style="color:#1661ab" class="fa fa-solid fa-share"></i></a>
            </div>
            <div class=" d-md-none mt-5">
                <?php if ($LayoutType !=3): ?>
                    <div>
                      <div style="display: flex; align-items: center;">
                        <?php if ($LayoutType == 1): ?>
                            <?php/*= $this->Form->create(null, [
                                'type'=>'get',
                                'class'=>'text-left mt-2'
                                ]) */?>
                            <!--    <label for="filterSelect">Sort by:</label><br>-->
                                <?php/*=  $this->Form->select('sort-order:', $filterOptions, [
                                    'id' => 'filterSelect',
                                    'class' => 'bg-white py-2 mt-2',
                                    'style' => 'width: 250px'
                                ]) */?>
                            <?php /*= $this->Form->end() */?>
                            <button class="show-card-btn mt-2 filter-button" id="menubar" onclick="openFilterSlideMenu()">
                                <img src="/images/filter-icon.svg" alt="filter-icon-img"/>
                            </button>
                        <?php endif;?>
                      </div>
                      <div role="menubar" id="filter-menu" class="side-nav shadow d-none">
                          <button aria-label="Close" id="close-button" class="sidebar-btn-close px-3 border-0" onclick="closeFilterSlideMenu()" style="cursor: pointer;">
                            ×
                          </button>
                          <div class="pull-left ml-4">
                            <h3>Filter Options</h3>
                          </div>
                          <br>
                          <!-- <div style="margin-top: 50px;">
                              <div class="text-left py-1 m-4">
                                  Show Text
                                  <label class="toggle-search-filter pull-right">
                                    <input type="checkbox">
                                    <span class="slider"></span>
                                  </label>
                              </div>
                              <div class="text-left py-1 m-4">
                                  Format Text
                                  <label class="toggle-search-filter pull-right">
                                    <input type="checkbox">
                                    <span class="slider"></span>
                                  </label>
                              </div>
                          </div> -->

                        <div class="small-dev-filters">
                          <?= $this->Form->create(null, [
                            'type'=>'POST', 'url' => [
                                'filter-'.$searchId,
                                '?' => $_GET
                            ],
                            'class'=>'accordion text-left search-accordion'
                                  ]) ?>
                              <div class="filter-group--header">Show entries with</div>
                              <div class="border p-3">
                                  <div class="row">
                                      <div class="col-sm-12 filter-checkbox">
                                          <?php if (!empty($filterArray['object']['image_type'])):?>
                                                <?php
                                                $arrayKeys = array_keys($filterArray['object']['image_type']);
                                                $listOfFilters = array_combine($arrayKeys, $arrayKeys);
                                                $selectedValues = $filteredArray['object']['image_type'];
                                                ?>
                                                <?=$this->Form->select(
                                                    'image_type',
                                                    $listOfFilters,
                                                    [
                                                        'multiple' => 'checkbox',
                                                        'value' => $selectedValues
                                                    ]
                                                );?>
                                          <?php endif; ?>
                                          <?php if (!empty($searchSettings['inscription'])):?>
                                                <?php if (!empty($filterArray['inscription'])) {
                                                    ?>
                                                    <?php foreach ($filterArray['inscription'] as $subFilter => $values):?>
                                                        <?php
                                                        $arrayKeys = array_keys($values);
                                                    $listOfFilters = array_combine($arrayKeys, $arrayKeys);
                                                    $selectedValues = $filteredArray['inscription'][$subFilter];
                                                    if (!empty($listOfFilters)) {
                                                        $listOfFilters['transliteration'] = $listOfFilters['transliteration'].' ('.$bucketsresult['transliteration'].')';
                                                    } ?>
                                                        <?=$this->Form->select(
                                                            $subFilter,
                                                            $listOfFilters,
                                                            [
                                                                'multiple' => 'checkbox',
                                                                'value' => $selectedValues
                                                            ]
                                                        ); ?>
                                                    <?php endforeach; ?>
                                                <?php
                                                } else {
                                                    ?>
                                                    <?php foreach ($searchSettings['inscription'] as $subFilter => $values):?>
                                                        <?=$this->Form->select(
                                                            $values,
                                                            [
                                                                'transliteration' => 'transliteration',
                                                            ],
                                                            [
                                                                'multiple' => 'checkbox',
                                                                    'value' => $subFilter
                                                            ]
                                                        ); ?>
                                                    <?php endforeach; ?>
                                                <?php
                                                } ?>
                                          <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                                <?php if (!empty($searchSettings['object'])):?>
                                    <div class="filter-group--header mt-3">Artifact</div>
                                    <?php foreach ($searchSettings['object'] as $key => $val):?>
                                        <?php if (!(array_key_exists($val, $filterArray['object'])) && $val != "image_type"):?>
                                            <?php
                                            $arr = ['dates_referenced','archive', 'excavation_no','written_in','accession_no'];
                                            if (in_array($val, $arr)) {
                                                continue;
                                            }
                                            ?>
                                            <?= $this->Accordion->partOpen($val, $mappingSearchSettings['object'][$val]) ?>
                                                <?= $this->Accordion->partClose() ?>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                    <?php if (!empty($filterArray['object'])): ?>
                                        <?php foreach ($filterArray['object'] as $subFilter => $values):?>
                                            <?php if (in_array($subFilter, $searchSettings['object']) && $subFilter !== 'image_type'): ?>
                                                <?= $this->Accordion->partOpen($subFilter, $mappingSearchSettings['object'][$subFilter]) ?>
                                            <div class="border filter-modal-block p-3">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                    <?php
                                                        $arrayKeys = array_keys($values);
                                                        $listOfFilters = array_combine($arrayKeys, $arrayKeys);
                                                        $selectedValues = $filteredArray['object'][$subFilter];
                                                        $listOfFilters = updateFilters($listOfFilters, $bucketsresult, $parentChildArray);
                                                    ?>
                                                    <?=$this->Form->select(
                                                        $subFilter,
                                                        $listOfFilters,
                                                        [
                                                            'multiple' => 'checkbox',
                                                            'value' => $selectedValues
                                                        ]
                                                    );?>
                                                    </div>
                                                </div>
                                            </div>
                                                <?= $this->Accordion->partClose() ?>
                                            <?php endif; ?>
                                        <?php endforeach;?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            <?php if (!empty($searchSettings['textual'])): ?>
                                            <div class="filter-group--header mt-3">Textual data</div>
                                            <?php foreach ($searchSettings['textual'] as $key => $val):?>
                                                <?php if (!(array_key_exists($val, $filterArray['textual']))):?>
                                                    <?= $this->Accordion->partOpen($val, $mappingSearchSettings['textual'][$val]) ?>
                                                    <?= $this->Accordion->partClose() ?>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                            <?php if (!empty($filterArray['textual'])): ?>
                                                <?php foreach ($filterArray['textual'] as $subFilter => $values):?>
                                                    <?php if (in_array($subFilter, $searchSettings['textual'])): ?>
                                                        <?= $this->Accordion->partOpen($subFilter, $mappingSearchSettings['textual'][$subFilter]) ?>
                                                <div class="border filter-modal-block p-3">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <?php
                                                            $arrayKeys = array_keys($values);
                                                            $listOfFilters = array_combine($arrayKeys, $arrayKeys);
                                                            $selectedValues = $filteredArray['textual'][$subFilter];
                                                            $listOfFilters = updateFilters($listOfFilters, $bucketsresult, $parentChildArray);
                                                            ?>
                                                            <?=$this->Form->select(
                                                                $subFilter,
                                                                $listOfFilters,
                                                                [
                                                                    'multiple' => 'checkbox',
                                                                    'value' => $selectedValues
                                                                ]
                                                            );?>
                                                        </div>
                                                    </div>
                                                </div>
                                                        <?= $this->Accordion->partClose() ?>
                                                    <?php endif; ?>
                                                <?php endforeach;?>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                        <?php if (!empty($searchSettings['publication'])): ?>
                                <div class="filter-group--header mt-3">Publication</div>
                                    <?php foreach ($searchSettings['publication'] as $key => $val):?>
                                        <?php if (!(array_key_exists($val, $filterArray['publication']))):?>
                                        <?= $this->Accordion->partOpen($val, $mappingSearchSettings['publication'][$val]) ?>
                                          <?= $this->Accordion->partClose() ?>
                                      <?php endif; ?>
                                  <?php endforeach; ?>
                                  <?php if(!empty($filterArray['textual'])): ?>
                                      <?php foreach ($filterArray['textual'] as $subFilter => $values):?>
                                          <?php if(in_array($subFilter, $searchSettings['textual'])): ?>
                                              <?= $this->Accordion->partOpen($subFilter, $mappingSearchSettings['textual'][$subFilter]) ?>
                                                  <div class="border filter-modal-block p-3">
                                                      <div class="row">
                                                          <div class="col-sm-12">
                                                              <?php
                                                  $arrayKeys = array_keys($values);
                                              $listOfFilters = array_combine($arrayKeys, $arrayKeys);
                                              $selectedValues = $filteredArray['textual'][$subFilter];
                                              $listOfFilters = updateFilters($listOfFilters, $bucketsresult, $parentChildArray);
                                              ?>
                                                              <?=$this->Form->select(
                                                  $subFilter,
                                                  $listOfFilters,
                                                  [
                                                                      'multiple' => 'checkbox',
                                                                      'value' => $selectedValues
                                                                  ]
                                              );?>
                                                          </div>
                                                      </div>
                                                  </div>
                                              <?= $this->Accordion->partClose() ?>
                                          <?php endif; ?>
                                      <?php endforeach;?>
                                  <?php endif; ?>
                              <?php endif; ?>

                              <?php if (!empty($searchSettings['publication'])): ?>
                                  <div class="filter-group--header mt-3">Publication</div>
                                  <?php foreach ($searchSettings['publication'] as $key => $val):?>
                                      <?php if(!(array_key_exists($val, $filterArray['publication']))):?>
                                          <?= $this->Accordion->partOpen($val, $mappingSearchSettings['publication'][$val]) ?>
                                          <?= $this->Accordion->partClose() ?>
                                      <?php endif; ?>
                                  <?php endforeach; ?>
                                  <?php if(!empty($filterArray['publication'])): ?>
                                      <?php foreach ($filterArray['publication'] as $subFilter => $values):?>
                                          <?php if (in_array($subFilter, $searchSettings['publication'])): ?>
                                              <?= $this->Accordion->partOpen($subFilter, $mappingSearchSettings['publication'][$subFilter]) ?>
                                                  <div class="border filter-modal-block p-3">
                                                      <div class="row">
                                                          <div class="col-sm-12">
                                                              <?php
                                                                  $arrayKeys = array_keys($values);
                                              $listOfFilters = array_combine($arrayKeys, $arrayKeys);
                                              $selectedValues = $filteredArray['publication'][$subFilter];
                                              $listOfFilters = updateFilters($listOfFilters, $bucketsresult, $parentChildArray);
                                              ?>
                                                              <?=$this->Form->select(
                                                  $subFilter,
                                                  $listOfFilters,
                                                  [
                                                                      'multiple' => 'checkbox',
                                                                      'value' => $selectedValues
                                                                  ]
                                              );?>
                                                          </div>
                                                      </div>
                                                  </div>
                                              <?= $this->Accordion->partClose() ?>
                                          <?php endif; ?>
                                      <?php endforeach;?>
                                  <?php endif; ?>
                              <?php endif; ?>

                              <div class="d-flex justify-content-center mt-4">
                                  <?=$this->form->submit("Apply", [
                                      'id' => 'filter-button-apply',
                                      'class' => 'btn cdli-btn-blue rounded-0 px-5 py-2 mb-4'
                                  ])?>
                              </div>

                          <?= $this->Form->end()?>
                        </div>

                      </div>

                      <div class="d-flex align-items-center justify-content-between mt-4">
                          <span class="text-muted result-text">Showing <?= count($result);?> entries of <?= $totalHits ?> results</span>
                      </div>
                    </div>
                <?php endif;?>
            </div>

        <!-- End Filter trigger button and  dropdown and pagination controls -->
        <!-- Download dropdown and pagination controls -->
        <?php if ($LayoutType !=3): ?>
            <div class="d-none d-md-flex align-items-center justify-content-between mt-5">
                <div>
                    <span class="text-muted">Showing <?= count($result);?> entries of <?= $totalHits ?> results</span>
                </div>

                <nav>
                    <ul class="pagination pagination-dark my-1 d-flex justify-content-md-end justify-content-center" id='page-bar'>
                        <?php
                            $previous = $Page == 1 ? '-' : $Page - 1;
            $current =  $Page;
            $currentplus = $Page + 2;
            $currentminus = $Page - 2;
            $next = ($Page + 1) > $lastPage ? '-' : $Page + 1;
            ?>
                        <li class="page-item" id="page-first">
                            <?= $this->Form->create(null, [
                    'url' => [
                        $searchId,
                        '?' => $Page == 1 ? $_GET : array_merge($_GET, ['Page' => 1 ])
                    ]
                ]); ?>
                                <?= $this->Form->button(
                    $Page == 1 ? '<img class="arrow-svg rotate-svg" src="/images/arrow1.svg" alt="Last Page"' : '<img class="arrow-svg rotate-svg" src="/images/arrow2.svg" alt="Last Page">',
                    [
                                        $Page == 1 ? 'disabled' : '',
                                        'class' => 'page-link text-dark',
                                        'aria-label' => 'Previous button',
                                        'escapeTitle' => false,
                                        'title' => 'First page'
                                    ]
                ); ?>
                            <?= $this->Form->end()?>
                        </li>
                        <?php
                        if ($Page > 2) {
                            echo '<li class="page-item page-link text-dark page-custom border">...</li>';
                        }
            ?>
                        <?php if ($next == '-' && $currentminus > 0): ?>
                        <li class="page-item" id="page-3">
                            <?= $this->Form->create(null, [
                    'url' => [
                        $searchId,
                        '?' => array_merge($_GET, ['Page' => $currentminus ])
                    ]
                ]); ?>
                                <?= $this->Form->button(
                    $currentminus,
                    [
                                        'class' => 'page-link text-dark page-custom border',
                                        'escapeTitle' => false,
                                        'title' => $currentminus
                                    ]
                ); ?>
                            <?= $this->Form->end()?>
                        </li>
                        <?php endif; ?>
                        <?php if (!($previous == '-')): ?>
                        <li class="page-item" id="page-1">
                            <?= $this->Form->create(null, [
                                'url' => [
                                    $searchId,
                                    '?' => array_merge($_GET, ['Page' => $previous ])
                                ]
                            ]); ?>
                                <?= $this->Form->button(
                                $previous,
                                [
                                        'class' => 'page-link text-dark page-custom border',
                                        'escapeTitle' => false,
                                        'title' => $previous
                                    ]
                            ); ?>
                            <?= $this->Form->end()?>
                        </li>
                        <?php endif;?>
                        <li class="page-item" id="page-2">
                            <?= $this->Form->button(
                                $current,
                                [
                                    'disabled',
                                    'class' => 'page-link text-light bg-dark page-custom border',
                                    'escapeTitle' => false,
                                    'title' => $current
                                ]
                                ); ?>
                        </li>
                        <?php if (!($next == '-')): ?>
                        <li class="page-item" id="page-3">
                            <?= $this->Form->create(null, [
                                'url' => [
                                    $searchId,
                                    '?' => array_merge($_GET, ['Page' => $next ])
                                ]
                            ]); ?>
                                <?= $this->Form->button(
                                $next,
                                [
                                        'class' => 'page-link text-dark page-custom border',
                                        'escapeTitle' => false,
                                        'title' => $next
                                    ]
                            ); ?>
                            <?= $this->Form->end()?>
                        </li>
                        <?php endif;?>
                        <?php if ($previous == '-' && $lastPage > $currentplus): ?>
                        <li class="page-item" id="page-3">
                            <?= $this->Form->create(null, [
                                'url' => [
                                    $searchId,
                                    '?' => array_merge($_GET, ['Page' => $currentplus ])
                                ]
                            ]); ?>
                                <?= $this->Form->button(
                                $currentplus,
                                [
                                        'class' => 'page-link text-dark page-custom border',
                                        'escapeTitle' => false,
                                        'title' => $currentplus
                                    ]
                            ); ?>
                            <?= $this->Form->end()?>
                        </li>
                        <?php endif; ?>
                        <?php
                        if ($Page < ($lastPage - 1)) {
                            echo '<li class="page-item page-link text-dark page-custom border">...</li>';
                        }
            ?>
                        <li class="page-item" id="page-last">
                            <?= $this->Form->create(null, [
                    'url' => [
                        $searchId,
                        '?' => $lastPage == $Page ? $_GET : array_merge($_GET, ['Page' => $lastPage ])
                    ]
                ]); ?>
                                <?= $this->Form->button(
                    $lastPage == $Page ? '<img class="arrow-svg" src="/images/arrow1.svg" alt="Last Page">' : '<img class="arrow-svg" src="/images/arrow2.svg" alt="Last Page">',
                    [
                                        $lastPage == $Page ? 'disabled' : '',
                                        'class' => 'page-link text-dark',
                                        'aria-label' => 'Next button',
                                        'escapeTitle' => false,
                                        'title' => 'Last page'
                                    ]
                ); ?>
                            <?= $this->Form->end()?>
                        </li>
                    </ul>
                </nav>
            </div>
        <?php endif;?>
        <!-- End Download dropdown and pagination controls -->

        <?php if ($LayoutType == 2):?>
            <div class="mt-5 rectangle-2 p-3 extended-search-actions" id="sort-options-bar">
        <!--        <span class="font-weight-bold">SORT OPTIONS:</span>

                <?php /*= $this->Form->create(null, ['type'=>'get','class'=>'sort-options'])*/?>
                    <div>
                        <label for="sort-options-1">1.</label>
                        <?php /*$this->Form->select('sort-order:', ['option1'], ['id' => 'sort-options-1', 'class' => 'bg-white pl-1 ml-1 py-2 sort-by-dd'])*/?>
                    </div>

                    <div>
                        <label for="sort-options-2">2.</label>
                        <?php /*= $this->Form->select('sort-order:', ['option2'], ['id' => 'sort-options-2', 'class' => 'bg-white pl-1 ml-1 py-2 sort-by-dd'])*/?>
                    </div>

                    <div>
                        <label for="sort-options-3">3.</label>
                        <?php /*= $this->Form->select('sort-order:', ['option3'], ['id' => 'sort-options-3', 'class' => 'bg-white pl-1 ml-1 py-2 sort-by-dd']) */?>
                    </div>
                <?php /*= $this->Form->end() */?>-->
            </div>
        <?php endif;?>

        <?php if (count($appliedFilters) > 0 && $LayoutType!=3): ?>
            <div class="mt-4 rectangle-2 p-3 extended-search-actions">
                <span class="font-weight-bold">APPLIED FILTERS:</span>
                <div>
                    <?php foreach ($appliedFilters as $filter):?>
                        <div class="btn bg-white p-2 m-1">
                            <?=$filter?>
                            <button class="btn bg-white p-0" aria-label="Remove filter button" title="Remove this filter from current results">
                                <span class="fa fa-times-circle" aria-hidden="true"></span>
                            </button>
                        </div>
                    <?php endforeach;?>
                </div>
                <?= $this->Form->create(null, [
                    'type'=>'POST', 'url' => [
                        'filter-reset-'.$searchId,
                        '?' => $_GET
                    ]
                ]) ?>
                <?= $this->Form->button(
                    'Clear all',
                    [
                        'class' => 'btn bg-dark text-white',
                        'aria-label' => 'Clear all',
                        'title' => 'Clear all applied filters'
                    ]
                ); ?>
                <?= $this->Form->end() ?>
            </div>
        <?php endif; ?>
        <input type="checkbox" id="hide-left-menu" name="hide-left-menu"/>
        <div class="row" id="moreOptionToggle">
            <div class="sideFilterBlockOptions d-none d-md-block col-md-3 mt-5">
                <p class="text-left font-weight-bold sidebarFilter-p float-left" style="font-size: large;">
                  <label for="hide-left-menu" class="pull-right btn btn-outline-secondary btn-sm" title="Toggle filter menu" style="font-size:inherit;">
                    More Options   <i class="fa fa-plus-circle" aria-hidden="true"></i><i class="fa fa-minus-circle" aria-hidden="true"></i>
                  </label>
                </p>
            </div>
        </div>
        <div class="row">
            <?php if ($compactFilterStatus || $fullFilterStatus): ?>
                <div class="sideFilterBlock sideFilterBlock-with-maxheight d-none d-md-block col-md-3 mt-5">

                        <div class="row text-left" style="justify-content: space-between;padding: inherit;">
                            <span type="text" class="text-large" style="color: #343a40;">MORE OPTIONS</span>
                            <label for="hide-left-menu" class="pull-right toggle-close-icon" title="Toggle filter menu">
                              <i class="fa fa-caret-left" aria-hidden="true"></i>
                            </label>
                        </div>
                        <hr>
                        <div>
                            <!-- <div class="text-left py-1">
                                Show Text
                                <label class="toggle pull-right">
                                  <input type="checkbox">
                                  <span class="slider"></span>
                                </label>
                            </div>  -->
                            <!-- <div class="text-left py-1">
                                Format Text
                                <label class="toggle pull-right">
                                  <input type="checkbox">
                                  <span class="slider"></span>
                                </label>
                            </div>  -->
                        </div>
                        <div class="pt-3 pb-3 text-left">
                            <?php if ($LayoutType == 1): ?>
                                <?= $this->Form->create(null, ['type'=>'get','id'=>'sort-by-dd'])?>
                                    <label for="filterSelect">Sort by:</label><br>
                                    <?= $this->Form->select('sort-order:', $filterOptions, [
                                        'id' => 'filterSelect',
                                        'class' => 'bg-white pl-1 mt-1 py-2 sort-by-dd',
                                        'style' => 'width: -webkit-fill-available !important'
                                    ])?>
                                <?= $this->Form->end() ?>
                            <?php endif;?>
                        </div>

                        <!--Filters  -->
                        <?= $this->Form->create(null, [
                            'type'=>'POST', 'url' => [
                                'filter-'.$searchId,
                                '?' => $_GET
                            ],
                            'class'=>'accordion text-left search-accordion'
                        ]) ?>

                                <div class="pb-3">
                                    <div class="pull-left"><span type="text" class="filter-option-heading">Filter Options</span></div>
                                    <div class="pull-right">
                                      <?=$this->form->submit("Apply", [
                                        'class' => 'text-large filter-apply-btn'
                                      ])?>
                                    </div>
                                </div>
                                <br>

                        <div class="d-flex mb-4">
                            <button class="btn bg-white cdli-blue p-0 exp-col-btn" id="filter-div-expand" onclick="expandAll()">Expand all</button>
                            <button class="btn bg-white cdli-blue text-dark p-0 ml-2 exp-col-btn" id="filter-div-collapse" onclick="collapseAll()" disabled>Collapse all</button>
                        </div>

                            <?= $this->Accordion->partOuterOpen('entries', 'Show Entries With') ?>
                                <div class="border p-3">
                                    <div class="row">
                                        <div class="col-sm-12 filter-checkbox">
                                            <?php if (!empty($filterArray['object']['image_type'])): ?>
                                                <?php
                                                    $arrayKeys = array_keys($filterArray['object']['image_type']);
                                                $listOfFilters = array_combine($arrayKeys, $arrayKeys);
                                                $selectedValues = $filteredArray['object']['image_type'];
                                                ?>
                                                <?=$this->Form->select(
                                                    'image_type',
                                                    $listOfFilters,
                                                    [
                                                        'multiple' => 'checkbox',
                                                        'value' => $selectedValues
                                                    ]
                                                );?>
                                            <?php endif; ?>
                                            <?php if (!empty($searchSettings['inscription'])):?>
                                                <?php if (!empty($filterArray['inscription'])) {
                                                    ?>
                                                    <?php foreach ($filterArray['inscription'] as $subFilter => $values):?>
                                                            <?php
                                                                $arrayKeys = array_keys($values);
                                                                $listOfFilters = array_combine($arrayKeys, $arrayKeys);
                                                                $selectedValues = $filteredArray['inscription'][$subFilter];
                                                                if(!empty($listOfFilters)) {
                                                                    $listOfFilters['transliteration'] = $listOfFilters['transliteration'].' ('.$bucketsresult['transliteration'].')';
                                                                }
                                                            ?>
                                                            <?=$this->Form->select(
                                                            $subFilter,
                                                            $listOfFilters,
                                                            [
                                                                    'multiple' => 'checkbox',
                                                                    'value' => $selectedValues
                                                                ]
                                                        ); ?>
                                                    <?php endforeach; ?>
                                                <?php
                                                } else {
                                                    ?>
                                                    <?php foreach ($searchSettings['inscription'] as $subFilter => $values):?>
                                                        <?=$this->Form->select(
                                                            $values,
                                                            [
                                                                'transliteration' => 'transliteration',
                                                            ],
                                                            [
                                                                'multiple' => 'checkbox',
                                                                'value' => $subFilter
                                                            ]
                                                            ); ?>
                                                    <?php endforeach; ?>
                                                <?php
                                                } ?>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            <?= $this->Accordion->partClose() ?>

                            <?= $this->Accordion->partOuterOpen('artifacts', 'Artifacts') ?>
                                <?php if (!empty($searchSettings['object'])): ?>
                                    <?php foreach ($searchSettings['object'] as $key => $val):?>
                                        <?php if (!(array_key_exists($val, $filterArray['object'])) && $val != "image_type"):?>
                                            <?php
                                                $arr = ['dates_referenced','archive', 'excavation_no','written_in','accession_no'];
                                            if (in_array($val, $arr)) {
                                                continue;
                                            }
                                            ?>
                                                <?= $this->Accordion->partOpen($val, $mappingSearchSettings['object'][$val]) ?>
                                                <?= $this->Accordion->partClose() ?>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                    <?php if (!empty($filterArray['object'])): ?>
                                        <?php foreach ($filterArray['object'] as $subFilter => $values):?>
                                            <?php if (in_array($subFilter, $searchSettings['object']) && $subFilter !== 'image_type'): ?>
                                                <?= $this->Accordion->partOpen(h($subFilter).'-desktop', $mappingSearchSettings['object'][$subFilter]) ?>
                                                    <div class="border filter-modal-block p-3">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <?php
                                                                    $arrayKeys = array_keys($values);
                                                $listOfFilters = array_combine($arrayKeys, $arrayKeys);
                                                $selectedValues = $filteredArray['object'][$subFilter];
                                                $listOfFilters =  updateFilters($listOfFilters, $bucketsresult, $parentChildArray);
                                                ?>
                                                                <?=$this->Form->select(
                                                    $subFilter,
                                                    $listOfFilters,
                                                    [
                                                                        'multiple' => 'checkbox',
                                                                        'value' => $selectedValues
                                                                    ]
                                                );
                                                ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?= $this->Accordion->partClose() ?>
                                            <?php endif; ?>
                                        <?php endforeach;?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            <?= $this->Accordion->partClose() ?>

                            <?= $this->Accordion->partOuterOpen('textual', 'Textual') ?>
                                <?php if (!empty($searchSettings['textual'])): ?>
                                    <?php foreach ($searchSettings['textual'] as $key => $val):?>
                                        <?php if (!(array_key_exists($val, $filterArray['textual']))):?>
                                            <?= $this->Accordion->partOpen($val, $mappingSearchSettings['textual'][$val]) ?>
                                            <?= $this->Accordion->partClose() ?>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                    <?php if (!empty($filterArray['textual'])): ?>
                                        <?php foreach ($filterArray['textual'] as $subFilter => $values):?>
                                            <?php if (in_array($subFilter, $searchSettings['textual'])): ?>
                                                <?= $this->Accordion->partOpen(h($subFilter).'-desktop', $mappingSearchSettings['textual'][$subFilter]) ?>
                                                    <div class="border filter-modal-block p-3">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <?php
                                                    $arrayKeys = array_keys($values);
                                                $listOfFilters = array_combine($arrayKeys, $arrayKeys);
                                                $selectedValues = $filteredArray['textual'][$subFilter];
                                                $listOfFilters = updateFilters($listOfFilters, $bucketsresult, $parentChildArray);
                                                ?>
                                                                <?=$this->Form->select(
                                                    $subFilter,
                                                    $listOfFilters,
                                                    [
                                                                        'multiple' => 'checkbox',
                                                                        'value' => $selectedValues
                                                                    ]
                                                );?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?= $this->Accordion->partClose() ?>
                                            <?php endif; ?>
                                        <?php endforeach;?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            <?= $this->Accordion->partClose() ?>

                            <?= $this->Accordion->partOuterOpen('publications', 'Publications') ?>
                                <?php if (!empty($searchSettings['publication'])): ?>
                                    <?php foreach ($searchSettings['publication'] as $key => $val):?>
                                        <?php if (!(array_key_exists($val, $filterArray['publication']))):?>
                                            <?= $this->Accordion->partOpen($val, $mappingSearchSettings['publication'][$val]) ?>
                                            <?= $this->Accordion->partClose() ?>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                    <?php if (!empty($filterArray['publication'])): ?>
                                        <?php foreach ($filterArray['publication'] as $subFilter => $values):?>
                                            <?php if (in_array($subFilter, $searchSettings['publication'])): ?>
                                                <?= $this->Accordion->partOpen(h($subFilter).'-desktop', $mappingSearchSettings['publication'][$subFilter]) ?>
                                                    <div class="border filter-modal-block p-3">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <?php
                                                                    $arrayKeys = array_keys($values);
                                                                    $listOfFilters = array_combine($arrayKeys, $arrayKeys);
                                                                    $selectedValues = $filteredArray['publication'][$subFilter];
                                                                    $listOfFilters =  updateFilters($listOfFilters, $bucketsresult, $parentChildArray);
                                                                ?>
                                                                <?=$this->Form->select(
                                                    $subFilter,
                                                    $listOfFilters,
                                                    [
                                                                        'multiple' => 'checkbox',
                                                                        'value' => $selectedValues
                                                                    ]
                                                );?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?= $this->Accordion->partClose() ?>
                                            <?php endif; ?>
                                        <?php endforeach;?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            <?= $this->Accordion->partClose() ?>

                        <?= $this->Form->end()?>
                        <!-- end Filters -->

                        <!-- Advnaced Options Menu -->
                        <br>
                        <div class="text-left mb-2 adv-features-heading"><span type="text" class="text-large">Advanced Features</span></div>
                        <div class="text-left p-3 adv-feature-border">
                              <?php
                                  // Add a link to the commodity visualization if there are results that can be visualized:
                                  if (count($commodityVizIds) > 0) {
                                      $commodityURL = $this->URLEncoding->getEncodedCommodityURL($commodityVizIds);
                                      if (strlen($commodityURL) <= 2048) {
                                          echo "<div class='mt-2'><span class='fa fa-external-link mr-1' aria-hidden='true'></span><a href='" . $commodityURL . "' style='color: black;'>Visualize commodity information from these texts</a></div>";
                                      } else {
                                          // Link is too long. Could show a message to let user know they can visualize the results if they prune a little.
                                      }
                                  }
    ?>
                            <div class="mt-2"><span class="fa fa-external-link mr-1" aria-hidden="true"></span><a href="/uqnu" style="color: black;"> Uqnu ATf Editor</a></div>
                            <div class="mt-2">
                                <label class="download-button-search-page" for="modal-1">
                                    <span class="fa fa-download mr-1" aria-hidden="true"></span><span class="download-result"> Download Search Results</span>
                                </label>
                            </div>
                        </div>
                </div>
            <?php endif; ?>
            <?php echo ($compactFilterStatus || $fullFilterStatus) ?
            "<div class='col-sm-12 col-md-9 mt-5'>" : "<div class='overflow-wrapper w-100'>"; ?>
                <?php if ($LayoutType == 1): ?>
                    <!-- show card layout -->
                    <div class="search-grid-layout text-left container mt-5" id="card-view">
                            <?php foreach ($result as $artifactId=>$row):?>
                                <div class="row mb-5">
                                    <?php
                                        $images = $this->ArtifactAssets->orderAssets($imagesLinks[$artifactId]);
                                        $hasThumbnail = (count($images) > 0) && !is_null($images[0]->getThumbnailPath());
                                        $thumbnail = $this->ArtifactAssets->getThumbnail(array_slice($images, 0, 1));
                                        $showingLineart = $hasThumbnail && $images[0]->asset_type === 'lineart';
                                    ?>
                                    <div class="col-lg-5 col-md-5 d-none d-md-block <?= $showingLineart ? 'bg-white' : '' ?>" style="background-color: #000;">
                                        <div class="search-media">
                                            <?php if ($hasThumbnail): ?>
                                                <a href="<?= $this->Url->build([
                                                    'controller' => 'Artifacts',
                                                    'action' => 'reader',
                                                    $artifactId,
                                                    $images[0]->id
                                                ]) ?>">
                                                    <figure>
                                                        <?= $this->Html->image($thumbnail, ['alt' => 'Artifact photograph']) ?>
                                                        <figcaption>
                                                            View full image
                                                            <span class="fa fa-share-square-o" aria-hidden="true"></span>
                                                        </figcaption>
                                                    </figure>
                                                </a>
                                            <?php else: ?>
                                                <figure>
                                                    <?= $this->Html->image($thumbnail, ['alt' => 'Artifact photograph']) ?>
                                                </figure>
                                            <?php endif; ?>

                                            <?php foreach (array_slice($images, 1) as $asset): ?>
                                                <p class="text-center">
                                                    <?= $this->Html->link('View ' . $asset->getDescription(), [
                                                        'controller' => 'Artifacts',
                                                        'action' => 'reader',
                                                        $artifactId,
                                                        $asset->id
                                                    ]) ?>
                                                </p>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                    <div class="search-result-card border px-4 py-5 col-lg-7 col-md-7">
                                        <h2><?= $this->Html->link(
                                    empty($row["artifact"]) ? '' : $row["artifact"]["designation"].' (P' . str_pad($artifactId, 6, '0', STR_PAD_LEFT).')',
                                    ['controller' => 'Artifacts', 'action' => 'view', $artifactId],
                                    ['class' => 'display-5', 'target' => '_blank']
                                );?></h2>

                                <?php if ($showedit): ?>
                                <div>    <?= $this->Html->link(
                                    'Edit Metadata',
                                    ['controller' => 'Artifacts', 'action' => 'edit', $artifactId],
                                                ['class'=> 'btn btn-light']
                                            ) ?>
                                            <?php if (!empty($row['inscription'])): ?>
                                                <?= $this->Html->link(
                                                'Edit Inscription',
                                                ['controller' => 'Inscriptions', 'action' => 'edit', $row['inscription']['id']],
                                                ['class'=> 'btn btn-light']
                                                ) ?>
                                            <?php else: ?>
                                                <?= $this->Html->link(
                                                    'Add Inscription',
                                                    ['controller' => 'Inscriptions', 'action' => 'add', '?' => ['artifact' => $artifactId]],
                                                //    ['class' => 'btn btn-primary cdli-btn-blue ml-3 p-2 w-30']
                                                ['class'=> 'btn btn-light']
                                                ) ?>
                                            <?php endif; ?>
                                        </div>
                                <?php endif; ?>


                                        <p class="mt-3 text-uppercase">
                                            <?php if (trim($row['artifact']['aseal_no'])): ?>
                                                <span class="p-2 badge badge-secondary"><?= __('Seal') ?></span>
                                            <?php endif; ?>
                                            <?php if (trim($row['artifact']['acomposite_no'])): ?>
                                                <span class="p-2 badge badge-secondary"><?= __('Composite') ?></span>
                                            <?php endif; ?>
                                            <?php if (trim($row['artifact']['seal_no'])): ?>
                                                <span class="p-2 badge badge-secondary"><?= __('Has seal impression(s)') ?></span>
                                            <?php endif; ?>
                                            <?php if (trim($row['artifact']['composite_no'])): ?>
                                                <span class="p-2 badge badge-secondary"><?= __('Witness') ?></span>
                                            <?php endif; ?>
                                            <?php if ($row['inscription'] && !$row['inscription']['is_atf2conll_diff_resolved']): ?>
                                                <span class="p-2 badge badge-secondary"><?= __('Annotation unreviewed') ?></span>
                                            <?php endif; ?>
                                        </p>

                                        <?php if (trim($row['artifact']['aseal_no'])): ?>
                                            <p><b>Seal No.:</b>
                                                <?=$row['artifact']['aseal_no']?>
                                            </p>
                                        <?php endif; ?>
                                        <?php if (trim($row['artifact']['acomposite_no'])): ?>
                                            <p><b>Witness to composite(s):</b>
                                                <?=$row['artifact']['composite_no']?>
                                            </p>
                                        <?php endif; ?>

                                        <?php if (trim($row['artifact']['seal_no'])): ?>
                                            <p><b>Impressed with seal(s):</b>
                                                <?=$row['artifact']['seal_no']?>
                                            </p>
                                        <?php endif; ?>
                                        <?php if (trim($row['artifact']['composite_no'])): ?>
                                            <p><b>Composite No.:</b>
                                                <?=$row['artifact']['acomposite_no']?>
                                            </p>
                                        <?php endif; ?>



                                            <p><b>Primary Publication:</b>
                                            <?php if (trim($row['artifact']['pub'])): ?>
                                                <?php foreach ($row['artifact']['pub']['publication']['authors'] as $author_index => $author): ?>
                                                    <?=$author->author ?>
                                                    <?= $author_index < count($row['artifact']['pub']['publication']['authors']) - 1 ? '; ' : '' ?>
                                                <?php endforeach; ?>
                                                <?=

                                                ' ('.
                                                $row['artifact']['pub']['publication']['year']
                                                .') '.
                                                $row['artifact']['pub']['publication']['designation']
                                                .' '.
                                                $row['artifact']['pub']['exact_reference']
                                                ?>
                                            </p>
<?php endif;?>
                                        <?php if (in_array('collection', $searchSettings['full:object'])) :?>
                                            <?php $collection = empty($row['collection']) ? 'NA' : $row['collection']['collection']; ?>
                                            <p><b>Collection:</b>
                                                <?=$collection?>
                                            </p>
                                            <?php
                                                $museumNum = empty($row["artifact"]) ? '' : $row["artifact"]["museum_no"];
                                                ?>
                                            <p><b>Museum no.:</b>
                                                <?= $museumNum ?>
                                            </p>
                                        <?php endif;?>
                                        <?php if (in_array('provenience', $searchSettings['full:object'])) :?>
                                            <?php $provenience = empty($row['provenience']) ? '' : $row['provenience']['provenience'];
                                            echo " <p>
                                                <b>
                                                    Provenience:
                                                </b> ".
                                            $provenience
                                            ."</p>"; ?>
                                        <?php endif;?>
                                        <?php if (in_array('period', $searchSettings['full:object'])) :?>
                                           <?php $period = empty($row['period']) ? '' : $row['period']['period'];
                                            echo " <p>
                                                <b>
                                                    Period:
                                                </b> ".
                                                $period
                                            ."</p>"; ?>
                                        <?php endif;?>
                                        <?php if (in_array('atype', $searchSettings['full:object'])) :?>
                                            <?php $ObjectType = empty($row['artifact_type']) ? '' : $row['artifact_type']['artifact_type'];
                                            echo " <p>
                                                <b>
                                                    Object Type:
                                                </b> ".
                                                $ObjectType
                                            ."</p>"; ?>
                                        <?php endif;?>
                                        <?php if (in_array('materials', $searchSettings['full:object'])) :?>
                                            <?php $materials = empty($row['material']) ? '' : $row['material']['material'];
                                            echo " <p>
                                                <b>
                                                    Material:
                                                </b> ".
                                                $materials
                                            ."</p>"; ?>
                                        <?php endif;?>
                                        <?php if (!empty($row['dates'])):?>
                                            <?php

                                                echo " <p>
                                                    <b>
                                                        Date:
                                                    </b>".
                                                    $row['dates']['dates']
                                                ."</p>"
                                            ?>
                                        <?php endif;?>
                                        <?php if (in_array('transliteration', $searchSettings['full:inscription'])) :?>
                                        <?php if (!empty($row['inscription'])):?>
                                            <?php

                                                echo " <p class='mt-3'>
                                                    <b>
                                                        Inscription:
                                                    </b> <br/>
                                                    </p>".
                                                    $row['inscription']['processedInscription'];
                                            ?>
                                        <?php endif;?>
                                        <?php endif;?>

                                    </div>
                                </div>
                            <?php endforeach;?>
                        </div>
                            <!-- end show card layout -->
                        <?php elseif ($LayoutType == 2):?>
                            <!-- show table layout -->
                        <div class="sticky-table-wrapper " style="">
                            <table class="table sticky-table" style="">
                                <thead>
                                  <tr>
                                      <th class="border-right position-sticky text-nowrap">CDLI NO.</th>
                                      <th class="pl-4">Primary Publication</th>
                                      <th class="pl-4">Museum number</th>
                                    <?php if (in_array('genres', $searchSettings['compact:textual'])) :?>
                                      <th><div class="cellwidth">Genre(s)</div></th>
                                    <?php endif;?>
                                    <?php if (in_array('languages', $searchSettings['compact:textual'])) :?>
                                      <th><div class="cellwidth">Language</div></th>
                                    <?php endif;?>
                                    <?php if (in_array('period', $searchSettings['compact:object'])) :?>
                                      <th><div class="cellwidth">Period</div></th>
                                    <?php endif;?>
                                    <?php if (in_array('archive', $searchSettings['compact:object'])) :?>
                                        <th><div class="cellwidth">Archive</div></th>
                                    <?php endif;?>
                                    <?php if (in_array('dates_referenced', $searchSettings['compact:object'])) :?>
                                        <th><div class="cellwidth">Date(s) referenced</div></th>
                                    <?php endif;?>
                                    <?php if (in_array('provenience', $searchSettings['compact:object'])) :?>
                                      <th ><div class="cellwidth">Provenience</div></th>
                                    <?php endif;?>
                                    <?php if (in_array('written_in', $searchSettings['compact:object'])) :?>
                                        <th><div class="cellwidth">Written in</div></th>
                                    <?php endif;?>
                                    <?php if (in_array('excavation_no', $searchSettings['compact:object'])) :?>
                                        <th><div class="cellwidth">Excavation number</div></th>
                                    <?php endif;?>
                                    <?php if (in_array('atype', $searchSettings['compact:object'])) :?>
                                        <th>Artifact type</th>
                                    <?php endif;?>
                                    <?php if (in_array('materials', $searchSettings['compact:object'])) :?>
                                        <th>Material(s)</th>
                                    <?php endif;?>
                                    <?php if (in_array('collection', $searchSettings['compact:object'])) :?>
                                        <th><div class="cellwidth">Collection(s)</div></th>
                                    <?php endif;?>
                                    <?php if (in_array('accession_no', $searchSettings['compact:object'])) :?>
                                        <th><div class="cellwidth">Accession number</div></th>
                                    <?php endif;?>
                                    <th>Date</th>
                                    <?php if (!empty($inscriptionsQuery) && in_array('transliteration', $searchSettings['compact:inscription'])) :?>
                                        <th>Transliteration/Translation</th>
                                    <?php endif;?>
                                  </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($result as $artifactId => $row) :?>
                                            <?php
                                                $museumNum = empty($row["artifact"]) ? '' : $row["artifact"]["museum_no"];
                                        $designation = empty($row["artifact"]) ? '' : $row["artifact"]["designation"];
                                        $pubId = empty($row["artifact"]) ? '' : $row["artifact"]["Pub_id"];
                                        $publicationText = strlen($designation) > 60
                                        ? substr($designation, 0, 60). "..." : $designation;
                                        ?>
                                            <tr class="border-top">
                                                <td class="border-right position-sticky">
                                                    <?= $this->Html->link(
                                            $artifactId,
                                            ['controller' => 'Artifacts', 'action' => 'view', $artifactId],
                                            ['class' => 'active', 'target' => '_blank']
                                        ) ?>
                                                </td>
                                                <td  class="pl-4">
                                                    <?php if (strlen($publicationText)) :?>
                                                        <?= $this->Html->link(
                                            $publicationText,
                                            ['controller' => 'Publications', 'action' => 'view', $pubId],
                                            ['class' => 'active', 'target' => '_blank']
                                                    ) ?>
                                                    <?php else :?>
                                                        <?= "NA" ?>
                                                    <?php endif;?>
                                                </td>
                                                <td  class="pl-4">
                                                    <?php echo $museumNum;?>
                                                </td>
                                                <?php if (in_array('genres', $searchSettings['compact:textual'])) :?>
                                                    <td  class="cellwidth">
                                                        <?=
                                                            $genres = empty($row['genres']) ? '' : $row['genres']['genres'];
                                                    ?>
                                                    </td>
                                                <?php endif;?>
                                                <?php if (in_array('languages', $searchSettings['compact:textual'])) :?>
                                                    <td  class="cellwidth">
                                                        <?=
                                                        $languages = empty($row['languages']) ? '' : $row['languages']['languages'];
                                                    ?>
                                                    </td>
                                                <?php endif;?>
                                                <?php if (in_array('period', $searchSettings['compact:object'])) :?>
                                                    <td  class="cellwidth">
                                                        <?=
                                                        $period = empty($row['period']) ? '' : $row['period']['period'];
                                                    ?>
                                                    </td>
                                                <?php endif;?>
                                                <?php if (in_array('archive', $searchSettings['compact:object'])) :?>
                                                    <td>
                                                        <?=
                                                        $archive = empty($row['archive']) ? '' : $row['archive']['archive'];
                                                    ?>
                                                    </td>
                                                <?php endif;?>
                                                <?php if (in_array('dates_referenced', $searchSettings['compact:object'])) :?>
                                                    <td>
                                                        <?=
                                                        $dates_referenced = empty($row['dates_referenced']) ? '' : $row['dates_referenced']['dates_referenced'];
                                                    ?>
                                                    </td>
                                                <?php endif;?>
                                                <?php if (in_array('provenience', $searchSettings['compact:object'])) :?>
                                                    <td  class="cellwidth">
                                                        <?=
                                                        $provenience = empty($row['provenience']) ? '' : $row['provenience']['provenience'];
                                                    ?>
                                                    </td>
                                                <?php endif;?>
                                                <?php if (in_array('written_in', $searchSettings['compact:object'])) :?>
                                                    <td>
                                                        <?=
                                                        $written_in = empty($row['written_in']) ? '' : $row['written_in']['written_in'];
                                                    ?>
                                                    </td>
                                                <?php endif;?>
                                                <?php if (in_array('excavation_no', $searchSettings['compact:object'])) :?>
                                                    <td>
                                                        <?=
                                                        $excavation_no = empty($row['excavation_no']) ? '' : $row['excavation_no']['excavation_no'];
                                                    ?>
                                                    </td>
                                                <?php endif;?>
                                                <?php if (in_array('atype', $searchSettings['compact:object'])) :?>
                                                    <td>
                                                        <?=
                                                        $ObjectType = empty($row['artifact_type']) ? '' : $row['artifact_type']['artifact_type'];
                                                    ?>
                                                    </td>
                                                <?php endif;?>
                                                <?php if (in_array('materials', $searchSettings['compact:object'])) :?>
                                                    <td>
                                                        <?=
                                                        $materials = empty($row['material']) ? '' : $row['material']['material'];
                                                    ?>
                                                    </td>
                                                <?php endif;?>
                                                <?php if (in_array('collection', $searchSettings['compact:object'])) :?>
                                                    <td>
                                                        <?=
                                                        $ObjectType = empty($row['collection']) ? '' : $row['collection']['collection'];
                                                    ?>
                                                    </td>
                                                <?php endif;?>
                                                <?php if (in_array('accession_no', $searchSettings['compact:object'])) :?>
                                                    <td>
                                                        <?=
                                                        $accession_no = empty($row['accession_no']) ? '' : $row['accession_no']['accession_no'];
                                                    ?>
                                                    </td>
                                                <?php endif;?>
                                                <td>
                                                        <?=
                                                        $dates = empty($row['dates']) ? 'NA' : $row['dates']['dates'];
                                        ?>
                                                </td>
                                                <td class="">
                                                    <!-- Display inscriptions only when searched in inscriptions field for compact search -->
                                                    <?php if (!empty($inscriptionsQuery) && in_array('transliteration', $searchSettings['compact:inscription']) && !empty($row['inscription']['processedInscription'])) :?>
                                                    <div class="td-y-scroll">
                                                        <?php
                                        foreach (explode("<br>", $row['inscription']['processedInscription']) as $line) {
                                            if (preg_match("/highlightText/", $line)) {
                                                echo $line."<br>";
                                            }
                                        }
                                                        ?>
                                                    </div>
                                                    <?php endif;?>
                                                </td>
                                            </tr>
                                    <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>

                    <!-- end show table layout -->

                <?php else:?>
                    <div class="my-5">
                        <p class="my-5">
                            Stats will come here
                        </p>
                    </div>
                <?php endif;?>
            </div>

        </div>
        </div>

        <!-- pagination bottom -->
        <?php if ($LayoutType !=3):?>
            <div class="mt-5 d-md-flex flex-row-reverse justify-content-between align-items-baseline search-bottom-controls">
                <nav class="mb-4">
                    <ul class="pagination pagination-dark my-1 d-flex justify-content-md-end justify-content-center" id='page-bar'>
                        <?php
                            $previous = $Page == 1 ? '-' : $Page - 1;
            $current =  $Page;
            $currentplus = $Page + 2;
            $currentminus = $Page - 2;
            $next = ($Page + 1) > $lastPage ? '-' : $Page + 1;
            ?>
                        <li class="page-item" id="page-first">
                            <?= $this->Form->create(null, [
                    'url' => [
                        $searchId,
                        '?' => $Page == 1 ? $_GET : array_merge($_GET, ['Page' => 1 ])
                    ]
                ]); ?>
                                <?= $this->Form->button(
                    $Page == 1 ? '<img class="arrow-svg rotate-svg" src="/images/arrow1.svg" alt="Last Page"' : '<img class="arrow-svg rotate-svg" src="/images/arrow2.svg" alt="Last Page">',
                    [
                                        $Page == 1 ? 'disabled' : '',
                                        'class' => 'page-link text-dark',
                                        'aria-label' => 'Previous button',
                                        'escapeTitle' => false,
                                        'title' => 'First page'
                                    ]
                ); ?>
                            <?= $this->Form->end()?>
                        </li>
                        <?php
                        if ($Page > 2) {
                            echo '<li class="page-item page-link text-dark page-custom border">...</li>';
                        }
            ?>
                        <?php if ($next == '-' && $currentminus > 0): ?>
                        <li class="page-item" id="page-3">
                            <?= $this->Form->create(null, [
                    'url' => [
                        $searchId,
                        '?' => array_merge($_GET, ['Page' => $currentminus ])
                    ]
                ]); ?>
                                <?= $this->Form->button(
                    $currentminus,
                    [
                                        'class' => 'page-link text-dark page-custom border',
                                        'escapeTitle' => false,
                                        'title' => $currentminus
                                    ]
                ); ?>
                            <?= $this->Form->end()?>
                        </li>
                        <?php endif; ?>
                        <?php if (!($previous == '-')): ?>
                        <li class="page-item" id="page-1">
                            <?= $this->Form->create(null, [
                                'url' => [
                                    $searchId,
                                    '?' => array_merge($_GET, ['Page' => $previous ])
                                ]
                            ]); ?>
                                <?= $this->Form->button(
                                $previous,
                                [
                                        'class' => 'page-link text-dark page-custom border',
                                        'escapeTitle' => false,
                                        'title' => $previous
                                    ]
                            ); ?>
                            <?= $this->Form->end()?>
                        </li>
                        <?php endif;?>
                        <li class="page-item" id="page-2">
                            <?= $this->Form->button(
                                $current,
                                [
                                    'disabled',
                                    'class' => 'page-link text-light bg-dark page-custom border',
                                    'escapeTitle' => false,
                                    'title' => $current
                                ]
                                ); ?>
                        </li>
                        <?php if (!($next == '-')): ?>
                        <li class="page-item" id="page-3">
                            <?= $this->Form->create(null, [
                                'url' => [
                                    $searchId,
                                    '?' => array_merge($_GET, ['Page' => $next ])
                                ]
                            ]); ?>
                                <?= $this->Form->button(
                                $next,
                                [
                                        'class' => 'page-link text-dark page-custom border',
                                        'escapeTitle' => false,
                                        'title' => $next
                                    ]
                            ); ?>
                            <?= $this->Form->end()?>
                        </li>
                        <?php endif;?>
                        <?php if ($previous == '-' && $lastPage > $currentplus): ?>
                        <li class="page-item" id="page-3">
                            <?= $this->Form->create(null, [
                                'url' => [
                                    $searchId,
                                    '?' => array_merge($_GET, ['Page' => $currentplus ])
                                ]
                            ]); ?>
                                <?= $this->Form->button(
                                $currentplus,
                                [
                                        'class' => 'page-link text-dark page-custom border',
                                        'escapeTitle' => false,
                                        'title' => $currentplus
                                    ]
                            ); ?>
                            <?= $this->Form->end()?>
                        </li>
                        <?php endif; ?>
                        <?php
                        if ($Page < ($lastPage - 1)) {
                            echo '<li class="page-item page-link text-dark page-custom border">...</li>';
                        }
            ?>
                        <li class="page-item" id="page-last">
                            <?= $this->Form->create(null, [
                    'url' => [
                        $searchId,
                        '?' => $lastPage == $Page ? $_GET : array_merge($_GET, ['Page' => $lastPage ])
                    ]
                ]); ?>
                                <?= $this->Form->button(
                    $lastPage == $Page ? '<img class="arrow-svg" src="/images/arrow1.svg" alt="Last Page">' : '<img class="arrow-svg" src="/images/arrow2.svg" alt="Last Page">',
                    [
                                        $lastPage == $Page ? 'disabled' : '',
                                        'class' => 'page-link text-dark',
                                        'aria-label' => 'Next button',
                                        'escapeTitle' => false,
                                        'title' => 'Last page'
                                    ]
                ); ?>
                            <?= $this->Form->end()?>
                        </li>
                    </ul>
                </nav>
                <div class="d-flex justify-content-between align-items-baseline">
                    <p class="text-muted mr-2">Results per page: </p>
                    <?php foreach ([25, 50, 100, 500, 1000] as $pageSizeElement) {
                    ?>
                        <?= $this->Form->create(null, [
                            'url' => [
                                $searchId,
                                '?' => modifyPageNumberIfPerSizeChanges($pageSizeElement)
                            ]
                        ]); ?>
                            <?= $this->Form->button(
                            ' <span>'.$pageSizeElement.'</span> ',
                            [
                                    'class' => ($PageSize == $pageSizeElement) ? 'btn bg-white border-dark view-btn mx-1' : 'btn bg-white view-btn mx-1',
                                    'escapeTitle' => false,
                                    'title' => $pageSizeElement
                                ]
                        ); ?>
                        <?= $this->Form->end()?>
                    <?php
                } ?>
                </div>
            </div>

        <?php echo $this->element('smoothscroll'); ?>

        <?php endif;?>
        <!-- end pagination bottom -->

        <!--share modal-->
        <div class="share-modal-overlay" id="share-modal">
            <div class="share-modal-popup">
                <div class="content">
                    <div class="mb-3 d-flex flex-row justify-content-between">
                        <h5 class="lead">Share or save this search query</h5>
                        <a class="share-modal-close" href="#">&times;</a>
                    </div>

                    <div class="d-flex flex-row mb-2">
                        <input id="shareFullURL" class="col-11 flex-grow-1 form form-control" value=<?php echo($share_link); ?>>
                        <button  onclick="copyText()" type="button" class="col-1 btn copyBtn cdli-btn-blue m-0" data-dismiss="modal">
                            <span class="copyBtn fa fa-thin fa-copy" aria-hidden="true"></span>
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <!-- filter modal -->
            <?php if (!($compactFilterStatus || $fullFilterStatus)):?>
                <div class="modal fade bd-example-modal-lg" id="filter-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg modal-dialog-centered" id="search-modal-dialog">
                        <div class="modal-content filter-modal" id="search-modal-content">
                            <div class="d-flex flex-row justify-content-between">
                                <h5 class="filter-heading">Filter options</h5>
                                <button type="button" class="filter-close" data-dismiss="modal">
                                    <span class="fa fa-times my-auto" aria-hidden="true"></span>
                                </button>
                            </div>

                            <div class="d-flex justify-content-end mt-2">
                                <button class="btn bg-white text-primary" id="side-div-expand" onclick="expandAll()">Expand All</button>
                                <button class="btn bg-white text-primary text-dark ml-2" id="side-div-collapse" onclick="collapseAll()" disabled>Collapse all</button>
                            </div>

                            <!--Filters  -->
                                <?= $this->Form->create(null, [
                                        'type'=>'POST',
                                        'url' => [
                                            'filter-'.$searchId,
                                            '?' => $_GET
                                        ],
                                        'class'=>'mt-2 accordion text-left'
                                                              ]) ?>
                                    <?= $this->Accordion->partOuterOpen('entries', 'Show Entries With') ?>
                                        <div class="border p-3">
                                            <div class="row">
                                                <div class="col-sm-12 filter-checkbox">
                                                    <?php if (!empty($filterArray['object']['image_type'])): ?>
                                                        <?php
                                                            $arrayKeys = array_keys($filterArray['object']['image_type']);
                                                        $listOfFilters = array_combine($arrayKeys, $arrayKeys);
                                                        $selectedValues = $filteredArray['object']['image_type'];
                                                        ?>
                                                        <?=$this->Form->select(
                                                            'image_type',
                                                            $listOfFilters,
                                                            [
                                                                'multiple' => 'checkbox',
                                                                'value' => $selectedValues
                                                            ]
                                                        );?>
                                                    <?php endif; ?>
                                                    <?php if (!empty($filterArray['inscription'])):?>
                                                        <?php foreach ($filterArray['inscription'] as $subFilter => $values):?>
                                                                <?php
                                                                    $arrayKeys = array_keys($values);
                                                            $listOfFilters = array_combine($arrayKeys, $arrayKeys);
                                                            $selectedValues = $filteredArray['inscription'][$subFilter];
                                                            ?>
                                                                <?=$this->Form->select(
                                                                $subFilter,
                                                                $listOfFilters,
                                                                [
                                                                        'multiple' => 'checkbox',
                                                                        'value' => $selectedValues
                                                                    ]
                                                            );?>
                                                        <?php endforeach;?>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?= $this->Accordion->partClose() ?>

                                    <?= $this->Accordion->partOuterOpen('artifacts', 'Artifacts') ?>
                                        <?php if (!empty($filterArray['object'])): ?>
                                          <?php foreach ($filterArray['object'] as $subFilter => $values):?>
                                              <?= $this->Accordion->partOpen($subFilter, $mappingSearchSettings['object'][$subFilter]) ?>
                                                  <div class="border filter-modal-block p-3">
                                                      <div class="row">
                                                          <div class="col-sm-12 col-md-4 col-lg-3">
                                                              <?php
                                                                  $arrayKeys = array_keys($values);
                                              $listOfFilters = array_combine($arrayKeys, $arrayKeys);
                                              $selectedValues = $filteredArray['object'][$subFilter];
                                              ?>
                                                              <?=$this->Form->select(
                                                  $subFilter,
                                                  $listOfFilters,
                                                  [
                                                                      'multiple' => 'checkbox',
                                                                      'value' => $selectedValues
                                                                  ]
                                              );?>
                                                          </div>
                                                      </div>
                                                  </div>
                                              <?= $this->Accordion->partClose() ?>
                                          <?php endforeach;?>
                                        <?php endif; ?>
                                    <?= $this->Accordion->partClose() ?>

                                    <?= $this->Accordion->partOuterOpen('textual', 'Textual') ?>
                                        <?php if (!empty($filterArray['textual'])): ?>

                                            <?php foreach ($filterArray['textual'] as $subFilter => $values):?>
                                                <?= $this->Accordion->partOpen($subFilter, $mappingSearchSettings['textual'][$subFilter]) ?>
                                                    <div class="border filter-modal-block p-3">
                                                        <div class="row">
                                                            <div class="col-sm-12 col-md-4 col-lg-3">
                                                                <?php
                                                                    $arrayKeys = array_keys($values);
                                                $listOfFilters = array_combine($arrayKeys, $arrayKeys);
                                                $selectedValues = $filteredArray['textual'][$subFilter];
                                                ?>
                                                                <?=$this->Form->select(
                                                    $subFilter,
                                                    $listOfFilters,
                                                    [
                                                        'multiple' => 'checkbox',
                                                        'value' => $selectedValues
                                                    ]
                                                );?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?= $this->Accordion->partClose() ?>
                                            <?php endforeach;?>
                                        <?php endif; ?>
                                    <?= $this->Accordion->partClose() ?>

                                    <?= $this->Accordion->partOuterOpen('publications', 'Publications') ?>
                                        <?php if (!empty($filterArray['publication'])): ?>
                                            <div class="filter-group--header mt-3">Publication</div>

                                            <?php foreach ($filterArray['publication'] as $subFilter => $values):?>
                                                <?= $this->Accordion->partOpen($subFilter, $mappingSearchSettings['publication'][$subFilter]) ?>
                                                    <div class="border filter-modal-block p-3">
                                                        <div class="row">
                                                            <div class="col-sm-12 col-md-4 col-lg-3">
                                                                <?php
                                                                    $arrayKeys = array_keys($values);
                                                $listOfFilters = array_combine($arrayKeys, $arrayKeys);
                                                $selectedValues = $filteredArray['publication'][$subFilter];
                                                ?>
                                                                <?=$this->Form->select(
                                                    $subFilter,
                                                    $listOfFilters,
                                                    [
                                                                        'multiple' => 'checkbox',
                                                                        'value' => $selectedValues
                                                                    ]
                                                );?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?= $this->Accordion->partClose() ?>
                                            <?php endforeach;?>
                                        <?php endif; ?>
                                    <?= $this->Accordion->partClose() ?>

                                    <div class="d-flex justify-content-center mt-4">
                                        <?=$this->form->submit("Apply", [
                                            'id' => 'filter-button-apply' ,
                                            'class' => 'btn cdli-btn-blue rounded-0 px-5 py-2'
                                        ])?>
                                    </div>
                                <?= $this->Form->end()?>
                            <!-- end Filters -->
                        </div>
                    </div>
                </div>
<!-- end filter modal -->
            <?php endif;?>
    <?php elseif (isset($result)): {
        echo '<div>No results found for '.$query.'</div>';
    } ?>
    <?php endif;?>

<!-- Exportds function -->
<?php
function exportLink($view, $name, $format, $aspect = null)
    {
        $params = $view->getRequest()->getAttribute('params');
        if (!is_null($aspect)) {
            $params = Hash::insert($params, 'aspect', $aspect);
        }
        $params = Hash::insert($params, 'format', $format);
        $url = Router::reverse($params);
        return $view->Html->link(__('As {0}', $name), $url, []);
    }
?>

    <!-- Download Modal -->
    <input class="download-modal-state" id="modal-1" type="checkbox" />
    <div class="download-modal">
      <label class="download-modal-bg" for="modal-1"></label>
          <div class="download-modal-inner">
            <label class="download-modal-close" for="modal-1"></label>
            <h3>Download Search Results</h3>
            <span type="text" style="font-size: 10pt;">Download the results of the current search results page. To change the number of results to download, adjust the number of results per page first. If you need to download more results, please use our <a href="https://github.com/cdli-gh/data">Github data downloads</a></span>
            <div class="container">
              <div class="row">
                  <div class="col-lg-4 col-md-6 col-sm-12 text-left">
                      <h5 class="resources-heading-color mt-4">Metadata / catalogue</h5>
                      <h6 class="pt-3 download-modal-margin">Flat catalogue</h6>
                      <?= exportLink($this, 'CSV', 'csv') ?><br>
                      <?= exportLink($this, 'TSV', 'tsv') ?>
                      <h6 class="pt-4 download-modal-margin">Expanded catalogue</h6>
                      <?= exportLink($this, 'JSON', 'json') ?>
                      <h6 class="pt-4 download-modal-margin">Linked catalogue</h6>
                      <?= exportLink($this, 'TTL', 'ttl') ?><br>
                      <?= exportLink($this, 'JSON-LD', 'jsonld') ?><br>
                      <?= exportLink($this, 'RDF/JSON', 'rdfjson') ?><br>
                      <?= exportLink($this, 'RDF/XML', 'rdf') ?>
                  </div>
                  <div class="col-lg-4 col-md-6 col-sm-12 text-left">
                      <h5 class="resources-heading-color mt-4">Text / annotations</h5>
                      <h6 class="pt-3 download-modal-margin">Text data</h6>
                      <?= exportLink($this, 'ATF', 'atf', 'inscriptions') ?><br>
                      <?= exportLink($this, 'JTF', 'jtf', 'inscriptions') ?>
                      <h6 class="pt-4 download-modal-margin">Annotation data</h6>
                      <?= exportLink($this, 'CDLI-CoNLL', 'cdli-conll', 'inscriptions') ?><br>
                      <?= exportLink($this, 'CoNLL-U', 'conll-u', 'inscriptions') ?>
                      <h6 class="pt-4 download-modal-margin">Linked annotations</h6>
                      <?= exportLink($this, 'TTL', 'ttl', 'inscriptions') ?><br>
                      <?= exportLink($this, 'JSON-LD', 'jsonld', 'inscriptions') ?><br>
                      <?= exportLink($this, 'RDF/JSON', 'rdfjson', 'inscriptions') ?><br>
                      <?= exportLink($this, 'RDF/XML', 'rdf', 'inscriptions') ?>
                  </div>
                  <div class="col-lg-4 col-md-6 col-sm-12 text-left">
                      <h6 class="pb-2 pt-1 download-modal-margin mt-4">Word lists</h6>
                      <?= $this->Html->link(__('As {0}', 'text'), [
                          'action' => 'tokenList',
                          'kind' => 'words',
                          'id' => $this->getRequest()->getParam('pass')[0],
                          '_ext' => 'txt'
                      ], [ 'class' => 'pt-4']) ?><br>
                      <?= $this->Html->link(__('As {0}', 'JSON'), [
                          'action' => 'tokenList',
                          'kind' => 'words',
                          'id' => $this->getRequest()->getParam('pass')[0],
                          '_ext' => 'json'
                      ]) ?>
                      <h6 class="pt-4" style="margin-bottom: auto;">Sign lists</h6>
                      <?= $this->Html->link(__('As {0}', 'text'), [
                          'action' => 'tokenList',
                          'kind' => 'signs',
                          'id' => $this->getRequest()->getParam('pass')[0],
                          '_ext' => 'txt'
                      ], [ 'class' => 'pt-4']) ?><br>
                      <?= $this->Html->link(__('As {0}', 'JSON'), [
                          'action' => 'tokenList',
                          'kind' => 'signs',
                          'id' => $this->getRequest()->getParam('pass')[0],
                          '_ext' => 'json'
                      ], [ 'class' => 'pb-2']) ?><br>
                      <h5 class="pb-2 pt-4" class="resources-heading-color">Related publications</h5>
                      <?= exportLink($this, 'CSV', 'csv', 'publications') ?><br>
                      <?= exportLink($this, 'BibTeX', 'bib', 'publications') ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var $copyurl = <?php echo(json_encode($share_link)); ?>;
    $('.copyBtn').on('click', function() {
        navigator.clipboard.writeText($copyurl);
        alert("The Share URL for this search query was copied to your clipboard.");
        return false;

    })
</script>
<noscript>
    <style>
    #menubar {
        display: none;
    }
    </style>
</noscript>
