<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */

 $current_heading="My";
 $breadcrumb = FALSE;
 echo $breadcrumb;
 $includeBread = TRUE;                                                                          
//  echo $current_heading;
?>


<main class="row justify-content-md-center">

    <div class="col-lg-6 boxed bg-light rounded" >
        <div class="capital-heading text-center"><?= __('My Account') ?></div>

        <form>
        <fieldset disabled>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label class="form-control-label font-weight-bold" for="input-username">Username</label>
                        <input type="text" style="width:100%" id="input-username" readonly class="form-control-plaintext"  value=<?= h($user['username']) ?>>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label class="form-control-label font-weight-bold" for="input-email">Email</label>
                        <input type="text" style="width:100%" id="input-username" readonly class="form-control-plaintext"  value=<?= h($user['email']) ?>>
                    </div>
                </div>
            </div>

            <?php if($user['author_id']){ ?>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label class="form-control-label font-weight-bold" >Author Name</label><br>
                        <?= $this->Html->link($author_name, ['controller' => 'Authors', 'action' => 'view', $user['author_id']])?>
                    </div>
                </div>
            </div>
            <?php } ?>

            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label class="form-control-label font-weight-bold" >Created</label>
                        <input type="text" style="width:100%" value="<?= h($user['created_at']) ?>"  readonly class="form-control-plaintext"  >
                    
                    </div>
                </div>
            </div>

        
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <?php if (in_array(1, $user['roles'])) { ?>
                            <label class="form-control-label font-weight-bold" >Administrator</label>
                            <input type="text" style="width:100%" value="Yes"  readonly class="form-control-plaintext"  >
                        <?php } ?>


                        <?php if (in_array(2, $user['roles'])) { ?>
                            <label class="form-control-label font-weight-bold" >Editor</label>
                            <input type="text" style="width:100%" value="Yes"  readonly class="form-control-plaintext"  >
                        <?php } ?>


                        <?php if (in_array(3, $user['roles'])) { ?>
                            <label class="form-control-label font-weight-bold" >Can Download HD Images</label>
                            <input type="text" style="width:100%" value="Yes"  readonly class="form-control-plaintext"  >
                    
                        <?php } ?>


                        <?php if (in_array(4, $user['roles'])) { ?>
                            <label class="form-control-label font-weight-bold" >Can View Private Artifacts</label>
                            <input type="text" style="width:100%" value="Yes"  readonly class="form-control-plaintext"  >
                    
                        <?php } ?>

                        <?php if (in_array(5, $user['roles'])) { ?>
                            <label class="form-control-label font-weight-bold" >Can View Private Inscriptions</label>
                            <input type="text" style="width:100%" value="Yes"  readonly class="form-control-plaintext"  >
                        <?php } ?>

                        <?php if (in_array(6, $user['roles'])) { ?>
                        <label class="form-control-label font-weight-bold" >Can Edit Transliterations</label>
                        <input type="text" style="width:100%" value="Yes"  readonly class="form-control-plaintext"  >
                    
                        <?php } ?>
                        <?php if (in_array(7, $user['roles'])) { ?>
                            <label class="form-control-label font-weight-bold" >Can View Private Images</label>
                            <input type="text" style="width:100%" value="Yes"  readonly class="form-control-plaintext"  >
                    
                        <?php } ?>
                        <?php if (in_array(8, $user['roles'])) { ?>
                            <label class="form-control-label font-weight-bold" >Can View CDLI Tablet Managers</label>
                            <input type="text" style="width:100%" value="Yes"  readonly class="form-control-plaintext"  >
                    
                        <?php } ?>
                        <?php if (in_array(9, $user['roles'])) { ?>
                            <label class="form-control-label font-weight-bold" >Can View CDLI Forums</label>
                            <input type="text" style="width:100%" value="Yes"  readonly class="form-control-plaintext"  >
                    
                        <?php } ?>
                    </div>
                </div>
            </div>
            
        </fieldset >
        </br>
            <div class=" row float-right ">
            <?php if($access){ ?>
            <?= $this->Html->link(__('Edit Staff Profile'), ['controller' => 'Staff', 'action' => 'edit', 'id' => 'profile'], ['class' => 'd-inline p-2 btn cdli-btn-blue mx-1']) ?>
            <?php } ?>
            <?php if($user['author_id']){ ?>
            <?= $this->Html->link(__('Edit Author Profile'), ['controller' => 'Authors', 'action' => 'edit', 'id' => 'profile'], ['class' => 'd-inline p-2 btn cbtn btn-secondary']) ?>
            <?php } ?>
            </div>
            </br>
            <div class="float-bottom" style="margin-top:40px">
            <label>If you want to deactivate your account, please email cdli-support (at) orinst.ox.ac.uk. </label>
            </div>
        </form>
    </div>

</main>
