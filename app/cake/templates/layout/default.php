<?php
use Cake\I18n\FrozenTime;
use Cake\Routing\Router;

/**
* CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
* Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
*
* Licensed under The MIT License
* For full copyright and license information, please see the LICENSE.txt
* Redistributions of files must retain the above copyright notice.
*
* @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
* @link          https://cakephp.org CakePHP(tm) Project
* @since         0.10.0
* @license       https://opensource.org/licenses/mit-license.php MIT License
*/

// Get  Roles of Authenticated User
$roles = $this->getRequest()->getSession()->read('Auth.User.roles');
$name = $this->getRequest()->getSession()->read('Auth.User.username');

// Check if role_id 1 or 2 or 12 is present
$ifRoleExists = !is_null($roles) ? array_intersect($roles, [1, 2, 12]) : [];

$ifRoleExists = !empty($ifRoleExists) ? 1 : 0;
?>
<!DOCTYPE html>
<html lang="en-US" prefix="og: http://ogp.me/ns#">
<head>
	<?= $this->Html->charset() ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>
		<?= h($this->fetch('title')) ?> - Cuneiform Digital Library Initiative
	</title>

	<?php if ($this->fetch('image')): ?>
		<meta name="twitter:card" content="summary_large_image">
		<meta property="og:image" content="<?= h(Router::url($this->fetch('image'), true)) ?>" />
	<?php else: ?>
		<meta name="twitter:card" content="summary">
	<?php endif; ?>

	<meta property="og:title" content="<?= h($this->fetch('title')) ?>" />
	<meta property="og:url" content="<?= Router::url($this->getRequest()->getRequestTarget(), true) ?>" />
	<meta property="og:site_name" content="CDLI">
	<meta name="twitter:site" content="@cdli_news">

	<?php if ($this->fetch('description')): ?>
		<meta name="description" content="<?= h($this->fetch('description')) ?>">
		<meta property="og:description" content="<?= h($this->fetch('description')) ?>" />
	<?php endif; ?>

	<?= $this->Html->meta('icon') ?>
	<?= $this->Html->css('main.css')?>
	<?= $this->Html->css('font-awesome/css/font-awesome.min.css')?>
	<?php
    // For using Bootstrap dropdowns, set variable $includePopper in your view
    if (isset($includePopper) && $includePopper) {
        echo $this->Html->script('popper.min.js');
    }
?>
    <?= $this->Html->script('popper.min.js');?>
	<?= $this->Html->script('jquery.min.js')?>
	<?= $this->Html->script('bootstrap.min.js')?>
	<?= $this->Html->script('js.js')?>
	<?= $this->Html->script('drawer.js', ['defer' => true]) ?>
    <?= $this->element('google-analytics'); ?>
	<?= $this->fetch('meta') ?>
	<?= $this->fetch('css') ?>
	<?= $this->fetch('library') ?>
</head>
<body>
	<?php
use Cake\Core\Configure;

$gtm_code = Configure::read('gtm_code');
// Google Tag Manager (noscript)
$script_inject = <<<EOD
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=$gtm_code&nojscript=true"
	height="0" width="0" style="display:none;visibility:hidden"></iframe><style>.copyBtn{display: none;}</style></noscript>
	EOD;
echo $script_inject;
// End Google Tag Manager (noscript)
?>
	<!-- White translucent film  -->
		<div class="translucent-film d-none" id="faded-bg"></div>
	<!-- End White translucent film  -->
	<noscript>
		<p class="alert alert-secondary text-center mb-0" role="alert">
			JavaScript is disabled or not supported in your browser.
			If possible, enable it for a better experience.
		</p>
	</noscript>
	<header>
		<div class="bg-foggy-grey navbar navbar-expand d-none d-lg-block">
			<nav class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav ml-auto">
				<?php if ($ifRoleExists) { ?>
                    <li class="nav-item mx-3">
                        <?= $this->Html->link(
    in_array(1, $roles) || in_array(2, $roles) || in_array(12, $roles) ? 'Admin Dashboard' : 'Dashboard',
    ['prefix' => 'Admin', 'controller' => 'Home', 'action' => 'dashboard'],
    ['class' => 'nav-link']
) ?>
                    </li>
                <?php } ?>
					<li class="publication-nav nav-item mx-3 dropdown no-js-dd">
					<a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Publications
					</a>
					<nav class="dropdown-menu no-js-dd-content mt-0">
						<?= $this->Html->link(
                            __('Cuneiform Digital Library Journal'),
                            ['prefix' => false, 'controller' => 'Articles', 'action' => 'index', 'cdlj'],
                            ['class' => 'dropdown-item']
                        ) ?>
						<?= $this->Html->link(
						    __('Cuneiform Digital Library Bulletin'),
						    ['prefix' => false, 'controller' => 'Articles', 'action' => 'index', 'cdlb'],
						    ['class' => 'dropdown-item']
						) ?>
						<?= $this->Html->link(
						    __('Cuneiform Digital Library Notes'),
						    ['prefix' => false, 'controller' => 'Articles', 'action' => 'index', 'cdln'],
						    ['class' => 'dropdown-item']
						) ?>
						<?= $this->Html->link(
						    __('Cuneiform Digital Library Preprint'),
						    ['prefix' => false, 'controller' => 'Articles', 'action' => 'index', 'cdlp'],
						    ['class' => 'dropdown-item']
						) ?>
					</nav>
				</li>

    <li class="mx-3 dropdown no-js-dd">
        <div class="resources-dropdown">
            <a class="nav-link dropdown-toggle dropbtn" href="/resources" role="button">
            Resources
            </a>
            <div class="resources-dropdown-outside" id="resources-dropdown-outside"></div>
                <div class="resources-dropdown-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-5 pt-4 px-5 pb-2 justify-content-center mt-2">
                                <a href="/resources/#cdli-resources">
                                    <h5 class="resources-heading-color">
                                        <img src="/images/resources.svg" alt="Resources-img"/> Resources
                                        <img src="/images/arrow-right-up.svg" alt="arrow-img"/>
                                    </h5>
                                </a>
                                <p class="resources-menu-space">
									<u><span class="fa fa-external-link" aria-hidden="true"></span>
										<?= $this->Html->link(
                                        'CDLI wiki',
                                        'https://cdli.ox.ac.uk/wiki/',
                                        ['class' => 'text-black']
                                    ); ?></u><br>
									<u><?= $this->Html->link(
                                        'CDLI Documentation',
                                        '/docs',
                                        ['class' => 'text-black']
                                    ); ?></u><br>
                                    <u><?= $this->Html->link(
                                        'CDLI Bibliography',
                                        ['controller' => 'Publications', 'action' => 'index'],
                                        ['class' => 'text-black']
                                    ); ?></u><br>
                                    <!--<u><?= $this->Html->link(
                                        'CDLI Tablet',
                                        ['controller' => 'CdliTablet', 'action' => 'index'],
                                        ['class' => 'text-black']
                                    ); ?></u><br>
                                    <u><?= $this->Html->link(
                                        'Cooked Agade',
                                        ['controller' => 'AgadeMails', 'action' => 'index'],
                                        ['class' => 'text-black']
                                    ); ?></u> -->
                                </p>
                            </div>
                            <div class="col-7 pt-4 px-5 pb-2 justify-content-center mt-2">
                                <a href="/resources/#composite-texts">
                                    <h5 class="resources-heading-color">
                                        <img src="/images/composite-text.svg" alt="composite-text-img"/> Composite Texts
                                        <img src="/images/arrow-right-up.svg" alt="arrow-img"/>
                                    </h5>
                                </a>
                                <p class="resources-menu-space">
                                    <u><?= $this->Html->link(
                                        'Composite literary texts & witnesses (scores)',
                                        ['controller' => 'Artifacts', 'action' => 'composites', 'Literary'],
                                        ['class' => 'text-black']
                                    ); ?></u><br>
                                    <u><?= $this->Html->link(
                                        'Royal literary texts & witnesses (scores)',
                                        ['controller' => 'Artifacts', 'action' => 'composites', 'Royal'],
                                        ['class' => 'text-black']
                                    ); ?></u><br>
                                    <u><?= $this->Html->link(
                                        'Seal & seal impressions',
                                        '/artifacts/seals',
                                        ['class' => 'text-black']
                                    ); ?></u><br>
                                    <u><?= $this->Html->link(
                                        'Lexical texts & their witnesses (scores)',
                                        ['controller' => 'Artifacts', 'action' => 'composites', 'Lexical'],
                                        ['class' => 'text-black']
                                    ); ?></u><br>
                                    <u><?= $this->Html->link(
                                        'Scientific texts & their witnesses (scores)',
                                        ['controller' => 'Artifacts', 'action' => 'composites', 'Scientific'],
                                        ['class' => 'text-black']
                                    ); ?></u>
                                </p>
                            </div>
                            <div class="w-100"></div>
                            <div class="col-5 pb-4 px-5 pt-1 justify-content-center">
                                <a  href="/resources/#references">
                                    <h5 class="resources-heading-color"><img src="/images/references.svg" alt="Resources-img"/> References<img src="/images/arrow-right-up.svg" alt="arrow-img"/>
                                    </h5>
                                </a>
                                <p class="resources-menu-space">
                                    <u><?= $this->Html->link(
                                        'Abbreviations',
                                        ['controller' => 'Abbreviations', 'action' => 'index'],
                                        ['class' => 'text-black']
                                    ); ?></u><br>
                                    <u><?= $this->Html->link(
                                        'Preferred Sign Readings',
                                        ['controller' => 'SignReadings', 'action' => 'view', 'preferred'],
                                        ['class' => 'text-black']
                                    ); ?></u><br>
                                    <u><?= $this->Html->link(
                                        'Sign Lists',
                                        'https://cdli.ox.ac.uk/wiki/sign_lists',
                                        ['class' => 'text-black']
                                    ); ?></u><br>
                                    <u><?= $this->Html->link(
                                        'Generated Token Lists',
                                        ['controller' => 'Resources', 'action' => 'tokenLists'],
                                        ['class' => 'text-black']
                                    ); ?></u><br>
                                    <u><?= $this->Html->link(
                                        'Ur III month names',
                                        'https://cdli.ucla.edu/tools/ur3months/month.html',
                                        ['class' => 'text-black']
                                    ); ?></u><br>
                                    <u><?= $this->Html->link(
                                        'Years',
                                        '#',
                                        ['class' => 'text-black']
                                    ); ?></u>
                                </p>
                            </div>
                            <div class="col-7 pb-4 px-5 pt-1 justify-content-center">
                                <a  href="/resources/#utilities-tools">
                                    <h5 class="resources-heading-color"><img src="/images/tools.svg" alt="Tools-img"/> Tools<img src="/images/arrow-right-up.svg" alt="arrow-img"/>
                                    </h5>
                                </a>
                                <p class="resources-menu-space">
                                    <u><?= $this->Html->link(
                                        'Metadata Checker',
                                        ['controller' => 'ArtifactsUpdates', 'action' => 'add'],
                                        ['class' => 'text-black']
                                    ); ?></u><br>
                                    <u><?= $this->Html->link(
                                        'Transliteration Checker',
                                        '/inscriptions/add',
                                        ['class' => 'text-black']
                                    ); ?></u><br>
                                    <u><?= $this->Html->link(
                                        'Uqnu : ATF & JTF editor/ uqnu',
                                        '/uqnu',
                                        ['class' => 'text-black']
                                    ); ?></u><br>
                                    <u><?= $this->Html->link(
                                        'CDLI-CoNLL Checker',
                                        ['controller' => 'Resources', 'action' => 'checkCdliConll'],
                                        ['class' => 'text-black']
                                    ); ?></u><br>
                                    <u><?= $this->Html->link(
                                        'Signfilter (Compare ATF against Token List)',
                                        ['controller' => 'Resources', 'action' => 'compare-text-to-token-list'],
                                        ['class' => 'text-black']
                                    ); ?></u><br>
                                    <u><?= $this->Html->link(
                                        'Convert, annotate, or translate',
                                        ['controller' => 'Resources', 'action' => 'tools'],
                                        ['class' => 'text-black']
                                    ); ?></u><br>
                                    <u><?= $this->Html->link(
                                        'Standalone Tools',
                                        'https://github.com/cdli-gh',
                                        ['class' => 'text-black']
                                    ); ?></u>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
		</li>

				<?php if (!$this->getRequest()->getSession()->read('Auth.User')) { ?>
						<li class="nav-item mx-3">
							<?= $this->Html->link(
                                        'Login',
                                        ['prefix' => false, 'controller' => 'Users', 'action' => 'login'],
                                        ['class' => 'nav-link']
                                    )
				    ?>
						</li>
						<li class="nav-item mx-3 pr-5">
							<?= $this->Html->link(
				        'Register',
				        ['prefix' => false, 'controller' => 'Users', 'action' => 'register'],
				        ['class' => 'nav-link']
				    )
				    ?>
						</li>
					<?php } else { ?>
						<li class="publication-nav nav-item mx-3 dropdown no-js-dd">
					<a class="nav-link dropdown-toggle" href="#" aria-label="My Profile" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<?php echo $name ?>
					</a>
					<nav class="dropdown-menu dropdown-menu-right no-js-dd-content">
                       <?= $this->Html->link("My Profile", [
                        'controller' => 'Users',
                        'action' => 'profile',
                        'prefix' => false
                        ], [
                        'class' => 'dropdown-item'
                        ]) ?>

                        <hr>
                        <?= $this->Html-> link(
                            'Logout',
                            [
                            'controller' => 'Logout',
                            'action' => 'index',
                            'prefix' => false
                        ],
                            [
                            'class' => 'dropdown-item',
                            'aria-label' => 'Log Out'
                            ]
                        ); ?>
					</nav>
				</li>
					<?php } ?>
				</ul>
			</nav>
		</div>

		<div class="header navbar navbar-expand-lg bg-transparent py-0" id="navbar-main">
			<div class="container-fluid d-flex align-items-center justify-content-between my-5">
				<a tabindex="0" class="navbar-brand logo" href="/">
					<div class="navbar-logo">
					<img src="/images/cdlilogo.svg" class="d-none d-md-block cdlilogo" alt="CDLI-logo" />
						<img src="/images/logo-no-text.svg" class="d-md-none" alt="CDLI-logo" />
					</div>
				</a>

				<nav class="collapse navbar-collapse" id="navbarText">
					<ul class="navbar-nav ml-auto">

						<li class="nav-item mx-3 ">
							<?= $this->Html->link(
                            'Browse',
                            ['prefix' => false, 'controller' => 'Home', 'action' => 'browse'],
                            ['class' => 'nav-link']
                        ) ?>
						</li>

						<li class="nav-item mx-3">
							<?= $this->Html->link("Contribute", ['_name' => 'contribute'], ['class' => 'nav-link']) ?>
						</li>

						<li class="nav-item mx-3">
							<?= $this->Html->link("About", ['_name' => 'about'], ['class' => 'nav-link'])?>
						</li>

						<li class="nav-item mx-3 mr-5 dropdown no-js-dd">
							<a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Search </a>
							<nav class="dropdown-menu border-0 shadow no-js-dd-content">
								<?= $this->Html->link(
							    __('Search'),
							    ['prefix' => false, 'controller' => 'Home', 'action' => 'index'],
							    ['class' => 'dropdown-item']
							) ?>
								<?= $this->Html->link(
								    __('Advanced search'),
								    ['prefix' => false, 'controller' => 'Advancedsearch', 'action' => 'index'],
								    ['class' => 'dropdown-item']
								) ?>
								<?= $this->Html->link(
								    __('Search settings'),
								    ['prefix' => false, 'controller' => 'SearchSettings', 'action' => 'index'],
								    ['class' => 'dropdown-item']
								) ?>
								<?= $this->Html->link(
								    __('Corpus search'),
								    '/cqp4rdf',
								    ['class' => 'dropdown-item']
								) ?>
							</nav>
						</li>
					</ul>
				</nav>
				<nav aria-label="menubar">
				<a tabindex="0" class="d-sm-block d-lg-none bg-transparent" id="menubar" onclick="openSlideMenu()">
					<span class="fa fa-bars fa-2x"></span>
				</a>
				</nav>
<!-- Start of Side Menu bar for small devices -->
				<div role="menubar" id="side-menu" class="side-nav shadow d-none">
					<button aria-label="Close" id="close-button" class="sidebar-btn-close px-3 border-0" onclick="closeSlideMenu()">
						×
					</button>
					<?= $this->Html->link(
								    __('Browse'),
								    ['prefix' => false, 'controller' => 'Home', 'action' => 'browse'],
								    ['class' => 'mt-5']
								) ?>
					<?= $this->Html->link(
					    __('Contribute'),
					    ['_name' => 'contribute']
					) ?>
					<?= $this->Html->link(
					    __('About'),
					    ['_name' => 'about']
					) ?>
          <?php if ($ifRoleExists) { ?>
						<?= $this->Html->link(in_array(1, $roles) || in_array(2, $roles) || in_array(12, $roles) ? "Admin Dashboard" : "Dashboard", '/admin/dashboard', ['class' => 'nav-link']) ?>
					<?php } ?>
					<a  data-toggle="collapse" href="#collapseExampleResources" role="button" aria-expanded="false" aria-haspopup="true" aria-controls="collapseExampleResources" class="border-0 mb-1">
						Resources<span id="drawer-arrowPublication" class="fa fa-angle-down float-right" aria-hidden="true"></span>
					</a>
					<div class="collapse embedded-links" id="collapseExampleResources">
              <a class='dropdown-item' aria-label='Resouces' href="/resources/#cdli-resources" onclick="closeSlideMenu()"><span type="text" style="font-size:18px;">CDLI Resources</span><br>Tablet,Bib and Documentation</a>
              <a class='dropdown-item' aria-label='Composites' href="/resources/#composite-texts" onclick="closeSlideMenu()"><span type="text" style="font-size:18px;">Composites</span><br>Scores, Seals and Impressions</a>
              <a class='dropdown-item' aria-label='References' href="/resources/#references" onclick="closeSlideMenu()">  <span type="text" style="font-size:18px;">References</span><br>Tablet, Bib and Documentation</a>
              <a class='dropdown-item' aria-label='Tools' href="/resources/#utilities-tools" onclick="closeSlideMenu()"><span type="text" style="font-size:18px;">Tools</span><br>Checkers, Converters and <br>Generator Tools</a>
              <a class='dropdown-item' aria-label='Other Resources' href="/resources/#other-resources" onclick="closeSlideMenu()"><span type="text" style="font-size:18px;">Other Resources</span><br>and Information</a>
					</div>
					<a  data-toggle="collapse" href="#collapseExampleSearch" role="button" aria-expanded="false" aria-haspopup="true" aria-controls="collapseExampleSearch" class="border-0 mb-1">
						Search <span id="drawer-arrowSearch" class="fa fa-angle-down float-right" aria-hidden="true"></span>
					</a>
					<div class="collapse embedded-links" id="collapseExampleSearch">
						<?= $this->Html->link(
					    __('Search'),
					    ['prefix' => false, 'controller' => 'Home', 'action' => 'index'],
					    ['class' => 'ml-5']
					) ?>
						<?= $this->Html->link(
						    __('Advanced search'),
						    ['prefix' => false, 'controller' => 'Advancedsearch', 'action' => 'index'],
						    ['class' => 'ml-5']
						) ?>
						<?= $this->Html->link(
						    __('Search settings'),
						    ['prefix' => false, 'controller' => 'SearchSettings', 'action' => 'index'],
						    ['class' => 'ml-5']
						) ?>
						<?= $this->Html->link(
						    __('Corpus search'),
						    '/cqp4rdf',
						    ['class' => 'ml-5']
						) ?>
					</div>

					<a  data-toggle="collapse" href="#collapseExamplePublication" role="button" aria-expanded="false" aria-haspopup="true" aria-controls="collapseExamplePublication" class="border-0 mb-1">
						Publications<span id="drawer-arrowPublication" class="fa fa-angle-down float-right" aria-hidden="true"></span>
					</a>
					<div class="collapse embedded-links" id="collapseExamplePublication">
						<?= $this->Html->link(
						    __('CDLJ'),
						    ['prefix' => false, 'controller' => 'Articles', 'action' => 'index', 'cdlj'],
						    ['class' => 'dropdown-item', 'aria-label' => 'Cuneiform Digital Library Journal']
						) ?>
						<?= $this->Html->link(
						    __('CDLB'),
						    ['prefix' => false, 'controller' => 'Articles', 'action' => 'index', 'cdlb'],
						    ['class' => 'dropdown-item', 'aria-label' => 'Cuneiform Digital Library Bulletin']
						) ?>
						<?= $this->Html->link(
						    __('CDLN'),
						    ['prefix' => false, 'controller' => 'Articles', 'action' => 'index', 'cdln'],
						    ['class' => 'dropdown-item', 'aria-label' => 'Cuneiform Digital Library Notes']
						) ?>
						<?= $this->Html->link(
						    __('CDLP'),
						    ['prefix' => false, 'controller' => 'Articles', 'action' => 'index', 'cdlp'],
						    ['class' => 'dropdown-item', 'aria-label' => 'Cuneiform Digital Library Preprint']
						) ?>
					</div>

					<?php if (!$this->getRequest()->getSession()->read('Auth.User')) { ?>
						<?= $this->Html->link(
						    __('Login'),
						    ['prefix' => false, 'controller' => 'Users', 'action' => 'login'],
						    ['class' => 'border-0 mb-1']
						) ?>
						<?= $this->Html->link(
						    __('Register'),
						    ['prefix' => false, 'controller' => 'Users', 'action' => 'register'],
						    ['class' => 'border-0 mb-1']
						) ?>
					<?php } else { ?>
					<a  data-toggle="collapse" href="#collapseExampleProfile" role="button" aria-label="My Profile" aria-expanded="false" aria-haspopup="true" aria-controls="collapseExampleProfile" class="border-0 mb-1">
						<?php echo $name ?><span id="drawer-arrowProfile" class="fa fa-angle-down float-right" aria-hidden="true"></span>
					</a>
					<div class="collapse embedded-links" id="collapseExampleProfile">
						<?= $this->Html->link("My Profile", [
						    'prefix' => false,
						    'controller' => 'users',
						    'action' => 'view',
						    $this->getRequest()->getSession()->read('Auth.User.username')
						]) ?>


						<?= $this->Html->link(
						    'Logout',
						    [
						        'controller' => 'Logout',
						        'action' => 'index',
						        'prefix' => false
						    ],
						    [
						        'class' => 'dropdown-item',
						        'aria-label' => 'Log Out'
						    ]
						); ?>
					</div>
					<?php }?>
				</div>
<!-- End of Side Menu bar for small devices -->
			</div>
		</div>
	</header>

	<div class="container-fluid text-center contentWrapper">
		<?= $this->Flash->render() ?>
		<?= $this->fetch('content') ?>
	</div>
	<footer>
		<div class="container">
			<div>
				<div class="row footer-1 py-5">
					<section aria-label="Navigate" class="col-lg-3 d-none d-lg-block">
						<h2 class="heading">Navigate</h2>
						<p><?= $this->Html->link(
						    __('Browse collection'),
						    ['prefix' => false, 'controller' => 'Home', 'action' => 'browse']
						) ?></p>
						<p><?= $this->Html->link(
						    __('Contribute'),
						    ['_name' => 'contribute']
						) ?></p>
						<p><?= $this->Html->link(
						    __('About CDLI'),
						    ['_name' => 'about']
						) ?></p>
						<p><?= $this->Html->link(
						    __('Search collection'),
						    ['prefix' => false, 'controller' => 'Home', 'action' => 'index']
						) ?></p>
            <p><?= $this->Html->link(
						    __('Terms of Use'),
						    '/terms-of-use'
						) ?></p>
					</section>
					<section aria-label="Acknowledgement" class="col-md-6 col-lg-4">
						<h2 class="heading">Acknowledgement</h2>
						<p class="backers">
							<?= $this->Html->link(
								__('Support for this initiative'),
								['controller' => 'sponsors', 'action' => 'index']
							) ?> has been generously provided by the
							<a href="https://mellon.org/" target="_blank">Mellon Foundation</a>,
                            the <a href="https://www.nsf.gov/" target="_blank" aria-label="National Science Foundation">NSF</a>,
							the <a href="https://www.neh.gov/" target="_blank" aria-label="National Endowment for the Humanities">NEH</a>,
							the <a href="https://www.imls.gov/" target="_blank" aria-label="Institute of Museum and Library Services">IMLS</a>,
							the <a href="https://www.mpg.de/en" target="_blank" aria-label="Max-Planck-Gesellschaft">MPS</a>,
              the <a href="https://researchsupport.admin.ox.ac.uk/funding/internal/jff" target="_blank" aria-label="Max-Planck-Gesellschaft">John Fell Fund</a>,
							<a href="http://www.ox.ac.uk/" target="_blank">Oxford University</a>
							and <a href="http://www.ucla.edu/" target="_blank" aria-label="University of California">UCLA</a>,
							with additional support
                            from <a href="https://www.sshrc-crsh.gc.ca/home-accueil-eng.aspx" target="_blank" aria-label="Social Sciences and Humanities Research Council">SSHRC</a> and
							the <a href="https://www.dfg.de/" target="_blank" lang="de" aria-label="Deutsche Forschungsgemeinschaft">DFG</a>.
							Computational resources and network services are provided by
							<a href="https://humtech.ucla.edu/" target="_blank" aria-label="University of California HumTech">UCLA’s HumTech</a>,
                            <a href="https://www.mpiwg-berlin.mpg.de/" target="_blank" aria-label="Max Planck Institute for the History of Science ">MPIWG</a>,
							and <a href="https://www.computecanada.ca/" target="_blank">Compute Canada</a> and others.


                        </p>
					</section>
                    <div class="col-lg-1 d-none d-lg-flex"></div>
					<section aria-label="Contact Us" class="col-md-6 col-lg-4 contact">
						<h2 class="heading">Contact Us</h2>
						    <ul class="pl-0">
						        <li style="list-style-type : none">Cuneiform Digital Library Initiative</li>
						        <li style="list-style-type : none">Linton Rd, Oxford OX2 6UD</li>
						        <li style="list-style-type : none">United Kingdom</li>
					        </ul>
						<div class="d-flex">
							<div class="twitter">
								<a href="https://twitter.com/cdli_news" target="_blank" aria-label="CDLI-twitter-page">
									<span class="fa fa-twitter" aria-hidden="true"></span>
								</a>
							</div>
							<div class="mail">
								<a href="mailto:cdli@ucla.edu" target="_blank" aria-label="mail-to-CDLI">
									<span class="fa fa-envelope fa-3x" aria-hidden="true"></span>
								</a>
							</div>
							<div> <a href="https://opencollective.com/cdli/donate" class="btn donate" role="button" target="_blank">Donate</a></div>
						</div>
					</a>
					    <p>Found a problem on the website of facing a technical issue? Write to cdli-support (at) orinst.ox.ac.uk</p>
					</section>
			</div>
		</div>
	</div>
</div>
<div class="footer-end p-4">
	<div class="text-center text-white">
		<?php $time = FrozenTime::now() ?>
		<p class="mb-0">© 2020-<?= $time->year ?> Cuneiform Digital Library Initiative.</p>
	</div>
</div>
</footer>
</body>
<?= $this->fetch('modal') ?>
<script>
$( document ).ready(function() {
	$('header .no-js-dd').removeClass('no-js-dd');
});
</script>
<?= $this->fetch('script') ?>
</html>
