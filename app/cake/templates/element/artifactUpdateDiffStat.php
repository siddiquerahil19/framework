<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead>
        <tr>
            <th><?= h('Artifact') ?></th>
            <th><?= h('Revision') ?></th>
            <th><?= h('Changed') ?></th>
            <th><?= h('Deleted') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($updateEvent->artifacts_updates as $update): ?>
            <?php
            $counts = ['changed' => 0, 'deleted' => 0];
            foreach ($update->getChanged() as $field) {
                $value = $update->get($field);
                if ($value === '') {
                    $counts['deleted'] += 1;
                } else {
                    $counts['changed'] += 1;
                }
            }
            ?>
            <tr>
                <td><?= empty($update->artifact_id) ? h($update->getDesignation()) : $this->Html->link(
                    h($update->getDesignation()),
                    ['controller' => 'Artifacts', 'action' => 'view', $update->artifact_id]
                ) ?></td>
                <td>
                    <?= $this->Html->link(
                        h($update->id),
                        ['controller' => 'ArtifactsUpdates', 'action' => 'view', $update->id]
                    ) ?>
                </td>
                <td><?= $counts['changed'] ?></td>
                <td><?= $counts['deleted'] ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
