<?php

use Cake\Utility\Inflector;

$getDisplayName = function ($controller) {
    $table = Inflector::tableize($controller);
    $entity = Inflector::singularize($table);
    return Inflector::humanize($entity);
};

if (!isset($data)) {
    $data = $this->Citation->getDefaultData();
} elseif (!is_array($data)) {
    $data = [$data];
}
?>

<div class="cite-bottom" style="width:-webkit-fill-available;">
    <fieldset>
        <?php if ($this->request->getParam('action') === 'index'): ?>
            <legend>Cite this Webpage</legend>
        <?php else:?>
            <legend>Cite this <?= $getDisplayName($this->request->getParam('controller')) ?></legend>
        <?php endif; ?>

        <button class="pull-right copy-clipbaord-btn-bottom" onclick="CopyToClipboardBottom('sample1');return false;" id="copy-button-bottom">
            <span class="copyBtn fa fa-thin fa-copy" aria-hidden="true"></span>
        </button>

        <?php if ($this->request->getParam('controller') === 'articles' && $this->request->getParam('action') == 'view'): ?>
            <button id="btnGroupDrop1" type="button" class="d-inline p-2 btn btn-sm cdli-btn-blue mx-3 float-right dropdown-toggle cite-download-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Download
            </button>
            <button id="btnGroupDrop1" type="button" class="d-inline p-2 btn btn-sm mx-3 float-right cite-download-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-download" aria-hidden="true" style="font-size: 15px;"></i>
            </button>

            <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                <?= $this->Html->link(
                    __('BibTex'),
                    [$article->article_type, $article->getArticleNumberPath(), '_ext' => 'bib'],
                    [
                        'escapeTitle' => false,
                        'class' => 'dropdown-item',
                        'role' => 'button',
                        'target' => '_blank'
                    ]
                ) ?>
                <?= $this->Html->link(
                    __('RIS'),
                    [$article->article_type, $article->getArticleNumberPath(), '_ext' => 'ris'],
                    [
                        'escapeTitle' => false,
                        'class' => 'dropdown-item',
                        'role' => 'button',
                        'target' => '_blank'
                    ]
                ) ?>
            </div>
        <?php endif; ?>

        <?php if ($this->request->getParam('controller') === 'publications' && $this->request->getParam('action') == 'view'): ?>
            <button id="btnGroupDrop1" type="button" class="d-inline p-2 btn btn-sm cdli-btn-blue mx-3 float-right dropdown-toggle cite-download-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Download
            </button>
            <button id="btnGroupDrop1" type="button" class="d-inline p-2 btn btn-sm mx-3 float-right cite-download-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-download" aria-hidden="true" style="font-size: 15px;"></i>
            </button>

            <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                <?= $this->Html->link(
                    __('BibTex'),
                    ['prefix' => false, 'controller' => 'Publications', 'action' => 'view', $publication->id, '_ext' => 'bib'],
                    [
                        'escapeTitle' => false,
                        'class' => 'dropdown-item',
                        'role' => 'button',
                        'target' => '_blank'
                    ]
                ) ?>
                <?= $this->Html->link(
                    __('RIS'),
                    ['prefix' => false, 'controller' => 'Publications', 'action' => 'view', $publication->id, '_ext' => 'ris'],
                    [
                        'escapeTitle' => false,
                        'class' => 'dropdown-item',
                        'role' => 'button',
                        'target' => '_blank'
                    ]
                ) ?>
            </div>
        <?php endif; ?>

        <div class="cite-bottom-tabs">
            <div class="tabset">
                <!-- Tab 1 -->
                <input type="radio" name="tabset" id="tab1" aria-controls="chicago" checked>
                <label for="tab1" class="pull-left prop ml-2">Chicago</label>
                <!-- Tab 2 -->
                <input type="radio" name="tabset" id="tab2" aria-controls="apa">
                <label for="tab2"  class="pull-left prop">APA</label>
                <!-- Tab 3 -->
                <input type="radio" name="tabset" id="tab3" aria-controls="harvard">
                <label for="tab3" class="pull-left prop">Harvard</label>
                <!-- Tab 4 -->
                <input type="radio" name="tabset" id="tab4" aria-controls="Bibtex">
                <label for="tab4"  class="pull-left prop">Bibtex</label>
                <!-- Tab 5 -->
                <input type="radio" name="tabset" id="tab5" aria-controls="RIS">
                <label for="tab5" class="pull-left prop">RIS</label>

                <div class="tab-panels" id="sample1" style="padding: 5px;">
                    <section id="chicago" class="tab-panel cite-bottom-color">
                        <p>
                            <?= $this->Citation->formatReference($data, 'bibliography', [
                                'template' => 'chicago-author-date',
                                'format' => 'html'
                            ]) ?>
                        </p>
                    </section>
                    <section id="apa" class="tab-panel cite-bottom-color">
                        <p>
                            <?= $this->Citation->formatReference($data, 'bibliography', [
                                'template' => 'apa',
                                'format' => 'html'
                            ]) ?>
                        </p>
                    </section>
                    <section id="harvard" class="tab-panel cite-bottom-color">
                        <p>
                            <?= $this->Citation->formatReference($data, 'bibliography', [
                                'template' => 'harvard-cite-them-right',
                                'format' => 'html'
                            ]) ?>
                        </p>
                    </section>
                    <section id="Bibtex" class="tab-panel bibtex-display">
                        <p>
                            <?= $this->Citation->formatReference($data, 'bibtex', [
                                'format' => 'html'
                            ]) ?>
                        </p>
                    </section>
                    <section id="RIS" class="tab-panel bibtex-display">
                        <p class="mt-2 text-left ml-3">
                            <pre class="text-left ml-4"><!--
                                --><?= $this->Citation->formatReference($data, 'ris', [
                                    'format' => 'html'
                                ]) ?><!--
                            --></pre>
                        </p>
                    </section>
                </div>
            </div>
        </div>
    </fieldset>
</div>


<script>
function CopyToClipboardBottom(id) {
    var r = document.createRange();
    r.selectNode(document.getElementById(id));
    window.getSelection().removeAllRanges();
    window.getSelection().addRange(r);
    var x = document.execCommand('copy');
    window.getSelection().removeAllRanges();
    setTooltip('Copied!');
    hideTooltip();
}

$('#copy-button-bottom').tooltip({
    trigger: 'click',
    placement: 'right'
});

function setTooltip(message) {
    $('#copy-button-bottom').tooltip('hide')
        .attr('data-original-title', message)
        .tooltip('show');
}

function hideTooltip() {
    setTimeout(function() {
        $('#copy-button-bottom').tooltip('hide');
    }, 1000);
}
</script>

<noscript>
    <style>
        .copy-clipbaord-btn-bottom {
            display: none;
        }
    </style>
</noscript>
