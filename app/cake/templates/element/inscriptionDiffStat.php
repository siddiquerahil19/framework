<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead>
        <tr align="left">
            <th scope="col"><?= h('Artifact') ?></th>
            <th scope="col"><?= h('Revision') ?></th>
            <th scope="col"><?= h('Changes') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($updateEvent->inscriptions as $update): ?>
            <tr align="left">
                <td><?= $this->Html->link(
                    h($update->artifact->designation . ' (' . $update->artifact->getCdliNumber() . ')'),
                    ['controller' => 'Artifacts', 'action' => 'view', $update->artifact->id]
                ) ?></td>
                <td><?= $this->Html->link(
                    h($update->id),
                    ['controller' => 'Inscriptions', 'action' => 'view', $update->id]
                ) ?></td>
                <td>
                    <?php if ($update->hasTokenWarnings() && $updateEvent->update_type === 'atf'): ?>
                        <?php foreach ($update->getTokenWarnings() as $warning): ?>
                            <div class="alert alert-<?= h($warning[0]) ?>">
                                <?= h($warning[1]) ?>
                            </div>
                        <?php endforeach; ?>
                    <?php elseif ($update->hasAnnotationWarnings() && $updateEvent->update_type === 'annotation'): ?>
                        <?php foreach ($update->getAnnotationWarnings() as $warning): ?>
                            <div class="alert alert-<?= h($warning[0]) ?>">
                                <?= h($warning[1]) ?>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    <pre><?=
                        $this->Diff->diffInscription($update->artifact->inscription, $update, $updateEvent->update_type)
                    ?></pre>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
