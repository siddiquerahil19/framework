<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead>
        <tr>
            <th><?= h('Artifact') ?></th>
            <th><?= h('Visual asset') ?></th>
            <th><?= h('Revision') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($updateEvent->artifact_asset_annotations as $annotation): ?>
            <tr>
                <td><?= $this->Html->link(
                    h($annotation->artifact_asset->artifact->designation),
                    ['controller' => 'Artifacts', 'action' => 'view', $annotation->artifact_asset->artifact->id]
                ) ?></td>
                <td>
                    <?= $this->Html->link(
                        h($annotation->artifact_asset->getDescription()),
                        ['controller' => 'ArtifactAssets', 'action' => 'view', $annotation->artifact_asset->id]
                    ) ?>
                </td>
                <td>
                    <?= h($annotation->id) ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
