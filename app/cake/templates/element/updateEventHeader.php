<div class="mb-2">
<?= $this->Html->link(
    'Update',
    ['prefix' => false, 'controller' => 'UpdateEvents', 'action' => 'view', $update_event->id],
    ['escape' => false]
) ?> made on <?= $update_event->created ?> by
    <?= $this->Html->link(
        $update_event->creator->author,
        ['prefix' => false, 'controller' => 'Authors', 'action' => 'view', $update_event->creator->id]
    ) ?>

for
<?php
    if (!empty($update_event['authors'])):
        $total = count($update_event['authors']);
        $i=0;
        foreach ($update_event['authors'] as $author):
            $i++;
            if ($i == $total && $total > 1) echo ' and ';?>
                <?= $this->Html->link(
                    $author->author,
                    ['controller' => 'Authors', 'action' => 'view', $author->id]
                );
                if ($i != $total) echo'; ';?>
        <?php endforeach;?>
    <?php else: ?>
    <?= $this->Html->link(
        $update_event->creator->author,
        ['prefix' => false, 'controller' => 'Authors', 'action' => 'view', $update_event->creator->id]
    ) ?>
<?php endif; ?>
</div>
