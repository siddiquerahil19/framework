<?php
/**
 * @var \App\View\AppView $this
 * @var string $model
 * @var \Cake\ORM\Entity[]|\Cake\Collection\CollectionInterface $entities
 * @var string $field
 * @var bool $sequence
 * @var string[] $joinFields
 * @var string $title
 */

use \Cake\Utility\Inflector;
use \Cake\ORM\TableRegistry;

$table = TableRegistry::getTableLocator()->get($model);
$idField = 'id';
$displayField = $table->getDisplayField();

if (!isset($entities)) {
    $entities = [];
}

if (!isset($field)) {
    $field = Inflector::underscore($model);
}

if (!isset($sequence)) {
    $sequence = false;
}

if (!isset($joinFields)) {
    $joinFields = [];
}

if (!isset($title)) {
    $title = Inflector::humanize($field);
}

$listTag = $sequence ? 'ol' : 'ul';
?>

<fieldset>
    <label for="<?= $field ?>[][<?= $idField ?>]"><?= __($model) ?></label>
    <?php if (empty($entities)): ?>
        <small id="<?= $field ?>-list-empty" class="form-text text-danger"><?= __('None selected') ?></small>
    <?php endif; ?>
    <<?= $listTag ?> id="<?= $field ?>-list">
        <?php foreach ($entities as $index => $entity): ?>
            <li>
                <input type="text" name="<?= $field ?>[<?= $index ?>][<?= $idField ?>]" value="<?= $entity->get($idField) ?>" hidden/>
                <?php if ($sequence): ?>
                    <input type="text" name="<?= $field ?>[<?= $index ?>][_joinData][sequence]" value="<?= $index + 1 ?>" hidden/>
                <?php endif; ?>

                <output><?= h($entity->get($displayField)) ?></output>
                <button class="btn cdli-btn-blue float-right" type="button"><?= __('Remove') ?></button>

                <?php if (!empty($joinFields)): ?>
                    <details open>
                        <summary style="color: #1661ab"><?= __('Additional info') ?></summary>
                        <div class="form-group">
                            <?php foreach ($joinFields as $joinField): ?>
                                <label for="<?= $field ?>[<?= $index ?>][_joinData][<?= $joinField ?>]"><?= $joinField ?></label>
                                <input type="text" class="form-control w-100 mb-3"
                                    name="<?= $field ?>[<?= $index ?>][_joinData][<?= $joinField ?>]"
                                    value="<?= $entity->_joinData->get($joinField) ?>"/>
                            <?php endforeach; ?>
                        </div>
                    </details>
                <?php endif; ?>
            </li>
        <?php endforeach; ?>
    </<?= $listTag ?>>

    <label for="<?= $field ?>_search">
        <small class="form-text text-muted"><?= __('Search for {0}', __($model)) ?></small>
    </label>
    <input type="text" id="<?= $field ?>_search" value="" class="form-control w-100 mb-3" autocomplete="off">
    <ul id="<?= $field ?>_search-list" class="list-unstyled"></ul>
</fieldset>

<?php $this->append('script'); ?>
<script>
$(function () {
    // BEGIN: CONFIGURATION
    var field = '<?= $field ?>'
    var idField = '<?= $idField ?>'
    var displayField = '<?= $displayField ?>'
    var includeSequence = <?= json_encode($sequence) ?>

    var joinFields = <?= json_encode($joinFields) ?>

    var messages = {
        add: '<?= __('Add') ?>',
        remove: '<?= __('Remove') ?>',
    }

    var searchUrl = '<?= $this->Url->build([
        'prefix' => false,
        'controller' => $model,
        'action' => 'search'
    ]) ?>'
    var csrfToken = <?= json_encode($this->request->getAttribute('csrfToken')) ?>

    // END: CONFIGURATION

    var search = $('#' + field + '_search')
    var searchResults = $('#' + field + '_search-list')
    var entities = $('#' + field + '-list')
    var entitiesEmpty = $('#' + field + '-list-empty')

    var remove = function (event) {
        event.preventDefault()
        $(this).parent().remove()

        entities.find('li').each(function (index, item) {
            var inputs = $(item).find('input')
            inputs.eq(1).val(index + 1)
            inputs.each(function (_, input) {
                var parts = $(input).attr('name').split('][')
                parts[0] = field + '[' + index
                $(input).attr('name', parts.join(']['))
            })
        })

        updateButtons()

        if (!entities.find('li').length) {
            entities.before(entitiesEmpty)
        }
    }

    // Check element added or removed from searchResults list
    var updateButtons = function () {
        var searchResultsArray = []

        entities.children().each(function () {
            searchResultsArray.push($(this).children().first().attr('value'))
        })

        searchResults.children().each(function () {
            if (searchResultsArray.includes($(this).attr('data-value'))) {
                $(this).find('button').attr('disabled', true)
            } else {
                $(this).find('button').attr('disabled', false)
            }
        })
    }

    search.on('input', function () {
        var query = $(this).val()

        if (query === '') {
            return
        }

        $.ajax({
            method: 'POST',
            contentType: 'text/plain',
            data: query,
            dataType: 'json',
            url: searchUrl,
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-CSRF-Token', csrfToken)
            }
        }).done(function (results) {
            searchResults.empty()
            for (var i = 0; i < results.length; i++) {
                var result = results[i]
                var resultElement = $('<li class="mb-2" data-value="' + result[idField] + '">' + result[displayField] + '</li>')
                var addButton = $('<button class="btn cdli-btn-blue" type="button">' + messages.add + '</button>')

                addButton.on('click', { entity: result }, function (event) {
                    event.preventDefault()
                    var sequence = entities.find('li').length
                    var entity = event.data.entity
                    var inputs = '<input type="text" name="' + field + '[' + sequence + '][' + idField + ']" value="' + entity[idField] + '" hidden/>'

                    if (includeSequence) {
                        inputs += '<input type="text" name="' + field + '[' + sequence + '][_joinData][sequence]" value="' + (sequence + 1) + '" hidden/>'
                    }

                    inputs += '<output>' + entity[displayField] + '</output>'

                    if (joinFields.length) {
                        inputs += '<details><summary style="color: #1661ab">Additional info</summary>'
                        for (var i = 0; i < joinFields.length; i++) {
                            var joinField = joinFields[i]
                            var name = field + '[' + sequence + '][_joinData][' + joinField + ']'
                            inputs += '<div class="form-group"><label for="' + name + '">' + joinField + '</label>'
                            inputs += '<input type="text" name="' + name + '" class="form-control w-100 mb-3"/></div>'
                        }
                        inputs += '</details>'
                    }

                    var entityElement = $('<li>' + inputs + '</li>')
                    var removeButton = $('<button class="btn cdli-btn-blue float-right" type="button">' + messages.remove + '</button>')
                    removeButton.on('click', remove)
                    entityElement.find('output').after(' ', removeButton)
                    entitiesEmpty.remove()
                    entities.append(entityElement)
                })
                resultElement.prepend(addButton, ' ')
                searchResults.append(resultElement)
            }

            updateButtons()
        })
    })

    entities.find('li button').on('click', remove)
})
</script>
<?php $this->end(); ?>
