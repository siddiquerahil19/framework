<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Language[]|\Cake\Collection\CollectionInterface $languages
 */
?>

<div class="d-flex justify-content-between align-items-end mb-2">
    <h1 class="display-3 header-txt text-left"><?= __('Languages') ?></h1>
    <?= $this->element('addButton'); ?>
</div>
<div class="text-left my-2">
    <?= $this->element('entityExport'); ?>
</div>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead align="left">
        <tr>
            <th scope="col"><?= $this->Paginator->sort('sequence') ?></th>
            <th scope="col"><?= $this->Paginator->sort('parent_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('language') ?></th>
            <th scope="col"><?= $this->Paginator->sort('protocol_code') ?></th>
            <th scope="col"><?= $this->Paginator->sort('inline_code') ?></th>
            <th scope="col"><?= $this->Paginator->sort('notes') ?></th>
            <?php if ($access_granted): ?>
                <th scope="col"><?= __('Action') ?></th>
            <?php endif ?>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($languages as $language): ?>
        <tr align="left">
            <td><?= $this->Number->format($language->sequence) ?></td>
            <td>
                <?php if ($language->has('parent_language')): ?>
                    <?= $this->Html->link(
                        $language->parent_language->language,
                        ['controller' => 'Languages', 'action' => 'view', $language->parent_language->id]
                    ) ?>
                <?php endif; ?>
            </td>
            <td>
                <?= $this->Html->link(
                    $language->language,
                    ['controller' => 'Languages', 'action' => 'view', $language->id]
                ) ?>
            </td>
            <td><?= h($language->protocol_code) ?></td>
            <td><?= h($language->inline_code) ?></td>
            <td><?= h($language->notes) ?></td>
            <?php if ($access_granted): ?>
                <td>
                    <?= $this->Html->link(
                        __('Edit'),
                        ['prefix' => 'Admin', 'action' => 'edit', $language->id],
                        ['escape' => false , 'class' => 'btn btn-warning btn-sm', 'title' => 'Edit']
                    ) ?>
                </td>
            <?php endif ?>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>

<?= $this->element('citeButton'); ?>
<?= $this->element('citeBottom'); ?>
