# Searching CDLI

The ElasticSearch plugin uses the ODM model instead of the ORM model like the MySQL database. To use ElasticSearch you need to first generate indices which are the equivalent of tables in an ORM database. You will be using Logstash to convert the SQL tables into corresponding indices.


# Setting Up ElasticSearch

Before proceeding further, please make sure you have the latest version of the database. The `search_club` channel in the Digital Cuneiform slack workspace is the best place to ascertain you have the latest version of the database.

## Enabling the ElasticSearch and Logstash containers



1. You will need to change the _config.json.dist_ (`dev/config.json.dist`) and _logstash.conf_ (`dev/data/logstash/pipeline/logstash.conf`) files. 

    Note: For the changes in these files to be reflected, you will have to restart the containers every time with the cdlidev.py script.

2. Open _config.json.dist_ and set **ElasticSearch** and **Logstash** enabled to true and also set `is_default` to true for both containers. Save and close this file.

        "elasticsearch": {
                "is_default" : true,
                "enabled": true,
                “scale" : 1
            },

        "logstash": {
                "is_default" : true,
                "enabled": true,
                “scale" : 1
          },


## Scheduling Logstash



1. You will be generating 3 indices for now: **artifacts_publications**, **inscription** and **advanced_artifacts**.
2. Each of these indices has separate JDBCs defined in _logstash.conf_. Each JDBC has a schedule defined which you have to change as required.

        # Run every 10 hour (this can be set according to requirement)
        schedule => "0 */10 * * *"
        #schedule => "*/30 * * * *" # Run every 30 min

3. The following steps can be followed for all the JDBCs at once or one at a time, depending on how powerful your system is, as it is a resource-intensive process.

    If you want to execute the JDBCs one at a time, comment out the other JDBCs.
    
    
    Another alternative is to set different schedules for each JDBC so that all of them don’t run at the same time. 

4. The schedules are set to `"0 */10 * * *"` by default. This means that the JDBC will run every 10 hours starting from 0000 hrs (system time).
5. For generating indices the first time, comment out the 10 hour schedule and set it to 30 minutes.

        # Run every 10 hour (this can be set according to requirement)
        #schedule => "0 */10 * * *"
        schedule => "*/30 * * * *" # Run every 30 min

6. Now spin up the containers with `./dev/cdlidev.py up`. 
7. The indices should be generated according to the schedule. To check generated indices visit: [http://localhost:9200/_cat/indices](http://localhost:9200/_cat/indices).

    Note: The indices may take quite some time to generate. Hang in there!

8. Once the indices are generated, go back to _logstash.conf_ and set the schedules back to 10 hours in each JDBC. 
9. You can then disable **Logstash** in _config.json.dist_, set `is_default` to false for the container, and begin searching with ElasticSearch by restarting the containers.


## Exploring the Indices



1. To preview data in indices visit: [http://localhost:9200/{index_name}/_search](http://localhost:9200/%7Bindex_name%7D/_search).

    Ex. [http://localhost:9200/artifacts_publications/_search](http://localhost:9200/artifacts_publications/_search).

2. To help you understand the structure of each index it should show you 10 rows for preview.
3. To delete any particular index use `curl -X DELETE "http://elastic:elasticchangeme@localhost:9200/{index_name}"`.
4. To delete all indices use `curl -X DELETE "http://elastic:elasticchangeme@localhost:9200/_all"`.
5. The 7th column on [http://localhost:9200/_cat/indices](http://localhost:9200/_cat/indices) tells us about the number of rows in the indices.
6. For the inscription index, it would be around the same size as of the inscription table in the SQL Database.
7. For the other indices it would be similar to the SQL query formed.
8. To verify if the rows for indices generated in ElasticSearch are the same as the ones in the SQL database, try to run the SQL query described for each index as "statement" in _logstash.conf_ in the PHPMYADMIN Terminal/Console (where SQL queries can be executed).

    Note: For advanced_artifacts, it may take a  minimum of 5 minutes for execution as it is a huge and complicated query.

