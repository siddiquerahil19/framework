## CDLI metadata fields

### Artifacts
| Title                        | Field                        | Details | 
|------------------------------|------------------------------|---------| 
|Artifact number               | `artifact_id`                |-        |
|CDLI comments                 | `cdli_comments`              |-        |
|Composite number              | `composite_no`               |-        |
|Artifact condition            | `condition_description`      |-        |
|Artifact type                 | `artifact_type`              |-        |
|Period                        | `period`                     |-        |
|Provenience                   | `provenience`                |-        |
|Origin date                   | `written_in`                 |-        |
|Archive                       | `archive`                    |-        |
|Composites                    | `composites`                 |-        |
|Seals                         | `seals`                      |-        |
|Collections                   | `collections`                |-        |
|Genre                         | `genres`                     |-        |
|Language                      | `languages`                  |-        |
|Material                      | `materials`                  |-        |
|Shadow CDLI comments          | `shadow_cdli_comments`       |-        |
|Shadow collection location    | `shadow_collection_location` |-        |
|Shadow collection comments    | `shadow_collection_comments` |-        |
|Shadow acquisition history    | `shadow_acquisition_history` |-        |
|Publications key              | `publications_key`           |-        |
|Publications type             | `publications_type`          |-        |
|Publications reference        | `publications_exact_ref`     |-        |
|Publications comment          | `publications_comment`       |-        |
|External resources            | `external_resources`         |-        |
|External resources key        | `external_resources_key`     |-        |
|Genres comment                | `genres_comment`             |-        |
|Genres uncertain              | `genres_uncertain`           |-        |
|Languages uncertain           | `languages_uncertain`        |-        |
|Materials aspect              | `materials_aspect`           |-        |
|Materials color               | `materials_color`            |-        |
|Materials uncertain           | `materials_uncertain`        |-        |
|Designation                   | `designation`                |-        |
|Elevation                     | `elevation`                  |-        |
|Excavation number             | `excavation_no`              |-        |
|Findspot comments             | `findspot_comments`          |-        |
|Findspot square               | `findspot_square`            |-        |
|Museum number                 | `museum_no`                  |-        |
|Artifact preservation         | `artifact_preservation`      |-        |
|                              | `is_public`                  |-        |
|Is the artifact public?       | `is_atf_public`              |-        |
|Are the images public?        | `are_images_public`          |-        |
|Seal number                   | `seal_no`                    |-        |
|Seal information              | `seal_information`           |-        |
|Stratigraphic level           | `stratigraphic_level`        |-        |
|Surface preservation          | `surface_preservation`       |-        |
|Thickness                     | `thickness`                  |-        |
|Height                        | `height`                     |-        |
|Width                         | `width`                      |-        |
|Weight                        | `weight`                     |-        |
|Is the provenience uncertain? | `is_provenience_uncertain`   |-        |
|Is period uncertain?          | `is_period_uncertain`        |-        |
|Accession number              | `accession_no`               |-        |
|Alternative years             | `alternative_years`          |-        |
|Period comments               | `period_comments`            |-        |
|Provenience comments          | `provenience_comments`       |-        |
|Is ATF school text?           | `is_school_text`             |-        |
|Written in                    | `written_in`                 |-        |
|Id the ATF type uncertain?    | `is_artifact_type_uncertain` |-        |
|Artifact comments             | `artifact_comments`          |-        |
|Is ATF retired?               | `retired`                    |-        |
|Does ATF have fragments?      | `has_fragments`              |-        |
|Is the ATF fake?              | `is_artifact_fake`           |-        |