# https://stackoverflow.com/a/74029319/5095300
DIACRITICS = [*0x1DC0..0x1DFF, *0x0300..0x036F, *0xFE20..0xFE2F].pack('U*')
def remove_diacritics(value)
    if value.nil?
        nil
    else
        value
            .unicode_normalize(:nfd)
            .tr(DIACRITICS, '')
            .unicode_normalize(:nfc)
    end
end

def filter(event)
    # Diacritics
    event.set('designation_ascii', remove_diacritics(event.get('designation')))
    event.set('dates_referenced_ascii', remove_diacritics(event.get('dates_referenced')))
    event.set('provenience_ascii', remove_diacritics(event.get('provenience')))
    event.set('written_in_ascii', remove_diacritics(event.get('written_in')))
    event.set('archive_ascii', remove_diacritics(event.get('archive')))

    # IDs
    cdli_id = ['P' + event.get('id').to_s.rjust(6, '0')]

    all_composite_no = []
    composite_no = event.get('composite_no')
    witness_composite_no = event.get('witness_composite_no')
    unless composite_no.nil?
        cdli_id << composite_no
        all_composite_no << composite_no
    end
    unless witness_composite_no.nil?
        all_composite_no += witness_composite_no
    end

    all_seal_no = []
    seal_no = event.get('seal_no')
    impression_seal_no = event.get('impression_seal_no')
    unless composite_no.nil?
        cdli_id << seal_no
        all_seal_no << seal_no
    end
    unless impression_seal_no.nil?
        all_seal_no += impression_seal_no
    end

    event.set('cdli_id', cdli_id)
    event.set('all_composite_no', all_composite_no)
    event.set('all_seal_no', all_seal_no)

    # Assets
    asset = []
    asset_type = event.get('asset_type')
    unless asset_type.nil?
        asset_artifact_aspect = event.get('asset_artifact_aspect')
        asset_file_format = event.get('asset_file_format')
        asset_is_public = event.get('asset_is_public')
        asset_type.each_index { |i|
            asset << {
                type: asset_type[i],
                artifact_aspect: asset_artifact_aspect[i],
                file_format: asset_file_format[i],
                is_public: !asset_is_public[i].to_i.zero?
            }
        }
    end
    event.set('asset', asset)

    # Update events
    update = []
    update_comments = event.get('update_comments')
    unless update_comments.nil?
        update_external_resource = event.get('update_external_resource')
        update_authors = event.get('update_authors')
        update_comments.each_index { |i|
            update << {
                comments: update_comments[i],
                external_resource: update_external_resource[i],
                authors: if update_authors[i].nil? then nil else update_authors[i].split('|') end,
                authors_ascii: if update_authors[i].nil? then nil else remove_diacritics(update_authors[i]).split('|') end
            }
        }
    end
    event.set('update', update)

    # Publications
    publication = []
    publication_type = event.get('publication_type')
    unless publication_type.nil?
        publication_exact_reference = event.get('publication_exact_reference')
        publication_bibtexkey = event.get('publication_bibtexkey')
        publication_designation = event.get('publication_designation')
        publication_year = event.get('publication_year')
        publication_title = event.get('publication_title')
        publication_book_title = event.get('publication_book_title')
        publication_chapter = event.get('publication_chapter')
        publication_series = event.get('publication_series')
        publication_oclc = event.get('publication_oclc')
        publication_address = event.get('publication_address')
        publication_volume = event.get('publication_volume')
        publication_pages = event.get('publication_pages')
        publication_publisher = event.get('publication_publisher')
        publication_journal = event.get('publication_journal')
        publication_entry_type = event.get('publication_entry_type')
        publication_authors = event.get('publication_authors')
        publication_editors = event.get('publication_editors')
        publication_type.each_index { |i|
            publication << {
                type: publication_type[i],
                exact_reference: if publication_designation[i].nil? || publication_exact_reference[i].nil? then nil else publication_designation[i] + ' ' + publication_exact_reference[i] end,
                bibtexkey: publication_bibtexkey[i],
                designation: publication_designation[i],
                year: publication_year[i],
                title: publication_title[i],
                book_title: publication_book_title[i],
                chapter: publication_chapter[i],
                series: publication_series[i],
                oclc: publication_oclc[i],
                address: publication_address[i],
                volume: publication_volume[i],
                pages: publication_pages[i],
                publisher: publication_publisher[i],
                journal: publication_journal[i],
                entry_type: publication_entry_type[i],
                authors: if publication_authors[i].nil? then nil else publication_authors[i].split('|') end,
                editors: if publication_editors[i].nil? then nil else publication_editors[i].split('|') end,

                designation_ascii: remove_diacritics(publication_designation[i]),
                exact_reference_ascii: if publication_designation[i].nil? || publication_exact_reference[i].nil? then nil else remove_diacritics(publication_designation[i] + ' ' + publication_exact_reference[i]) end,
                series_ascii: remove_diacritics(publication_series[i]),
                journal_ascii: remove_diacritics(publication_journal[i]),
                title_ascii: remove_diacritics(publication_title[i]),
                authors_ascii: if publication_authors[i].nil? then nil else remove_diacritics(publication_authors[i]).split('|') end,
                editors_ascii: if publication_editors[i].nil? then nil else remove_diacritics(publication_editors[i]).split('|') end
            }
        }
    end
    event.set('publication', publication)

    # ATF
    atf = event.get('atf')
    unless atf.nil?
        lines = atf.split(/[\r\n]+/)
        atf_structure = []
        atf_comment = []
        atf_transcription = []
        atf_translation = {}
        atf_transliteration_lines = []
        lines.each { |line|
            if line.match(/^@/)
                atf_structure << line[1..-1]
            elsif line.match(/^#tr\.ts: /)
                atf_transcription << line[8..-1]
            elsif line.match(/^#tr/)
                language = line[4..5]
                line = line[8..-1].gsub(/ $/, '')
                if atf_translation.key?(language)
                    atf_translation[language] = atf_translation[language] + ' ' + line
                else
                    atf_translation[language] = line
                end
            elsif line.match(/^[#$] /)
                atf_comment << line[2..-1]
            elsif line.match(/^\d/)
                atf_transliteration_lines << line.gsub(/^\d+\'?\. /, '').gsub(/ $/, '')
            end
        }
        event.set('atf_structure', atf_structure)
        event.set('atf_comment', atf_comment)
        event.set('atf_transcription', atf_transcription)
        event.set('atf_translation', atf_translation.each_pair { |key, value| [key, value] })
        event.set('atf_transliteration_lines', atf_transliteration_lines)
        event.set('atf_transliteration', atf_transliteration_lines.join(' '))
    end

    return [event]
end
