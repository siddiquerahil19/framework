SELECT
    artifacts.id,
    artifacts.designation,
    artifacts.museum_no,
    artifacts.accession_no,
    artifacts.composite_no,
    artifacts.seal_no,
    artifacts.excavation_no,
    artifacts.findspot_square,
    artifacts.findspot_comments,
    artifacts.condition_description,
    artifacts.elevation,
    artifacts.artifact_preservation,
    artifacts.surface_preservation,
    artifacts.stratigraphic_level,
    artifacts.alternative_years,
    artifacts.provenience_comments,
    artifacts.period_comments,
    artifacts.dates_referenced,
    artifacts.is_public,
    artifacts.is_atf_public,
    artifacts.are_images_public,
    artifacts.artifact_comments,
    artifacts.retired,

    artifact_types.artifact_type,
    proveniences.provenience,
    regions.region,
    origins.provenience AS 'written_in',
    archives.archive,
    periods.period,
    periods.sequence as 'period_sequence',

    materials.material,
    materials.material_aspect,
    materials.material_color,
    collections.collection,
    genres.genre,
    languages.language,
    artifact_assets.asset_type,
    artifact_assets.artifact_aspect AS 'asset_artifact_aspect',
    artifact_assets.file_format AS 'asset_file_format',
    artifact_assets.is_public AS 'asset_is_public',

    artifact_composites.composite_no AS 'witness_composite_no',
    artifact_seals.seal_no AS 'impression_seal_no',

    inscriptions.id AS 'inscription_id',
    inscriptions.atf,
    inscriptions.jtf,
    inscriptions.annotation,
    inscriptions.is_atf2conll_diff_resolved,

    publications.publication_type,
    publications.publication_exact_reference,
    publications.publication_bibtexkey,
    publications.publication_designation,
    publications.publication_year,
    publications.publication_title,
    publications.publication_book_title,
    publications.publication_chapter,
    publications.publication_series,
    publications.publication_oclc,
    publications.publication_address,
    publications.publication_volume,
    publications.publication_pages,
    publications.publication_publisher,
    publications.publication_journal,
    publications.publication_entry_type,
    publications.publication_authors,
    publications.publication_editors,

    update_events.event_comments AS 'update_comments',
    update_events.external_resource AS 'update_external_resource',
    update_events.authors AS 'update_authors'
FROM artifacts

LEFT JOIN (
    WITH RECURSIVE cte (`id`, `artifact_type`, `parent_id`) AS (
        SELECT `id`, `artifact_type`, `parent_id` FROM `artifact_types` WHERE `parent_id` IS NULL
        UNION ALL
        SELECT D.`id`, CONCAT(cte.`artifact_type`, ' > ', D.`artifact_type`), D.`parent_id` FROM `artifact_types` D
        INNER JOIN cte ON cte.`id` = D.`parent_id`
    ) SELECT * FROM cte
) AS artifact_types ON artifacts.artifact_type_id = artifact_types.id
LEFT JOIN proveniences ON artifacts.provenience_id = proveniences.id
LEFT JOIN regions ON proveniences.region_id = regions.id
LEFT JOIN proveniences AS origins ON artifacts.written_in = origins.id
LEFT JOIN archives ON artifacts.archive_id = archives.id
LEFT JOIN periods ON artifacts.period_id = periods.id

# Materials
LEFT JOIN (
    SELECT
        artifacts_materials.artifact_id,
        GROUP_CONCAT(materials.material SEPARATOR '; ') AS 'material',
        GROUP_CONCAT(material_aspects.material_aspect SEPARATOR '; ') AS 'material_aspect',
        GROUP_CONCAT(material_colors.material_color SEPARATOR '; ') AS 'material_color'
    FROM artifacts_materials

    LEFT JOIN (
        WITH RECURSIVE cte (`id`, `material`, `parent_id`) AS (
            SELECT `id`, `material`, `parent_id` FROM `materials` WHERE `parent_id` IS NULL
            UNION ALL
            SELECT D.`id`, CONCAT(cte.`material`, ' > ', D.`material`), D.`parent_id` FROM `materials` D
            INNER JOIN cte ON cte.`id` = D.`parent_id`
        ) SELECT * FROM cte
    ) AS materials
    ON artifacts_materials.material_id = materials.id

    LEFT JOIN material_aspects
    ON artifacts_materials.material_aspect_id = material_aspects.id

    LEFT JOIN material_colors
    ON artifacts_materials.material_color_id = material_colors.id

    GROUP BY artifacts_materials.artifact_id
) AS materials
ON artifacts.id = materials.artifact_id

# Collections
LEFT JOIN (
    SELECT
        artifacts_collections.artifact_id,
        GROUP_CONCAT(collections.collection SEPARATOR '; ') AS 'collection'
    FROM artifacts_collections
    LEFT JOIN collections ON artifacts_collections.collection_id = collections.id
    GROUP BY artifacts_collections.artifact_id
) AS collections
ON artifacts.id = collections.artifact_id

# Genres
LEFT JOIN (
    SELECT
        artifacts_genres.artifact_id,
        GROUP_CONCAT(genres.genre SEPARATOR '; ') AS 'genre'
    FROM artifacts_genres

    LEFT JOIN (
        WITH RECURSIVE cte (`id`, `genre`, `parent_id`) AS (
            SELECT `id`, `genre`, `parent_id` FROM `genres` WHERE `parent_id` IS NULL
            UNION ALL
            SELECT D.`id`, CONCAT(cte.`genre`, ' > ', D.`genre`), D.`parent_id` FROM `genres` D
            INNER JOIN cte ON cte.`id` = D.`parent_id`
        ) SELECT * FROM cte
    ) AS genres
    ON artifacts_genres.genre_id = genres.id

    GROUP BY artifacts_genres.artifact_id
) AS genres
ON artifacts.id = genres.artifact_id

# Languages
LEFT JOIN (
    SELECT
        artifacts_languages.artifact_id,
        GROUP_CONCAT(languages.language SEPARATOR '; ') AS 'language'
    FROM artifacts_languages

    LEFT JOIN (
        WITH RECURSIVE cte (`id`, `language`, `parent_id`) AS (
            SELECT `id`, `language`, `parent_id` FROM `languages` WHERE `parent_id` IS NULL
            UNION ALL
            SELECT D.`id`, CONCAT(cte.`language`, ' > ', D.`language`), D.`parent_id` FROM `languages` D
            INNER JOIN cte ON cte.`id` = D.`parent_id`
        ) SELECT * FROM cte
    ) AS languages
    ON artifacts_languages.language_id = languages.id

    GROUP BY artifacts_languages.artifact_id
) AS languages
ON artifacts.id = languages.artifact_id

# Images
LEFT JOIN (
    SELECT
        artifact_assets.artifact_id,
        GROUP_CONCAT(IFNULL(artifact_assets.asset_type, '') SEPARATOR '; ') AS 'asset_type',
        GROUP_CONCAT(IFNULL(artifact_assets.artifact_aspect, '') SEPARATOR '; ') AS 'artifact_aspect',
        GROUP_CONCAT(IFNULL(artifact_assets.file_format, '') SEPARATOR '; ') AS 'file_format',
        GROUP_CONCAT(IFNULL(artifact_assets.is_public, '') SEPARATOR '; ') AS 'is_public'
    FROM artifact_assets
    GROUP BY artifact_assets.artifact_id
) AS artifact_assets
ON artifacts.id = artifact_assets.artifact_id

# Inscription
LEFT JOIN (
    SELECT * FROM inscriptions WHERE inscriptions.is_latest = 1
) AS inscriptions
ON artifacts.id = inscriptions.artifact_id

# Composites associated with witness
LEFT JOIN (
    SELECT
        artifacts_composites.artifact_id,
        GROUP_CONCAT(artifacts_composites.composite_no SEPARATOR ': ') as 'composite_no'
    FROM artifacts_composites
    GROUP BY artifacts_composites.artifact_id
) AS artifact_composites
ON artifacts.id = artifact_composites.artifact_id

# Seals associated with impression
LEFT JOIN (
    SELECT
        artifacts_seals.artifact_id,
        GROUP_CONCAT(artifacts_seals.seal_no SEPARATOR '; ') as 'seal_no'
    FROM artifacts_seals
    GROUP BY artifacts_seals.artifact_id
) AS artifact_seals
ON artifacts.id = artifact_seals.artifact_id

# Publications
LEFT JOIN (
    SELECT
        entities_publications.entity_id,
        GROUP_CONCAT(IFNULL(entities_publications.publication_type, '') SEPARATOR '; ') AS 'publication_type',
        GROUP_CONCAT(IFNULL(entities_publications.exact_reference, '') SEPARATOR '; ') AS 'publication_exact_reference',
        GROUP_CONCAT(IFNULL(publications.bibtexkey, '') SEPARATOR '; ') AS 'publication_bibtexkey',
        GROUP_CONCAT(IFNULL(publications.designation, '') SEPARATOR '; ') AS 'publication_designation',
        GROUP_CONCAT(IFNULL(publications.year, '') SEPARATOR '; ') AS 'publication_year',
        GROUP_CONCAT(IFNULL(publications.title, '') SEPARATOR '; ') AS 'publication_title',
        GROUP_CONCAT(IFNULL(publications.book_title, '') SEPARATOR '; ') AS 'publication_book_title',
        GROUP_CONCAT(IFNULL(publications.chapter, '') SEPARATOR '; ') AS 'publication_chapter',
        GROUP_CONCAT(IFNULL(publications.series, '') SEPARATOR '; ') AS 'publication_series',
        GROUP_CONCAT(IFNULL(publications.oclc, '') SEPARATOR '; ') AS 'publication_oclc',
        GROUP_CONCAT(IFNULL(publications.address, '') SEPARATOR '; ') AS 'publication_address',
        GROUP_CONCAT(IFNULL(publications.volume, '') SEPARATOR '; ') AS 'publication_volume',
        GROUP_CONCAT(IFNULL(publications.pages, '') SEPARATOR '; ') AS 'publication_pages',
        GROUP_CONCAT(
            COALESCE(publications.publisher, publications.organization, publications.school, '')
            SEPARATOR '; '
        ) AS 'publication_publisher',

        GROUP_CONCAT(IFNULL(journals.journal, '') SEPARATOR '; ') AS 'publication_journal',
        GROUP_CONCAT(IFNULL(entry_types.label, '') SEPARATOR '; ') AS 'publication_entry_type',
        GROUP_CONCAT(IFNULL(authors.author, '') SEPARATOR '; ') AS 'publication_authors',
        GROUP_CONCAT(IFNULL(editors.author, '') SEPARATOR '; ') AS 'publication_editors'
    FROM entities_publications

    LEFT JOIN publications ON entities_publications.publication_id = publications.id
    LEFT JOIN journals ON publications.journal_id = journals.id
    LEFT JOIN entry_types ON publications.entry_type_id = entry_types.id

    # Authors
    LEFT JOIN (
        SELECT
            authors_publications.publication_id,
            GROUP_CONCAT(authors.author SEPARATOR '|') AS 'author'
        FROM authors_publications
        LEFT JOIN authors ON authors_publications.author_id = authors.id
        GROUP BY authors_publications.publication_id
    ) AS authors
    ON publications.id = authors.publication_id

    # Editors
    LEFT JOIN (
        SELECT
            editors_publications.publication_id,
            GROUP_CONCAT(authors.author SEPARATOR '|') AS 'author'
        FROM editors_publications
        LEFT JOIN authors ON editors_publications.editor_id = authors.id
        GROUP BY editors_publications.publication_id
    ) AS editors
    ON publications.id = editors.publication_id

    WHERE entities_publications.table_name = 'artifacts'
    GROUP BY entities_publications.entity_id
) AS publications
ON artifacts.id = publications.entity_id

# Credits
LEFT JOIN (
    SELECT
        updates.artifact_id,
        GROUP_CONCAT(IFNULL(update_events.event_comments, '') SEPARATOR '|') AS 'event_comments',
        GROUP_CONCAT(IFNULL(external_resources.external_resource, '') SEPARATOR '; ') AS 'external_resource',
        CONCAT(IFNULL(creators.author, ''), '|', GROUP_CONCAT(IFNULL(authors.author, '') SEPARATOR '; ')) AS 'authors'
    FROM update_events

    LEFT JOIN (
        SELECT artifact_id, update_events_id AS 'update_event_id' FROM artifacts_updates
        UNION ALL
        SELECT artifact_id, update_event_id FROM inscriptions
    ) AS updates
    ON update_events.id = updates.update_event_id

    # External resources
    LEFT JOIN external_resources
    ON update_events.external_resource_id = external_resources.id

    # Creator
    LEFT JOIN authors AS creators
    ON update_events.created_by = creators.id

    # Authors
    LEFT JOIN (
        SELECT
            authors_update_events.update_event_id,
            GROUP_CONCAT(authors.author SEPARATOR '|') AS 'author'
        FROM authors_update_events
        LEFT JOIN authors ON authors_update_events.author_id = authors.id
        GROUP BY authors_update_events.update_event_id
    ) AS authors
    ON update_events.id = authors.update_event_id

    GROUP BY updates.artifact_id
) AS update_events
ON artifacts.id = update_events.artifact_id
